#ifndef SOCCER_PREDICT_STATE_HPP
#define SOCCER_PREDICT_STATE_HPP

#include <soccer/state.hpp>

namespace soccer {

template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score >
struct predict_state
    : state< PlayerId, Player, Ball, Time, PlayMode, Team, Score >
{


    Time M_start_time;
};

}

namespace soccer { namespace traits {

template < t

}} // end namespace traits

#endif // SOCCER_PREDICT_STATE_HPP
