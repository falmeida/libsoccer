#ifndef SOCCER_PLAYER_ALGORITHMS_HPP
#define SOCCER_PLAYER_ALGORITHMS_HPP

#include <soccer/player_concepts.hpp>

#include <soccer/core/access.hpp>

#include <soccer/util/bare_type.hpp>

namespace soccer {

/*!
  \brief calculate the direction between two players
  */
template < typename Player >
inline
// typename boost::enable_if< detail::has_direction_descriptor< Player > >::type
double
direction( Player const& from_p1, Player const& from_p2 )
{
    BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

    return direction( get_position( from_p2 ), get_position( from_p1 ) );
}


/*!
  \brief check if two players are from the same side
  */
template < typename Player >
bool
same_side( Player const& player1,
           Player const& player2 )
{
    BOOST_CONCEPT_ASSERT(( PlayerIdConcept<Player> ));
    return get_side( player1 ) == get_side( player2 );
}


/*!
\brief Check if player has an unknown number
\details \details_get
\tparam Player \tparam_player
\param player \param_player
\return true if the player has an unknown number
*/
template < typename Player >
bool
is_number_unknown( Player const& player )
{
    BOOST_CONCEPT_ASSERT(( PlayerIdConcept<Player> ));

    return get_number( player ) == get_unknown_number< typename soccer::util::bare_type< Player >::type >();
}

/*!
\brief Check if player has an unknown number
\details \details_get
\tparam Player \tparam_player
\param player \param_player
\return true if the player has an unknown number
*/
template < typename Player >
bool
is_side_unknown( Player const& player )
{
    BOOST_CONCEPT_ASSERT(( PlayerIdConcept<Player> ));

    typedef typename soccer::side_type< typename soccer::util::bare_type< Player >::type >::type side_t;
    return get_side( player ) == get_unknown_side< side_t >();
}

} // end namespace soccer

#endif // SOCCER_PLAYER_ALGORITHMS_HPP
