#ifndef SOCCER_EFFECTOR_ACCELERATION_HPP
#define SOCCER_EFFECTOR_ACCELERATION_HPP

#include <soccer/defines.h>

#include <soccer/concepts/object_concepts.hpp>

SOCCER_NAMESPACE_BEGIN

namespace core_dispatch {

    template < typename Power,
               typename Direction,
               typename Object,
               typename ObjectModel,
               typename Tag >
    struct acceleration_effector_impl;

    template < typename Power,
               typename Direction,
               typename Object,
               typename ObjectModel >
    struct acceleration_effector_impl< Power, Direction, Object, ObjectModel, dynamic_object_model_tag >
    {
        static inline void apply( Object& object,
                                  ObjectModel const& object_model,
                                  soccer::dynamic_object_model_tag )
        {
            BOOST_CONCEPT_ASSERT(( MutableMobileObjectConcept< Object > ));

            auto pos = soccer::get_position( object );
            auto vel = soccer::get_velocity( object );

            const auto decay = soccer::get_decay( object_model );

            boost::geometry::add_point( pos, vel );
            boost::geometry::multiply_value( vel, decay );

            soccer::set_position( pos, object );
            soccer::set_velocity( vel, object );
        }
    };

}


template < typename Power,
           typename Direction,
           typename MobileObject,
           typename ObjectModel >
void
apply_acceleration( Power const power,
                    Direction const direction,
                    MobileObject& mobile_object,
                    ObjectModel const& object_model )
{
    BOOST_CONCEPT_ASSERT(( MutableMobileObjectConcept< MobileObject > ));
    typedef typename soccer::tag< ObjectModel >::type model_tag;

    typedef core_dispatch::acceleration_effector_impl< MobileObject, ObjectModel, model_tag > impl;
    impl::apply( power, direction, mobile_object, object_model );

}

template < typename MobileObject,
           typename ObjectModel,
           typename Strategy >
/*!
 * \brief The ball_acceleration_generator struct
 * \tparam Ball ball model
 * \tparam BallModel ball physics model
 */
struct acceleration_effector
{
    BOOST_CONCEPT_ASSERT(( MutableMobileObjectConcept< MobileObject > ));
    // BOOST_CONCEPT_ASSERT(( accelerationModelConcept< accelerationModel > ));

    MobileObject M_object;
    ObjectModel const* M_object_model;

    acceleration_effector( MobileObject const& object,
                           ObjectModel const& object_model )
        : M_object( object )
        , M_object_model( &object_model )
    {

    }

    Object operator()()
    {
        apply_acceleration( M_object, *M_object_model, Strategy );

        return M_object;
    }
};

SOCCER_NAMESPACE_END



#endif // SOCCER_EFFECTOR_ACCELERATION_HPP
