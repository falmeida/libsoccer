#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <tuple>
#include <type_traits>

#include <boost/type_traits.hpp>

namespace soccer {

template <std::size_t N, std::size_t I, typename Tuple>
struct evaluate_all
{
    template <typename T>
    static bool eval(T const & t, Tuple const & preds)
    {
        return std::get<I>(preds)(t)
            && evaluate_all<N, I + 1, Tuple>::eval(t, preds);
    }
};

template <std::size_t N, typename Tuple>
struct evaluate_all<N, N, Tuple>
{
    template <typename T>
    static bool eval(T const &, Tuple const &)
    {
        return true;
    }
};

template <std::size_t N, std::size_t I, typename Tuple>
struct evaluate_any
{
    template <typename T>
    static bool eval(T const & t, Tuple const & preds)
    {
        return std::get<I>(preds)(t)
            || evaluate_any<N, I + 1, Tuple>::eval(t, preds);
    }
};

template <std::size_t N, typename Tuple>
struct evaluate_any<N, N, Tuple>
{
    template <typename T>
    static bool eval(T const &, Tuple const &)
    {
        return false;
    }
};

template < template < std::size_t, std::size_t, class C > class LogicalOperation,
           typename ...Preds>
struct conjunction
{
private:
    typedef std::tuple<Preds...> tuple_type;
    tuple_type preds;

public:
    conjunction(Preds const &... p) : preds(p...) { }

    template <typename T>
    bool operator()(T const & t) const
    {
        return LogicalOperation<sizeof...(Preds), 0, tuple_type>::eval(t, preds);
    }
};

template <typename ...Preds>
conjunction< evaluate_all, typename std::decay<Preds>::type...> make_conjunction(Preds &&... preds)
{
    return conjunction< evaluate_all,
                        typename std::decay<Preds>::type...>( std::forward<Preds>(preds)... );
}

template <typename ...Preds>
conjunction< evaluate_any, typename std::decay<Preds>::type...> make_disjunction(Preds &&... preds)
{
    return conjunction< evaluate_any,
                        typename std::decay<Preds>::type...>( std::forward<Preds>(preds)... );
}


/*!
  \brief check if all of the predicates if true when applied to a value
  */
template<class InputUnaryPredicateIterator >
bool all_of (InputUnaryPredicateIterator first,
             InputUnaryPredicateIterator last,
             typename boost::unary_traits< boost::iterator_value< InputUnaryPredicateIterator >::type >::argument_type value )
{
   typedef boost::iterator_value< InputUnaryPredicateIterator >::type unary_predicate;
   BOOST_CONCEPT_ASSERT(( boost::UnaryPredicate< unary_predicate,
                          boost::unary_traits< unary_predicate >::argument_type ));

    while ( first != last )
    {
      if ( !(*first)( value ) )
          return false;
      ++first;
    }
    return true;
}

/*!
  \brief check if any of the predicates if true when applied to a value
  */
template<class InputUnaryPredicateIterator >
bool any_of (InputUnaryPredicateIterator first,
             InputUnaryPredicateIterator last,
             typename boost::unary_traits< boost::iterator_value< InputUnaryPredicateIterator >::type >::argument_type value )
{
   typedef boost::iterator_value< InputUnaryPredicateIterator >::type unary_predicate;
   BOOST_CONCEPT_ASSERT(( boost::UnaryPredicate< unary_predicate,
                          boost::unary_traits< unary_predicate >::argument_type ));

    while ( first != last )
    {
      if ( (*first)( value ) )
          return true;
      ++first;
    }
    return false;
}


}

#endif // UTILITY_HPP
