#ifndef SOCCER_FORMATION_CONCEPTS_HPP
#define SOCCER_FORMATION_CONCEPTS_HPP

#include <soccer/formation_traits.hpp>

#include <soccer/core/position_type.hpp>
#include <soccer/core/role_iterator_type.hpp>
#include <soccer/core/role_id_type.hpp>

#include <soccer/core/tags.hpp>

#include <soccer/position_concepts.hpp>

#include <boost/functional.hpp>

#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

#include <boost/type_traits.hpp>
#include <boost/mpl/has_xxx.hpp>

namespace soccer {

BOOST_concept(FormationConcept,(F))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< formation_tag, typename tag< F >::type >::value ),
                              "Type is not categorized with the expected category tag");
    typedef typename position_type< F >::type position_t;
    typedef typename role_id_type<F>::type role_id_t;
    typedef typename role_iterator_type<F>::type role_iterator_t;

    BOOST_CONCEPT_ASSERT(( PositionConcept<position_t> ));
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<role_iterator_t> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< role_iterator_t >::type,
                                              role_id_t>::value ),
                            "Role iterator value type does not match role descriptor type");

    BOOST_CONCEPT_USAGE(FormationConcept)
    {
        const_constraints( f );
    }

private:
    void const_constraints( const F& f )
    {
        role_iterator_ts = get_roles( f );
        // _pos = get_position( _pid, f );
    }

    std::pair< role_iterator_t, role_iterator_t > role_iterator_ts;
    role_id_t _pid;
    position_t _pos;
    F f;
};

}

#endif // SOCCER_FORMATION_CONCEPTS_HPP
