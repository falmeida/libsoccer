#ifndef SOCCER_REGISTER_SHOOT_HPP
#define SOCCER_REGISTER_SHOOT_HPP

#include <soccer/register/action.hpp>

//
// SHOOT REGISTRATION MACROS
//

/*!
\brief \brief_macro{shoot type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_OBJECT, static object}. \details_macro_const
\param Shoot \param_macro_type{Shoot}
\param TargetPositionType \param_macro_pos_type{position}
\param FirstVelocityType \param_macro_vel_type{position}
*/
#define SOCCER_DETAIL_SPECIALIZE_SHOOT_TRAITS(Shoot, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TAG_TRAITS(Shoot, shoot_tag) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_TRAITS(Shoot, PlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_TRAITS(Shoot, TargetPositionType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_TRAITS(Shoot, FirstVelocityType)


/*!
\brief \brief_macro{shoot type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_SHOOT, Shoot}. \details_macro
\param Shoot \param_macro_type{Shoot}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_shoot_target_pos}
\param FirstVelocityType \param_macro_member{\macro_shoot_first_vel}
*/
#define SOCCER_REGISTER_SHOOT(Shoot, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_SHOOT_TRAITS(Shoot, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Shoot, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Shoot, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Shoot, FirstVelocityType, FirstVelocity) \
}}}

/*!
\brief \brief_macro{const shoot type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_SHOOT_CONST, Shoot}. \details_macro_const
\param Shoot \param_macro_type{Shoot}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_shoot_target_pos}
\param FirstVelocityType \param_macro_member{\macro_shoot_first_vel}
*/
#define SOCCER_REGISTER_SHOOT_CONST(Shoot, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_SHOOT_TRAITS(Shoot, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Shoot, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Shoot, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Shoot, FirstVelocityType, FirstVelocity) \
}}}


/*!
\brief \brief_macro{shoot type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_SHOOT, Shoot}. \details_macro
\param Shoot \param_macro_type{Shoot}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_shoot_target_pos}
\param FirstVelocityType \param_macro_member{\macro_shoot_first_vel}
*/
#define SOCCER_REGISTER_SHOOT_GET_SET(Shoot, \
                                      PlayerIdType, GetPlayerId, SetPlayerId, \
                                      TargetPositionType, GetTargetPosition, SetTargetPosition, \
                                      FirstVelocityType, GetFirstVelocity, SetFirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_SHOOT_TRAITS(Shoot, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Shoot, PlayerIdType, GetPlayerId, SetPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Shoot, TargetPositionType, GetTargetPosition, SetTargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Shoot, FirstVelocityType, GetFirstVelocity, SetFirstVelocity) \
}}}

#endif // SOCCER_REGISTER_SHOOT_HPP
