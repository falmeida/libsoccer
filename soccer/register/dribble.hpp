#ifndef SOCCER_REGISTER_DRIBBLE_HPP
#define SOCCER_REGISTER_DRIBBLE_HPP

#include <soccer/register/action.hpp>

//
// DRIBBLE REGISTRATION MACROS
//

/*!
\brief \brief_macro{dribble type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_DETAIL_SPECIALIZE_DRIBBLE_TRAITS, dribble object}. \details_macro
\param Dribble \param_macro_type{Dribble}
\param PlayerIdType \param_macro_player_id_type{player_id}
\param TargetPositionType \param_macro_pos_type{position}
\param FirstVelocityType \param_macro_vel_type{velocity}
*/
#define SOCCER_DETAIL_SPECIALIZE_DRIBBLE_TRAITS(Dribble, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TAG_TRAITS(Dribble, dribble_tag) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_TRAITS(Dribble, PlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_TRAITS(Dribble, TargetPositionType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_TRAITS(Dribble, FirstVelocityType)


/*!
\brief \brief_macro{dribble type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_DRIBBLE, Dribble}. \details_macro
\param Dribble \param_macro_type{Dribble}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_dribble_pos}
\param FirstVelocityType \param_macro_member{\macro_dribble_first_vel}
*/
#define SOCCER_REGISTER_DRIBBLE(Dribble, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_DRIBBLE_TRAITS(Dribble, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Dribble, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Dribble, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Dribble, FirstVelocityType, FirstVelocity) \
}}}

/*!
\brief \brief_macro{const dribble type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_DRIBBLE_CONST, Dribble}. \details_macro_const
\param Dribble \param_macro_type{Dribble}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_dribble_pos}
\param FirstVelocityType \param_macro_member{\macro_dribble_first_vel}
*/
#define SOCCER_REGISTER_DRIBBLE_CONST(Dribble, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_DRIBBLE_TRAITS(Dribble, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Dribble, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Dribble, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Dribble, FirstVelocityType, FirstVelocity) \
}}}


/*!
\brief \brief_macro{dribble type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_DRIBBLE_GET_SET, Dribble}. \details_macro
\param Dribble \param_macro_type{Dribble}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_dribble_pos}
\param FirstVelocityType \param_macro_member{\macro_dribble_first_vel}
*/
#define SOCCER_REGISTER_DRIBBLE_GET_SET(Dribble, \
                                      PlayerIdType, GetPlayerId, SetPlayerId, \
                                      TargetPositionType, GetTargetPosition, SetTargetPosition, \
                                      FirstVelocityType, GetFirstVelocity, SetFirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_DRIBBLE_TRAITS(Dribble, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Dribble, PlayerIdType, GetPlayerId, SetPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Dribble, TargetPositionType, GetTargetPosition, SetTargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Dribble, FirstVelocityType, GetFirstVelocity, SetFirstVelocity) \
}}}

#endif // SOCCER_REGISTER_DRIBBLE_HPP
