#ifndef SOCCER_REGISTER_UTIL_HPP
#define SOCCER_REGISTER_UTIL_HPP

// Object tag traits
#include <soccer/core/tag.hpp>

// Static and mobile object traits
// #include <soccer/core/position_type.hpp>
// #include <soccer/core/velocity_type.hpp>

// Player id traits
// #include <soccer/core/side_type.hpp>
// #include <soccer/core/number_type.hpp>

// State traits
// #include <soccer/core/ball_type.hpp>
// #include <soccer/core/player_type.hpp>
// #include <soccer/core/player_id_type.hpp>
// #include <soccer/core/player_iterator_type.hpp>

#include <cstddef>

//
// Macros for meta-traits definition
//

// TYPE TRAITS MACRO SPECIALIZATION
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_TRAITS(ObjectNameInStruct, MemberNameInStruct, \
                                                      ObjectType, MemberType) \
    template<> struct ObjectNameInStruct##_##MemberNameInStruct<ObjectType> \
        { typedef MemberType type; };

#define SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(StructName, ObjectType, ValueType) \
    template<> struct StructName<ObjectType> { typedef ValueType type; };

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_TRAITS_SHORT(ObjectType, MemberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_TRAITS(ObjectType, ObjectType, MemberType, MemberType)


// NON-CONST MEMBER ACCESS MACRO SPECIALIZATION

// GET/SET FIELD ACCESS MACRO SPECIALIZATION

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET(StructName, ObjectType, ValueType, Field) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get(ObjectType const& obj) \
        { return  obj.Field; } \
    };

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(StructName, ObjectType, ValueType, Field) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get(ObjectType const& obj) \
        { return  obj.Field; } \
        static inline void set(ValueType const& value, ObjectType& obj) \
        { obj.Field = value; } \
    };


// CONST MEMBER ACCESS MACRO SPECIALIZATION
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_CONST_ACCESS_FULL(ObjectNameInStruct, ObjectType, MemberTypeNameInStruct, MemberType, Get) \
    template<> struct ObjectNameInStruct##_##MemberNameInStruct##_##_access<ObjectType> \
    { \
        static inline MemberType get(ObjectType const& obj) { return obj.Get; } \
    };

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_CONST_ACCESS(ObjectType, MemberType, Get) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_CONST_ACCESS_FULL(ObjectType, ObjectType, MemberType, MemberType, Get)


// GET/SET with key MEMBER ACCESS MACRO SPECIALIZATION
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_WITH_KEY(StructName, ObjectType, KeyType, ValueType, Get) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get( KeyType const& key, ObjectType const& obj) \
        { return  obj.Get( key ); } \
    };

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET_WITH_KEY(StructName, ObjectType, KeyType, ValueType, Get, Set) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get( KeyType const& key, ObjectType const& obj ) \
            { return  obj.Get( key ); } \
        static inline void set( KeyType const& key, ValueType const& value, ObjectType& obj ) \
            { obj.Set( key, value ); } \
    };

// GET/SET with key FREE ACCESS MACRO SPECIALIZATION

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_WITH_KEY(StructName, ObjectType, KeyType, ValueType, Get ) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get( KeyType const& key, ObjectType const& obj) \
        { return  Get( key, obj ); } \
    };

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET_WITH_KEY(StructName, ObjectType, KeyType, ValueType, Get, Set) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get( KeyType const& key, ObjectType const& obj) \
            { return  Get( key, obj ); } \
        static inline void set( KeyType const& key, ValueType const& value, ObjectType& obj ) \
            { Set( key, value, obj ); } \
    };

// GET/SET METHOD ACCESS MACRO SPECIALIZATION

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET(StructName, ObjectType, ValueType, Get) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get(ObjectType const& obj) \
        { return  obj.Get(); } \
    };

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(StructName, ObjectType, ValueType, Get, Set) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get(ObjectType const& obj) \
        { return  obj.Get(); } \
        static inline void set( ValueType const& value, ObjectType& obj ) \
        { obj.Set( value ); } \
    };

// GET/SET METHOD ACCESS MACRO SPECIALIZATION

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET(StructName, ObjectType, ValueType, Get) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get(ObjectType const& obj) \
        { return  Get( obj ); } \
    };


#define SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(StructName, ObjectType, ValueType, Get, Set) \
    template<> struct StructName<ObjectType> \
    { \
        static inline ValueType get(ObjectType const& obj) \
            { return  Get( obj ); } \
        static inline void set( ValueType const& value, ObjectType& obj ) \
            { Set( value, obj ); } \
    };

#endif // SOCCER_REGISTER_UTIL_HPP
