#ifndef SOCCER_REGISTER_PLAYER_HPP
#define SOCCER_REGISTER_PLAYER_HPP

#include <soccer/register/object.hpp>

#include <soccer/register/player_id.hpp>

#ifndef DOXYGEN_NO_SPECIALIZATIONS

#endif // DOXYGEN_NO_SPECIALIZATIONS


// Starting point, specialize basic traits necessary to register a point
#define SOCCER_DETAIL_SPECIALIZE_STATIC_PLAYER_TRAITS(Player, PositionType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(Player, static_player_tag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_TRAITS(Player, SideType) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_TRAITS(Player, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_TRAITS(Player, SideType, NumberType)

#define SOCCER_DETAIL_SPECIALIZE_DYNAMIC_PLAYER_TRAITS(Player, PositionType, VelocityType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(Player, dynamic_player_tag, PositionType, VelocityType ) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_TRAITS(Player, SideType) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_TRAITS(Player, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_TRAITS(Player, SideType, NumberType)

//
// PLAYER REGISTRATION MACROS
//

/*!
\brief \brief_macro{static player type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_PLAYER, static player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_STATIC_PLAYER(Player, PositionType, SideType, NumberType, Position, Side, Number ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_PLAYER_TRAITS(Player, PositionType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS(Player, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS(Player, SideType, Side) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS(Player, NumberType, Number) \
SOCCER_TRAITS_NAMESPACE_END


/*!
\brief \brief_macro{static player type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_PLAYER, dynamic player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param VelocityType \param_macro_veltype{vel}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Velocity \param_macro_member{\macro_velocity}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_DYNAMIC_PLAYER(Player, PositionType, VelocityType, SideType, NumberType, Position, Velocity, Side, Number ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_PLAYER_TRAITS(Player, PositionType, VelocityType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS(Player, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS(Player, VelocityType, Velocity) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS(Player, SideType, Side) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS(Player, NumberType, Number) \
SOCCER_TRAITS_NAMESPACE_END


//
// CONST PLAYER REGISTRATION MACROS
//

/*!
\brief \brief_macro{static player type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_PLAYER_CONST, const static player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_STATIC_PLAYER_CONST(Player, PositionType, SideType, NumberType, Position, Side, Number ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_PLAYER_TRAITS(Player, PositionType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_CONST(Player, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_CONST(Player, SideType, Side) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_CONST(Player, NumberType, Number) \
SOCCER_TRAITS_NAMESPACE_END


/*!
\brief \brief_macro{static player type}  \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_PLAYER_CONST, const dynamic player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param VelocityType \param_macro_veltype{vel}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Velocity \param_macro_member{\macro_velocity}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_DYNAMIC_PLAYER_CONST(Player, PositionType, VelocityType, SideType, NumberType, Position, Velocity, Side, Number ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_PLAYER_TRAITS(Player, PositionType, VelocityType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_CONST(Player, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_CONST(Player, VelocityType, Velocity) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_CONST(Player, SideType, Side) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_CONST(Player, NumberType, Number) \
SOCCER_TRAITS_NAMESPACE_END

//
// PLAYER GET/SET REGISTRATION MACROS
//

/*!
\brief \brief_macro{static player type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_PLAYER, static player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_STATIC_PLAYER_GET_SET(Player, \
                                              PositionType, GetPosition, SetPosition, \
                                              SideType, GetSide, SetSide, \
                                              NumberType, GetNumber, SetNumber ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_PLAYER_TRAITS(Player, PositionType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET(Player, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_GET_SET(Player, SideType, GetSide, SetSide) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_GET_SET(Player, NumberType, GetNumber, SetNumber) \
SOCCER_TRAITS_NAMESPACE_END


/*!
\brief \brief_macro{static player type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_PLAYER, dynamic player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param VelocityType \param_macro_veltype{vel}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Velocity \param_macro_member{\macro_velocity}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_DYNAMIC_PLAYER_GET_SET( Player, \
                                                PositionType, GetPosition, SetPosition, \
                                                VelocityType, GetVelocity, SetVelocity, \
                                                SideType, GetSide, SetSide, \
                                                NumberType, GetNumber, SetNumber ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_PLAYER_TRAITS(Player, PositionType, VelocityType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET(Player, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET(Player, VelocityType, GetVelocity, SetVelocity) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_GET_SET(Player, SideType, GetSide, SetSide) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_GET_SET(Player, NumberType, GetNumber, SetNumber) \
SOCCER_TRAITS_NAMESPACE_END


//
// PLAYER GET/SET FREE FUNCTION REGISTRATION MACROS
//

/*!
\brief \brief_macro{static player type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_PLAYER_GET_SET_FREE, static player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_STATIC_PLAYER_GET_SET_FREE(Player, \
                                                   PositionType, GetPosition, SetPosition, \
                                                   SideType, GetSide, SetSide, \
                                                   NumberType, GetNumber, SetNumber ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_PLAYER_TRAITS(Player, PositionType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET_FREE(Player, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_GET_SET_FREE(Player, SideType, GetSide, SetSide) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_GET_SET_FREE(Player, NumberType, GetNumber, SetNumber) \
SOCCER_TRAITS_NAMESPACE_END


/*!
\brief \brief_macro{static player type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_PLAYER_GET_SET_FREE, dynamic player type}
\param Player \param_macro_type{Player}
\param PositionType \param_macro_postype{pos}
\param VelocityType \param_macro_veltype{vel}
\param SideType \param_macro_sidetype{side}
\param NumberType \param_macro_numbertype{number}
\param Position \param_macro_member{\macro_position}
\param Velocity \param_macro_member{\macro_velocity}
\param Side \param_macro_member{\macro_side}
\param Number \param_macro_member{\macro_number}
*/
#define SOCCER_REGISTER_DYNAMIC_PLAYER_GET_SET_FREE(Player, \
                                                    PositionType, GetPosition, SetPosition, \
                                                    VelocityType, GetVelocity, SetVelocity, \
                                                    SideType, GetSide, SetSide, \
                                                    NumberType, GetNumber, SetNumber ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_PLAYER_TRAITS(Player, PositionType, VelocityType, SideType, NumberType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET_FREE(Player, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET_FREE(Player, VelocityType, GetVelocity, SetVelocity) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_GET_SET_FREE(Player, SideType, GetSide, SetSide) \
    SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_GET_SET_FREE(Player, NumberType, GetNumber, SetNumber) \
SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_REGISTER_PLAYER_HPP
