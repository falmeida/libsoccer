#ifndef SOCCER_REGISTER_TACKLE_HPP
#define SOCCER_REGISTER_TACKLE_HPP

#include <soccer/register/action.hpp>

//
// TACKLE REGISTRATION MACROS
//

/*!
\brief \brief_macro{tackle type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_DETAIL_SPECIALIZE_TACKLE_TRAITS, tackle object}. \details_macro
\param Tackle \param_macro_type{Tackle}
\param PlayerIdType \param_macro_player_id_type{player_id}
\param TargetPositionType \param_macro_pos_type{position}
\param FirstVelocityType \param_macro_vel_type{velocity}
*/
#define SOCCER_DETAIL_SPECIALIZE_TACKLE_TRAITS(Tackle, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TAG_TRAITS(Tackle, tackle_tag) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_TRAITS(Tackle, PlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_TRAITS(Tackle, TargetPositionType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_TRAITS(Tackle, FirstVelocityType)


/*!
\brief \brief_macro{tackle type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_TACKLE, Tackle}. \details_macro
\param Tackle \param_macro_type{Tackle}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_tackle_pos}
\param FirstVelocityType \param_macro_member{\macro_tackle_first_vel}
*/
#define SOCCER_REGISTER_TACKLE(Tackle, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_TACKLE_TRAITS(Tackle, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Tackle, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Tackle, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Tackle, FirstVelocityType, FirstVelocity) \
}}}

/*!
\brief \brief_macro{const tackle type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_TACKLE_CONST, Tackle}. \details_macro_const
\param Tackle \param_macro_type{Tackle}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_tackle_pos}
\param FirstVelocityType \param_macro_member{\macro_tackle_first_vel}
*/
#define SOCCER_REGISTER_TACKLE_CONST(Tackle, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_TACKLE_TRAITS(Tackle, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Tackle, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Tackle, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Tackle, FirstVelocityType, FirstVelocity) \
}}}


/*!
\brief \brief_macro{tackle type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_TACKLE_GET_SET, Tackle}. \details_macro
\param Tackle \param_macro_type{Tackle}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_tackle_pos}
\param FirstVelocityType \param_macro_member{\macro_tackle_first_vel}
*/
#define SOCCER_REGISTER_TACKLE_GET_SET(Tackle, \
                                      PlayerIdType, GetPlayerId, SetPlayerId, \
                                      TargetPositionType, GetTargetPosition, SetTargetPosition, \
                                      FirstVelocityType, GetFirstVelocity, SetFirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_TACKLE_TRAITS(Tackle, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Tackle, PlayerIdType, GetPlayerId, SetPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Tackle, TargetPositionType, GetTargetPosition, SetTargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Tackle, FirstVelocityType, GetFirstVelocity, SetFirstVelocity) \
}}}

#endif // SOCCER_REGISTER_TACKLE_HPP
