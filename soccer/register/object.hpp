#ifndef SOCCER_REGISTER_OBJECT_HPP
#define SOCCER_REGISTER_OBJECT_HPP

#include <soccer/register/util.hpp>

#ifndef DOXYGEN_NO_SPECIALIZATIONS

#define SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(ObjectType, ObjectTag) \
        template<> struct tag<ObjectType> { typedef ObjectTag type; };

// Position stuff

#define SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_TRAITS(ObjectType, PositionType) \
        template<> struct position_type<ObjectType> { typedef PositionType type; };
        // SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_TRAITS(ObjectType, PositionType)

// Starting point, specialize basic traits necessary to register a static object
#define SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(ObjectType, ObjectTag) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_TRAITS(ObjectType, PositionType)


// Specialize position access class
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS(ObjectType, PositionType, Get, Set) \
    template<> struct position_access<ObjectType> \
    { \
        static inline PositionType get(ObjectType const& obj) { return obj.Get; } \
        static inline void set(PositionType const& value, ObjectType& obj) { obj.Set= value; } \
    };
    // SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS(Object, PositionType, Get, Set)

// Const version
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_CONST(ObjectType, PositionType, Get) \
    template<> struct position_access<ObjectType> \
    { \
        static inline PositionType get(ObjectType const& obj) { return obj.Get; } \
    };
    // SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_CONST_ACCESS(ObjectType, PositionType, Get)


// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET(ObjectType, PositionType, Get, Set) \
    template<> struct position_access<ObjectType> \
    { \
        static inline PositionType get(ObjectType const& obj) \
        { return  obj.Get (); } \
        static inline void set(PositionType const& value, ObjectType& obj) \
        { obj.Set ( value ); } \
    };
    // SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_GET_SET_ACCESS(Object, PositionType, Get, Set)

// Getter/setter free funciton version
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET_FREE(ObjectType, PositionType, Get, Set) \
    template<> struct position_access<ObjectType> \
    { \
        static inline PositionType get(ObjectType const& obj) \
        { return  Get( obj ); } \
        static inline void set(PositionType const& value, ObjectType& obj) \
        { Set( value, obj ); } \
    };

// Starting point, specialize basic traits necessary to register a mobile object
#define SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(ObjectType, ObjectTag) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_TRAITS(ObjectType, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_TRAITS(ObjectType, VelocityType)

// velocity stuff
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_TRAITS(ObjectType, VelocityType) \
        template<> struct velocity_type<ObjectType> { typedef VelocityType type; };

// Specialize position access class
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS(ObjectType, VelocityType, GetVelocity, SetVelocity) \
    template<> struct velocity_access<ObjectType> \
    { \
        static inline VelocityType get(ObjectType const& obj) { return obj.GetVelocity; } \
        static inline void set( VelocityType const& value, ObjectType& obj) { obj.SetVelocity= value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_CONST(Object, VelocityType, Get) \
    template<> struct velocity_access<ObjectType> \
    { \
        static inline VelocityType get(ObjectType const& obj) { return obj.Get; } \
    };


// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET(ObjectType, VelocityType, Get, Set) \
    template<> struct velocity_access<ObjectType> \
    { \
        static inline VelocityType get(ObjectType const& obj) \
        { return  obj.Get (); } \
        static inline void set( VelocityType const& value, ObjectType& obj) \
        { obj.Set ( value ); } \
    };

// Getter/setter free function version
#define SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET_FREE(ObjectType, VelocityType, Get, Set) \
    template<> struct velocity_access<ObjectType> \
    { \
        static inline VelocityType get(ObjectType const& obj) \
        { return  Get( obj ); } \
        static inline void set( VelocityType const& value, ObjectType& obj) \
        { Set ( value, obj ); } \
    };

#endif // DOXYGEN_NO_SPECIALIZATIONS

//
// OBJECT REGISTRATION MACROS
//

/*!
\brief \brief_macro{static object type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_OBJECT, static object}. \details_macro_const
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_OBJECT(ObjectType, ObjectTag, PositionType, Position ) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS(ObjectType, PositionType, Position) \
    SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static object type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_OBJECT, dynamic object}. \details_macro_const
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_OBJECT(ObjectType, ObjectTag, PositionType, Position, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN  \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS(ObjectType, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS(ObjectType, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static object type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_OBJECT_CONST, const static object}. \details_macro_const
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_OBJECT_CONST(ObjectType, ObjectTag, PositionType, Position ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_CONST(ObjectType, PositionType, Position) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static object type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_OBJECT_CONST, const dynamic object}. \details_macro_const
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_OBJECT_CONST(ObjectType, ObjectTag, PositionType, Position, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_CONST(ObjectType, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_CONST(ObjectType, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_END


/*!
\brief \brief_macro{static object type} \brief_macro_get_set
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_OBJECT_GET_SET, static object}. \details_macro
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_OBJECT_GET_SET(ObjectType, ObjectTag, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET(ObjectType, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static object type} \brief_macro_get_set
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_OBJECT_GET_SET, dynamic object}. \details_macro
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_OBJECT_GET_SET(ObjectType, ObjectTag, PositionType, GetPosition, SetPosition, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET(ObjectType, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET(ObjectType, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_END



/*!
\brief \brief_macro{static object type} \brief_macro_get_set
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_OBJECT_GET_SET, static object}. \details_macro
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_OBJECT_GET_SET_FREE(ObjectType, ObjectTag, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET_FREE(ObjectType, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static object type} \brief_macro_get_set
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_OBJECT_GET_SET, dynamic object}. \details_macro
\param Object \param_macro_type{Object}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_OBJECT_GET_SET_FREE(ObjectType, ObjectTag, PositionType, GetPosition, SetPosition, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(ObjectType, ObjectTag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET_FREE(ObjectType, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET_FREE(ObjectType, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_REGISTER_OBJECT_HPP
