#ifndef SOCCER_REGISTER_STATE_HPP
#define SOCCER_REGISTER_STATE_HPP

#include <soccer/register/object.hpp>

#ifndef DOXYGEN_NO_SPECIALIZATIONS

//
// PLAYER TYPE TRAITS
//

#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_TRAITS(State, PlayerType) \
        template<> struct player_type<State> { typedef PlayerType type; };

// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ACCESS_GET_SET(State, PlayerIdType, PlayerType, Get, Set) \
    template<> struct player_access<State> \
    { \
        static inline PlayerType get( PlayerIdType const pid, State const& s) \
        { return  s.Get(pid); } \
        static inline void set(PlayerIdType const pid, PlayerType const& value, State& s) \
        { s. Set ( pid, value ); } \
    };

// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ACCESS_GET_SET_FREE(State, PlayerIdType, PlayerType, Get, Set) \
    template<> struct player_access<State> \
    { \
        static inline PlayerType get( PlayerIdType const pid, State const& s) \
        { return  Get( pid, s ); } \
        static inline void set(PlayerIdType const pid, PlayerType const& value, State& s) \
        { Set( pid, value, s ); } \
    };

//
// PLAYER ID TYPE TRAITS
//


#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ID_TRAITS(State, PlayerIdType) \
        template<> struct player_id_type<State> { typedef PlayerIdType type; };

//
// PLAYER ITERATOR TYPE TRAITS
//

#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ITERATOR_TRAITS(State, PlayerIteratorType) \
        template<> struct player_iterator_type<State> { typedef PlayerIteratorType type; };


// Specialize player side access class
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ID_ITERATORS_ACCESS(State, PlayerIteratorType, Get) \
    template<> struct player_iterators_access<State> \
    { \
        static inline std::pair< PlayerIteratorType, PlayerIteratorType > get(State const& s) \
        { return s.Get; } \
    };

// Getter version
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ID_ITERATORS_ACCESS_GET_SET(State, PlayerIteratorType, Get) \
    template<> struct player_iterators_access<State> \
    { \
        static inline std::pair< PlayerIteratorType, PlayerIteratorType > get(State const& s) \
        { return  s.Get(); } \
    };

// Getter free function version
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ID_ITERATORS_ACCESS_GET_SET_FREE(State, PlayerIteratorType, Get) \
    template<> struct player_iterators_access<State> \
    { \
        static inline std::pair< PlayerIteratorType, PlayerIteratorType > get(State const& s) \
        { return Get( s ); } \
    };

//
// BALL TYPE TRAITS
//

#define SOCCER_DETAIL_SPECIALIZE_STATE_BALL_TRAITS(State, BallType) \
        template<> struct ball_type<State> { typedef BallType type; };

// Specialize player side access class
#define SOCCER_DETAIL_SPECIALIZE_STATE_BALL_ACCESS(State, BallType, Get, Set) \
    template<> struct ball_access<State> \
    { \
        static inline BallType get(State const& s) { return s. Get; } \
        static inline void set(BallType const& value, State& s) { s. Set = value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_STATE_BALL_ACCESS_CONST(State, BallType, Get) \
    template<> struct ball_access<State> \
    { \
        static inline BallType get(State const& s) { return s. Get; } \
    };


// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_STATE_BALL_ACCESS_GET_SET(State, BallType, Get, Set) \
    template<> struct ball_access<State> \
    { \
        static inline BallType get(State const& s) \
        { return  s. Get (); } \
        static inline void set( BallType const& value, State& s) \
        { s. Set ( value ); } \
    };

// Getter/setter free function version
#define SOCCER_DETAIL_SPECIALIZE_STATE_BALL_ACCESS_GET_SET_FREE(State, BallType, Get, Set) \
    template<> struct ball_access<State> \
    { \
        static inline BallType get(State const& s) \
        { return  Get( s ); } \
        static inline void set(BallType const& value, State& s) \
        { Set ( value, s ); } \
    };


//
// PLAYMODE TYPE TRAITS
//

#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYMODE_TRAITS(State, PlayModeType) \
        template<> struct playmode_type<State> { typedef PlayModeType type; };

// Specialize player side access class
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYMODE_ACCESS(State, PlayModeType, Get, Set) \
    template<> struct playmode_access<State> \
    { \
        static inline PlayModeType get(State const& s) { return s. Get; } \
        static inline void set(PlayModeType const& value, State& s) { s. Set = value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYMODE_ACCESS_CONST(State, PlayModeType, Get) \
    template<> struct playmode_access<State> \
    { \
        static inline PlayModeType get(State const& s) { return s. Get; } \
    };


// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYMODE_ACCESS_GET_SET(State, PlayModeType, Get, Set) \
    template<> struct playmode_access<State> \
    { \
        static inline PlayModeType get(State const& s) \
        { return  s. Get (); } \
        static inline void set(PlayModeType const& value, State& s) \
        { s. Set ( value ); } \
    };

// Getter/setter free function version
#define SOCCER_DETAIL_SPECIALIZE_STATE_PLAYMODE_ACCESS_GET_SET_FREE(State, PlayModeType, Get, Set) \
    template<> struct playmode_access<State> \
    { \
        static inline PlayModeType get(State const& s) \
        { return  Get( s ); } \
        static inline void set(PlayModeType const& value, State& s) \
        { Set ( value, s ); } \
    };


//
// TIME TYPE TRAITS
//
#define SOCCER_DETAIL_SPECIALIZE_STATE_TIME_TRAITS(State, TimeType) \
        template<> struct time_type<State> { typedef TimeType type; };

// Specialize player side access class
#define SOCCER_DETAIL_SPECIALIZE_STATE_TIME_ACCESS(State, TimeType, Get, Set) \
    template<> struct time_access<State> \
    { \
        static inline TimeType get(State const& s) { return s. Get; } \
        static inline void set(TimeType const& value, State& s) { s. Set = value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_STATE_TIME_ACCESS_CONST(State, TimeType, Get) \
    template<> struct time_access<State> \
    { \
        static inline TimeType get(State const& s) { return s. Get; } \
    };


// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_STATE_TIME_ACCESS_GET_SET(State, TimeType, Get, Set) \
    template<> struct time_access<State> \
    { \
        static inline TimeType get(State const& s) \
        { return  s. Get (); } \
        static inline void set(TimeType const& value, State& s) \
        { s. Set ( value ); } \
    };

// Getter/setter free function version
#define SOCCER_DETAIL_SPECIALIZE_STATE_TIME_ACCESS_GET_SET_FREE(State, TimeType, Get, Set) \
    template<> struct time_access<State> \
    { \
        static inline TimeType get(State const& s) \
        { return  Get( s ); } \
        static inline void set(TimeType const& value, State& s) \
        { Set ( value, s ); } \
    };

//
// SCORE TYPE TRAITS
//

#endif // DOXYGEN_NO_SPECIALIZATIONS


// Starting point, specialize basic traits necessary to register a point
#define SOCCER_DETAIL_SPECIALIZE_STATE_TRAITS(State, BallType, PlayerIdType, PlayerType, PlayerIteratorType, TimeType, PlayModeType) \
    SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(State, state_tag) \
    SOCCER_DETAIL_SPECIALIZE_STATE_BALL_TRAITS(State, BallType) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ID_TRAITS(State, PlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_TRAITS(State, PlayerType) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ITERATOR_TRAITS(State, PlayerIteratorType) \
    SOCCER_DETAIL_SPECIALIZE_STATE_TIME_TRAITS(State, TimeType) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYMODE_TRAITS(State, PlayModeType)

//
// STATE GET/SET REGISTRATION MACROS
//

/*!
\brief \brief_macro{state type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATE_GET_SET, state type}
\param State \param_macro_type{State}
\param BallType \param_macro_balltype{ball}
\param PlayerType \param_macro_playertype{player}
\param Get \param_macro_member{\macro_get_ball}
\param Set \param_macro_member{\macro_set_ball}
\param GetPlayer \param_macro_member{\macro_get_player}
\param SetPlayer \param_macro_member{\macro_set_player}
*/
#define SOCCER_REGISTER_STATE_GET_SET(State, \
                                      BallType, GetBall, SetBall, \
                                      PlayerIdType, GetPlayerIds, \
                                      PlayerType, GetPlayer, SetPlayer) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATE_TRAITS(State, BallType, PlayerType) \
    SOCCER_DETAIL_SPECIALIZE_STATE_BALL_ACCESS_GET_SET(State, BallType, GetBall, SetBall) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ID_ITERATORS_ACCESS_GET_SET(State, PlayerIteratorType, GetPlayerIds) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ACCESS_GET_SET(State, PlayerIdType, PlayerType, GetPlayer, SetPlayer) \
    SOCCER_TRAITS_NAMESPACE_END

//
// STATE GET/SET FREE FUNCTIONS REGISTRATION MACROS
//

/*!
\brief \brief_macro{state type}
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATE_GET_SET, state type}
\param State \param_macro_type{State}
\param BallType \param_macro_balltype{ball}
\param PlayerType \param_macro_playertype{player}
\param Get \param_macro_member{\macro_get_ball}
\param Set \param_macro_member{\macro_set_ball}
\param GetPlayer \param_macro_member{\macro_get_player}
\param SetPlayer \param_macro_member{\macro_set_player}
*/
#define SOCCER_REGISTER_STATE_GET_SET_FREE(State, \
                                           BallType, GetBall, SetBall, \
                                           PlayerIdType, PlayerIteratorType, GetPlayerIds, \
                                           PlayerType, GetPlayer, SetPlayer, \
                                           TimeType, GetTime, SetTime, \
                                           PlayModeType, GetPlayMode, SetPlayMode ) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATE_TRAITS(State, BallType, PlayerIdType, PlayerType, PlayerIteratorType, TimeType, PlayModeType) \
    SOCCER_DETAIL_SPECIALIZE_STATE_BALL_ACCESS_GET_SET_FREE(State, BallType, GetBall, SetBall) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ID_ITERATORS_ACCESS_GET_SET_FREE(State, PlayerIteratorType, GetPlayerIds) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYER_ACCESS_GET_SET_FREE(State, PlayerIdType, PlayerType, GetPlayer, SetPlayer) \
    SOCCER_DETAIL_SPECIALIZE_STATE_TIME_ACCESS_GET_SET_FREE(State, TimeType, GetTime, SetTime) \
    SOCCER_DETAIL_SPECIALIZE_STATE_PLAYMODE_ACCESS_GET_SET_FREE(State, PlayModeType, GetPlayMode, SetPlayMode) \
    SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_REGISTER_STATE_HPP
