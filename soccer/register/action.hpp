#ifndef SOCCER_REGISTER_ACTION_HPP
#define SOCCER_REGISTER_ACTION_HPP

#include <soccer/register/object.hpp>

#define SOCCER_DETAIL_SPECIALIZE_ACTION_TAG_TRAITS(Action, ActionTag) \
    SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(Action, ActionTag) \


//
// Specialize action executor id access class
//
#define SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_TRAITS(Action, PlayerIdType) \
        template<> struct executor_id_type<Action> { typedef PlayerIdType type; };

// Specialize action executor id access class
#define SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Action, PlayerIdType, GetExecutorId, SetExecutorId) \
    template<> struct executor_id_access<Action> \
    { \
        static inline PlayerIdType get(Action const& s) { return s. GetExecutorId; } \
        static inline void set(Action& s, PlayerIdType const& value) { s. SetExecutorId = value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Action, PlayerIdType, Get) \
    template<> struct executor_id_access<Action> \
    { \
        static inline PlayerIdType get(Action const& s) { return s. Get; } \
    };

// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Action, PlayerIdType, Get, Set) \
    template<> struct executor_id_access<Action> \
    { \
        static inline PlayerIdType get(Action const& s) \
        { return  s. Get (); } \
        static inline void set(Action& s, PlayerIdType const& value) \
        { s. Set ( value ); } \
    };


//
// Specialize action target position access class
//
#define SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_TRAITS(Action, TargetPositionType) \
        template<> struct action_target_position_type<Action> { typedef TargetPositionType type; };

// Specialize action target position access class
#define SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Action, TargetPositionType, GetPosition, SetPosition) \
    template<> struct action_target_position_access<Action> \
    { \
        static inline TargetPositionType get(Action const& s) { return s. GetPosition; } \
        static inline void set(Action& s, TargetPositionType const& value) { s. SetPosition = value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Action, TargetPositionType, Get) \
    template<> struct action_target_position_access<Action> \
    { \
        static inline TargetPositionType get(Action const& s) { return s. Get; } \
    };

// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Action, TargetPositionType, Get, Set) \
    template<> struct action_target_position_access<Action> \
    { \
        static inline TargetPositionType get(Action const& s) \
        { return  s. Get (); } \
        static inline void set(Action& s, TargetPositionType const& value) \
        { s. Set ( value ); } \
    };


//
// Specialize action first velocity access class
//
#define SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_TRAITS(Action, FirstVelocityType) \
        template<> struct action_first_velocity_type<Action> { typedef FirstVelocityType type; };

// Specialize action first velocity access class
#define SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Action, FirstVelocityType, GetVelocity, SetVelocity) \
    template<> struct action_first_velocity_access<Action> \
    { \
        static inline FirstVelocityType get(Action const& s) { return s. GetVelocity; } \
        static inline void set(Action& s, FirstVelocityType const& value) { s. SetVelocity = value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Action, FirstVelocityType, Get) \
    template<> struct action_first_velocity_access<Action> \
    { \
        static inline FirstVelocityType get(Action const& s) { return s. Get; } \
    };

// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Action, FirstVelocityType, Get, Set) \
    template<> struct action_first_velocity_access<Action> \
    { \
        static inline FirstVelocityType get(Action const& s) \
        { return  s. Get (); } \
        static inline void set(Action& s, FirstVelocityType const& value) \
        { s. Set ( value ); } \
    };




#endif // SOCCER_REGISTER_ACTION_HPP
