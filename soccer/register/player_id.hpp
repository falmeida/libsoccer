#ifndef SOCCER_REGISTER_PLAYER_ID_HPP
#define SOCCER_REGISTER_PLAYER_ID_HPP

#include <soccer/register/util.hpp>

#include <soccer/core/player_id_type.hpp>

//
// PLAYER_ID SIDE TRAITS
//

#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_TRAITS(Player, SideType) \
        template<> struct side_type<Player> { typedef SideType type; };

// Specialize player side access class
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS(Player, SideType, GetSide, SetSide) \
    template<> struct side_access<Player> \
    { \
        static inline SideType get(Player const& p) { return p.GetSide; } \
        static inline void set(Player& p, SideType const& value) { p.SetSide= value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_CONST(Player, SideType, Get) \
    template<> struct side_access<Player> \
    { \
        static inline SideType get(Player const& p) { return p. Get; } \
    };


// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_GET_SET(Player, SideType, Get, Set) \
    template<> struct side_access<Player> \
    { \
        static inline SideType get(Player const& p) \
        { return  p. Get (); } \
        static inline void set(Player& p, SideType const& value) \
        { p. Set ( value ); } \
    };

// Getter/setter free function version
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_SIDE_ACCESS_GET_SET_FREE(Player, SideType, Get, Set) \
    template<> struct side_access<Player> \
    { \
        static inline SideType get(Player const& p) \
        { return  Get( p ); } \
        static inline void set(Player& p, SideType const& value) \
        { Set ( value, p ); } \
    };

//
// PLAYER_ID NUMBER TRAITS


#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_TRAITS(Player, NumberType) \
        template<> struct number_type<Player> { typedef NumberType type; };

// Specialize player side access class
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS(Player, NumberType, GetNumber, SetNumber) \
    template<> struct number_access<Player> \
    { \
        static inline NumberType get(Player const& p) { return p.GetNumber; } \
        static inline void set(Player& p, NumberType const& value) { p.SetNumber= value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_CONST(Player, NumberType, Get) \
    template<> struct number_access<Player> \
    { \
        static inline NumberType get(Player const& p) { return p. Get; } \
    };


// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_GET_SET(Player, NumberType, Get, Set) \
    template<> struct number_access<Player> \
    { \
        static inline NumberType get(Player const& p) \
        { return  p. Get (); } \
        static inline void set(Player& p, NumberType const& value) \
        { p. Set ( value ); } \
    };


// Getter/setter free function version
#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_NUMBER_ACCESS_GET_SET_FREE(Player, NumberType, Get, Set) \
    template<> struct number_access<Player> \
    { \
        static inline NumberType get(Player const& p) \
        { return  Get( p ); } \
        static inline void set(Player& p, NumberType const& value) \
        { Set ( value, p ); } \
    };

//
// PLAYER ID TRAITS
//

#define SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_TRAITS(Player, SideType, NumberType) \
        template<> struct player_id_type<Player> { typedef NumberType number_type; typedef SideType side_type; };

#endif // SOCCER_REGISTER_PLAYER_ID_HPP
