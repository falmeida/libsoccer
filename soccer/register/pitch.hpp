#ifndef SOCCER_REGISTER_PITCH_HPP
#define SOCCER_REGISTER_PITCH_HPP

#include <soccer/register/object.hpp>

#define SOCCER_DETAIL_SPECIALIZE_PITCH_TRAITS(Pitch, CoordinateType, LengthType ) \
        SOCCER_DETAIL_SPECIALIZE_TAG_TRAITS(Pitch,pitch_tag) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(coordinate_type, Pitch, CoordinateType) \
        SOCCER_DETAIL_SPECIALIZE_TYPE_TRAITS(length_type, Pitch, LengthType)

#define SOCCER_REGISTER_PITCH(Pitch, CoordinateType, LengthType, \
                              PitchTopLeftX, PitchTopLeftY, \
                              PitchLength, PitchWidth, \
                              GoalAreaWidth, GoalAreaLength, \
                              PenaltyAreaWidth, PenaltyAreaLength, \
                              PenaltyCircleRadius, PenaltySpotDistance, \
                              GoalWidth, GoalDepth, \
                              CenterCircleRadius, CornerArcRadius, GoalPostRadius ) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_PITCH_TRAITS(Pitch, CoordinateType, LengthType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(pitch_top_left_x_access, Pitch, CoordinateType, PitchTopLeftX ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(pitch_top_left_y_access, Pitch, CoordinateType, PitchTopLeftY ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(pitch_length_access, Pitch, LengthType, PitchLength ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(pitch_width_access, Pitch, LengthType, PitchWidth ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(goal_area_length_access, Pitch, LengthType, GoalAreaLength ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(goal_area_width_access, Pitch, LengthType, GoalAreaWidth ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(penalty_area_length_access, Pitch, LengthType, PenaltyAreaLength ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(penalty_area_width_access, Pitch, LengthType, PenaltyAreaWidth ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(penalty_circle_radius_access, Pitch, LengthType, PenaltyCircleRadius ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(penalty_spot_distance_access, Pitch, LengthType, PenaltySpotDistance ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(goal_depth_access, Pitch, LengthType, GoalDepth ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(goal_width_access, Pitch, LengthType, GoalWidth ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(center_circle_radius_access, Pitch, LengthType, CenterCircleRadius ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(corner_arc_radius_access, Pitch, LengthType, CornerArcRadius ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FIELD_GET_SET(goal_post_radius_access, Pitch, LengthType, GoalPostRadius ) \
    SOCCER_TRAITS_NAMESPACE_END


#define SOCCER_REGISTER_PITCH_GET_SET(Pitch, CoordinateType, LengthType, \
                                      GetPitchTopLeftX, SetPitchTopLeftX, \
                                      GetPitchTopLeftY, SetPitchTopLeftY, \
                                      GetPitchLength, SetPitchLength, \
                                      GetPitchWidth, SetPitchWidth, \
                                      GetGoalAreaWidth, SetGoalAreaWidth, \
                                      GetGoalAreaLength, SetGoalAreaLength, \
                                      GetPenaltyAreaWidth, SetPenaltyAreaWidth, \
                                      GetPenaltyAreaLength, SetPenaltyAreaLength, \
                                      GetPenaltyCircleRadius, SetPenaltyCircleRadius, \
                                      GetPenaltySpotDistance, SetPenaltySpotDistance, \
                                      GetGoalWidth, SetGoalWidth, \
                                      GetGoalDepth, SetGoalDepth, \
                                      GetCenterCircleRadius, SetCenterCircleRadius, \
                                      GetCornerArcRadius, SetCornerArcRadius, \
                                      GetGoalPostRadius, SetGoalPostRadius ) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_PITCH_TRAITS(Pitch, CoordinateType, LengthType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(pitch_top_left_x_access, Pitch, CoordinateType, GetPitchTopLeftX, SetPitchTopLeftX ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(pitch_top_left_y_access, Pitch, CoordinateType, GetPitchTopLeftY, SetPitchTopLeftY ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(pitch_length_access, Pitch, LengthType, GetPitchLength, SetPitchLength  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(pitch_width_access, Pitch, LengthType, GetPitchWidth, SetPitchWidth ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(goal_area_length_access, Pitch, LengthType, GetGoalAreaLength, SetGoalAreaLength  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(goal_area_width_access, Pitch, LengthType, GetGoalAreaWidth, SetGoalAreaWidth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(penalty_area_length_access, Pitch, LengthType, GetPenaltyAreaLength, SetPenaltyAreaLength  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(penalty_area_width_access, Pitch, LengthType, GetPenaltyAreaWidth, SetPenaltyAreaWidth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(penalty_circle_radius_access, Pitch, LengthType, GetPenaltyCircleRadius, SetPenaltyCircleRadius  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(penalty_spot_distance_access, Pitch, LengthType, GetPenaltySpotDistance, SetPenaltySpotDistance  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(goal_depth_access, Pitch, LengthType, GetGoalDepth, SetGoalDepth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(goal_width_access, Pitch, LengthType, GetGoalWidth, SetGoalWidth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(center_circle_radius_access, Pitch, LengthType, GetCenterCircleRadius, SetCenterCircleRadius  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(corner_arc_radius_access, Pitch, LengthType, GetCornerArcRadius, SetCornerArcRadius  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_METHOD_GET_SET(goal_post_radius_access, Pitch, LengthType, GetGoalPostRadius, SetGoalPostRadius  ) \
    SOCCER_TRAITS_NAMESPACE_END


#define SOCCER_REGISTER_PITCH_GET_SET_FREE(Pitch, CoordinateType, LengthType, \
                                          GetPitchTopLeftX, SetPitchTopLeftX, \
                                          GetPitchTopLeftY, SetPitchTopLeftY, \
                                          GetPitchLength, SetPitchLength, \
                                          GetPitchWidth, SetPitchWidth, \
                                          GetGoalAreaWidth, SetGoalAreaWidth, \
                                          GetGoalAreaLength, SetGoalAreaLength, \
                                          GetPenaltyAreaWidth, SetPenaltyAreaWidth, \
                                          GetPenaltyAreaLength, SetPenaltyAreaLength, \
                                          GetPenaltyCircleRadius, SetPenaltyCircleRadius, \
                                          GetPenaltySpotDistance, SetPenaltySpotDistance, \
                                          GetGoalWidth, SetGoalWidth, \
                                          GetGoalDepth, SetGoalDepth, \
                                          GetCenterCircleRadius, SetCenterCircleRadius, \
                                          GetCornerArcRadius, SetCornerArcRadius, \
                                          GetGoalPostRadius, SetGoalPostRadius ) \
    SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_PITCH_TRAITS(Pitch, CoordinateType, LengthType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(pitch_top_left_x_access, Pitch, CoordinateType, GetPitchTopLeftX, SetPitchTopLeftX ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(pitch_top_left_y_access, Pitch, CoordinateType, GetPitchTopLeftY, SetPitchTopLeftY ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(pitch_length_access, Pitch, LengthType, GetPitchLength, SetPitchLength  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(pitch_width_access, Pitch, LengthType, GetPitchWidth, SetPitchWidth ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(goal_area_length_access, Pitch, LengthType, GetGoalAreaLength, SetGoalAreaLength  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(goal_area_width_access, Pitch, LengthType, GetGoalAreaWidth, SetGoalAreaWidth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(penalty_area_length_access, Pitch, LengthType, GetPenaltyAreaLength, SetPenaltyAreaLength  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(penalty_area_width_access, Pitch, LengthType, GetPenaltyAreaWidth, SetPenaltyAreaWidth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(penalty_circle_radius_access, Pitch, LengthType, GetPenaltyCircleRadius, SetPenaltyCircleRadius  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(penalty_spot_distance_access, Pitch, LengthType, GetPenaltySpotDistance, SetPenaltySpotDistance  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(goal_depth_access, Pitch, LengthType, GetGoalDepth, SetGoalDepth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(goal_width_access, Pitch, LengthType, GetGoalWidth, SetGoalWidth  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(center_circle_radius_access, Pitch, LengthType, GetCenterCircleRadius, SetCenterCircleRadius  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(corner_arc_radius_access, Pitch, LengthType, GetCornerArcRadius, SetCornerArcRadius  ) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_MEMBER_ACCESS_FREE_GET_SET(goal_post_radius_access, Pitch, LengthType, GetGoalPostRadius, SetGoalPostRadius  ) \
    SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_REGISTER_PITCH_HPP
