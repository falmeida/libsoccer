#ifndef SOCCER_REGISTER_INTERCEPTION_HPP
#define SOCCER_REGISTER_INTERCEPTION_HPP

#include <soccer/register/action.hpp>

//
// INTERCEPTION REGISTRATION MACROS
//

/*!
\brief \brief_macro{interception type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_DETAIL_SPECIALIZE_INTERCEPTION_TRAITS, interception object}. \details_macro
\param Interception \param_macro_type{Interception}
\param PlayerIdType \param_macro_player_id_type{player_id}
\param TargetPositionType \param_macro_pos_type{position}
\param FirstVelocityType \param_macro_vel_type{velocity}
*/
#define SOCCER_DETAIL_SPECIALIZE_INTERCEPTION_TRAITS(Interception, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TAG_TRAITS(Interception, interception_tag) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_TRAITS(Interception, PlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_TRAITS(Interception, TargetPositionType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_TRAITS(Interception, FirstVelocityType)


/*!
\brief \brief_macro{interception type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_INTERCEPTION, Interception}. \details_macro
\param Interception \param_macro_type{Interception}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_interception_pos}
\param FirstVelocityType \param_macro_member{\macro_interception_first_vel}
*/
#define SOCCER_REGISTER_INTERCEPTION(Interception, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_INTERCEPTION_TRAITS(Interception, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Interception, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Interception, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Interception, FirstVelocityType, FirstVelocity) \
}}}

/*!
\brief \brief_macro{const interception type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_INTERCEPTION_CONST, Interception}. \details_macro_const
\param Interception \param_macro_type{Interception}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_interception_pos}
\param FirstVelocityType \param_macro_member{\macro_interception_first_vel}
*/
#define SOCCER_REGISTER_INTERCEPTION_CONST(Interception, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_INTERCEPTION_TRAITS(Interception, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Interception, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Interception, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Interception, FirstVelocityType, FirstVelocity) \
}}}


/*!
\brief \brief_macro{interception type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_INTERCEPTION_GET_SET, Interception}. \details_macro
\param Interception \param_macro_type{Interception}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_interception_pos}
\param FirstVelocityType \param_macro_member{\macro_interception_first_vel}
*/
#define SOCCER_REGISTER_INTERCEPTION_GET_SET(Interception, \
                                      PlayerIdType, GetPlayerId, SetPlayerId, \
                                      TargetPositionType, GetTargetPosition, SetTargetPosition, \
                                      FirstVelocityType, GetFirstVelocity, SetFirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_INTERCEPTION_TRAITS(Interception, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Interception, PlayerIdType, GetPlayerId, SetPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Interception, TargetPositionType, GetTargetPosition, SetTargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Interception, FirstVelocityType, GetFirstVelocity, SetFirstVelocity) \
}}}


#endif // SOCCER_REGISTER_INTERCEPTION_HPP
