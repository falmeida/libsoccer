#ifndef SOCCER_REGISTER_MOVE_HPP
#define SOCCER_REGISTER_MOVE_HPP

#include <soccer/register/action.hpp>

//
// MOVE REGISTRATION MACROS
//

/*!
\brief \brief_macro{move type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_DETAIL_SPECIALIZE_MOVE_TRAITS, move object}. \details_macro
\param Move \param_macro_type{Move}
\param PlayerIdType \param_macro_player_id_type{player_id}
\param TargetPositionType \param_macro_pos_type{position}
\param FirstVelocityType \param_macro_vel_type{velocity}
*/
#define SOCCER_DETAIL_SPECIALIZE_MOVE_TRAITS(Move, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TAG_TRAITS(Move, move_tag) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_TRAITS(Move, PlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_TRAITS(Move, TargetPositionType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_TRAITS(Move, FirstVelocityType)


/*!
\brief \brief_macro{move type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_MOVE, Move}. \details_macro
\param Move \param_macro_type{Move}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_move_pos}
\param FirstVelocityType \param_macro_member{\macro_move_first_vel}
*/
#define SOCCER_REGISTER_MOVE(Move, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_MOVE_TRAITS(Move, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Move, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Move, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Move, FirstVelocityType, FirstVelocity) \
}}}

/*!
\brief \brief_macro{const move type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_MOVE_CONST, Move}. \details_macro_const
\param Move \param_macro_type{Move}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_move_pos}
\param FirstVelocityType \param_macro_member{\macro_move_first_vel}
*/
#define SOCCER_REGISTER_MOVE_CONST(Move, PlayerIdType, PlayerId, TargetPositionType, TargetPosition, FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_MOVE_TRAITS(Move, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Move, PlayerIdType, PlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Move, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Move, FirstVelocityType, FirstVelocity) \
}}}


/*!
\brief \brief_macro{move type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_MOVE_GET_SET, Move}. \details_macro
\param Move \param_macro_type{Move}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_move_pos}
\param FirstVelocityType \param_macro_member{\macro_move_first_vel}
*/
#define SOCCER_REGISTER_MOVE_GET_SET(Move, \
                                      PlayerIdType, GetPlayerId, SetPlayerId, \
                                      TargetPositionType, GetTargetPosition, SetTargetPosition, \
                                      FirstVelocityType, GetFirstVelocity, SetFirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_MOVE_TRAITS(Move, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Move, PlayerIdType, GetPlayerId, SetPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Move, TargetPositionType, GetTargetPosition, SetTargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Move, FirstVelocityType, GetFirstVelocity, SetFirstVelocity) \
}}}

#endif // SOCCER_REGISTER_MOVE_HPP
