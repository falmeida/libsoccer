#ifndef SOCCER_REGISTER_BALL_HPP
#define SOCCER_REGISTER_BALL_HPP

#include <soccer/register/object.hpp>

/*!
\brief \brief_macro{static ball type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_BALL, static Ball}. \details_macro_const
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_BALL(Ball, PositionType, Position ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(Ball, static_ball_tag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS(Ball, PositionType, Position) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{dynamic ball type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_BALL, dynamic Ball}. \details_macro_const
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_BALL(Ball, PositionType, Position, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(Ball, dynamic_ball_tag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS(Ball, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS(Ball, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static ball type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_BALL_CONST, const static Ball}. \details_macro_const
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_BALL_CONST(Ball, PositionType, Position ) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(Ball, static_ball_tag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_CONST(Ball, PositionType, Position) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{dynamic ball type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_BALL_CONST, const dynamic Ball}. \details_macro_const
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_BALL_CONST(Ball, PositionType, Position, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(Ball, dynamic_ball_tag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_CONST(Ball, PositionType, Position) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_CONST(Ball, VelocityType, Velocity) \
SOCCER_TRAITS_NAMESPACE_END


/*!
\brief \brief_macro{static ball type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_BALL_GET_SET, static Ball}. \details_macro
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_BALL_GET_SET(Ball, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(Ball, static_ball_tag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET(Ball, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static Ball type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_BALL_GET_SET, dynamic Ball}. \details_macro
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_BALL_GET_SET(Ball, PositionType, GetPosition, SetPosition, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(Ball, dynamic_ball_tag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET(Ball, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET(Ball, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_END



/*!
\brief \brief_macro{static ball type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_STATIC_BALL_GET_SET_FREE, static Ball}. \details_macro
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
*/
#define SOCCER_REGISTER_STATIC_BALL_GET_SET_FREE(Ball, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_STATIC_OBJECT_TRAITS(Ball, static_ball_tag, PositionType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET_FREE(Ball, PositionType, GetPosition, SetPosition) \
SOCCER_TRAITS_NAMESPACE_END

/*!
\brief \brief_macro{static Ball type} \brief_macro_const
\ingroup register
\details \details_macro{SOCCER_REGISTER_DYNAMIC_BALL_GET_SET_FREE, dynamic Ball}. \details_macro
\param Ball \param_macro_type{Ball}
\param PositionType \param_macro_postype{position}
\param Position\param_macro_member{\macro_pos}
\param VelocityType \param_macro_veltype{velocity}
\param Velocity\param_macro_member{\macro_vel}
*/
#define SOCCER_REGISTER_DYNAMIC_BALL_GET_SET_FREE(Ball, PositionType, GetPosition, SetPosition, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_BEGIN \
    SOCCER_DETAIL_SPECIALIZE_DYNAMIC_OBJECT_TRAITS(Ball, dynamic_ball_tag, PositionType, VelocityType) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_POSITION_ACCESS_GET_SET_FREE(Ball, PositionType, GetPosition, SetPosition) \
    SOCCER_DETAIL_SPECIALIZE_OBJECT_VELOCITY_ACCESS_GET_SET_FREE(Ball, VelocityType, GetVelocity, SetVelocity) \
SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_REGISTER_BALL_HPP
