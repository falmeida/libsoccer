#ifndef SOCCER_REGISTER_PASS_HPP
#define SOCCER_REGISTER_PASS_HPP

#include <soccer/register/action.hpp>

//
// Specialize pass receiver id access class
//
#define SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_TRAITS(Pass, PlayerIdType) \
        template<> struct receiver_id_type<Pass> { typedef PlayerIdType type; };

// Specialize action executor id access class
#define SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS(Pass, PlayerIdType, Get, Set) \
    template<> struct receiver_id_access<Pass> \
    { \
        static inline PlayerIdType get(Pass const& s) { return s. Get; } \
        static inline void set(Pass& s, PlayerIdType const& value) { s. Set = value; } \
    };

// Const version
#define SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS_CONST(Pass, PlayerIdType, Get) \
    template<> struct receiver_id_access<Pass> \
    { \
        static inline PlayerIdType get(Pass const& s) { return s. Get; } \
    };

// Getter/setter version
#define SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS_GET_SET(Pass, PlayerIdType, Get, Set) \
    template<> struct receiver_id_access<Pass> \
    { \
        static inline PlayerIdType get(Pass const& s) \
        { return  s. Get (); } \
        static inline void set(Pass& s, PlayerIdType const& value) \
        { s. Set ( value ); } \
    };


//
// PASS REGISTRATION MACROS
//

/*!
\brief \brief_macro{pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_DETAIL_SPECIALIZE_PASS_TRAITS, pass}. \details_macro_const
\param Pass \param_macro_type{Pass}
\param TargetPositionType \param_macro_pos_type{position}
\param FirstVelocityType \param_macro_vel_type{position}
*/
#define SOCCER_DETAIL_SPECIALIZE_FULL_PASS_TRAITS(Pass, ExecutorPlayerIdType, ReceiverPlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TAG_TRAITS(Pass, pass_tag) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_TRAITS(Pass, ExecutorPlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_TRAITS(Pass, ReceiverPlayerIdType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_TRAITS(Pass, TargetPositionType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_TRAITS(Pass, FirstVelocityType)

/*!
\brief \brief_macro{pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_DETAIL_SPECIALIZE_PASS_TRAITS, pass}. \details_macro_const
\param Pass \param_macro_type{Pass}
\param TargetPositionType \param_macro_pos_type{position}
\param FirstVelocityType \param_macro_vel_type{position}
*/
#define SOCCER_DETAIL_SPECIALIZE_PASS_TRAITS(Pass, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_PASS_TRAITS(Pass, PlayerIdType, PlayerIdType, TargetPositionType, FirstVelocityType)


/*!
\brief \brief_macro{pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_FULL_PASS, Pass}. \details_macro
\param Pass \param_macro_type{Pass}
\param PlayerIdType \param_macro_pos_type{player_id_type}
\param TargetPositionType \param_macro_member{\macro_pass_target_pos}
\param FirstVelocityType \param_macro_member{\macro_pass_first_vel}
*/
#define SOCCER_REGISTER_FULL_PASS(Pass, ExecutorPlayerIdType, ExecutorPlayerId, \
                            ReceiverPlayerIdType, ReceiverPlayerId, \
                            TargetPositionType, TargetPosition, \
                            FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_FULL_PASS_TRAITS(Pass, ExecutorPlayerIdType, ReceiverPlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Pass, ExecutorPlayerIdType, ExecutorPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS(Pass, ReceiverPlayerIdType, ReceiverPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Pass, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Pass, FirstVelocityType, FirstVelocity) \
}}}

/*!
\brief \brief_macro{const pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_FULL_PASS_CONST, Pass}. \details_macro_const
\param Pass \param_macro_type{Pass}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_pass_target_pos}
\param FirstVelocityType \param_macro_member{\macro_pass_first_vel}
*/
#define SOCCER_REGISTER_FULL_PASS_CONST(Pass, ExecutorPlayerIdType, ExecutorPlayerId, \
                                    ReceiverPlayerIdType, ReceiverPlayerId, \
                                    TargetPositionType, TargetPosition, \
                                    FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_FULL_PASS_TRAITS(Pass, ExecutorPlayerIdType, ReceiverPlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Pass, ExecutorPlayerIdType, ExecutorPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS_CONST(Pass, ReceiverPlayerIdType, ReceiverPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Pass, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Pass, FirstVelocityType, FirstVelocity) \
}}}


/*!
\brief \brief_macro{pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_FULL_PASS_GET_SET, Pass}. \details_macro
\param Pass \param_macro_type{Pass}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_pass_pos}
\param FirstVelocityType \param_macro_member{\macro_pass_first_vel}
*/
#define SOCCER_REGISTER_FULL_PASS_GET_SET(Pass, \
                                      ExecutorPlayerIdType, GetExecutorPlayerId, SetExecutorPlayerId, \
                                      ReceiverPlayerIdType, GetReceiverPlayerId, SetReceiverPlayerId, \
                                      TargetPositionType, GetTargetPosition, SetTargetPosition, \
                                      FirstVelocityType, GetFirstVelocity, SetFirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_FULL_PASS_TRAITS(Pass, ExecutorPlayerIdType, ReceiverPlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Pass, ExecutorPlayerIdType, GetExecutorPlayerId, SetExecutorPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS_GET_SET(Pass, ReceiverPlayerIdType, GetReceiverPlayerId, SetReceiverPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Pass, TargetPositionType, GetTargetPosition, SetTargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Pass, FirstVelocityType, GetFirstVelocity, SetFirstVelocity) \
}}}

//
// Pass registration shortcut
//
/*!
\brief \brief_macro{pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_PASS, Pass}. \details_macro
\param Pass \param_macro_type{Pass}
\param PlayerIdType \param_macro_pos_type{player_id_type}
\param TargetPositionType \param_macro_member{\macro_pass_target_pos}
\param FirstVelocityType \param_macro_member{\macro_pass_first_vel}
*/
#define SOCCER_REGISTER_PASS(Pass, PlayerIdType, ExecutorPlayerId, ReceiverPlayerId, \
                            TargetPositionType, TargetPosition, \
                            FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_PASS_TRAITS(Pass, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS(Pass, ExecutorPlayerIdType, ExecutorPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS(Pass, PlayerIdType, ReceiverPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS(Pass, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS(Pass, FirstVelocityType, FirstVelocity) \
}}}

/*!
\brief \brief_macro{const pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_PASS_CONST, Pass}. \details_macro_const
\param Pass \param_macro_type{Pass}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_pass_target_pos}
\param FirstVelocityType \param_macro_member{\macro_pass_first_vel}
*/
#define SOCCER_REGISTER_PASS_CONST(Pass, PlayerIdType, ExecutorPlayerId, ReceiverPlayerId, \
                                    TargetPositionType, TargetPosition, \
                                    FirstVelocityType, FirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_PASS_TRAITS(Pass, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_CONST(Pass, PlayerIdType, ExecutorPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS_CONST(Pass, PlayerIdType, ReceiverPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_CONST(Pass, TargetPositionType, TargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_CONST(Pass, FirstVelocityType, FirstVelocity) \
}}}


/*!
\brief \brief_macro{pass type} \brief_macro
\ingroup register
\details \details_macro{SOCCER_REGISTER_PASS_GET_SET, Pass}. \details_macro
\param Pass \param_macro_type{Pass}
\param PlayerIdType \param_macro_postype{player_id_type}
\param TargetPositionType \param_macro_member{\macro_pass_pos}
\param FirstVelocityType \param_macro_member{\macro_pass_first_vel}
*/
#define SOCCER_REGISTER_PASS_GET_SET(Pass, \
                                      PlayerIdType, GetExecutorPlayerId, SetExecutorPlayerId, GetReceiverPlayerId, SetReceiverPlayerId, \
                                      TargetPositionType, GetTargetPosition, SetTargetPosition, \
                                      FirstVelocityType, GetFirstVelocity, SetFirstVelocity ) \
namespace soccer { namespace action { namespace traits {  \
    SOCCER_DETAIL_SPECIALIZE_PASS_TRAITS(Pass, PlayerIdType, TargetPositionType, FirstVelocityType) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_EXECUTOR_ID_ACCESS_GET_SET(Pass, PlayerIdType, GetExecutorPlayerId, SetExecutorPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_PASS_RECEIVER_ID_ACCESS_GET_SET(Pass, PlayerIdType, GetReceiverPlayerId, SetReceiverPlayerId) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_TARGET_POSITION_ACCESS_GET_SET(Pass, TargetPositionType, GetTargetPosition, SetTargetPosition) \
    SOCCER_DETAIL_SPECIALIZE_ACTION_FIRST_VELOCITY_ACCESS_GET_SET(Pass, FirstVelocityType, GetFirstVelocity, SetFirstVelocity) \
}}}

#endif // SOCCER_REGISTER_PASS_HPP
