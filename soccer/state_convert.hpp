#ifndef SOCCER_STATE_CONVERT_HPP
#define SOCCER_STATE_CONVERT_HPP

#include <soccer/state_traits.hpp>

#include <boost/mpl/assert.hpp>

namespace soccer {

namespace detail {
namespace conversion {


}
}

namespace dispatch
{


template < typename SourceStateTag,
           typename TargetStateTag,
           typename SourceState,
           typename TargetState >
struct convert
{
    BOOST_MPL_ASSERT_MSG
        (
            false,
            NOT_OR_NOT_YET_IMPLEMENTED_FOR_THIS_GEOMETRY_TYPE ,
            (SourceState, TargetState>)
        );
};

template
<
    soccer::static_state_tag,
    soccer::static_state_tag,
    typename SourceState,
    typename TargetState
>
struct convert<SourceState, TargetState>
{
    // Same state type -> copy information and convert types
    // Actually: we try now to just copy it
    static inline void apply(SourceState const& source, TargetState& destination )
    {
        typename TargetState::ball_t state2_ball;
        typename TargetState::ball_t::position_descriptor state2_ball_position;

        // Copy ball
        boost::geometry::convert( soccer::get_position( soccer::ball( source )),
                                  state2_ball_position );
        soccer::set_position( state2_ball_position, state2_ball );
        soccer::set_ball( state2_ball, destination );

        // Copy time
        soccer::set_time( soccer::time( source ), destination );

        // Copy players
        typename SourceState::player_iterator_t pf, pl;
        for ( boost::tie( pf, pl ) = soccer::players( source ); pf != pl; pf++ )
        {
            typename TargetState::player_t state2_player;
            typename TargetState::player_t::position_descriptor state2_player_position;

            boost::geometry::convert( soccer::get_position( soccer::get_player( *pf, source )),
                                      state2_player_position );
            soccer::set_position( state2_player_position, state2_player );

            soccer::add_player( state2_player, destination );
        }

    }
};

template < typename TargetStateTag,
           typename SourceState,
           typename TargetState >
struct convert< soccer::static_state_tag,
                TargetStateTag,
                SourceState,
                TargetState >
{
    // Same state type -> copy information and convert types
    // Actually: we try now to just copy it
    static inline void apply( SourceState const& source, TargetState& target )
    {
        typename TargetState::ball_t target_state_ball;
        typename TargetState::ball_t::position_descriptor target_state_ball_position;

        // Copy ball
        boost::geometry::convert( soccer::get_position( soccer::ball( source )),
                                  target_state_ball_position );
        soccer::set_position( target_state_ball_position, target_state_ball );
        soccer::set_ball( target_state_ball, target );

        // Copy time
        soccer::set_time( soccer::time( source ), target );

        // Copy players
        typename SourceState::player_iterator_t pf, pl;
        for ( boost::tie( pf, pl ) = soccer::players( source ); pf != pl; pf++ )
        {
            typename TargetState::player_t target_state_player;
            typename TargetState::player_t::position_descriptor target_player_position;

            boost::geometry::convert( soccer::get_position( soccer::get_player( *pf, source )),
                                      target_player_position );
            soccer::set_position( target_player_position, target_state_player );

            soccer::add_player( target_state_player, target );
        }

    }
};


/*!
 * \brief convert a source static state to a target dynamic state
 */
template < typename SourceState,
           typename TargetState >
struct convert< soccer::dynamic_state_tag,
                soccer::dynamic_state_tag,
                SourceState,
                TargetState >
{
    // Same state type -> copy information and convert types
    // Actually: we try now to just copy it
    static inline void apply( SourceState const& source, TargetState& target )
    {
        typedef soccer::state_traits<SourceState> source_state_traits_t;

        typedef soccer::state_traits<TargetState> target_state_traits_t;
        typedef soccer::ball_traits< typename target_state_traits_t::ball_t > target_state_ball_traits_t;
        typedef soccer::player_traits< typename target_state_traits_t::player_t > target_state_player_traits_t;

        typename target_state_ball_traits_t::position_descriptor target_ball_position;
        typename target_state_ball_traits_t::velocity_descriptor target_ball_velocity;

        // Copy ball
        boost::geometry::convert( soccer::get_position( soccer::ball( source )),
                                  target_ball_position );
        boost::geometry::convert( soccer::velocity( soccer::ball( source )),
                                  target_ball_velocity );
        soccer::set_position( target_ball_position, target_state_ball );
        soccer::set_velocity( target_ball_velocity, target_state_ball );
        soccer::set_ball( target_state_ball, target );

        // Copy time
        soccer::set_time( soccer::time( source ), target  );

        // Copy players
        typename source_state_traits_t::player_iterator_t pf, pl;
        for ( boost::tie( pf, pl ) = soccer::players( source ); pf != pl; pf++ )
        {
            typename target_state_traits_t::player_t target_state_player;
            typename target_state_player_traits_t::number_descriptor target_player_number;
            typename target_state_player_traits_t::side_t target_player_side;
            typename target_state_player_traits_t::position_descriptor target_player_position;
            typename target_state_player_traits_t::velocity_descriptor target_player_velocity;
            typename target_state_player_traits_t::direction_descriptor target_player_body_direction;

            auto source_player = soccer::get_player( *pf, source );
            target_player_number = soccer::get_number( source_player );
            soccer::set_number( target_player_number,
                                target_state_player );
            target_player_side = soccer::get_side( source_player );
            soccer::set_side( target_player_side,
                              target_state_player );
            boost::geometry::convert( soccer::get_position( soccer::get_player( *pf, source )),
                                      target_player_position );
            soccer::set_position( target_player_position,
                                  target_state_player );
            boost::geometry::convert( soccer::velocity( soccer::get_player( *pf, source )),
                                      target_player_velocity );
            soccer::set_velocity( target_player_velocity,
                                  target_state_player );
            target_player_body_direction = soccer::body( source_player );
            soccer::set_body( target_player_body_direction,
                              target_state_player );

            soccer::add_player( target_state_player, target );
        }

    }
};

/*!
 * \brief convert a source dynamic state to a target static state
 */
template < typename SourceState,
           typename TargetState >
struct convert< soccer::dynamic_state_tag,
                soccer::static_state_tag,
                SourceState,
                TargetState >
{
    // Same state type -> copy information and convert types
    // Actually: we try now to just copy it
    static inline
    void apply(SourceState const& source, TargetState& destination )
    {
        typename TargetState::ball_t target_state_ball;
        typename TargetState::ball_t::position_descriptor state2_ball_position;

        // Copy ball
        boost::geometry::convert( soccer::get_position( soccer::ball( source )),
                                  state2_ball_position );
        soccer::set_position( state2_ball_position, target_state_ball );
        soccer::set_ball( target_state_ball, destination );

        // Copy time
        soccer::set_time( soccer::time( source ), destination );

        // Copy players
        typename SourceState::player_iterator_t pf, pl;
        for ( boost::tie( pf, pl ) = soccer::players( source ); pf != pl; pf++ )
        {
            typename TargetState::player_t state2_player;
            typename TargetState::player_t::position_descriptor state2_player_position;

            boost::geometry::convert( soccer::get_position( soccer::get_player( *pf, source )),
                                      state2_player_position );
            soccer::set_position( state2_player_position, state2_player );

            soccer::add_player( state2_player, destination );
        }

    }
};

}

}

/*!
\brief Converts one state to another state
\details The convert algorithm converts one state to another state. This only if it is possible and applicable.
\ingroup convert
\tparam SourceState \tparam_state
\tparam TargetState \tparam_state
\param state1 \param_state (source)
\param state2 \param_state (target)
*/
template <typename SourceState, typename TargetState>
inline void convert(SourceState const & source,
                    TargetState& target)
{
    BOOST_CONCEPT_CHECK(( soccer::StateConcept<SourceState> ));
    BOOST_CONCEPT_CHECK(( soccer::MutableStateConcept<TargetState> ));
    concept::check_concepts_and_equal_dimensions<Geometry1 const, Geometry2>();

    dispatch::convert
        <
            typename soccer::state_tag<SourceState>::type,
            typename soccer::state_tag<SourceState>::type,
            dimension<Geometry1>::type::value,
            Geometry1,
            Geometry2
        >::apply(source, target);
}


}

#endif // STATE_CONVERT_HPP
