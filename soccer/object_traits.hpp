#ifndef SOCCER_OBJECT_TRAITS_HPP
#define SOCCER_OBJECT_TRAITS_HPP

#include <soccer/core/position_type.hpp>
#include <soccer/core/velocity_type.hpp>

#include <soccer/core/tags.hpp>

namespace soccer {

template < typename StaticObject >
struct static_object_traits
{
    typedef typename position_type< StaticObject >::type position_type;
};

template < typename DynamicObject >
struct dynamic_object_traits
    : public static_object_traits< DynamicObject >
{
    typedef typename velocity_type< DynamicObject >::type velocity_type;
};

}

#endif // SOCCER_OBJECT_TRAITS_HPP
