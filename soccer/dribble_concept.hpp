#ifndef SOCCER_DRIBBLE_CONCEPT_HPP
#define SOCCER_DRIBBLE_CONCEPT_HPP

#include <soccer/dribble_traits.hpp>

#include <soccer/action_concepts.hpp>
#include <soccer/position_concepts.hpp>

namespace soccer {

BOOST_concept(DribbleConcept,(D))
    : public ActionConcept<D>
{
    typedef typename player_id_type< D >::type player_id_t;
    typedef typename duration_type< D >::type duration_t;
    typedef typename position_type< D >::type position_t;
    typedef typename speed_type< D >::type speed_t;
    typedef typename direction_type< D >::type direction_t;

    BOOST_CONCEPT_ASSERT(( PositionConcept<position_t> ));

    BOOST_CONCEPT_USAGE(DribbleConcept)
    {
        const_constraints( _dribble );
    }

private:
    void const_constraints( const D& const_dribble )
    {
        _pid = get_executor_id( const_dribble );
        _dur = get_duration( const_dribble );
        _pos = get_position( const_dribble );
        _speed = get_ball_first_speed( const_dribble );
    }


    D _dribble;
    player_id_t _pid;
    duration_t _dur;
    position_t _pos;
    speed_t _speed;
    direction_t _dir;
};


BOOST_concept(MutableDribbleConcept,(D))
    : public DribbleConcept<D>
{
    typedef typename player_id_type< D >::type player_id_t;
    typedef typename duration_type< D >::type duration_t;
    typedef typename position_type< D >::type position_t;
    typedef typename speed_type< D >::type speed_t;
    typedef typename direction_type< D >::type direction_t;

    BOOST_CONCEPT_USAGE(MutableDribbleConcept)
    {
        set_dribbler_id( _pid, _dribble );
        set_duration( _dur, _dribble );
        set_target_position( _pos, _dribble );
        set_ball_first_speed( _speed, _dribble );
    }

private:
    D _dribble;
    player_id_t _pid;
    duration_t _dur;
    position_t _pos;
    speed_t _speed;
    direction_t _dir;
};

}

#endif // SOCCER_DRIBBLE_CONCEPT_HPP
