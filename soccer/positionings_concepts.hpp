#ifndef SOCCER_POSITIONING_CONCEPTS_HPP
#define SOCCER_POSITIONING_CONCEPTS_HPP

#include <soccer/positionings_traits.hpp>


#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

/* namespace detail {
    BOOST_HAS_XXX_TRAIT(player_id_t);
} */

BOOST_concept(PositioningsConcept,(P))
{
    /*BOOST_STATIC_ASSERT_MSG(( detail::has_player_id_t<P>,
                              "Positioning concept must define a player_id_t type" )); */
    // typedef typename positioning_id_type< P >::type positioning_id_t;
    // typedef typename positioning_iterator_type< P >::type positioning_id_iterator;
	
    BOOST_CONCEPT_USAGE(PositioningsConcept)
	{
        // const_constraints( p );
	}

private:
    void const_constraints( const P& const_positionings )
    {
       //  positionings_iterators = get_positionings( const_positionings );
        // positioning_id = get_positioning( positioning_id, const_positionings );
    }
	
    /*P p;
    std::pair< positioning_id_iterator, positioning_id_iterator > positionings_iterators;
    positioning_id_t positioning_id;
    positionings_size_type num_pls; */
};

BOOST_concept(MutablePositioningsConcept,(P))
    : PositioningsConcept<P>
{
    // typedef typename positioning_id_type< P >::type positioning_id_t;
    // typedef typename positioning_iterator_type< P >::type positioning_id_iterator;

    BOOST_CONCEPT_USAGE(MutablePositioningsConcept)
    {
        // set_positioning( positioning_id, positioning_id, p );
        // swap_positioning( positioning_id, positioning_id, p );
    }

    // P p;
    // positioning_id_t positioning_id;
};

}
#endif // SOCCER_POSITIONING_CONCEPTS_HPP
