#ifndef SOCCER_STL_PAIR_AS_POINT
#define SOCCER_STL_PAIR_AS_POINT

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/core/coordinate_type.hpp>

#include <cstddef>

#include <boost/geometry.hpp>

#include <utility>
#include <tuple>

namespace boost { namespace geometry { namespace traits {

    // std::pair< T, T> point adaptor
    template < typename CoordinateType >
    struct tag< std::pair< CoordinateType, CoordinateType > > { typedef point_tag type; };

    template < typename CoordinateType >
    struct dimension< std::pair< CoordinateType, CoordinateType > > : boost::mpl::int_<2> {};

    template < typename CoordinateType >
    struct coordinate_type< std::pair< CoordinateType, CoordinateType > > { typedef CoordinateType type; };

    // TODO What if another kind of coordinate system is required
    template < typename CoordinateType >
    struct coordinate_system< std::pair< CoordinateType, CoordinateType > > { typedef boost::geometry::cs::cartesian type; };


    template <>
    template < typename CoordinateType >
    struct access<std::pair< CoordinateType, CoordinateType >, 0 >
    {
        static inline CoordinateType get(std::pair< CoordinateType, CoordinateType > const& point)
        {
            return point.first;
        }
        static inline void set(std::pair< CoordinateType, CoordinateType>& point,
                               CoordinateType const& value)
        {
            point.first = value;
        }
    };

    template <>
    template < typename CoordinateType >
    struct access<std::pair< CoordinateType, CoordinateType >, 1>
    {
        static inline CoordinateType get(std::pair< CoordinateType, CoordinateType> const& point)
        {
            return point.second;
        }
        static inline void set( std::pair< CoordinateType, CoordinateType >& point,
                                CoordinateType const& value)
        {
            point.second = value;
        }
    };

} // end namespace traits

template <>
template < typename CoordinateType >
struct tag< std::pair< CoordinateType, CoordinateType > >
{
    typedef point_tag type;
};

}} // end namespace boost::geometry


#include <soccer/register/object.hpp>
#include <soccer/core/tags.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

    template <>
    template < typename CoordinateType >
    struct tag < std::pair< CoordinateType, CoordinateType > > { typedef point_tag type; };
    // Register pair as a position
    // template < typename CoordinateType >
    template <>
    template < typename CoordinateType >
    struct coordinate_type< std::pair< CoordinateType, CoordinateType > > { typedef CoordinateType type; };

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_STL_PAIR_AS_PLAYER_ID
