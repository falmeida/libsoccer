#ifndef SOCCER_STL_PAIR_AS_PLAYER_ID
#define SOCCER_STL_PAIR_AS_PLAYER_ID

#include <soccer/defines.h>

#include <utility>

#include <soccer/core/access.hpp>
// #include <soccer/register/player_id.hpp>


SOCCER_TRAITS_NAMESPACE_BEGIN

// Types Registration
template < typename Number, typename TeamSide >
struct tag< std::pair< Number, TeamSide > > { typedef dynamic_ball_tag type; };

template < typename Number, typename TeamSide >
struct number_type< std::pair< Number, TeamSide > > { typedef Number type; };

template < typename Number, typename TeamSide >
struct side_type< std::pair< Number, TeamSide > > { typedef TeamSide type; };

//
// Access Registration
//

template < typename Number, typename TeamSide >
struct number_access< std::pair< Number, TeamSide > >
{
    static inline Number
    get( std::pair< Number, TeamSide > const& _pair ) { return _pair.first; }

    static inline void
    set( Number const& number, std::pair< Number, TeamSide >& _pair )
    {
        _pair.first = number;
    }
};

template < typename Number, typename TeamSide >
struct side_access< std::pair< Number, TeamSide > >
{
    static inline TeamSide
    get( std::pair< Number, TeamSide > const& _pair) { return _pair.second; }

    static inline void
    set( TeamSide const& team_side, std::pair< Number, TeamSide >& _pair)
    {
        _pair.second = team_side;
    }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_STL_PAIR_AS_PLAYER_ID
