#ifndef SOCCER_STATE_TRAITS_HPP
#define SOCCER_STATE_TRAITS_HPP

#include <soccer/core/player_id_type.hpp>
#include <soccer/core/player_type.hpp>
#include <soccer/core/player_iterator_type.hpp>
#include <soccer/core/time_type.hpp>
#include <soccer/core/playmode_type.hpp>
#include <soccer/core/score_type.hpp>
#include <soccer/core/team_type.hpp>

#include <soccer/core/tag.hpp>

namespace soccer {

template < typename State >
struct state_traits
{

    typedef typename player_id_type< State >::type player_id_type;
    typedef typename soccer::player_iterator_type< State >::type player_iterator_type;
    typedef typename player_type< State >::type player_type;
    typedef typename time_type< State >::type time_type;
    typedef typename playmode_type< State >::type playmode_type;
    typedef typename score_type< State >::type score_type;
    typedef typename tag< State >::type category;

};

} // end namespace soccer

#endif // SOCCER_STATE_TRAITS_HPP
