#ifndef SOCCER_PLAYER_TYPE_TRAITS_HPP
#define SOCCER_PLAYER_TYPE_TRAITS_HPP

namespace soccer {

template <typename PlayerType>
struct player_type_with_role_traits
{
    typedef PlayerType::role_descriptor role_descriptor;
};

template <typename PlayerType>
struct player_type_with_attraction
{
    typedef PlayerType::attraction_descriptor attraction_descriptor;
};

template <typename PlayerType>
struct player_type_with_position_constraints
{
    typedef PlayerType::coordinate_descriptor coordinate_descriptor;
};

}

#endif // SOCCER_PLAYER_TYPE_TRAITS_HPP
