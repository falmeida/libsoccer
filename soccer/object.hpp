#ifndef SOCCER_OBJECT_HPP
#define SOCCER_OBJECT_HPP

#include <soccer/object_traits.hpp>
#include <soccer/soccer_concepts.hpp>

#include <boost/type_traits.hpp>
#include <boost/concept_check.hpp>

namespace soccer {

template <typename Position>
struct static_object
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));

    static_object( ) { }

    static_object( Position position )
        : M_position( position )
    { }

    Position M_position;
};

template <typename Position,
          typename Uncertainty>
struct uncertain_static_object
    : public static_object<Position>
{
    // BOOST_CONCEPT_ASSERT(( UncertaintyConcept<Uncertainty> ));

    Uncertainty M_position_uncertainty;
};

template < typename Position,
           typename Velocity = Position >
struct dynamic_object
    : public static_object< Position >
{
    BOOST_CONCEPT_ASSERT(( VelocityConcept<Velocity> ));

    dynamic_object( )
        : static_object< Position >()
    { }

    dynamic_object( Position position )
        : static_object< Position >( position )
    {

    }

    dynamic_object( Position position, Velocity velocity )
        : static_object< Position >( position )
        , M_velocity( velocity )
    {

    }

    Velocity M_velocity;
};

template <typename Position,
          typename PositionUncertainty,
          typename Velocity = Position,
          typename VelocityUncertainty = PositionUncertainty>
struct uncertain_dynamic_object
    : public dynamic_object<Position, Velocity>
{
    // BOOST_CONCEPT_ASSERT(( UncertaintyConcept<PositionUncertainty> ));
    // BOOST_CONCEPT_ASSERT(( UncertaintyConcept<VelocityUncertainty> ));

    PositionUncertainty m_position_uncertainty;
    VelocityUncertainty m_velocity_uncertainty;
};

} // end namespace soccer


#include <soccer/register/object.hpp>
#include <soccer/core/tags.hpp>

NAMESPACE_SOCCER_TRAITS_BEGIN

//
// static_object TYPE REGISTRATION
//
template <>
template < typename Position >
struct tag< soccer::static_object< Position > > { typedef static_object_tag type; };

template <>
template < typename Position >
struct position_type< soccer::static_object< Position > > { typedef Position type; };

//
// static_object DATA ACCESS REGISTRATION
//
template <>
template < typename Position >
struct position_access< soccer::static_object< Position > >
{
    static inline Position const& get( soccer::static_object< Position > const& obj ) { return obj.M_position; }
    static inline void set( Position const& _pos, soccer::static_object< Position >& obj ) { obj.M_position = _pos; }
};

//
// dynamic_object TYPE REGISTRATION
//
template <>
template < typename Position >
struct tag< soccer::dynamic_object< Position > > { typedef dynamic_object_tag type; };

template <>
template < typename Position,
           typename Velocity >
struct velocity_type< soccer::dynamic_object< Position, Velocity > > { typedef Velocity type; };

//
// dynamic_object DATA ACCESS REGISTRATION
//
template <>
template < typename Position,
           typename Velocity >
struct velocity_access< soccer::dynamic_object< Position, Velocity > >
{
    static inline Velocity const& get( soccer::dynamic_object< Position, Velocity > const& obj ) { return obj.M_velocity; }
    static inline void set( Velocity const& _vel, soccer::dynamic_object< Position, Velocity> const& obj ) { obj.M_velocity = _vel; }
};


NAMESPACE_SOCCER_TRAITS_END

#endif // SOCCER_OBJECT_HPP
