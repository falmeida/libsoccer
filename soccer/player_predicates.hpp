#ifndef SOCCER_PLAYER_PREDICATES_HPP
#define SOCCER_PLAYER_PREDICATES_HPP

#include <soccer/player_concepts.hpp>

#include <soccer/geometry.hpp>

#include <functional>

namespace soccer {

    /////////////////////////////
    // Player Unary Predicates //
    /////////////////////////////

    template <typename Player>
    struct is_player_pos_before_x
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef typename coordinate_type< typename position_type< Player >::type >::type coordinate_t;

        coordinate_t M_coord;
        is_player_pos_before_x( const coordinate_t coord)
            : M_coord( coord )
        { }

        bool operator() ( const Player& player )
        {
            return get_x( get_position( player ) ) < M_coord;
        }
    };

    template <typename Player>
    struct is_player_pos_after_x
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef typename coordinate_type< typename position_type< Player >::type >::type coordinate_t;

        coordinate_t M_coord;
        is_player_pos_after_x( const coordinate_t coord)
            : M_coord( coord )
        { }

        bool operator() ( const Player& player )
        {
            return get_x( get_position( player ) ) > M_coord;
        }
    };

    template <typename Player>
    struct is_player_pos_before_y
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef typename coordinate_type< typename position_type< Player >::type >::type coordinate_t;

        coordinate_t M_coord;
        is_player_pos_before_y( const coordinate_t coord)
            : M_coord( coord )
        { }

        bool operator() ( const Player& player )
        {
            return get_y( get_position( player ) ) < M_coord;
        }
    };

    template <typename Player>
    struct is_player_pos_after_y
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef typename coordinate_type< typename position_type< Player >::type >::type coordinate_t;

        coordinate_t M_coord;
        is_player_pos_after_y( const coordinate_t coord)
            : M_coord( coord )
        { }

        bool operator() ( const Player& player )
        {
            return get_y( get_position( player ) ) > M_coord;
        }
    };

    template <typename Player>
    struct player_from_side_unary_predicate
        : public std::unary_function< Player, bool >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef typename side_type< Player >::type side_t;

	player_from_side_unary_predicate()
    {
        // TODO How is m_side initiated => Should be to unknown side
	}

        player_from_side_unary_predicate( const side_t side )
            : M_side( side )
        {

        }

        bool operator() ( const Player& player ) const
        {
            return get_side( player ) == M_side;
        }

        side_t M_side;
    };

    /*!
     \brief Determine if a given player has an unknown number
     */
    template <typename Player>
    struct is_player_number_unknown
        : public std::unary_function< Player, bool >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef player_traits<Player> player_traits_t;

        bool operator() ( const Player& player ) const
        {
            return get_number( player ) == player_traits_t::unknown_number();
        }
    };

    /*!
     \brief Determine if a given player has a known number
     */
    template <typename Player>
    struct is_player_number_known
        : public std::unary_function< Player, bool >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef player_traits<Player> player_traits_t;

        bool operator() ( const Player& player ) const
        {
            return number( player ) != player_traits_t::unknown_number();
        }
    };

    /*!
     \brief Determine if a given player has an unknown side
     */
    template <typename Player>
    struct is_player_side_unknown
        : public std::unary_function< Player, bool >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef player_traits<Player> player_traits_t;

        bool operator() ( const Player& player ) const
        {
            return get_side( player ) == player_traits_t::unknown_side();
        }
    };

    /*!
     \brief Determine if a given player has a known side
     */
    template <typename Player>
    struct is_player_side_known
        : public std::unary_function< Player, bool >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef player_traits<Player> player_traits_t;

        bool operator() ( const Player& player ) const
        {
            return side( player ) != player_traits_t::unknown_side();
        }
    };


    /*!
      \brief Check if player body is facing a given target position
      \struct is_player_body_facing_direction
      */
    template <typename Player>
    struct is_player_body_facing_direction
        : public std::unary_function< bool, Player >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef typename direction_type< Player >::type direction_t;
        typedef typename position_type< Player >::type position_t;

        const direction_t M_direction;
        const direction_t M_direction_thr;

        is_player_body_facing_direction( const direction_t direction,
                                         const direction_t direction_thr )
            : M_direction( direction )
            , M_direction_thr( direction_thr )
        {

        }

        bool operator() ( const Player& p )
        {
            const direction_t dir_diff = M_direction - get_body_direction( p );
            return dir_diff < M_direction_thr;
        }
    };

    /*!
      \brief Check if player body is facing a given target position
      \class IsPlayerBodyFacingPosition
      */
    template < typename Player >
    struct is_player_body_facing_position
        : public std::unary_function< bool, Player >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef player_traits< Player > player_traits_t;
        typedef typename direction_type< Player >::type direction_t;
        typedef typename position_type< Player >::type position_t;

        const position_t M_target_position;
        const direction_t M_direction_thr;

        is_player_body_facing_position( const position_t target_position,
                                        const direction_t direction_thr )
            : M_target_position( target_position )
            , M_direction_thr( direction_thr )
        {

        }

        bool operator() ( const Player& player )
        {
            direction_t required_body_direction = direction( get_position( player ), M_target_position );
            is_player_body_facing_direction<Player> pred( required_body_direction,
                                                          M_direction_thr );
            return pred( player );
        }
    };

    /*!
      \brief Check if player is at a given position
      \struct is_player_at_position
      */
    template < typename Player,
               typename Distance >
    struct is_player_at_position
        : public std::unary_function< bool, Player >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        typedef typename position_type< Player >::type position_descriptor;

        const position_descriptor M_target_position;
        const Distance M_distance_thr;

        is_player_at_position( const position_descriptor target_position,
                               const Distance distance_thr )
            : M_target_position( target_position )
            , M_distance_thr( distance_thr )
        {

        }

        bool operator() ( const Player& player )
        {
            const auto target_distance = distance( get_position( player ), M_target_position );

            return target_distance < M_distance_thr;
        }
    };


    //////////////////////////////
    // Player Binary Predicates //
    //////////////////////////////


    /*!
      \brief Check which player has the lowest x-coordinate
      \returns true if the first player x-coordinate is lower than the 2nd player or false otherwise
      */
    template <typename Player>
    struct player_with_lower_pos_x
        : std::binary_function< Player, Player, bool >
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        player_with_lower_pos_x( )
        {

        }

        bool operator()( const Player& p1,
                         const Player& p2 )
        {
            return get_x( get_position( p1 ) ) < get_x( get_position( p2 ) );
        }
    };

    /*!
      \brief Check which player has the highest x-coordinate
      \returns true if the first player x-coordinate is higher than the 2nd player or false otherwise
      */
    template <typename Player>
    struct player_with_higher_pos_x
        : std::binary_function< Player, Player, bool>
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        player_with_higher_pos_x( )
        {

        }

        bool operator()( const Player& p1,
                         const Player& p2 )
        {
            return get_x( get_position( p1 ) ) < get_x( get_position( p2 ) );
        }
    };

    /*!
      \brief Check which player has the lowest y-coordinate
      \returns true if the first player y-coordinate is lower than the 2nd player or false otherwise
      */
    template <typename Player>
    struct player_with_lower_pos_y
        : std::binary_function< Player, Player, bool>
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        player_with_lower_pos_y( )
        {

        }

        bool operator()( const Player& p1,
                         const Player& p2 )
        {
            return get_y( get_position( p1 ) ) < get_y( get_position( p2 ) );
        }
    };

    /*!
      \brief Check which player has the highest x-coordinate
      \returns true if the first player x-coordinate is higher than the 2nd player or false otherwise
      */
    template <typename Player>
    struct player_with_higher_pos_y
        : std::binary_function< Player, Player, bool>
    {
        BOOST_CONCEPT_ASSERT(( StaticPlayerConcept<Player> ));

        player_with_higher_pos_y( )
        {

        }

        bool operator()( const Player& p1,
                         const Player& p2 )
        {
            return get_y( get_position( p1 ) ) < get_y( get_position( p2 ) );
        }
    };

} // end namespace soccer

#endif // SOCCER_PLAYER_PREDICATES_HPP
