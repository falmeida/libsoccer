#ifndef SOCCER_POINT_GENERATOR_HPP
#define SOCCER_POINT_GENERATOR_HPP

#include <boost/geometry.hpp>
#include <boost/math/constants/constants.hpp>

#include <boost/type_traits.hpp>
#include <boost/functional.hpp>
#include <boost/concept_check.hpp>

#include <list>
#include <functional>

namespace soccer {

static const double deg2rad = boost::math::constants::pi<double>() / 180.0;


template <typename NumberType>
/*!
 * \brief Generate values starting from a value and incrementing step
 */
struct value_generator
    : public std::unary_function< void, NumberType > {

    NumberType M_start_value;
    NumberType M_step_value;
    NumberType M_curr_value;

    value_generator( NumberType start_value,
                     NumberType step_value)
        : M_start_value( start_value )
        , M_step_value( step_value )
        , M_curr_value( M_start_value )
    { }

    NumberType operator() ()
    {
        const auto val = M_start_value;
        M_curr_value += M_curr_value + M_step_value;
        return val;
    }
};


template < typename Value >
/*!
 * \brief Generate values starting from a value and incrementing step
 * \struct ranged value generator
 */
struct ranged_value_generator
    : public std::unary_function< void, Value > {

    Value M_start_value;
    Value M_step_value;
    std::size_t M_step;
    std::size_t M_num_steps;

    ranged_value_generator( Value start_value,
                            Value end_value,
                            std::size_t num_steps )
        : M_start_value( start_value )
        , M_num_steps( M_num_steps )
    {
        M_step_value = ( end_value - start_value ) / num_steps;
    }

    operator bool()
    {
        return M_step < M_num_steps;
    }

    Value operator() ()
    {
        const auto val = M_start_value + M_step * M_step_value;
        M_step++;
        return val;
    }
};


template <typename OutputPosition,
          typename InputPosition,
          typename DistanceUnaryGenerator,
          typename DistanceUnaryPredicate>
/*!
 * \brief Generate points within a given distance range and a static direction
 * \struct point_static_direction_generator
 */
struct point_static_direction_generator
    : public std::unary_function< void, OutputPosition >
{
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::Point<OutputPosition> ));
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::ConstPoint<InputPosition> ));

    typedef boost::unary_traits<DistanceUnaryGenerator> distance_generator_traits_t;
    typedef boost::unary_traits<DistanceUnaryPredicate> distance_predicate_traits_t;
    // BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< DistanceUnaryGenerator, Distance, Distance> ));
    // BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< DistanceStepUnaryFunctor, Distance, Distance> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename distance_generator_traits_t::result_type,
                                              typename distance_predicate_traits_t::argument_type>::value),
                            "Distance descriptor types mismatch in DistanceUnaryGenerator and DistanceUnaryPredicate");

    point_static_direction_generator( const InputPosition start_position,
                                      const DistanceUnaryGenerator dist_generator,
                                      const DistanceUnaryPredicate dist_predicate,
                                      const double direction = 0.0 ) // [-180,180[
        : M_start_position( start_position )
        , M_distance_generator( dist_generator )
        , M_distance_predicate( dist_predicate )
        , M_current_distance( M_distance_generator() )
        , M_direction_x( std::cos( direction * deg2rad ) )
        , M_direction_y( std::sin( direction * deg2rad ) )
    {

    }

    OutputPosition operator()( )
    {
        OutputPosition output_position;

        soccer::set_x( soccer::get_x( M_start_position ) +
                       M_current_distance * M_direction_x,
                       output_position );
        soccer::set_y( soccer::get_y( M_start_position ) +
                       M_current_distance * M_direction_y,
                       output_position );

        M_current_distance = M_distance_generator( );

        return output_position;
    }

    operator bool()
    {
        return M_distance_predicate( M_current_distance );
    }

    InputPosition M_start_position;
    DistanceUnaryGenerator M_distance_generator;
    DistanceUnaryPredicate M_distance_predicate;
    typename distance_generator_traits_t::result_type M_current_distance;
    double M_direction_x;
    double M_direction_y;
};


template <typename OutputPosition,
          typename InputPosition,
          typename Distance,
          typename DirectionStepUnaryFunctor>
/*!
 * \brief Generate points of a given a direction range and a static distance
 * \struct point_static_distance_generator
 */
struct point_static_distance_generator
    : public std::unary_function< void, OutputPosition >
{
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::Point<OutputPosition> ));
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::ConstPoint<InputPosition> ));
    BOOST_CONCEPT_ASSERT(( boost::UnaryFunctionConcept< DirectionStepUnaryFunctor, double, double> ));
    typedef typename boost::geometry::coordinate_type<InputPosition>::type input_coordinate_type;
    point_static_distance_generator( const InputPosition& start_position,
                                     const Distance distance,
                                     const DirectionStepUnaryFunctor direction_step_functor,
                                     const double direction_upper_bound = 360.0,
                                     const double direction_start = 0.0,
                                     const double direction_lower_bound = 0.0 )
        : M_start_position( start_position )
        , M_direction_lower_bound( direction_lower_bound  )
        , M_direction_upper_bound( direction_upper_bound )
        , M_direction_step_functor( direction_step_functor )
        , M_current_direction( direction_start )
        , M_distance( distance )
    {

    }

    OutputPosition operator()( )
    {
        OutputPosition output_position;
        soccer::set_x( soccer::get_x( M_start_position ) +
                       M_distance * std::cos( M_current_direction * deg2rad ),
                       output_position );
        soccer::set_y( soccer::get_y( M_start_position ) +
                       M_distance * std::sin( M_current_direction * deg2rad ),
                       output_position );
        M_current_direction = M_direction_step_functor( M_current_direction );

        return output_position;
    }

    operator bool()
    {
        return M_current_direction >= M_direction_lower_bound &&
               M_current_direction < M_direction_upper_bound;
    }

    InputPosition M_start_position;
    double M_direction_lower_bound;
    double M_direction_upper_bound;
    DirectionStepUnaryFunctor M_direction_step_functor;
    double M_current_direction;
    Distance M_distance;
};


/*!
 * \brief Generate points of a given a direction range and a static distance
 * \struct composite_point_generator
 * \tparam PointGenerator point generator model
 */
template < typename OutputPosition,
           typename PointGenerator >
struct composite_point_generator
    : public std::unary_function< void, typename PointGenerator::result_type >
{
    typedef std::list< boost::shared_ptr< PointGenerator > > point_generators_container_type;
    typedef typename point_generators_container_type::iterator point_generator_iterator;

    composite_point_generator()
        : M_current_point_generator()
    {

    }

    void add_generator( PointGenerator* point_generator )
    {
        M_point_generators.push_back( boost::shared_ptr<PointGenerator>( point_generator ) );
    }

    operator bool()
    {
        return M_current_point_generator != M_point_generators.end();
    }

    OutputPosition operator()()
    {
        if ( M_current_point_generator == point_generator_iterator() )
        {
            M_current_point_generator = M_point_generators.begin();
        }
        else if ( !(*M_current_point_generator) )
        {
            M_current_point_generator++;
        }

        if ( M_current_point_generator == M_point_generators.end() )
        {
            assert( false );
        }

        return (*M_current_point_generator)();
    }

    point_generators_container_type M_point_generators;
    point_generator_iterator M_current_point_generator;
};

}

#endif // SOCCER_POINT_GENERATOR_HPP
