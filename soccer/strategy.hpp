#ifndef SOCCER_STRATEGY_HPP
#define SOCCER_STRATEGY_HPP

#include <soccer/positionings_map.hpp>
#include <soccer/position_concepts.hpp>

#include <map>

namespace soccer {

// Strategy as TeamStrategy and PlayerStrategy
template < typename TacticId,
           typename FormationId,
           typename FluxId,
           typename PlayerId,
           typename RoleId,
           typename Position
           >
struct strategy
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));

    typedef std::map< RoleId, Position > PlayerPositionsMap;
    typedef std::map< PlayerId, RoleId > PlayerPositioningsMap;

    typedef boost::transform_iterator< std::function<RoleId(const typename PlayerPositionsMap::value_type)>,
                                       typename PlayerPositionsMap::const_iterator > role_iterator_t;

    /*!
     * \brief default constructor
     */
    strategy()
        /* : M_current_tactic_id( null_tactic_id() )
        , M_current_formation_id( null_formation_id() )
        , M_current_flux_id( null_flux_id() ) */
    {

    }

    /*!
     * \brief copy constructor
     * \param other strategy instance
     */
    strategy( const strategy& other )
        : M_current_tactic_id( other.M_current_tactic_id )
        , M_current_formation_id( other.M_current_formation_id )
        , M_current_flux_id( other.M_current_flux_id )
        , M_player_positions( other.M_player_positions )
        , M_player_positionings( other.M_player_positionings )
    {

    }

    /*!
      \brief assignable traits
    */
    strategy& operator=( const strategy& other )
    {
        if ( this != &other )
        {
            this->M_current_tactic_id = other.M_current_tactic_id;
            this->M_current_formation_id = other.M_current_formation_id;
            this->M_current_flux_id = other.M_current_flux_id;
            this->M_player_positions = other.M_player_positions;
            this->M_player_positionings = other.M_player_positionings;
        }
        return *this;
    }

    // member variables
    TacticId M_current_tactic_id;
    FormationId M_current_formation_id;
    FluxId M_current_flux_id;
    PlayerPositionsMap M_player_positions;
    PlayerPositioningsMap M_player_positionings;
};

template < typename TacticId,
           typename FormationId,
           typename FluxId,
           typename PlayerId,
           typename RoleId,
           typename Position >
strategy< TacticId,
          FormationId,
          FluxId,
          PlayerId,
          RoleId,
          Position >
make_strategy()
{
    return strategy< TacticId,
                     FormationId,
                     FluxId,
                     PlayerId,
                     RoleId,
                     Position >( );
}

} // end namespace soccer


#include <soccer/core/access.hpp>

// REGISTER STRATEGY
namespace soccer { namespace traits {

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct tag< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef strategy_tag type; };

//
// strategy TYPE REGISTRATION
//
template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct tactic_id_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef TacticId type; };

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct formation_id_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef FormationId type; };

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct flux_id_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef FluxId type; };

// template < >
// template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
// struct player_role_id_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef PlayerId type; };

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct player_id_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef PlayerId type; };

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct role_id_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef RoleId type; };

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct role_iterator_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > >
{ typedef typename strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position >::role_iterator_t type; };

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct position_type< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > > { typedef Position type; };

//
// strategy DATA ACCESS REGISTRATION
//

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct role_iterators_access< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > >
{
    typedef strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position >  strategy_t;

    static inline
    std::pair< typename strategy_t::role_iterator_t, typename strategy_t::role_iterator_t >
    get( strategy_t const& st )
    {
        static std::function< RoleId( const typename strategy_t::PlayerPositionsMap::value_type )>
                       map_key_getter = [](const typename strategy_t::PlayerPositionsMap::value_type& v){ return v.first; };
        return std::make_pair( boost::make_transform_iterator( st.M_player_positions.begin(), map_key_getter ),
                               boost::make_transform_iterator( st.M_player_positions.end(), map_key_getter ) );
    }
};

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct tactic_id_access< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > >
{
    typedef strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position >  strategy_t;

    static inline
    TacticId const& get( strategy_t const& st ) { return st.M_current_tactic_id; }

    static inline
    void set( TacticId const& _tactic_id, strategy_t& st ) { st.M_current_tactic_id = _tactic_id; }
};

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct formation_id_access< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > >
{
    typedef strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position >  strategy_t;

    static inline
    FormationId const& get( strategy_t const& st ) { return st.M_current_formation_id; }

    static inline
    void set( FormationId const& _formation_id, strategy_t& st ) { st.M_current_formation_id = _formation_id; }
};

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct flux_id_access< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > >
{
    typedef strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position >  strategy_t;

    static inline
    FluxId const& get( strategy_t const& st ) { return st.M_current_flux_id; }

    static inline
    void set( FluxId const& _flux_id, strategy_t& st ) { st.M_current_flux_id = _flux_id; }
};

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct position_access< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > >
{
    typedef strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position >  strategy_t;

    static inline
    Position const& get( RoleId const& pid, strategy_t const& st ) { return st.M_player_positions.at(pid); }

    static inline
    void set( RoleId const& _pid, Position const& _pos,  strategy_t& st ) { st.M_player_positions[_pid] = _pos; }
};

template < >
template < typename TacticId, typename FormationId, typename FluxId, typename PlayerId, typename RoleId, typename Position >
struct positioning_access< strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position > >
{
    typedef strategy< TacticId, FormationId, FluxId, PlayerId, RoleId, Position >  strategy_t;

    static inline
    RoleId const& get( PlayerId const& pid, strategy_t const& st ) { return st.M_player_positionings.at(pid); }

    static inline
    void set( PlayerId const& _pid, RoleId const& _role_id,  strategy_t& st ) { st.M_player_positionings[_pid] = _role_id; }
};


}} // end namespace soccer::traits

#endif // SOCCER_STRATEGY_HPP
