#ifndef SOCCER_PLAYER_CONCEPTS_HPP
#define SOCCER_PLAYER_CONCEPTS_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import helper traits classes
#include <soccer/traits/player_traits.hpp>

//! Import aditional concepts to be checked
#include <soccer/concepts/object_concepts.hpp>
#include <soccer/concepts/soccer_concepts.hpp>

//! Import type deduction helpers classes
#include <soccer/core/number_type.hpp>
#include <soccer/core/side_type.hpp>
#include <soccer/core/position_type.hpp>
#include <soccer/core/velocity_type.hpp>
#include <soccer/core/direction_type.hpp>

#include <soccer/core/tags.hpp>

#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(PlayerIdConcept,(P))
{
    /* BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< player_id_tag, typename tag< P >::type >::value ),
                            "Type category mismatch"); */

    typedef typename number_type< P >::type number_t;
    typedef typename side_type< P >::type side_t;

    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructible< side_t > ));
    BOOST_CONCEPT_ASSERT(( boost::CopyConstructible< number_t > ));
    BOOST_CONCEPT_ASSERT(( boost::EqualityComparableConcept< side_t > ));
    BOOST_CONCEPT_ASSERT(( boost::AssignableConcept< side_t > ));

    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructible< number_t > ));
    BOOST_CONCEPT_ASSERT(( boost::CopyConstructible< number_t > ));
    BOOST_CONCEPT_ASSERT(( boost::EqualityComparableConcept< number_t > ));
    BOOST_CONCEPT_ASSERT(( boost::AssignableConcept< number_t > ));

    BOOST_CONCEPT_USAGE(PlayerIdConcept)
    {
        const_constraints( _player );
    }
private:
    void const_constraints( const P& const_player )
    {
        _side = get_side( const_player );
        _num = get_number( const_player );
    }

    P _player;
    side_t _side;
    number_t _num;
};

BOOST_concept(MutablePlayerIdConcept,(P))
    : public PlayerIdConcept<P>
{
     typedef typename number_type< P >::type number_t;
     typedef typename side_type< P >::type side_t;

    BOOST_CONCEPT_USAGE(MutablePlayerIdConcept)
    {
        set_side( _side, _player );
        set_number( _side, _player );
    }

private:

    P _player;
    side_t _side;
    number_t _num;
};

BOOST_concept(StaticPlayerConcept,(P))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< player_tag, typename tag< P >::type >::value ),
                            "Type category mismatch");

    BOOST_CONCEPT_ASSERT(( PlayerIdConcept< P > ));
    BOOST_CONCEPT_ASSERT(( StaticObjectConcept< P > ));

    BOOST_CONCEPT_ASSERT(( boost::CopyConstructibleConcept< P > ));
    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructibleConcept< P > ));
    BOOST_CONCEPT_ASSERT(( boost::AssignableConcept< P > ));

    BOOST_CONCEPT_USAGE(StaticPlayerConcept)
    {

    }

};

BOOST_concept(MutableStaticPlayerConcept,(P))
    : StaticPlayerConcept<P>
{

    BOOST_CONCEPT_ASSERT(( MutableStaticObjectConcept<P> ));
    BOOST_CONCEPT_ASSERT(( MutablePlayerIdConcept<P> ));

    BOOST_CONCEPT_USAGE(MutableStaticPlayerConcept)
    {

    }

};


BOOST_concept(DynamicPlayerConcept,(P))
    : public StaticPlayerConcept<P>
{
    BOOST_CONCEPT_ASSERT(( MobileObjectConcept< P > ));

    BOOST_CONCEPT_USAGE(DynamicPlayerConcept)
    {

    }


};


BOOST_concept(MutableDynamicPlayerConcept,(P))
    : public MutableStaticPlayerConcept<P>
{
    typedef typename velocity_type< P >::type velocity_t;
    typedef typename direction_type< P >::type direction_t;

    BOOST_CONCEPT_ASSERT(( MutableMobileObjectConcept<P> ));

    BOOST_CONCEPT_USAGE(MutableDynamicPlayerConcept)
    {
        set_player_body_direction( dir, p );
        set_player_face_direction( dir, p );
    }

private:
    P p;
    direction_t dir;
};


} // end namespace soccer

#endif // SOCCER_PLAYER_CONCEPTS_HPP
