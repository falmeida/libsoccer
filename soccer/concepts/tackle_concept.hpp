#ifndef SOCCER_TACKLE_CONCEPT_HPP
#define SOCCER_TACKLE_CONCEPT_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import helper traits classes
#include <soccer/traits/tackle_traits.hpp>

//! Import relevant concepts to be checked
#include <soccer/concepts/action_concepts.hpp>
#include <soccer/concepts/position_concepts.hpp>

#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>
#include <boost/type_traits.hpp>


SOCCER_NAMESPACE_BEGIN


BOOST_concept(TackleConcept,(T))
    : public ActionConcept<T>
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< tackle_tag, typename tag< T >::type >::value ),
                             "Type is not categorized with tackle_tag");
    typedef typename position_type< T >::type position_t;
    typedef bool foul_t;

    BOOST_CONCEPT_USAGE(TackleConcept)
    {
        const_constraints( t );
    }

private:
    void const_constraints( const T& const_tackle )
    {
        pos = get_position( const_tackle );
        // fd = get_tackle_foul( const_tackle );
    }

    T t;
    position_t pos;
    foul_t fd;

};


BOOST_concept(MutableTackleConcept,(T))
    : public TackleConcept<T>
{
    typedef typename position_type< T >::type position_t;
    typedef bool foul_t;


    BOOST_CONCEPT_USAGE(MutableTackleConcept)
    {
        set_position( pos, t );
        // set_tackle_foul( fd, t );
    }

    T t;
    position_t pos;
    foul_t fd;
};


SOCCER_NAMESPACE_END

#endif // SOCCER_TACKLE_CONCEPT_HPP
