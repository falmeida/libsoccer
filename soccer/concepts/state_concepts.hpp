#ifndef SOCCER_STATE_CONCEPTS_HPP
#define SOCCER_STATE_CONCEPTS_HPP

//! Useful define macros
#include <soccer/defines.h>

#include <soccer/core/access.hpp>

//! Include relevant soccer concepts
#include <soccer/concepts/player_concepts.hpp>
#include <soccer/concepts/ball_concepts.hpp>
#include <soccer/concepts/playmode_concepts.hpp>

#include <soccer/traits/state_traits.hpp>

#include <boost/type_traits.hpp>

#include <boost/concept/assert.hpp>
#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

SOCCER_NAMESPACE_BEGIN

BOOST_concept(StateConcept,(State))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< state_tag, typename tag< State >::type >::value ),
                            "Type category mismatch");

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename ball_type< State >::type ball_t;
    typedef typename playmode_type< State >::type playmode_t;
    typedef typename time_type< State >::type time_t;
    typedef typename score_type< State >::type score_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename team_type< State >::type team_t;
    // typedef typename state_traits_t::players_size_type players_size_type;


    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructibleConcept< State > ));
    // BOOST_CONCEPT_ASSERT(( boost::CopyConstructibleConcept< State > ));
    // HACK Commented out because of rcsc::WorldModel
    // BOOST_CONCEPT_ASSERT(( boost::AssignableConcept< State > ));

    BOOST_CONCEPT_ASSERT(( StaticBallConcept< ball_t > ));
    // BOOST_CONCEPT_ASSERT(( StaticPlayerConcept< player_t > ));
    BOOST_CONCEPT_ASSERT(( PlayModeConcept< playmode_t > ));
    // BOOST_CONCEPT_ASSERT(( TimeConcept< time_t > ));

    BOOST_STATIC_ASSERT_MSG(( boost::is_convertible< typename std::iterator_traits< player_iterator_t >::value_type,
                                                     player_id_t >::value),
                            "Player id types do not match");

    BOOST_CONCEPT_USAGE(StateConcept)
    {

        // boost::function_requires< boost::CopyConstructibleConcept< State > >();
        // boost::function_requires< boost::AssignableConcept< State > >();

        // Types check

        const_constraints( state );
    }

private:
    virtual void const_constraints( State const& const_state )
    {
        _time = get_time( const_state );
        _playmode = get_playmode( const_state );
        _player_iterators = get_players( const_state );
        _ball = get_ball( const_state );
        _player = get_player( _pid, const_state );
        _score = get_left_score( const_state );
        _score = get_right_score( const_state );
        _team = get_left_team( const_state );
        _team = get_right_team( const_state );

        //_num_players = numPlayers( const_state );
        //_num_left_players = num_left_players( const_state );
        //_num_right_players = num_right_players( const_state );
        //assert( _num_players == ( _num_left_players + _num_right_players ) );

        // assert( soccer::get_left_side< side_t >() != soccer::get_right_side< side_t >() );
    }

    State state;
    ball_t _ball;
    player_id_t _pid;
    player_t _player;
    std::pair< player_iterator_t, player_iterator_t > _player_iterators;
    time_t _time;
    score_t _score;
    team_t _team;
    playmode_t _playmode;
};


BOOST_concept(StateWithTeamAffiliationConcept,(State))
    : public StateConcept< State >
{
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;
    typedef typename score_type< State >::type score_t;
    typedef typename team_type< State >::type team_t;
    typedef typename teammate_iterator_type< State >::type teammate_iterator_t;

    BOOST_CONCEPT_USAGE(StateWithTeamAffiliationConcept)
    {
        const_constraints( state );
    }

private:
    virtual void const_constraints( State const& const_state )
    {
        _side = our_side( const_state );
        _side = their_side( const_state );
        _teammate_iterators = our_players( const_state );
        _teammate_iterators = their_players( const_state );
        _score = get_our_score( const_state );
        _score = get_their_score( const_state );
        _team = get_our_team( const_state );
        _team = get_their_team( const_state );
    }

    State state;
    side_t _side;
    std::pair< teammate_iterator_t, teammate_iterator_t > _teammate_iterators;
    score_t _score;
    team_t _team;
};


BOOST_concept(MutableStateConcept,(S))
    : public StateConcept<S>
{
    typedef StateConcept< S > base_type;

    BOOST_CONCEPT_USAGE(MutableStateConcept)
    {
        set_player( _pid, _player, _state );
        set_ball( _ball, _state );
        set_time( _time, _state );
        set_left_team( _team, _state );
        set_right_team( _team, _state );
        set_left_score( _score, _state );
        set_right_score( _score, _state );
    }

    S _state;
    typename base_type::time_t _time;
    typename base_type::player_id_t _pid;
    typename base_type::player_t _player;
    typename base_type::ball_t _ball;
    typename base_type::playmode_t _pmode;
    typename base_type::score_t _score;
    typename base_type::team_t _team;
};


SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_CONCEPTS_HPP
