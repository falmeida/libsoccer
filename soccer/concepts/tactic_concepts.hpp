#ifndef SOCCER_TACTIC_CONCEPTS_HPP
#define SOCCER_TACTIC_CONCEPTS_HPP

#include <soccer/tactic_traits.hpp>

#include <soccer/core/value_type.hpp>
#include <soccer/core/key_type.hpp>
#include <soccer/core/tags.hpp>


#include <boost/concept/assert.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(TacticConcept,(T))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< tactic_tag, typename tag< T >::type >::value ),
                            "Type category mismatch");

    typedef typename value_type< T >::type value_type;
    typedef typename key_type< T >::type key_type;

    BOOST_CONCEPT_USAGE(TacticConcept)
    {
        const_constraints( _tactic );
    }

private:

    void const_constraints( const T& const_tactic)
    {
        _weight = get_tactic_weight( _key, const_tactic );
    }

    T _tactic;
    value_type _weight;
    key_type _key;
};

BOOST_concept(MutableTacticConcept,(T))
    : public TacticConcept<T>
{
     typedef typename value_type< T >::type value_type;
     typedef typename key_type< T >::type key_type;

    BOOST_CONCEPT_USAGE(MutableTacticConcept)
    {
        set_tactic_weight( _key, _value, _tactic );
    }

    T _tactic;
    value_type _value;
    key_type _key;
};

}

#endif // SOCCER_TACTIC_CONCEPTS_HPP
