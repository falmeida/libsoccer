#ifndef SOCCER_ACTION_CHAIN_CONCEPTS_HPP
#define SOCCER_ACTION_CHAIN_CONCEPTS_HPP

#include <soccer/action_chain_traits.hpp>

#include <boost/concept/detail/concept_def.hpp>
#include <boost/concept_check.hpp>

namespace soccer {

BOOST_concept(ActionStatePairConcept,(ActionStatePair))
{
    typedef action_state_pair_traits<ActionStatePair> action_state_pair_traits_t;

    BOOST_CONCEPT_USAGE(ActionStatePairConcept)
    {
        const_constraints( asp );
    }

private:
    void const_constraints( const ActionStatePair& const_asp )
    {
        _state = state( asp );
        _action = action( action );
    }

    ActionStatePair asp;
    typename action_state_pair_traits_t::action_type _action;
    typename action_state_pair_traits_t::state_type _state;
};

BOOST_concept(MutableActionStatePairConcept,(ActionStatePair))
    : public ActionStatePairConcept<ActionStatePair>
{
    typedef action_state_pair_traits<ActionStatePair> action_state_pair_traits_t;

    BOOST_CONCEPT_USAGE(MutableActionStatePairConcept)
    {
        ActionStatePair asp( _action, _state );
    }

    typename action_state_pair_traits_t::action_type _action;
    typename action_state_pair_traits_t::state_type _state;
};

BOOST_concept(ActionChainConcept,(ActionChain))
{


    BOOST_CONCEPT_USAGE(ActionChainConcept)
    {
        const_constraints( ac );
    }

private:
    void const_constraints( const ActionChain& const_ac )
    {
        /* first_action( const_ac );
        first_state( const_ac );
        last_action( const_ac );
        last_state( const_ac );
        actions( const_ac );
        states( const_ac ); */
    }

    ActionChain ac;

};

BOOST_concept(MutableActionChainConcept,(ActionChain))
    : public ActionChainConcept<ActionChain>
{
    typedef action_chain_traits<ActionChain> action_chain_traits_t;

    BOOST_CONCEPT_USAGE(MutableActionChainConcept)
    {
        push( action_chain, action, state );
    }

    ActionChain action_chain;
    typename action_chain_traits_t::action_type action;
    typename action_chain_traits_t::state_type _state;
};



}

#endif // SOCCER_ACTION_CHAIN_CONCEPTS_HPP
