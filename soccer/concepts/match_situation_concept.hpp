#ifndef SOCCER_MATCH_SITUATION_CONCEPT_HPP
#define SOCCER_MATCH_SITUATION_CONCEPT_HPP

#include <soccer/core/access.hpp>

#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(MatchSituationConcept,(MS))
{
    /* BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< playmode_tag, typename tag< MS >::type >::value ),
                            "Type category mismatch"); */

    BOOST_CONCEPT_USAGE( MatchSituationConcept )
    {
        const_constraints( _ms );
    }

private:
    void const_constraints( MS const& const_ms )
    {

        // is_undefined( const_ms );
        is_before_kick_off( const_ms );
        is_time_over( const_ms );
        is_goal( const_ms );

        is_kick_off( const_ms );

        is_play_on( const_ms );
        is_throw_in( const_ms );
        is_goal_kick( const_ms );
        is_goalie_catch( const_ms );
        is_corner_kick( const_ms );
        is_free_kick( const_ms );
        is_indirect_free_kick( const_ms );
        is_offside( const_ms );

        is_penalty_setup( const_ms );
        is_penalty_ready( const_ms );
    }

    MS _ms;
};


BOOST_concept(MutableMatchSituationConcept,(MS))
    : public MatchSituationConcept<MS>
{

    BOOST_CONCEPT_USAGE(MutableMatchSituationConcept)
    {
        set_undefined( _ms );
        set_kick_off( _ms );
        set_before_kick_off( _ms );
        set_time_over( _ms );
        set_play_on( _ms );
        set_throw_in( _ms );
        set_goal_kick( _ms );
        set_goalie_catch( _ms );
        set_corner_kick( _ms );
        set_free_kick( _ms );
        set_indirect_free_kick( _ms );
        set_goal( _ms );
        set_offside( _ms );

        set_penalty_setup( _ms );
        set_penalty_ready( _ms );
    }

    MS _ms;
};

} // end namespace soccer

#endif // SOCCER_MATCH_SITUATION_CONCEPT_HPP
