#ifndef SOCCER_MOVE_CONCEPT_HPP
#define SOCCER_MOVE_CONCEPT_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import helper traits classes
#include <soccer/traits/move_traits.hpp>

//! Import relevant concepts to be checked
#include <soccer/concepts/action_concepts.hpp>

#include <boost/type_traits.hpp>


SOCCER_NAMESPACE_BEGIN

BOOST_concept(MoveConcept,(M))
    : public ActionConcept<M>
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< move_tag, typename tag< M >::type >::value),
                              "Type is not categorized with the expected category tag");
    typedef typename position_type< M >::type position_t;
    typedef typename speed_type< M >::type speed_t;

    BOOST_CONCEPT_ASSERT(( PositionConcept<position_t> ));

    BOOST_CONCEPT_USAGE(MoveConcept)
    {
        const_constraints( _move );
    }

private:
    void const_constraints ( const M& const_move )
    {
        _pos = get_position( const_move );
        _speed = get_first_speed( const_move );
    }

    M _move;
    position_t _pos;
    speed_t _speed;
};

BOOST_concept(MutableMoveConcept,(M))
    : public MoveConcept<M>
{
    typedef typename position_type< M >::type position_t;
    typedef typename speed_type< M >::type speed_t;

    BOOST_CONCEPT_USAGE(MutableMoveConcept)
    {
        set_position( _pos, _move );
        set_first_speed( _speed, _move );
    }

private:
    M _move;
    position_t _pos;
    speed_t _speed;
};


SOCCER_NAMESPACE_END

#endif // SOCCER_MOVE_CONCEPT_HPP
