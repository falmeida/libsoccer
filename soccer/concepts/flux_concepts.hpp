#ifndef SOCCER_FLUX_CONCEPTS_HPP
#define SOCCER_FLUX_CONCEPTS_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Include relevant trais classes
#include <soccer/traits/flux_traits.hpp>

#include <soccer/core/flux_value_type.hpp>
#include <soccer/core/matrix_size_type.hpp>

#include <soccer/core/tags.hpp>

//! Include additional concepts to be checked
#include <soccer/concepts/soccer_concepts.hpp>
#include <soccer/concepts/position_concepts.hpp>
#include <soccer/concepts/triangulation_concept.hpp>

SOCCER_NAMESPACE_BEGIN

BOOST_concept(FluxConcept,(F))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< flux_tag, typename tag< F >::type >::value ),
                            "Type category mismatch");

    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructibleConcept<F> ));

    BOOST_CONCEPT_USAGE(FluxConcept)
    {

    }

private:
    void const_constraints( const F& const_flux )
    {

    }

    F flux;
};


BOOST_concept(FluxMatrixConcept,(F))
    : public FluxConcept<F>
{
     BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< flux_matrix_tag, typename tag< F >::type >::value ),
                             "Type category mismatch");

     typedef typename flux_value_type< F >::type value_t;
     typedef std::size_t matrix_index_t;

     BOOST_CONCEPT_USAGE(FluxMatrixConcept)
     {
        const_constraints( f );
     }

private:
    void const_constraints( const F& const_flux )
    {
        sz = length( const_flux );
        sz = width( const_flux );
        val = value( sz, sz, const_flux );
    }

    F f;
    matrix_index_t sz;
    value_t val;

};

BOOST_concept(FluxTriangulationConcept,(F))
    : public FluxConcept<F>
{
     BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< flux_triangulation_tag, typename tag< F >::type >::value ),
                             "Type category mismatch");

     typedef typename triangulation_type< F >::type triangulation_t;

     BOOST_CONCEPT_ASSERT(( TriangulationConcept< triangulation_t > ));

     typedef typename position_type< F >::type position_t;
     typedef typename flux_value_type< F >::type value_t;
     typedef typename vertex_id_type< F >::type vertex_id_t;
     typedef typename vertex_type< F >::type vertex_t;
     typedef typename vertex_iterator_type< F >::type vertex_iterator_t;
     typedef typename face_type< F >::type face_t;
     typedef typename face_id_type< F >::type face_id_t;
     typedef typename vertex_circulator_type< F >::type vertex_circulator_t;
     typedef typename face_iterator_type< F >::type face_iterator_t;

    // BOOST_CONCEPT_ASSERT(( PositionConcept<position_t> ));

    /*BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<vertex_iterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< vertex_iterator >::type,
                                              vertex_t>::value ),
                            "Value type of vertex iterator does not match the vertex descriptor type");

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<vertex_circulator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< vertex_circulator >::type,
                                              vertex_t>::value ),
                            "Value type of triangle vertex iterator does not match the vertex descriptor type");

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<face_iterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_convertible< typename boost::iterator_value< face_iterator >::type,
                              face_t>::value ),
                            "Value type of triangle iterator does not match the triangle descriptor type"); */

    // Assert that each type concept has the correct traits

    BOOST_CONCEPT_USAGE(FluxTriangulationConcept)
    {
        const_constraints( f );
    }

private:
    void const_constraints( const F& const_flux )
    {
        get_triangulation( const_flux );
        get_value( _vid, const_flux );
        vd = get_vertex( _vid, const_flux );
        _vid = get_infinite_vertex( const_flux );
        vertices_iterators = get_vertices( const_flux );
        pos = get_position( vd, const_flux );
        val = get_flux( vd, const_flux );
        // get_face( _fid, const_flux );
        face_iterators = get_faces( const_flux );
        vertex_circulators = get_incident_vertices( _vid, const_flux );
    }

    F f;
    vertex_id_t _vid;
    value_t val;
    position_t pos;
    vertex_t vd;
    std::pair< vertex_iterator_t,
               vertex_iterator_t> vertices_iterators;

    face_id_t _fid;
    std::pair< face_iterator_t,
               face_iterator_t> face_iterators;
    std::pair< vertex_circulator_t,
               vertex_circulator_t> vertex_circulators;
};


BOOST_concept(MutableFluxMatrixConcept,(F))
    : public FluxMatrixConcept<F>
{
     BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< flux_matrix_tag, typename tag< F >::type >::value ),
                             "Type category mismatch");

    typedef std::size_t grid_size_t;
    typedef typename flux_value_type< F >::type value_t;

    BOOST_CONCEPT_USAGE(MutableFluxMatrixConcept)
    {
        F f( sz, sz );
        set_min_value( val, fm );
        set_max_value( val, fm );
        set_length( val, fm );
        set_width( val, fm );
        set_value( sz, sz, val, fm );
    }

private:

    F fm;
    grid_size_t sz;
    value_t val;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_FLUX_CONCEPTS_HPP
