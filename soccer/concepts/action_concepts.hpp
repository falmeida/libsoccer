#ifndef SOCCER_ACTION_CONCEPTS_HPP
#define SOCCER_ACTION_CONCEPTS_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/traits/action_traits.hpp>

//! Import tags used in the soccer library
#include <soccer/core/tags.hpp>

#include <boost/concept/assert.hpp>
#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>


SOCCER_NAMESPACE_BEGIN

BOOST_concept(ActionConcept,(Action))
{
    typedef typename player_id_type< Action >::type player_id_t;
    typedef typename duration_type< Action >::type duration_t;

    BOOST_CONCEPT_ASSERT(( boost::EqualityComparable<player_id_t> ));
    BOOST_CONCEPT_ASSERT(( boost::ComparableConcept<duration_t> ));

    BOOST_CONCEPT_USAGE(ActionConcept)
    {
        const_constraints( _action );
    }

private:
    void const_constraints( Action& const_action )
    {
        _duration = get_duration( const_action );
        _executor = get_executor_id( const_action );
    }

    Action _action;
    player_id_t _executor;
    duration_t _duration;
};

BOOST_concept(ActionWithBallConcept,(Action))
{
    typedef typename velocity_type< Action >::type velocity_t;

    BOOST_CONCEPT_USAGE(ActionWithBallConcept)
    {
        const_constraints( _action );
    }

private:
    void const_constraints( Action& const_action )
    {
        _vel = first_ball_velocity( const_action );
        num_kicks( const_action );
    }

    Action _action;
    velocity_t _vel;
    size_t _num_kicks;
};


BOOST_concept(MutableActionConcept,(Action))
    : public ActionConcept<Action>
{
    typedef typename player_id_type< Action >::type player_id_t;
    typedef typename duration_type< Action >::type duration_t;

    BOOST_CONCEPT_USAGE(MutableActionConcept)
    {
        set_duration( _dur, _action );
        set_executor_id( _executor_id, _action );
    }

    Action _action;
    duration_t _dur;
    player_id_t _executor_id;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_ACTION_CONCEPTS_HPP
