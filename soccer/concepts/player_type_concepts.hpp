#ifndef SOCCER_PLAYER_TYPE_CONCEPTS_HPP
#define SOCCER_PLAYER_TYPE_CONCEPTS_HPP

#include <soccer/player_type_traits.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer
{

BOOST_CONCEPT(PlayerTypeWithRole,(PTR))
{
    typedef player_type_with_role_traits<PTR> base_type;

    BOOST_CONCEPT_USAGE(PlayerTypeWithRole)
    {
        const_constraints( ptr );
    }

private:
    void const_constraints( const PTR& const_ptr )
    {
        role = role( const_ptr );
    }

    PTR ptr;
    typename base_type::role_descriptor role;
};

BOOST_CONCEPT(MutablePlayerTypeWithRole,(PTR))
    : PlayerTypeWithRoleConcept<PTR>
{
    typedef player_type_with_role_traits<PTR> base_type;

    BOOST_CONCEPT_USAGE(MutablePlayerTypeWithRole)
    {
        set_role( role, ptr );
    }

private:
    PTR ptr;
    typename base_type::role_descriptor role;
}

BOOST_CONCEPT(PlayerTypeWithAttraction,(PTA))
{
    typedef player_type_with_attraction_traits<PTA> base_type;

    BOOST_CONCEPT_USAGE(PlayerTypeWithAttraction)
    {
        const_constraints( pta );
    }

private:
    void const_constraints( const PTA& const_pta )
    {
        attraction = attraction_attack( const_pta );
        attraction = attraction_defense( const_pta );
        attraction = attraction_x( const_pta );
        attraction = attraction_y( const_pta );
    }

    PTA pta;
    typename base_type::attraction_descriptor attraction;
};

BOOST_CONCEPT(MutablePlayerTypeWithAttraction,(PTA))
    : PlayerTypeWithAttraction<PTA>
{
    typedef player_type_with_attraction_traits<PTA> base_type;

    BOOST_CONCEPT_USAGE(MutablePlayerTypeWithAttraction)
    {
        set_attraction_attack( attraction, pta );
        set_attraction_defense( attraction, pta );
        set_attraction_x( attraction, pta );
        set_attraction_y( attraction, pta );
    }

private:
    PTA pta;
    typename base_type::attraction_descriptor attraction;
}


BOOST_CONCEPT(PlayerTypeWithPositionConstraints,(PTPC))
{
    typedef player_type_with_position_constraints_traits<PTPC> base_type;

    BOOST_CONCEPT_USAGE(PlayerTypeWithPositionConstraints)
    {
        const_constraints( ptpc );
    }

private:
    void const_constraints( const PTPC& const_ptpc )
    {
        coord = position_x_min( const_ptpc );
        coord = position_y_min( const_ptpc );
        coord = position_x_max( const_ptpc );
        coord = position_y_max( const_ptpc );
    }

    PTPC ptpc;
    typename base_type::coordinate_descriptor coord;
};

BOOST_CONCEPT(MutablePlayerTypeWithPositionConstraints,(PTPC))
    : layerTypeWithPositionConstraints<PTPC>
{
    typedef player_type_with_position_constraints_traits<PTPC> base_type;

    BOOST_CONCEPT_USAGE(MutablePlayerTypeWithPositionConstraints)
    {
        set_position_x_min( coord, const_ptpc );
        set_position_y_min( coord, const_ptpc );
        set_position_x_max( coord, const_ptpc );
        set_position_y_max( coord, const_ptpc );
    }

private:
    PTPC ptpc;
    typename base_type::coordinate_descriptor coord;
}

}
#endif // SOCCER_PLAYER_TYPE_CONCEPTS_HPP
