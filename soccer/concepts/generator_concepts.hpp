#ifndef SOCCER_GENERATOR_CONCEPTS_HPP
#define SOCCER_GENERATOR_CONCEPTS_HPP

#include <soccer/generator_traits.hpp>

#include <boost/type_traits.hpp>
#include <boost/concept/assert.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(GeneratorConcept,(G))
{
    typedef generator_traits<G> generator_traits_t;
    typedef generator_traits_t::result_type result_type;
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< generator_traits_t::argument_type, void >::value,
                              "Generator must be a unary function with no arguments" ));

    BOOST_CONCEPT_USAGE(GeneratorConcept)
    {
        res = gen();
        const_constraints( gen );
    }

private:
    void const_constraints( const G& const_gen )
    {
        bool res = has_next( gen );
    }

    G gen;
    result_type res;
};

}
#endif // SOCCER_GENERATOR_CONCEPTS_HPP
