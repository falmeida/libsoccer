#ifndef SOCCER_OBJECT_MODEL_HPP
#define SOCCER_OBJECT_MODEL_HPP

namespace soccer {

template < typename Size = float,
           typename Speed = float,
           typename Acceleration = Speed >
struct object_model_simple
{
    Size M_size;
    Speed M_decay;
    Speed M_speed_max;
    Speed M_speed_min;
    Acceleration M_accel_max;
};

} // end namespace soccer

namespace soccer { namespace traits {

//
// ball_model_simple TYPE REGISTRATION
//
template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct tag< object_model_simple< Size, Speed, Acceleration > >
{
    typedef /* object_model_simple_tag */ void type;
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct size_type< object_model_simple< Size, Speed, Acceleration > >
{
    typedef Size type;
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct speed_type< object_model_simple< Size, Speed, Acceleration > >
{
    typedef Speed type;
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct acceleration_type< object_model_simple< Size, Speed, Acceleration > >
{
    typedef Acceleration type;
};

//
// ball_model_simple DATA ACCESS REGISTRATION
//

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct size_access < object_model_simple< Size, Speed, Acceleration > >
{
    typedef object_model_simple< Size, Speed, Acceleration > object_model_simple_t;

    static inline
    Size const& get( object_model_simple_t const& bm ) { return bm.M_size; }
    static inline
    void set( Size const& _val, object_model_simple_t& bm ) { bm.M_size = _val; }
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct speed_max_access < object_model_simple< Size, Speed, Acceleration > >
{
    typedef object_model_simple< Size, Speed, Acceleration > object_model_simple_t;

    static inline
    Speed const& get( object_model_simple_t const& bm ) { return bm.M_speed_max; }
    static inline
    void set( Speed const& _val, object_model_simple_t& bm ) { bm.M_speed_max = _val; }
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct speed_min_access < object_model_simple< Size, Speed, Acceleration > >
{
    typedef object_model_simple< Size, Speed, Acceleration > object_model_simple_t;

    static inline
    Speed const& get( object_model_simple_t const& bm ) { return bm.M_speed_min; }
    static inline
    void set( Speed const& _val, object_model_simple_t& bm ) { bm.M_speed_min = _val; }
};


template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct accel_max_access < object_model_simple< Size, Speed, Acceleration > >
{
    typedef object_model_simple< Size, Speed, Acceleration > object_model_simple_t;

    static inline
    Acceleration const& get( object_model_simple_t const& bm ) { return bm.M_accel_max; }
    static inline
    void set( Acceleration const& _val, object_model_simple_t& bm ) { bm.M_accel_max = _val; }
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct decay_access < object_model_simple< Size, Speed, Acceleration > >
{
    typedef object_model_simple< Size, Speed, Acceleration > object_model_simple_t;

    static inline
    Speed const& get( object_model_simple_t const& bm ) { return bm.M_decay; }
    static inline
    void set( Speed const& _val, object_model_simple_t& bm ) { bm.M_decay = _val; }
};

}}

#endif // SOCCER_OBJECT_MODEL_HPP
