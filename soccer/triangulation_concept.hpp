#ifndef SOCCER_GEOMETRY_TRIANGULATION_CONCEPT_HPP
#define SOCCER_GEOMETRY_TRIANGULATION_CONCEPT_HPP

#include <soccer/triangulation_traits.hpp>

#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(TriangulationVertexConcept,(TV))
{
    /* BOOST_CONCEPT_ASSERT_MSG(( boost::is_convertible< typename tag< TV >::type,
                                                      vertex_tag >::value ),
                               "Not a triangulation vertex"); */
    typedef typename position_type< TV >::type point_t;


    BOOST_CONCEPT_USAGE(TriangulationVertexConcept)
    {
        const_constraints( tv );
    }

    private:
        void const_constraints( const TV& const_triang )
        {
            _point = point( tv );
        }

    TV tv;
    point_t _point;
};

BOOST_concept(TriangulationFaceConcept,(TF))
{
    /* BOOST_CONCEPT_ASSERT_MSG(( boost::is_convertible< typename tag< TF >::type,
                                                      triangulation_face_tag >::value ),
                               "Not a triangulation face"); */


    BOOST_CONCEPT_USAGE(TriangulationFaceConcept)
    {
        const_constraints( tf );
    }

    private:
        void const_constraints( const TF& const_triang )
        {
            // _point = point( tv );
        }

    TF tf;
};

BOOST_concept(TriangulationConcept,(T))
{
    typedef typename triangulation_traits< T >::type traits_t;

    // BOOST_CONCEPT_ASSERT(( TriangulationVertexConcept< typename T::vertex_type > ));

    BOOST_CONCEPT_USAGE(TriangulationConcept)
    {
        const_constraints( t );
    }

private:
    void const_constraints( const T& const_triang )
    {
        // _vertex = nearest_vertex( pos, const_triang );
        // _edge = nearest_edge( pos, const_triang );
        _vertex_iterators = vertices( const_triang );
        _vertex_id = *_vertex_iterators.first;
        _vertex_id = infinite_vertex( const_triang );

        _edge_iterators = edges( const_triang );
        _edge_id = *_edge_iterators.first;

        _face_iterators = faces( const_triang );
        _face_id = *_face_iterators.first;

        // is_infinite( _vertex );
        _vertex_circulator = incident_vertices( _vertex_id );
        _edge_circulator = incident_edges( _vertex_id );
        _face_circulator = incident_faces( _vertex_id );
    }

    T t;

    typename traits_t::vertex_id_type _vertex_id;
    typename traits_t::edge_id_type _edge_id;
    typename traits_t::face_id_type _face_id;

    std::pair< typename traits_t::vertex_iterator_type,
               typename traits_t::vertex_iterator_type > _vertex_iterators;
    std::pair< typename traits_t::edge_iterator_type,
               typename traits_t::edge_iterator_type > _edge_iterators;
    std::pair< typename traits_t::face_iterator_type,
               typename traits_t::face_iterator_type > _face_iterators;

    typename traits_t::vertex_circulator_type _vertex_circulator;
    typename traits_t::edge_circulator_type _edge_circulator;
    typename traits_t::face_circulator_type _face_circulator;
};


BOOST_concept(MutableTriangulationConcept,(T)(Position))
    : public TriangulationConcept< T >
{
    BOOST_CONCEPT_USAGE(MutableTriangulationConcept)
    {
        insert( _position, t );
    }

    T t;
    Position _position;

};

} // end namespace soccer

#endif // SOCCER_GEOMETRY_TRIANGULATION_CONCEPT_HPP
