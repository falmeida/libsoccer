#ifndef SETPLAYS_SET_HPP
#define SETPLAYS_SET_HPP

#include <vector>

namespace soccer {

template <typename SetplaySetType,
          typename SetplayId>
struct setplay_set {
    typedef SetplaySetType setplay_set_type_descriptor;
    typedef SetplayId setplay_id_descriptor;

    // Accessors
    virtual SetplaySetType set_type( const SetplaySetType type ) { M_type = type; }
    virtual void add_setplay( const SetplayId setplay_id ) { M_setplays.insert( setplay_id ); }

    // Modifiers
    virtual void set_type( const SetplayType type ) { M_type = type; }
    virtual void add_setplay( const SetplayId setplay_id ) { M_setplays.insert( setplay_id ); }

    SetplaySetType M_type;
    std::set< SetplayId > M_setplays;
};

}

#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace soccer {

struct setplay_set_reader {
    const std::string M_file_name;
    setplay_set_reader( const std::string& file_path )
        : M_file_name( file_path )
    {

    }

    template <typename SetplaySetType,
              typename SetplayId >
    void operator()( setplay_set<SetplaySetType, SetplayId>& setplay_set )
    {
        char setplay_ids[1024];
        // Declare the supported options.
        boost::program_options::options_description desc("TacticWeighted options");
        desc.add_options()
            ("help", "produce help message")
            ("type", boost::program_options::value<Attraction>(&setplay_set.M_type), "setplay set type")
            ("setplays", boost::program_options::value<std::string>(setplay_ids), "setplays")
        ;

        std::ifstream settings_file( M_file_name.c_str() );
        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_config_file( settings_file, desc), vm);
        boost::program_options::notify(vm);

        try
        {
            typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
            boost::char_separator<char> sep("-;|");
            tokenizer tokens(str, sep);

            // Convert read set-play ids to the correct type
            for ( tokenizer::iterator tok_iter = tokens.begin(); tok_iter != tokens.end(); ++tok_iter )
            {
                setplay_set.add_setplay( boost::lexical_cast<SetplayId>( *tok_iter ) );
            }
        }
        catch ( boost::bad_lexical_cast & )
        {
            assert( false );
        }

        if ( decision_algorithm != -1 )
        {
            setplay_set.M_decision_algorithm = soccer::integer_to_decision_algorithm_map.at( decision_algorithm );
        }


        if ( vm.count("help") )
        {
            std::cout << desc << std::endl;
            return;
        }
    }
};

}

#endif // SETPLAYS_SET_HPP
