#ifndef SOCCER_STRATEGY_CONCEPTS_HPP
#define SOCCER_STRATEGY_CONCEPTS_HPP

#include <soccer/strategy_traits.hpp>

#include <soccer/core/tags.hpp>

#include <boost/concept/assert.hpp>
#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(StrategyConcept,(S))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< strategy_tag, typename tag< S >::type >::value ),
                            "Type category mismatch");

    typedef typename position_type< S >::type position_t;
    typedef typename role_id_type< S >::type role_id_t;
    typedef typename role_iterator_type< S >::type role_iterator_t;

    typedef typename tactic_id_type< S >::type tactic_id_t;
    typedef typename formation_id_type< S >::type formation_id_t;
    typedef typename flux_id_type< S >::type flux_id_t;

    // typedef typename strategy_traits_t::tactic_t tactic_t;
    // typedef typename strategy_traits_t::formation_t formation_t;
    // typedef typename strategy_traits_t::flux_t flux_t;
    // typedef typename strategy_traits_t::positionings_t positionings_t;

    // typedef typename strategy_traits_t::tactic_iterator tactic_iterator;
    // typedef typename strategy_traits_t::formation_iterator formation_iterator;
    // typedef typename strategy_traits_t::flux_iterator flux_iterator;

    BOOST_CONCEPT_ASSERT(( PositionConcept< position_t > ));

    /*BOOST_CONCEPT_ASSERT(( TacticConcept< tactic_t> ));
    BOOST_CONCEPT_ASSERT(( soccer::FormationConcept< formation_t> ));
    BOOST_CONCEPT_ASSERT(( soccer::FluxConcept< flux_t> ));

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< role_iterator_t> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< role_iterator_t>::type,
                              role_id_t>::value ),
                            "Player iterator value type does not match player id descriptor");

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< tactic_iterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< tactic_iterator>::type,
                              tactic_id_t>::value ),
                            "Tactic iterator value type does not match tactic id descriptor");

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< formation_iterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< formation_iterator>::type,
                              formation_id_t>::value ),
                            "Formation iterator value type does not match formation id descriptor");

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< flux_iterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< flux_iterator>::type,
                              flux_id_t>::value ),
                            "Flux iterator value type does not match flux id descriptor"); */

    BOOST_CONCEPT_USAGE(StrategyConcept)
    {
        const_constraints( s );
    }

private:
    void const_constraints( const S& const_strategy )
    {
        tactic_id = get_tactic_id( const_strategy );
        // tactics_iterators = tactics( const_strategy );

        form_id = get_formation_id( const_strategy );
        // formations_iterators = formations( const_strategy );

        flux_id = get_flux_id( const_strategy );
        // fluxes_iterators = fluxes( const_strategy );

        role_iterators = get_roles( const_strategy );
        pos = get_position( pid, const_strategy );
        // pid = get_positioning( pid, const_strategy );
    }

    S s;
    std::pair< role_iterator_t,
               role_iterator_t > role_iterators;
    /*std::pair< tactic_iterator,
               tactic_iterator > tactics_iterators;
    std::pair< formation_iterator,
               formation_iterator > formations_iterators;
    std::pair< flux_iterator,
               flux_iterator > fluxes_iterators;*/

    position_t pos;
    role_id_t pid;

    tactic_id_t tactic_id;
    formation_id_t form_id;
    flux_id_t flux_id;

    /* const tactic_t& tactic_desc;
    const formation_t& form_desc;
    const flux_t& flux_desc;*/
};

BOOST_concept(MutableStrategyConcept,(S))
    : StrategyConcept<S>
{
    typedef typename position_type< S >::type position_t;
    typedef typename role_id_type< S >::type role_id_t;

    typedef typename tactic_id_type< S >::type tactic_id_t;
    typedef typename formation_id_type< S>::type formation_id_t;
    typedef typename flux_id_type< S >::type flux_id_t;

    BOOST_CONCEPT_USAGE(MutableStrategyConcept)
    {
        set_position( pid, pos, s );
        // set_positioning( pid, pid, s );

        set_tactic_id( tactic_id, s );
        set_formation_id( form_id, s );
        set_flux_id( flux_id, s );
    }

private:
    S s;
    position_t pos;
    role_id_t pid;

    tactic_id_t tactic_id;
    formation_id_t form_id;
    flux_id_t flux_id;
};


} // end namespace soccer

#endif // SOCCER_STRATEGY_CONCEPTS_HPP
