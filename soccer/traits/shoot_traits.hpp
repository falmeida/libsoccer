#ifndef SOCCER_SHOOT_TRAITS_HPP
#define SOCCER_SHOOT_TRAITS_HPP

#include <soccer/core/speed_type.hpp>
#include <soccer/core/position_type.hpp>
#include <soccer/action_traits.hpp>

namespace soccer {

template < typename Shoot >
struct shoot_traits
    : public action_traits<Shoot>
{
    typedef typename soccer::position_type< Shoot >::type position_type;
    typedef typename soccer::speed_type< Shoot >::type speed_type;
};

}

#endif // SOCCER_SHOOT_TRAITS_HPP
