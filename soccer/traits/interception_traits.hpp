#ifndef SOCCER_INTERCEPTION_TRAITS_HPP
#define SOCCER_INTERCEPTION_TRAITS_HPP

namespace soccer {

template <typename Interception>
struct interception_traits {
    typedef typename Interception::position_descriptor position_descriptor;
    typedef typename Interception::player_id_t player_id_t;
    typedef typename Interception::duration_descriptor duration_descriptor;
    typedef typename Interception::speed_descriptor speed_descriptor;
};


template <typename InterceptionTable>
struct interception_table_traits
{
    typedef typename InterceptionTable::interception_id_descriptor interception_id_descriptor;
    typedef typename InterceptionTable::interception_descriptor interception_descriptor;
    typedef typename InterceptionTable::interception_iterator interception_iterator;

    inline static interception_id_descriptor null_interception_id()
    {
        return InterceptionTable::null_interception_id();
    }
};

}
#endif // SOCCER_INTERCEPTION_TRAITS_HPP
