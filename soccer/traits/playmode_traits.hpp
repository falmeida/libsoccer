#ifndef SOCCER_PLAYMODE_TRAITS_HPP
#define SOCCER_PLAYMODE_TRAITS_HPP

#include <soccer/core/side_type.hpp>
#include <soccer/core/match_situation_type.hpp>
#include <soccer/core/time_type.hpp>
#include <soccer/core/tag.hpp>

namespace soccer {

template <typename PlayMode>
struct playmode_traits {
    typedef typename side_type< PlayMode >::type side_type;
    typedef typename time_type< PlayMode >::type time_type;
    typedef typename match_situation_type< PlayMode >::type match_situation_type;
};

}
#endif // SOCCER_PLAYMODE_TRAITS_HPP
