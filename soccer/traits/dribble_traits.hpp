#ifndef SOCCER_DRIBBLE_TRAITS_HPP
#define SOCCER_DRIBBLE_TRAITS_HPP

#include <soccer/action_traits.hpp>

namespace soccer {

template <typename Dribble>
struct dribble_traits
    : public action_traits<Dribble>
{
    typedef typename Dribble::position_descriptor position_descriptor;
    typedef typename Dribble::speed_descriptor speed_descriptor;
    typedef typename Dribble::direction_descriptor direction_descriptor;
};

}

#endif // SOCCER_DRIBBLE_TRAITS_HPP
