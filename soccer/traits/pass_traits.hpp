#ifndef SOCCER_PASS_TRAITS_HPP
#define SOCCER_PASS_TRAITS_HPP

#include <soccer/action_traits.hpp>

namespace soccer {

// pass category tags
struct direct_pass_tag { };
struct through_pass_tag { };
struct lead_pass_tag { };
struct cross_pass_tag { };

template <typename Pass>
struct pass_traits
    : public action_traits<Pass>
{
    typedef typename Pass::position_descriptor position_descriptor;
    typedef typename Pass::speed_descriptor speed_descriptor;
    typedef typename Pass::direction_descriptor direction_descriptor;
    typedef typename Pass::action_category action_category;

};

namespace detail {

    inline bool is_direct_pass( direct_pass_tag ) { return true; }
    template <typename T>
    inline bool is_direct_pass( T ) { return false; }

    inline bool is_through_pass( through_pass_tag ) { return true; }
    template <typename T>
    inline bool is_through_pass( T ) { return false; }

    inline bool is_lead_pass( lead_pass_tag ) { return true; }
    template <typename T>
    inline bool is_lead_pass( T ) { return false; }

    inline bool is_cross_pass( cross_pass_tag ) { return true; }
    template <typename T>
    inline bool is_cross_pass( T ) { return false; }
}

/*!
\brief check if the pass is direct
\returns true if the pass is direct, or false otherwise.
*/
template <typename Pass>
bool is_direct_pass(const Pass&)
{
    typedef typename tag< Pass >::type Cat;
    return detail::is_direct_pass( Cat() );
}

/*!
\brief check if the pass is direct
\returns true if the pass is direct, or false otherwise.
*/
template <typename Pass>
bool is_through_pass(const Pass&)
{
    typedef typename tag< Pass >::type Cat;
    return detail::is_through_pass( Cat() );
}

/*!
\brief check if the pass is direct
\returns true if the pass is direct, or false otherwise.
*/
template <typename Pass>
bool is_lead_pass(const Pass&)
{
    typedef typename tag< Pass >::type Cat;
    return detail::is_lead_pass( Cat() );
}

/*!
\brief check if the pass is direct
\returns true if the pass is direct, or false otherwise.
*/
template <typename Pass>
bool is_cross_pass(const Pass&)
{
    typedef typename tag< Pass >::type Cat;
    return detail::is_cross_pass( Cat() );
}

}
#endif // SOCCER_PASS_TRAITS_HPP
