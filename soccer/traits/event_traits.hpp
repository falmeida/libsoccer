#ifndef EVENT_TRAITS_HPP
#define EVENT_TRAITS_HPP

namespace soccer {
namespace event{

// event category tag
struct kick_off_tag { };
struct throw_in_tag { };
struct goal_kick_tag { };
struct corner_kick_in_tag { };
struct free_kick_tag { };
struct indirect_free_kick_tag{ };

struct offside_tag{ };

struct hold_ball_tag { };
struct pass_tag { };
struct dribble_tag { };
struct shoot_tag { };
struct move_tag { };
struct catch_tag { };

namespace detail {
    inline bool is_with_ball( pass_tag ) { return true; }
    inline bool is_with_ball( dribble_tag ) { return true; }
    inline bool is_with_ball( shoot_tag ) { return true; }
    inline bool is_with_ball( catch_tag ) { return true; }
    template <typename T>
    inline bool is_with_ball( T ) { return false; }
}

template <typename Event>
struct event_traits {
    typedef typename Event::time_descriptor time_descriptor;
    typedef typename Event::category_tag category_tag;
};

template <typename BallDispute>
struct ball_dispute_traits
    : public event_traits<BallDispute>
{
    typedef BallDispute::player_iterator_t player_iterator_t;
    typedef BallDispute::players_size_type players_size_type;
};


template <typename Action>
struct action_event_traits
    : public event_traits< Action > {

};

}
}

#endif // EVENT_TRAITS_HPP
