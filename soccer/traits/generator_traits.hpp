#ifndef SOCCER_GENERATOR_TRAITS_HPP
#define SOCCER_GENERATOR_TRAITS_HPP

#include <boost/functional.hpp>

namespace soccer {

template <typename Generator>
struct generator_traits
    : public boost::unary_traits< Generator > {

};

}
#endif // SOCCER_GENERATOR_TRAITS_HPP
