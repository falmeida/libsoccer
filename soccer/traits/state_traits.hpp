#ifndef SOCCER_STATE_TRAITS_HPP
#define SOCCER_STATE_TRAITS_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/core/player_id_type.hpp>
#include <soccer/core/player_type.hpp>
#include <soccer/core/player_iterator_type.hpp>
#include <soccer/core/time_type.hpp>
#include <soccer/core/playmode_type.hpp>
#include <soccer/core/score_type.hpp>
#include <soccer/core/team_type.hpp>

#include <soccer/core/teammate_iterator_type.hpp>

#include <soccer/core/tag.hpp>

SOCCER_NAMESPACE_BEGIN

template < typename State >
struct state_traits
{

    typedef typename player_id_type< State >::type player_id_type;
    typedef typename player_iterator_type< State >::type player_iterator_type;
    typedef typename player_type< State >::type player_type;
    typedef typename time_type< State >::type time_type;
    typedef typename playmode_type< State >::type playmode_type;
    typedef typename score_type< State >::type score_type;
    typedef typename tag< State >::type category;
};

template < typename State >
struct state_with_team_affiliation_traits
        : public state_traits< State >
{
    typedef typename teammate_iterator_type< State >::type teammate_iterator_type;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_TRAITS_HPP
