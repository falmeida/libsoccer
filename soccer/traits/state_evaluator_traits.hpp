#ifndef SOCCER_TRAITS_STATE_EVALUATOR_HPP
#define SOCCER_TRAITS_STATE_EVALUATOR_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/core/state_type.hpp>
#include <soccer/core/result_type.hpp>

SOCCER_NAMESPACE_BEGIN

template < typename State >
struct state_evaluator_traits {
    typedef result_type< State >::type result_type;
    typedef state_type< State >::type state_type;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_TRAITS_STATE_EVALUATOR_HPP
