#ifndef SOCCER_ACTION_CHAIN_TRAITS_HPP
#define SOCCER_ACTION_CHAIN_TRAITS_HPP

namespace soccer {

template <typename ActionStatePair>
struct action_state_pair_traits {
    typedef typename ActionStatePair::state_type state_type;
    typedef typename ActionStatePair::action_type action_type;
};

template <typename ActionChain>
struct action_chain_traits {
    typedef typename ActionChain::action_state_pair_type action_state_pair_type;
    // Shortcut types
    typedef typename action_state_pair_type::state_type state_type;
    typedef typename action_state_pair_type::action_type action_type;

    typedef typename ActionChain::action_iterator action_state_iterator;
};

}

#endif // SOCCER_ACTION_CHAIN_TRAITS_HPP
