#ifndef SOCCER_STRATEGY_TRAITS_HPP
#define SOCCER_STRATEGY_TRAITS_HPP

#include <soccer/core/position_type.hpp>
#include <soccer/core/role_id_type.hpp>
#include <soccer/core/role_iterator_type.hpp>
#include <soccer/core/formation_id_type.hpp>
#include <soccer/core/flux_id_type.hpp>
#include <soccer/core/tactic_id_type.hpp>


namespace soccer {

template < typename Strategy >
struct strategy_traits {
    typedef typename position_type< Strategy >::type position_type;
    typedef typename role_id_type< Strategy >::type role_id_type;
    typedef typename role_iterator_type< Strategy >::type role_iterator_type;

    typedef typename tactic_id_type< Strategy >::type tactic_id_type;
    typedef typename formation_id_type< Strategy >::type formation_id_type;
    typedef typename flux_id_type< Strategy >::type flux_id_type;
};

}
#endif // SOCCER_STRATEGY_TRAITS_HPP
