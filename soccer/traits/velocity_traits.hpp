#ifndef SOCCER_VELOCITY_TRAITS_HPP
#define SOCCER_VELOCITY_TRAITS_HPP

#include <soccer/core/coordinate_type.hpp>

namespace soccer {

template < typename Velocity >
struct velocity_traits {
    typedef typename coordinate_type< Velocity >::type coordinate_type;
};

}

#endif // SOCCER_VELOCITY_TRAITS_HPP
