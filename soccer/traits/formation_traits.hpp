#ifndef SOCCER_FORMATION_TRAITS_HPP
#define SOCCER_FORMATION_TRAITS_HPP

#include <soccer/core/role_id_type.hpp>
#include <soccer/core/role_iterator_type.hpp>
#include <soccer/core/position_type.hpp>
#include <soccer/core/tag.hpp>
#include <soccer/core/tags.hpp>

#include <functional>

namespace soccer {

/*!
  \struct formation_traits
  \param Formation The formation type whose associated types are being accessed.
  */
template < typename Formation >
struct formation_traits
{
    typedef typename position_type< Formation >::type position_type;
    typedef typename role_id_type< Formation >::type role_id_type;
    typedef typename role_iterator_type< Formation >::type role_iterator_type;
    typedef typename tag< Formation >::type category;
};

namespace detail {
    inline bool is_static( formation_static_tag ) { return true; }
    inline bool is_static( formation_dynamic_tag ) { return false; }
}

template <typename Formation>
inline bool is_static( const Formation& )
{
    typedef typename tag<Formation>::type Cat;
    return detail::is_static( Cat() );
}

template <typename Formation>
inline bool is_dynamic( const Formation& f)
{
    return !is_static(f);
}

}
#endif // SOCCER_FORMATION_TRAITS_HPP
