#ifndef SOCCER_POSITIONING_TRAITS_HPP
#define SOCCER_POSITIONING_TRAITS_HPP

namespace soccer {

template <typename Positioning>
struct positionings_traits {
    typedef typename Positioning::positioning_id_descriptor positioning_id_descriptor;
    typedef typename Positioning::positioning_id_iterator positioning_id_iterator;
    typedef typename Positioning::positionings_size_type positionings_size_type;
	
    /* inline player_role_descriptor static null_role()
	{
        return Positioning::null_role();
    } */
};

}
#endif // SOCCER_POSITIONING_TRAITS_HPP
