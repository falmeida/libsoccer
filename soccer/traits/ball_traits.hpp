#ifndef SOCCER_BALL_TRAITS_HPP
#define SOCCER_BALL_TRAITS_HPP

#include <soccer/traits/object_traits.hpp>

namespace soccer {

template < typename Ball >
struct static_ball_traits
    : public static_object_traits< Ball >
{

};

template < typename Ball >
struct dynamic_ball_traits
    : public dynamic_object_traits< Ball >
{

};


template <typename Ball>
struct static_ball_archetype
{
    typedef typename Ball::position_descriptor position_descriptor;
    typedef void velocity_descriptor;
    typedef static_ball_tag ball_category_tag;
};

template <typename Ball>
struct dynamic_ball_archetype
    : static_ball_archetype<Ball> {
    typedef typename Ball::velocity_descriptor velocity_descriptor;
    typedef dynamic_ball_tag ball_category_tag;
};

}

#endif // SOCCER_BALL_TRAITS_HPP
