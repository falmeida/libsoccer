#ifndef SOCCER_TACTIC_TRAITS_HPP
#define SOCCER_TACTIC_TRAITS_HPP

namespace soccer {

template <typename Tactic>
struct tactic_traits
{
    typedef typename Tactic::weight_descriptor weight_descriptor;
};

}

#endif // SOCCER_TACTIC_TRAITS_HPP
