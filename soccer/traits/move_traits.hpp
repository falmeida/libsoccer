#ifndef SOCCER_MOVE_TRAITS_HPP
#define SOCCER_MOVE_TRAITS_HPP

#include <soccer/action_traits.hpp>

namespace soccer {

template <typename Move>
struct move_traits
    : public action_traits<Move>
{
    typedef typename Move::position_descriptor position_descriptor;
    typedef typename Move::speed_descriptor speed_descriptor;
};

}

#endif // SOCCER_MOVE_TRAITS_HPP
