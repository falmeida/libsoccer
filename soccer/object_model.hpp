#ifndef SOCCER_OBJECT_MODEL_HPP
#define SOCCER_OBJECT_MODEL_HPP

namespace soccer {

template < typename Size,
           typename Weight = Size,
           typename Speed = Weight,
           typename Acceleration = Speed,
           typename Percentage = float >
struct mobile_object_model {


    mobile_object_model( Size size, Speed speed_max, Acceleration accel_max, Percentage decay )
        : M_size( size )
        , M_speed_max( speed_max )
        , M_accel_max( accel_max )
        , M_decay( decay )
    {

    }

    Weight M_weight;
    Size M_size;
    Speed M_speed_max;
    Acceleration M_accel_max;
    Percentage M_decay;
};

} // end namespace soccer

#include <boost/program_options.hpp>

namespace soccer {

struct mobile_object_model_reader {

    template < typename Size,
               typename Weight,
               typename Speed,
               typename Acceleration,
               typename Percentage >
    bool operator()( mobile_object_model< Size, Weight, Speed, Acceleration, Percentage >& _mobile_object,
                     std::istream& is )
    {
        boost::program_options::options_description options_desc("Mobile Object Model Options");
        options_desc.add()
                ("help", "produce help message")
                ("size", boost::program_options::value<Size>(&_mobile_object.M_size)->required(), "object size")
                ("weight", boost::program_options::value<Size>(&_mobile_object.M_weight)->required(), "object weight")
                ("speed_max", boost::program_options::value<Speed>(&_mobile_object.M_speed_max)->required(), "object maximum speed")
                ("accel_max", boost::program_options::value<Acceleration>(&_mobile_object.M_accel_max)->required(), "object maximum acceleration")
                ("decay", boost::program_options::value<Percentage>(&_mobile_object.M_decay)->required(), "object inertia decay")
        ;

        boost::program_options::variables_map vm;
        try
        {
            boost::program_options::store( boost::program_options::parse_config_file( is, options_desc), vm );
            boost::program_options::notify(vm);
        }
        catch ( boost::program_options::error& e )
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << options_desc << std::endl;
            return false;
        }

        if ( vm.count("help") )
        {
            std::cout << options_desc << std::endl;
        }

        return true;
    }
};

} // end namespace soccer


#include <soccer/core/access.hpp>

namespace soccer { namespace traits {

//
// Types Registration
//
template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct tag< mobile_object_model< Size, Speed, Acceleration, Percentage > >
    { typedef dynamic_object_model_tag type; };

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct size_type< mobile_object_model< Size, Speed, Acceleration, Percentage > >
    { typedef Size type; };

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct speed_type< mobile_object_model< Size, Speed, Acceleration, Percentage > >
    { typedef Speed type; };

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct acceleration_type< mobile_object_model< Size, Speed, Acceleration, Percentage > >
    { typedef Acceleration type; };

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct decay_type< mobile_object_model< Size, Speed, Acceleration, Percentage > >
    { typedef Percentage type; };

//
// Data Access Registration
//

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct size_access< mobile_object_model< Size, Speed, Acceleration, Percentage > >
{
    static inline Size const&
    get( mobile_object_model< Size, Speed, Acceleration, Percentage > const& _model )
        { return _model.M_size; }

    static inline void
    set( Size const& _size, mobile_object_model< Size, Speed, Acceleration, Percentage >& _model )
        { _model.M_size = _size; }
};

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct speed_max_access< mobile_object_model< Size, Speed, Acceleration, Percentage > >
{
    static inline Speed const&
    get( mobile_object_model< Size, Speed, Acceleration, Percentage > const& _model )
        { return _model.M_speed_max; }

    static inline void
    set( Speed const& _speed, mobile_object_model< Size, Speed, Acceleration, Percentage >& _model )
        { _model.M_speed_max = _speed; }
};

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct accel_max_access< mobile_object_model< Size, Speed, Acceleration, Percentage > >
{
    static inline Acceleration const&
    get( mobile_object_model< Size, Speed, Acceleration, Percentage > const& _model )
        { return _model.M_accel_max; }

    static inline void
    set( Acceleration const& _accel, mobile_object_model< Size, Speed, Acceleration, Percentage >& _model )
        { _model.M_accel_max = _accel; }
};

template <>
template < typename Size, typename Speed, typename Acceleration, typename Percentage >
struct decay_access< mobile_object_model< Size, Speed, Acceleration, Percentage > >
{
    static inline Percentage const&
    get( mobile_object_model< Size, Speed, Acceleration, Percentage > const& _model )
        { return _model.M_decay; }

    static inline void
    set( Percentage const& _decay, mobile_object_model< Size, Speed, Acceleration, Percentage >& _model )
        { _model.M_decay = _decay; }
};

}} // end namespace soccer::traits

#endif // SOCCER_OBJECT_MODEL_HPP
