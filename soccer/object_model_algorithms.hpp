#ifndef SOCCER_BALL_MODEL_ALGORITHMS_HPP
#define SOCCER_BALL_MODEL_ALGORITHMS_HPP

#include <soccer/ball_concepts.hpp>

namespace soccer
{

template < typename MobileObject,
           typename MobileObjectModel >
typename position_type< MobileObject >::type
get_inertia_position( MobileObject const& _obj,
                      MobileObjectModel const& _model,
                      const unsigned int _num_iterations = 1 )
{
    // BOOST_CONCEPT_ASSERT(( BallConcept< Ball > ));

    auto _inertia_pos = get_position( _obj );
    for ( auto i = 0u; i < _num_iterations; i++ )
    {
        set_x( _inertia_pos * get_decay( _model ), _inertia_pos );
        set_y( _inertia_pos * get_decay( _model ), _inertia_pos );
    }

    return _inertia_pos;
}

template < typename MobileObject,
           typename MobileObjectModel >
typename velocity_type< MobileObject >::type
get_inertia_velocity( MobileObject const& _obj,
                      MobileObjectModel const& _model,
                      const unsigned int _num_iterations = 1 )
{
    // BOOST_CONCEPT_ASSERT(( BallConcept< Ball > ));

    auto _inertia_vel = get_velocity( _obj );
    for ( auto i = 0u; i < _num_iterations; i++ )
    {
        set_x( _inertia_vel * get_decay( _model ), _inertia_vel );
        set_y( _inertia_vel * get_decay( _model ), _inertia_vel );
    }

    return _inertia_vel;
}

}

#endif // SOCCER_BALL_MODEL_ALGORITHMS_HPP
