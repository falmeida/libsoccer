#ifndef SOCCER_HOLD_BALL_HPP
#define SOCCER_HOLD_BALL_HPP

#include <soccer/shoot_concept.hpp>
#include <soccer/action.hpp>

#include <soccer/state_traits.hpp>

namespace soccer {

//
// hold ball implementation
//
template <typename PlayerIdDescriptor,
          typename DurationDescriptor >
struct hold_ball
    : public action<PlayerIdDescriptor,DurationDescriptor>
{
    typedef hold_ball_tag action_category;

    hold_ball()
        : action<PlayerIdDescriptor,DurationDescriptor>(ActionType::HoldBall)
    { }
};


/*!
  \brief shortcut to create a hold ball type from a state descriptor
  */
template <typename State>
struct hold_ball_from_state_generator
{
    BOOST_CONCEPT_ASSERT(( StaticStateConcept< State> ));

    typedef hold_ball< typename state_traits< State >::player_id_descriptor,
                       typename state_traits< State >::time_descriptor > type;
};

}

#endif // SOCCER_HOLD_BALL_HPP
