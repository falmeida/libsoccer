#ifndef SOCCER_GEOMETRY_TRIANGULATION_TRAITS_HPP
#define SOCCER_GEOMETRY_TRIANGULATION_TRAITS_HPP

#include <soccer/core/access.hpp>

namespace soccer {

template < typename Triangulation >
struct triangulation_traits
{
    typedef typename vertex_type< Triangulation >::type vertex_type;
    typedef typename edge_type< Triangulation >::type edge_type;
    typedef typename face_type< Triangulation >::type face_type;

    typedef typename vertex_iterator_type< Triangulation >::type vertex_iterator_type;
    typedef typename edge_iterator_type< Triangulation >::type edge_iterator_type;
    typedef typename face_iterator_type< Triangulation >::type face_iterator_type;

    typedef typename vertex_circulator_type< Triangulation >::type vertex_circulator_type;
    typedef typename edge_circulator_type< Triangulation >::type edge_circulator_iterator_type;
    typedef typename face_circulator_type< Triangulation >::type face_circulator_iterator_type;
};

} // end namespace soccer

#endif // SOCCER_GEOMETRY_TRIANGULATION_TRAITS_HPP
