#ifndef SOCCER_PLAYMODE_CONCEPTS_HPP
#define SOCCER_PLAYMODE_CONCEPTS_HPP

#include <soccer/playmode_traits.hpp>

#include <soccer/match_situation_concept.hpp>
#include <soccer/core/access.hpp>

#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(PlayModeConcept,(PM))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< playmode_tag, typename tag< PM >::type >::value ),
                            "Type category mismatch");
    typedef typename side_type< PM >::type side_t;
    typedef typename time_type< PM >::type time_t;
    typedef typename match_situation_type< PM >::type match_situation_t;

    BOOST_CONCEPT_ASSERT(( MatchSituationConcept< match_situation_t > ));

    BOOST_CONCEPT_USAGE( PlayModeConcept )
    {
        const_constraints( g );
    }

private:
    void const_constraints( const PM& const_playmode )
    {
        _side = get_side( const_playmode );
        _time = get_time( const_playmode );
        _ms = get_match_situation( const_playmode );
    }

    PM g;
    side_t _side;
    time_t _time;
    match_situation_t _ms;
};


BOOST_concept(MutablePlayModeConcept,(PM))
    : public PlayModeConcept<PM>
{
    typedef typename side_type< PM >::type side_t;
    typedef typename time_type< PM >::type time_t;
    typedef typename match_situation_type< PM >::type match_situation_t;

    BOOST_CONCEPT_ASSERT(( MutableMatchSituationConcept< match_situation_t > ));

    BOOST_CONCEPT_USAGE(MutablePlayModeConcept)
    {
        set_side( _side, _pm );
        set_time( _time , _pm );
        set_match_situation( _ms, _pm );
    }

    PM _pm;
    side_t _side;
    time_t _time;
    match_situation_t _ms;
};

}

#endif // SOCCER_PLAYMODE_CONCEPTS_HPP
