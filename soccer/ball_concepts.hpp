#ifndef SOCCER_BALL_CONCEPTS_HPP
#define SOCCER_BALL_CONCEPTS_HPP

#include <soccer/object_concepts.hpp>

#include <boost/type_traits.hpp>

namespace soccer {

BOOST_concept(StaticBallConcept,(B))
    : public StaticObjectConcept<B>
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< ball_tag, typename tag< B >::type >::value ),
                            "Type category mismatch");

    BOOST_CONCEPT_USAGE(StaticBallConcept)
    {

    }

};

BOOST_concept(MutableStaticBallConcept,(B))
    : public MutableStaticObjectConcept<B>
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< ball_tag, typename tag< B >::type >::value ),
                            "Type category mismatch");

    BOOST_CONCEPT_USAGE(MutableStaticBallConcept)
    {

    }

};

BOOST_concept(DynamicBallConcept,(B))
    : public MobileObjectConcept<B>
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< ball_tag, typename tag< B >::type >::value ),
                            "Type category mismatch");

    BOOST_CONCEPT_USAGE(DynamicBallConcept)
    {
        // const_constraints( ball );
   }

private:
   /* void const_constraints(const Ball& )
   {
       _vel = velocity( const_ball );
       _speed = speed( const_ball );
   }

   Ball _ball; */
};


BOOST_concept(MutableDynamicBallConcept,(B))
   : public MutableMobileObjectConcept<B>
{

    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< ball_tag, typename tag< B >::type >::value ),
                            "Type category mismatch");

   BOOST_CONCEPT_USAGE(MutableDynamicBallConcept)
   {

   }

};

}
#endif // SOCCER_BALL_CONCEPTS_HPP
