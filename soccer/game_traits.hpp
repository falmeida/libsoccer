#ifndef SOCCER_MODEL_TRAITS_HPP
#define SOCCER_MODEL_TRAITS_HPP

namespace soccer {

template <typename SoccerModel>
struct soccer_model_traits {
    typedef typename SoccerModel::players_size_type players_size_type;
    typedef typename SoccerModel::position_descriptor position_descriptor;
    typedef typename SoccerModel::coordinate_descriptor coordinate_descriptor;
    typedef typename SoccerModel::time_descriptor time_descriptor;
    typedef typename SoccerModel::speed_descriptor speed_descriptor;
    typedef typename SoccerModel::size_descriptor size_descriptor;
};

}
#endif // SOCCER_STATIC_SOCCER_MODEL_TRAITS_HPP
