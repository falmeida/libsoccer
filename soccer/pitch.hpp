#ifndef SOCCER_PITCH_HPP
#define SOCCER_PITCH_HPP

#include <boost/concept/assert.hpp>
#include <boost/type_traits.hpp>

#include <soccer/register/pitch.hpp>

namespace soccer {

template < typename Coordinate,
           typename Length = Coordinate >
struct pitch {
    // BOOST_STATIC_ASSERT(( boost::is_floating_point< Coordinate >::value ));

    Coordinate M_top_left_x;
    Coordinate M_top_left_y;
    Length M_pitch_length;
    Length M_pitch_width;
    Length M_goal_area_width;
    Length M_goal_area_length;
    Length M_penalty_area_width;
    Length M_penalty_area_length;
    Length M_penalty_circle_radius;

    Length M_goal_width;
    Length M_goal_depth;

    Length M_penalty_spot_distance;
    Length M_center_circle_radius;
    Length M_corner_arc_radius;
    Length M_goal_post_radius;
};

} // end namespace soccer

#include <boost/program_options.hpp>

namespace soccer {

struct object_model_reader {

    template < typename Coordinate,
               typename Length >
    bool operator()( pitch< Coordinate, Length >& _pitch,
                     std::istream& is )
    {
        boost::program_options::options_description options_desc("Pitch Model Options");
        options_desc.add()
                ("help", "produce help message")
                ("pitch_top_left_x", boost::program_options::value<Coordinate>(&_pitch.M_top_left_x)->required(), "pitch top left x-axis coordinate")
                ("pitch_top_left_y", boost::program_options::value<Coordinate>(&_pitch.M_top_left_y)->required(), "pitch top left y-axis coordinate")
                ("pitch_length", boost::program_options::value<Length>(&_pitch.M_pitch_length)->required(), "pitch length")
                ("pitch_width", boost::program_options::value<Length>(&_pitch.M_pitch_width)->required(), "pitch width")
                ("goal_area_length", boost::program_options::value<Length>(&_pitch.M_goal_area_length)->required(), "goal area length")
                ("goal_area_width", boost::program_options::value<Length>(&_pitch.M_goal_area_width)->required(), "goal area width")
                ("penalty_area_width", boost::program_options::value<Length>(&_pitch.M_penalty_area_width)->required(), "penalty area width")
                ("penalty_area_length", boost::program_options::value<Length>(&_pitch.M_penalty_area_length)->required(), "penalty area length")
                ("penalty_circle_radius", boost::program_options::value<Length>(&_pitch.M_penalty_circle_radius)->required(), "penalty circle radius")
                ("penalty_spot_distance", boost::program_options::value<Length>(&_pitch.M_penalty_spot_distance)->required(), "penalty spot distance")
                ("goal_width", boost::program_options::value<Length>(&_pitch.M_goal_width)->required(), "goal width")
                ("goal_length", boost::program_options::value<Length>(&_pitch.M_goal_depth)->required(), "goal length")
                ("center_circle_radius", boost::program_options::value<Length>(&_pitch.M_center_circle_radius)->required(), "center circle radius")
                ("corner_arc_radius", boost::program_options::value<Length>(&_pitch.M_corner_arc_radius)->required(), "corner arc radius")
                ("goal_post_radius", boost::program_options::value<Length>(&_pitch.M_goal_post_radius)->required(), "goal post radius")
        ;

        boost::program_options::variables_map vm;
        try
        {
            boost::program_options::store( boost::program_options::parse_config_file( is, options_desc), vm );
            boost::program_options::notify(vm);
        }
        catch ( boost::program_options::error& e )
        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << options_desc << std::endl;
            return false;
        }

        if ( vm.count("help") )
        {
            std::cout << options_desc << std::endl;
        }

        return true;
    }
};

} // end namespace soccer

NAMESPACE_SOCCER_TRAITS_BEGIN
//
// pitch TYPES REGISTRATION
//
template <>
template < typename Coordinate,
           typename Length >
struct tag< pitch< Coordinate, Length > > { typedef pitch_tag type; };

//
// pitch DATA ACCESS REGISTRATION
//
template <>
template < typename Coordinate,
           typename Length >
struct pitch_top_left_x_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Coordinate get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_top_left_x; }
    static inline
    void set( Coordinate const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_top_left_x = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct pitch_top_left_y_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Coordinate get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_top_left_y; }
    static inline
    void set( Coordinate const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_top_left_y = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct pitch_length_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_pitch_length; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_pitch_length = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct pitch_width_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_pitch_width; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_pitch_width = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct goal_area_length_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_goal_area_length; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_goal_area_length = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct goal_area_width_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_goal_area_width; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_goal_area_width = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct penalty_area_length_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_penalty_area_length; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_penalty_area_length = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct penalty_area_width_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_penalty_area_width; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_penalty_area_width = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct penalty_circle_radius_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_penalty_circle_radius; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_penalty_circle_radius = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct penalty_spot_distance_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_penalty_spot_distance; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_penalty_spot_distance = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct goal_depth_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_goal_depth; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_goal_depth = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct goal_width_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_goal_width; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_goal_width = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct center_circle_radius_access< soccer::pitch< Coordinate, Length > >
{ };

template <>
template < typename Coordinate,
           typename Length >
struct corner_arc_radius_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_corner_arc_radius; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_corner_arc_radius = value; }
};

template <>
template < typename Coordinate,
           typename Length >
struct goal_post_radius_access< soccer::pitch< Coordinate, Length > >
{
    static inline
    Length get( soccer::pitch< Coordinate, Length > const& pitch )
        { return pitch.M_goal_post_radius; }
    static inline
    void set( Length const& value,
              soccer::pitch< Coordinate, Length >& pitch )
        { pitch.M_goal_post_radius = value; }
};

NAMESPACE_SOCCER_TRAITS_END



#endif // SOCCER_PITCH_HPP
