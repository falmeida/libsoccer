#ifndef SOCCER_DEFENSE_HPP
#define SOCCER_DEFENSE_HPP


#include <map>
#include <set>

enum class DefensiveStrategyType {
    ManMarking,  // Man marking
    ZoneMarking // Zone marking
};

/*!
 * \struct TeamDefensiveStrategy
 * \brief Describes the individual strategy employed by each player if any
 */
template < typename PlayerId,
           typename Strategy >
struct TeamDefensiveStrategy {

    //!< General team defensive strategy
    std::map< PlayerId, Strategy > M_player_strategies;
};


class ManMarkingStrategy { };

class OffensiveManMarking: public ManMarkingStrategy { };

class DefensiveManMarking: public ManMarkingStrategy { };


template < typename PlayerId>
struct DefensiveStrategy {


    virtual DefensiveStrategyType type() const = 0;
};


template < typename Zone >
struct ZoneMarking
        : public DefensiveStrategy {

    ZoneMarking( Zone zone )
        : M_zone( zone )
    { }

    DefensiveStrategyType type() const
    {
        return DefensiveStrategyType::ZoneMarking;
    }

    ///<! The zone to mark
    Zone M_zone;
};

template < typename PlayerId >
struct ManMarking
        : public DefensiveStrategy {

    ManMarking( PlayerId pid ) :
    {
        M_pids.insert( pid );
    }

    DefensiveStrategyType type() const { return DefensiveStrategyType::ManMarking; }

    //!< Player identifier to mark directly
    std::set< PlayerId > M_pids;
};

#endif // SOCCER_DEFENSE_HPP
