#ifndef SOCCER_STATE_PREDICATES_HPP
#define SOCCER_STATE_PREDICATES_HPP

#include <soccer/soccer/state_concepts.hpp>

#include <soccer/player_predicates.hpp>

#include <boost/iterator/filter_iterator.hpp>

#include <boost/concept_check.hpp>

#include <boost/mpl/if.hpp>
#include <boost/type_traits.hpp>
#include <boost/functional.hpp>

#include <boost/tuple/tuple.hpp>

#include <functional>
#include <cassert>

namespace soccer {


/*!
 * \brief The ball_kickable_in_state struct
 * \tparam State A model of a state concept
 * \tparam PlayerCanKickPredicate A binary predicate that takes a player and a state
 */
template < typename State,
           typename PlayerCanKickPredicate >
struct ball_kickable_in_state
    : public std::unary_function< State, bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename soccer::player_iterator_type< State >::type player_iterator_t;

    // BOOST_CONCEPT_ASSERT(( boost::BinaryPredicateConcept< PlayerCanKickPredicate, player_id_t, State > ));

    PlayerCanKickPredicate M_player_can_kick_pred;
    ball_kickable_in_state( PlayerCanKickPredicate player_can_kick_pred )
        : M_player_can_kick_pred( player_can_kick_pred )
    {

    }

    bool operator()( State const& state )
    {
        player_iterator_t _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( state );
        for ( auto itp = _player_begin; itp != _player_end; itp++ )
        {
            if ( M_player_can_kick_pred( *itp, state ) )
            {
                return true;
            }
        }
        return false;
    }
};

} // end namespace soccer

#endif // SOCCER_STATE_PREDICATES_HPP
