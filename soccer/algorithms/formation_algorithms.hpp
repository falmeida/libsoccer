#ifndef SOCCER_FORMATION_ALGORITHMS_HPP
#define SOCCER_FORMATION_ALGORITHMS_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import relevant concepts to be checked
#include <soccer/concepts/formation_concepts.hpp>

#include <soccer/core/access.hpp>

#include <soccer/CGAL/triangulation.hpp>

#include <boost/tuple/tuple.hpp>

#include <boost/utility/enable_if.hpp>


SOCCER_NAMESPACE_BEGIN

/*!
  \brief Determine position of a player for triangulation formations
*/
template < typename Formation,
           typename State >
typename position_type<Formation>::type
get_position( typename role_id_type<Formation>::type const _role_id,
              State const& ,
              Formation const& _formation,
              formation_static_tag )
{
    BOOST_CONCEPT_ASSERT(( FormationConcept<Formation> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename tag<Formation>::type Cat;

    return formation_position( _role_id, _formation );
}

/*!
  \brief Determine position of a player for triangulation formations
*/
template < typename Formation,
           typename State >
typename position_type<Formation>::type
get_position( typename role_id_type<Formation>::type const role,
              State const& state,
              Formation const& form,
              formation_dynamic_tag )
{
    BOOST_CONCEPT_ASSERT(( FormationConcept<Formation> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename tag<Formation>::type Cat;

    return traits::position_access< typename soccer::util::bare_type< Formation >::type >::get( role, state, form );
}

/*!
  \brief Determine position of a player for triangulation formations
*/
template < typename Formation,
           typename State >
typename position_type<Formation>::type
position( typename role_id_type<Formation>::type const role,
          State const& state,
          Formation const& form )
{
    BOOST_CONCEPT_ASSERT(( FormationConcept<Formation> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename tag<Formation>::type Cat;

    return get_position( role, state, form, Cat() );
}

/*!
  \brief Find the player id nearest to a given position
  \param position position to consider
  \param formation the formation to consider
  \param state the current state
  \returns The id of the nearest player or the null player id if none is found
  */
template <typename Position,
          typename Formation,
          typename State>
typename role_id_type<Formation>::type
nearest_role( Position const& _position,
              Formation const& _formation,
              State const& _state )
{
    BOOST_CONCEPT_ASSERT(( FormationConcept<Formation> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename position_type<Formation>::type position_t;
    typedef typename role_id_type<Formation>::type role_id_t;
    typedef typename role_iterator_type<Formation>::type role_iterator_t;

    role_iterator_t vi, end;
    boost::tie( vi, end ) = players( _formation );
    if ( vi == end )
    {
        return get_null_value< Formation >();
    }

    auto nearest_role_id = *vi;
    const auto nearest_role_id_distance = distance( _position, get_position( _formation, *vi, _state ) );
    vi++;
    for (  ; vi != end; vi++ )
    {
        const auto other_distance = distance( _position, get_position( _formation, *vi, _state ));
        if ( other_distance < nearest_role_id_distance )
        {
            nearest_role_id_distance = other_distance;
            nearest_role_id = *vi;
        }
    }
    return nearest_role_id;
}


SOCCER_NAMESPACE_END

#endif // SOCCER_FORMATION_ALGORITHMS_HPP
