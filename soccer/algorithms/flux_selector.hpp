#ifndef SOCCER_FLUX_SELECTOR_HPP
#define SOCCER_FLUX_SELECTOR_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import relevant concepts to be checked
#include <soccer/concepts/strategy_concepts.hpp>

#include <boost/static_assert.hpp>
#include <boost/type_traits.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <cassert>

SOCCER_NAMESPACE_BEGIN

/*!
 \brief tactic_flux_selector
 */
template < typename TacticId,
           typename FluxId >
struct tactic_flux_selector
{

    typedef std::map< TacticId, FluxId> TacticFluxMap;
    TacticFluxMap M_tactic_flux_map;

    tactic_flux_selector()
    {

    }

    template < typename State,
               typename Strategy >
    void operator()( State const&, Strategy& strategy )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        BOOST_CONCEPT_ASSERT(( MutableStrategyConcept< Strategy > ));
        typedef typename tactic_id_type< Strategy >::type tactic_id_t;
        typedef typename flux_id_type< Strategy >::type flux_id_t;

        BOOST_STATIC_ASSERT_MSG(( boost::is_same< TacticId, tactic_id_t>::value ),
                                "TacticId types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< FluxId, flux_id_t >::value ),
                                "FluxId types mismatch");

        const auto _current_tactic_id = get_tactic_id( strategy );

        typename TacticFluxMap::const_iterator itf = M_tactic_flux_map.find( _current_tactic_id );
        assert( itf != M_tactic_flux_map.end() );

        set_flux_id( itf->second, strategy );
    }

    void add( TacticId tactic_id, FluxId flux_id )
    {
        assert( M_tactic_flux_map.find( tactic_id ) == M_tactic_flux_map.end() );
        M_tactic_flux_map.insert( std::make_pair( tactic_id, flux_id ));
    }
};


/*!
 * \brief The tactic_flux_selector_reader struct
 */
struct tactic_flux_selector_reader {

private:

    std::string read_line( std::istream& is )
    {
        std::string line_buf;
        while (std::getline( is, line_buf )) {
            if ( line_buf.empty()
                         || line_buf[0] == '#'
                         || ! line_buf.compare( 0, 2, "//" ) )
            {
                // TODO Does this ignore empty spaces?
                continue; // Ignore comments and empty lines
            }
            break;
        }
        return line_buf;
    }

    std::ifstream M_ifs;

public:
    tactic_flux_selector_reader( const std::string& file_name )
        : M_ifs( file_name.c_str(), std::ifstream::in )
    {

    }

    template < typename TacticId,
               typename FluxId >
    void operator()( tactic_flux_selector<TacticId, FluxId>& flux_selector )
    {
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< TacticId, int >::value ),
                                "TactidId type is invalid");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< FluxId, int >::value ),
                                "FluxId type is invalid");

        std::istringstream istr( this->read_line( M_ifs ) );
        // Read the number of tactics
        int num_tactics = -1;
        istr >> num_tactics;
        assert( num_tactics != -1 );

        for ( int t = 0 ; t < num_tactics; t++ )
        {
            // read line record
            istr.clear();
            istr.str( this->read_line( M_ifs ) );

            // 1st column => read tactic id
            int tactic_id = -1;
            istr >> tactic_id;
            assert( tactic_id != -1 );

            // 2nd column => read formation for each situation
            int flux_id = -1;
            istr >> flux_id;
            assert( flux_id != -1 );

            flux_selector.add( tactic_id, flux_id );
        }
    }
};



/*!
 * \brief check sanity of tactic flux selector based on strategy
 */
template < typename Strategy,
           typename TacticId,
           typename FluxId,
           typename TacticsContainer,
           typename FluxesContainer>
bool
tactic_flux_selector_sanity_check( const tactic_flux_selector<TacticId, FluxId>& flux_selector,
                                   TacticsContainer const& tactics_store,
                                   FluxesContainer const& fluxes_store)
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));
    typedef typename flux_id_type< Strategy >::type flux_id_t;
    typedef typename tactic_id_type< Strategy >::type tactic_id_t;

    BOOST_CONCEPT_ASSERT(( boost::UniqueAssociativeContainerConcept< TacticsContainer >));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< tactic_id_t, typename TacticsContainer::key_type >::value ),
                            "Strategy tactic id and flux selector tactic id types store mismatch");
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< tactic_id_t, TacticId>::value ),
                            "Strategy tactic id and flux selector tactic id types mismatch");

    BOOST_CONCEPT_ASSERT(( boost::UniqueAssociativeContainerConcept< FluxesContainer >));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< flux_id_t, typename FluxesContainer::key_type >::value ),
                            "Strategy flux id and flux selector flux id types in store mismatch");
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< flux_id_t, FluxId >::value ),
                            "Strategy flux id and flux selector flux id types mismatch");


    // Check if all flux ids in flux selector are registered
    bool sane = true;

    std::set< FluxId > unregistered_fluxes;
    std::set< TacticId > unregistered_tactics;

    for ( typename tactic_flux_selector<TacticId, FluxId>::TacticFluxMap::const_iterator it = flux_selector.M_tactic_flux_map.begin();
          it != flux_selector.M_tactic_flux_map.end(); it++ )
    {
        bool tactic_id_exists = tactics_store.find( it->first ) != tactics_store.end();
        if ( !tactic_id_exists )
        {
            unregistered_tactics.insert( it->first );
        }

        bool flux_id_exists = fluxes_store.find( it->second ) != fluxes_store.end();
        if ( !flux_id_exists && unregistered_fluxes.find( it->second ) == unregistered_fluxes.end() )
        {
            unregistered_fluxes.insert( it->second );
        }

        sane = sane && tactic_id_exists && flux_id_exists;
    }

    if ( !sane )
    {
        std::cerr << "tactic_flux_selector_sanity_check ERRORS" << std::endl;
        if ( !unregistered_tactics.empty() )
        {
            std::cerr << "Unregistered tactics = " << unregistered_tactics.size() << "[ ";
            for ( typename std::set< TacticId>::const_iterator itt = unregistered_tactics.begin();
                  itt != unregistered_tactics.end(); itt++ )
            {
                std::cerr << *itt << " ";
            }
            std::cerr << "]" << std::endl;
        }

        if ( !unregistered_fluxes.empty() )
        {
            std::cerr << "Unregistered fluxes= " << unregistered_fluxes.size() << "[ ";
            for ( typename std::set< FluxId>::const_iterator itt = unregistered_fluxes.begin();
                  itt != unregistered_fluxes.end(); itt++ )
            {
                std::cerr << *itt << " ";
            }
            std::cerr << "]" << std::endl;
        }

    }

    return sane;
}


SOCCER_NAMESPACE_END

#endif // SOCCER_FLUX_SELECTOR_HPP
