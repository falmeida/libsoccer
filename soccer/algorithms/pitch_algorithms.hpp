#ifndef SOCCER_PITCH_ALGORITHMS_HPP
#define SOCCER_PITCH_ALGORITHMS_HPP

//! Import relevant define macros
#include <soccer/defines.h>

//! Import relevant soccer concepts
#include <soccer/concepts/pitch_concept.hpp>
#include <soccer/concepts/position_concepts.hpp>

#include <soccer/core/access.hpp>

SOCCER_NAMESPACE_BEGIN

/*!
 * \brief Team goal line coordinate
 * \returns coordinate of the team goal line
 */
template < typename Pitch,
           typename Side >
typename coordinate_type< Pitch >::type
get_goal_line( Side const& side,
               Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        return side == get_left_side< Side >()
                ? get_left_goal_line( pitch )
                : get_right_goal_line( pitch );
}

/*!
 * \brief Team goal area line coordinate
 * \returns coordinate of the team goal area line
 */
template < typename Pitch,
           typename Side >
typename coordinate_type< Pitch >::type
get_goal_area_line( Side const& side,
                    Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        return side == get_left_side< Side >()
                ? get_left_goal_area_line( pitch )
                : get_right_goal_area_line( pitch );
}


/*!
 * \brief Team goal area line coordinate
 * \returns coordinate of the team goal area line
 */
template < typename Pitch,
           typename Side >
typename coordinate_type< Pitch >::type
get_penalty_area_line( Side const& side,
                       Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        return side == get_left_side< Side >()
                ? get_left_penalty_area_line( pitch )
                : get_right_penalty_area_line( pitch );
}

/*!
  \brief Check if a position is within a team's penalty area
  \param side the side of the team to consider
  \param position the position to check
  \param pitch the pitch
  \returns true if the position is within the team's penalty area or false otherwise
  */
template < typename Side,
           typename Pitch,
           typename Position >
bool
is_within_penalty_area( Side const& side,
                        Position const& position,
                        Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return side == get_left_side< Side >()
            ? is_within_left_penalty_area( position, pitch )
            : is_within_right_penalty_area( position, pitch );
}

/*!
  \brief Check if a position is within a team's goal area
  \param side the side of the team to consider
  \param position the position to check
  \param pitch the pitch
  \returns true if the position is within the team's goal area or false otherwise
  */
template < typename Side,
           typename Pitch,
           typename Position >
bool
is_within_goal_area( Side const& side,
                     Position const& position,
                     Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return side == get_left_side< Side >()
            ? is_within_left_goal_area( position, pitch )
            : is_within_right_goal_area( position, pitch );
}

/*!
  \brief Check if a position is within a team's goal area
  \param side the side of the team to consider
  \param position the position to check
  \param pitch the pitch
  \returns true if the position is within the team's goal area or false otherwise
  */
template < typename Side,
           typename Pitch,
           typename Position >
bool
is_within_midfield_area( Side const& side,
                              Position const& position,
                              Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return side == get_left_side< Side >()
            ? is_within_left_midfield_area( position, pitch )
            : is_within_right_midfield_area( position, pitch );
}

template < typename Pitch >
typename length_type< Pitch >::type
get_pitch_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_pitch_half_length( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_length( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_goal_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_goal_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_goal_area_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_goal_area_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_goal_area_half_length( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_goal_area_length( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_penalty_area_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_penalty_area_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_penalty_area_half_length( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_penalty_area_length( pitch ) / 2.0;
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_midfield_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_x( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_pitch_half_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_left_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_pitch_top_left_x( pitch );
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_right_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_x( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_pitch_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_left_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_left_goal_line( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_goal_area_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_right_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_right_goal_line( pitch ) -
                     (get_pitch_right_sign< Pitch >() * get_goal_area_length( pitch ));
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_left_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_left_goal_line( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_penalty_area_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_right_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_right_goal_line( pitch ) -
                     (get_pitch_right_sign< Pitch >() * get_penalty_area_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) - get_goal_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) + get_goal_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) - get_penalty_area_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) + get_penalty_area_half_width( pitch ) ));
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) - get_goal_area_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) + goal_area_half_width( pitch ) ));
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_sideline( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_pitch_top_left_y( pitch );
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_sideline( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() * get_pitch_width( pitch ));
    return res;
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_top_left_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ),
                           get_pitch_top_left_y( pitch ) );
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_top_right_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ) + get_pitch_length( pitch ),
                           get_pitch_top_left_y( pitch ) );
}


template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_bottom_left_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_width( pitch ) );
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_bottom_right_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ) + get_pitch_length( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_width( pitch ));
}

/************************
 * Goal Center Position *
 ************************/
template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_center_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_center_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_half_width( pitch ));
}

/*!
 * \brief Team goal center position
 * \returns position of the goal center for the team side
 */
template < typename OutputPosition,
           typename Side,
           typename Pitch >
OutputPosition
get_goal_center_position( Side const& side,
                          Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<OutputPosition> ));

        return side == get_left_side< Side >()
                ? get_left_goal_center_position<OutputPosition>( pitch )
                : get_right_goal_center_position<OutputPosition>( pitch );
}

/****************************
 * Goal Upper Post Position *
 ****************************/

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_upper_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) -
                           get_goal_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_upper_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) -
                           get_goal_half_width( pitch ));
}

/*!
 * \brief Team goal upper post position
 * \returns position of the goal upper post for the team side
 */
template < typename OutputPosition,
           typename Pitch,
           typename Side >
OutputPosition
get_goal_upper_post( Side const& side,
                     Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    BOOST_CONCEPT_ASSERT(( MutablePositionConcept<OutputPosition> ));

    return side == get_left_side< Side >()
        ? get_left_goal_upper_post<OutputPosition>( pitch )
        : get_right_goal_upper_post<OutputPosition>( pitch );
}


/****************************
 * Goal Lower Post Position *
 ****************************/


template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_lower_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) +
                           get_goal_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_lower_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) +
                           get_goal_half_width( pitch ));
}


/*!
 * \brief Team goal lower post position
 * \returns position of the goal lower post for the team side
 */
template < typename OutputPosition,
           typename Pitch,
           typename Side >
OutputPosition
get_goal_lower_post( Side const& side,
                     Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<OutputPosition> ));

        return side == get_left_side< Side >()
                ? get_left_goal_lower_post<OutputPosition>( pitch )
                : get_right_goal_lower_post<OutputPosition>( pitch );
}


/*!
 * \brief get_kick_off_position
 * \param pitch
 * \return
 */
template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_kick_off_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ) + get_pitch_half_length( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_half_width( pitch ));
}


/*!
 * \brief get_left_goal_kick_upper_position
 * \param pitch
 */
template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_kick_upper_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_area_line( pitch ),
                           get_top_goal_area_line( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_kick_upper_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_area_line( pitch ),
                           get_top_goal_area_line( pitch ));
}

/*!
 * \brief get_left_goal_kick_lower_position
 * \param pitch
 * \return
 */
template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_kick_lower_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_area_line( pitch ),
                           get_bottom_goal_area_line( pitch ));
}

/*!
 * \brief get_right_goal_kick_lower_position
 * \param pitch
 * \return
 */
template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_kick_lower_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( right_goal_area_line( pitch ),
                           bottom_goal_area_line( pitch ));
}

/*!
 * \struct check if a position is within the pitch
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_pitch( InputPosition const& pos,
                      Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_bottom_sideline( pitch );
}

/*!
 * \struct check if a position is within the left penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_left_midfield_area( InputPosition const& pos,
                                   Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_midfield_line( pitch ) &&
           get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_bottom_side_line( pitch );
}

/*!
 * \struct check if a position is within the right penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_right_midfield_area( InputPosition const& pos,
                                    Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_x( pos ) >= get_midfield_line( pitch ) &&
           get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_bottom_side_line( pitch );
}

/*!
 * \struct check if a position is within the left penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_left_penalty_area( InputPosition const& pos,
                                  Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_left_penalty_area_line( pitch ) &&
           get_y( pos ) >= get_top_penalty_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_penalty_area_line( pitch );
}

/*!
 * \struct check if a position is within the right penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_right_penalty_area( InputPosition const& pos,
                                   Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_x( pos ) >= get_right_penalty_area_line( pitch ) &&
           get_y( pos ) >= get_top_penalty_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_penalty_area_line( pitch );
}

/*!
 * \struct check if a position is within the left goal area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_left_goal_area( InputPosition const& pos,
                               Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_left_goal_area_line( pitch ) &&
           get_y( pos ) >= get_top_goal_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_goal_area_line( pitch );
}

/*!
 * \struct check if a position is within the right penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_right_goal_area( InputPosition const& pos,
                                Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_x( pos ) >= get_right_goal_area_line( pitch ) &&
           get_y( pos ) >= get_top_goal_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_goal_area_line( pitch );
}



template < typename InputPosition,
           typename Pitch >
bool is_within_upper_field( InputPosition const& pos,
                            Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_top_sideline( pitch ) + get_pitch_half_width( pitch );
}


/*!
 * \brief Determine the left team's goal post position nearest to a given position
 */
template < typename Position,
           typename Pitch,
           typename OutputPosition = Position >
OutputPosition
get_left_team_near_goal_post( Position const& pos,
                              Pitch const& pitch)
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    auto _left_goal_upper_post = get_left_goal_upper_post< Position >( pitch );
    auto _left_goal_lower_post = get_left_goal_lower_post< Position >( pitch );
    auto left_goal_upper_post_dist = distance( _left_goal_upper_post, pos );
    auto left_goal_lower_post_dist = distance( _left_goal_lower_post, pos );
    return left_goal_upper_post_dist < left_goal_lower_post_dist
            ? _left_goal_upper_post
            : _left_goal_lower_post;
}

/*!
 * \brief Determine the right team's goal post position nearest to a given position
 */
template < typename Position,
           typename Pitch,
           typename OutputPosition = Position >
OutputPosition
get_right_team_near_goal_post( Position const& pos,
                               Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    auto _right_goal_upper_post = get_right_goal_upper_post< Position >( pitch );
    auto _right_goal_lower_post = get_right_goal_lower_post< Position >( pitch );
    auto right_goal_upper_post_dist = distance( _right_goal_upper_post, pos );
    auto right_goal_lower_post_dist = distance( _right_goal_lower_post, pos );
    return right_goal_upper_post_dist < right_goal_lower_post_dist
            ? _right_goal_upper_post
            : _right_goal_lower_post;
}

/*!
 * \brief Determine the team's goal post nearest to a given position
 */
template < typename TeamSide,
           typename Position,
           typename Pitch,
           typename OutputPosition = Position >
OutputPosition
get_team_near_goal_post( TeamSide const& side,
                         Position const& pos,
                         Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));

    return side == get_left_side< TeamSide >()
            ? get_left_team_near_goal_post< Position, Pitch, OutputPosition >( pos, pitch )
            : get_right_team_near_goal_post< Position, Pitch, OutputPosition >( pos, pitch );
}

/*!
 * \brief Constrain a position within the pitch region
 * \tparam Pitch model of a soccer pitch
 * \tparam Position mutable model of a position
 * \param position the position to be constrained
 * \param pitch instante of the soccer pitch
 * \param left_offset left padding
 * \param right_offset right padding
 * \param top_offset top padding
 * \param bottom_offset bottom padding
 */
template < typename Position,
           typename Pitch >
void constrain_position_to_pitch( Position& position,
                                  Pitch const& pitch,
                                  typename coordinate_type< Pitch >::type left_padding = 0,
                                  typename coordinate_type< Pitch >::type right_padding = 0,
                                  typename coordinate_type< Pitch >::type top_padding = 0,
                                  typename coordinate_type< Pitch >::type bottom_padding = 0 )
{
    BOOST_CONCEPT_ASSERT(( MutablePositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    // static field variables
    static const auto pitch_left = get_left_goal_line( pitch );
    static const auto pitch_right = get_right_goal_line( pitch );
    static const auto pitch_top = get_top_sideline( pitch );
    static const auto pitch_bottom = get_bottom_sideline( pitch );

    if ( get_x( position ) < pitch_left )
    {
        set_x( pitch_left + left_padding , position );
    }
    else if( get_x( position ) > pitch_right )
    {
        set_x( pitch_right - right_padding , position);
    }

    if ( get_y( position ) < pitch_top )
    {
        set_y( pitch_top + top_padding, position);
    }
    else if ( get_y( position ) > pitch_bottom )
    {
        set_y( pitch_bottom - bottom_padding, position);
    }
}

/*!
 \brief Constrain a position to the soccer pitch
 \tparam Pitch model
 */
template < typename Pitch >
struct ConstrainPositionToPitch
{
private:
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    typedef typename coordinate_type< Pitch >::type coordinate_t;

    Pitch const& M_pitch;
    coordinate_t M_pitch_left_padding;
    coordinate_t M_pitch_right_padding;
    coordinate_t M_pitch_top_padding;
    coordinate_t M_pitch_bottom_padding;

public:
    ConstrainPositionToPitch( Pitch const& pitch )
        : M_pitch( pitch )
        , M_pitch_left_padding( 0 )
        , M_pitch_right_padding( 0 )
        , M_pitch_top_padding( 0 )
        , M_pitch_bottom_padding( 0 )
    {

    }

    ConstrainPositionToPitch( Pitch const& pitch,
                              const coordinate_t padding )
        : M_pitch( &pitch )
        , M_pitch_left_padding( padding )
        , M_pitch_right_padding( padding )
        , M_pitch_top_padding( padding )
        , M_pitch_bottom_padding( padding )
    {
        assert( padding >= 0 );
    }

    ConstrainPositionToPitch( Pitch const& pitch,
                              const coordinate_t horizontal_padding,
                              const coordinate_t vertical_padding )
        : M_pitch( pitch )
        , M_pitch_left_padding( horizontal_padding )
        , M_pitch_right_padding( horizontal_padding )
        , M_pitch_top_padding( vertical_padding )
        , M_pitch_bottom_padding( vertical_padding )
    {
        assert( horizontal_padding >= 0 );
        assert( vertical_padding >= 0 );
    }

    ConstrainPositionToPitch( Pitch const& pitch,
                              const coordinate_t left_padding,
                              const coordinate_t right_padding,
                              const coordinate_t top_padding,
                              const coordinate_t bottom_padding )
        : M_pitch( pitch )
        , M_pitch_left_padding( left_padding )
        , M_pitch_right_padding( right_padding )
        , M_pitch_top_padding( top_padding )
        , M_pitch_bottom_padding( bottom_padding )
    {
        assert( left_padding >= 0 );
        assert( right_padding >= 0 );
        assert( top_padding >= 0 );
        assert( bottom_padding >= 0 );
    }

    template < typename Position >
    void operator()( Position& position )
    {
        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<Position> ));

        return constrain_position_to_pitch( position,
                                            M_pitch,
                                            M_pitch_left_padding,
                                            M_pitch_right_padding,
                                            M_pitch_top_padding,
                                            M_pitch_bottom_padding );
    }
};

SOCCER_NAMESPACE_END

#endif // SOCCER_PITCH_ALGORITHMS_HPP
