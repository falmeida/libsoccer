#ifndef SOCCER_PASS_ALGORITHMS_HPP
#define SOCCER_PASS_ALGORITHMS_HPP

//! Include useful macros
#include <soccer/defines.h>

#include <soccer/concepts/pass_concept.hpp>
#include <soccer/state_algorithms.hpp>

#include <boost/iterator/filter_iterator.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <boost/concept_check.hpp>

SOCCER_NAMESPACE_BEGIN

// Receiver pre-selector
template <typename Scene>
struct AdjacentReceiverSelector {

};



// PassSuccessChecker algorithms


/// PassGenerator algorithms
/*!
  * 1) Pre-select receivers
  * 2) For each pre-selected receiver:
  * 2.1) Find relevant opponents
  * 2.2) If any relevant opponent can intercept
  */


template < typename PassGenerator, // Generate passes from two player ids, a range of opponent threats and state
           typename State,
           typename PlayerIdGraph,
           typename PassOutputIterator
           >
void generate_direct_passes( typename player_id_type< State >::type const sender_pid,
                             State const& state,
                             PlayerIdGraph const& pid_graph,
                             PassGenerator pass_generator,
                             PassGenerator pass_checker,
                             PassOutputIterator out_passes )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::AdjacencyGraph< PlayerIdGraph > ));

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_id_type< State >::type,
                                              typename boost::graph_traits< PlayerIdGraph >::vertex_descriptor >::value),
                              "State player id and graph vertex descriptor types mismatch");

    typedef boost::graph_traits< PlayerIdGraph >::adjacency_iterator adjacency_iterator;

    auto sender_side = get_side( get_player( sender_pid, state ));

    // Filter iterator selection predicates
    auto receiver_selection_pred = [&state, sender_side]( player_id_t const pid )
        {
            return get_side( get_player( pid, state ) ) == sender_side;
        };
    auto opponent_selection_pred = std::not1( receiver_selection_pred );

    adjacency_iterator sender_adjacent_pid_begin, sender_adjacent_pid_end;
    boost::tie( sender_adjacent_pid_begin, sender_adjacent_pid_end ) = boost::adjacent_vertices( sender_pid, pid_graph );
    auto receiver_pid_begin = boost::make_filter_iterator( receiver_selection_pred,
                                                           sender_adjacent_pid_begin,
                                                           sender_adjacent_pid_end );
    auto receiver_pid_end = boost::make_filter_iterator( receiver_selection_pred,
                                                         sender_adjacent_pid_end,
                                                         sender_adjacent_pid_end );
    for ( auto it_receiver_pid = receiver_pid_begin; it_receiver_pid  != receiver_pid_end; it_receiver_pid ++ )
    {
        auto receiver_pid = *it_receiver_pid;
        adjacency_iterator receiver_adjacent_pid_begin, receiver_adjacent_pid_end;
        boost::tie( receiver_adjacent_pid_begin, receiver_adjacent_pid_end ) = boost::adjacent_vertices( receiver_pid, pid_graph );

        auto opponent_threat_pid_begin = boost::make_filter_iterator( opponent_selection_pred,
                                                                      receiver_adjacent_pid_begin,
                                                                      receiver_adjacent_pid_end );
        auto opponent_threat_pid_end = boost::make_filter_iterator( opponent_selection_pred,
                                                                    receiver_adjacent_pid_end,
                                                                    receiver_adjacent_pid_end );

        // Check if pass can be generated to receiver based on its adjacent opponents
        pass_generator( sender_pid,
                        receiver_pid,
                        opponent_threat_pid_begin,
                        opponent_threat_pid_end,
                        state,
                        out_passes );
    }
}


template < typename PassGenerator, // Generate passes from two player ids and state
           typename State,
           typename PlayerIdInputIterator,
           typename PassOutputIterator
           >
void generate_passes( typename player_id_type< State >::type const from_pid,
                      State const& state,
                      PlayerIdInputIterator player_begin,
                      PlayerIdInputIterator player_end,
                      PassGenerator pass_generator,
                      PassOutputIterator out_passes )
{
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< PlayerIdInputIterator > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value< PlayerIdInputIterator >::type,
                                              typename player_id_type< State >::type >::value),
                              "Pass and state player id types mismatch");

    for ( auto itp = player_begin; itp != player_end ; itp++ )
    {
        auto _pass = pass_generator( from_pid, *itv, state );
        if ( _pass )
        {
           *out_passes = _pass;
        }
    }
}


SOCCER_NAMESPACE_END


#endif // SOCCER_PASS_ALGORITHMS_HPP
