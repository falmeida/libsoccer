#ifndef SOCCER_TACTIC_SELECTOR_HPP
#define SOCCER_TACTIC_SELECTOR_HPP

//! Include useful macros
#include <soccer/defines.h>

//! Include relevant concepts to be checked
#include <soccer/concepts/soccer_concepts.hpp>
#include <soccer/concepts/strategy_concepts.hpp>

#include <boost/type_traits.hpp>
#include <boost/functional.hpp>

#include <boost/static_assert.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <algorithm>

namespace soccer {

/*!
 \brief Tactics selection based on state assessment (categorizer) functor
 */
template <typename Strategy,
          typename StateAssessmentFunctor>
struct state_assessment_tactic_selector
{
    BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));

    typedef boost::unary_traits< StateAssessmentFunctor > state_assessment_functor_t;
    typedef typename state_assessment_functor_t::result_type state_assessment_category;
    typedef typename state_assessment_functor_t::argument_type state_descriptor;

    BOOST_STATIC_ASSERT_MSG(( boost::is_enum< state_assessment_category >::value),
                             "Result type of StateAssessmentFunctor must be an enum" );

    typedef typename tactic_id_type<Strategy>::type tactic_id_t;

    // BOOST Writable Property Map Traits
    typedef state_assessment_category key_type;
    typedef tactic_id_t value_type;

    typedef std::map< state_assessment_category, tactic_id_t > CategorizedTactics;
    CategorizedTactics m_tactics_by_state_assessment_category;

    StateAssessmentFunctor M_state_assessor;

    state_assessment_tactic_selector( const StateAssessmentFunctor state_assessor )
        : M_state_assessor( state_assessor )
    {

    }

    template < typename State >
    void operator()( State const& state,
                     Strategy& strategy )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< State, state_descriptor >::value),
                                 "StateAssessmentFunctor argument must be of type State" );

        const auto category = M_state_assessor( state );

        const auto itf = m_tactics_by_state_assessment_category.find( category );

        // TODO Allow default tactic ?
        assert( itf != m_tactics_by_state_assessment_category.end() );

        const auto new_tactic_id = itf->second;
        set_current_tactic_id( new_tactic_id, strategy );
    }
};


/*!
 \brief Tactics selection based on state assessment (categorizer) functor
 */
template < typename OpponentIdFunctor,
           typename ScoreAssessmentFunctor,
           typename TacticId >
struct time_opponent_score_tactic_selector
{
    typedef boost::unary_traits< ScoreAssessmentFunctor > score_assessment_functor_type;
    typedef typename score_assessment_functor_type::result_type score_assessment_type;

    BOOST_CONCEPT_ASSERT(( StateConcept<typename score_assessment_functor_type::argument_type > ));

    typedef boost::unary_traits< OpponentIdFunctor > opponent_id_functor_type;
    typedef typename opponent_id_functor_type::result_type opponent_id_type;

    typedef typename opponent_id_functor_type::argument_type state_type;

    BOOST_CONCEPT_ASSERT(( StateConcept< state_type > ));

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename score_assessment_functor_type::argument_type,
                              typename opponent_id_functor_type::argument_type >::value),
                            "Functors argument types mismatch");

    typedef typename time_type< state_type >::type time_descriptor;

    typedef std::map< time_descriptor, TacticId > TimeTacticMap;
    typedef std::map<score_assessment_type, TimeTacticMap> ScoreTimeTacticMap;
    typedef std::map<opponent_id_type, ScoreTimeTacticMap> OpponentScoreTimeTacticMap;
    ScoreTimeTacticMap M_score_time_tactic_map;
    OpponentScoreTimeTacticMap M_opponent_score_time_tactic_map;


    OpponentIdFunctor M_opponent_id_functor;
    ScoreAssessmentFunctor M_score_assessment_functor;

    time_opponent_score_tactic_selector(  OpponentIdFunctor opponent_id_functor,
                                          ScoreAssessmentFunctor score_assessment_functor )
        : M_opponent_id_functor( opponent_id_functor )
        , M_score_assessment_functor( score_assessment_functor )
    {

    }

    template < typename State, typename Strategy >
    void operator()( State const& state, Strategy& strategy )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< State, state_type >::value),
                                 "State types do not match" );

        BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));

        typedef typename tactic_id_type< Strategy >::type tactic_id_descriptor;

        BOOST_STATIC_ASSERT_MSG(( boost::is_same< TacticId, tactic_id_descriptor>::value),
                                 "TacticId types do not match" );

        // TODO Find opponent id
        const auto opponent_id = M_opponent_id_functor( state );
        const auto itof = M_opponent_score_time_tactic_map.find( opponent_id );
        assert( itof != M_opponent_score_time_tactic_map.end() );

        const ScoreTimeTacticMap& score_time_tactics = itof->second;
        const auto score_assessment = M_score_assessment_functor( state );
        const auto itsf = score_time_tactics.find( score_assessment );

        assert( itsf != score_time_tactics.end() );

        const TimeTacticMap& time_tactics = itsf->second;
        typename TimeTacticMap::const_iterator itt = time_tactics.begin();
        const auto _time = get_time( state );
        while ( itt != time_tactics.end() && _time > itt->first )
        {
            itt++;
        }

        assert( itt != time_tactics.end() );

        const auto new_tactic_id = itt->second;

        set_tactic_id( new_tactic_id, strategy );
    }

    virtual void add_tactic( opponent_id_type opponent_id,
                             score_assessment_type score_assessment,
                             time_descriptor end_time,
                             TacticId tactic_id )
    {
        typename TimeTacticMap::value_type time_tactic_value = std::make_pair( end_time, tactic_id );

        typename OpponentScoreTimeTacticMap::iterator ito = M_opponent_score_time_tactic_map.find( opponent_id );
        if ( ito == M_opponent_score_time_tactic_map.end() )
        {
            ScoreTimeTacticMap score_time_tactics;
            TimeTacticMap time_tactics;
            time_tactics.insert( time_tactic_value );
            score_time_tactics.insert( typename ScoreTimeTacticMap::value_type( score_assessment, time_tactics) );
            M_opponent_score_time_tactic_map.insert( typename OpponentScoreTimeTacticMap::value_type( opponent_id, score_time_tactics ));
            return;
        }

        ScoreTimeTacticMap& score_time_tactics = ito->second;
        typename ScoreTimeTacticMap::iterator its = score_time_tactics.find( score_assessment );
        if ( its == score_time_tactics.end() )
        {
            TimeTacticMap time_tactics;
            time_tactics.insert( time_tactic_value );
            score_time_tactics.insert(typename ScoreTimeTacticMap::value_type( score_assessment, time_tactics));
            return;
        }

        TimeTacticMap& time_tactics = its->second;
        time_tactics.insert( time_tactic_value );
    }

};

/*!
 * \brief The time_opponent_score_tactic_selector_reader struct
 */
struct time_opponent_score_tactic_selector_reader
{
private:

    std::string read_line( std::istream& is )
    {
        std::string line_buf;
        while (std::getline( is, line_buf )) {
            if ( line_buf.empty()
                         || line_buf[0] == '#'
                         || ! line_buf.compare( 0, 2, "//" ) )
            {
                // TODO Does this ignore empty spaces?
                continue; // Ignore comments and empty lines
            }
            break;
        }
        return line_buf;
    }

    std::ifstream M_ifs;

public:
    time_opponent_score_tactic_selector_reader( const std::string& file_name )
        : M_ifs( file_name.c_str(), std::ifstream::in )
    {

    }

    template <typename OpponentIdFunctor,
              typename ScoreAssessmentFunctor,
              typename TacticId>
    void operator()( time_opponent_score_tactic_selector<OpponentIdFunctor, ScoreAssessmentFunctor, TacticId>& tactic_selector )
    {
        // BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));
        // BOOST_STATIC_ASSERT_MSG(( boost::is_same< Opponent))

        assert( M_ifs.is_open() );
        std::istringstream istr( this->read_line( M_ifs ) );

        // Read the number of tactics
        int num_time_tactics = -1;
        istr >> num_time_tactics;
        assert( num_time_tactics != -1 );

        // Read the number of opponents to which the tactics apply
        int num_opponents = -1;
        istr >> num_opponents;
        assert( num_opponents != -1 );

        std::cout << "time_opponent_score_tactic_selector_reader" << std::endl;
        std::cout << "NumTimeTactics=" << num_time_tactics
                  << " NumOpponents=" << num_opponents
                  << std::endl;

        for ( int tt = 0; tt < num_time_tactics ; tt++ )
        {
            istr.clear();
            istr.str( read_line( M_ifs ));
            // Read one time tactic per line
            int end_time = -1;
            istr >> end_time;
            assert( end_time != -1 );

            for ( int o = 0; o < num_opponents; o++ )
            {
                int opponent_id = -1;
                istr >> opponent_id ;
                assert( opponent_id != -1 );

                static const int num_score_assessments = 5;
                // Num of score tactics
                for ( int sa = 0; sa < num_score_assessments; sa++ )
                {
                    // read tactic id
                    int tactic_id = -1;
                    istr >> tactic_id ;
                    assert( tactic_id != -1 );

                    soccer::ScoreAssessment score_assessment = soccer::integer_to_score_assessment_map.at( sa );
                    tactic_selector.add_tactic( opponent_id,
                                                score_assessment,
                                                end_time,
                                                tactic_id );
                    std::cout << "Read tactic (oppId,ScoreAssessment,EndTime,TacticId)="
                              << opponent_id << ","
                              << sa << ","
                              << end_time << ","
                              << tactic_id
                              << std::endl;
                }

            }

        }

    }

};


/*!
 * \brief check sanity of tactic_state_assessment formation selector based on strategy
 */
template < typename Strategy,
           typename OpponentIdFunctor,
           typename ScoreAssessmentFunctor,
           typename TacticId >
bool
time_opponent_score_tactic_selector_sanity_check( const time_opponent_score_tactic_selector< OpponentIdFunctor,
                                                                                             ScoreAssessmentFunctor,
                                                                                             TacticId >& tactic_selector )
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));

    typedef soccer::strategy_traits<Strategy> strategy_traits_t;
    typedef typename tactic_id_type<Strategy>::type strategy_tactic_id_t;

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< strategy_tactic_id_t, TacticId >::value),
                            "Tactic id descriptor types of strategy and tactic selector mismatch");

    typedef time_opponent_score_tactic_selector<OpponentIdFunctor,
                                                ScoreAssessmentFunctor,
                                                TacticId> tactic_selector_t;

    bool sane = true;

    std::set< TacticId > unregistered_tactics;

    for ( typename tactic_selector_t::OpponentScoreTimeTacticMap::const_iterator ito = tactic_selector.M_opponent_score_time_tactic_map.begin();
          ito != tactic_selector.M_opponent_score_time_tactic_map.end(); ito++ )
    {
        for ( typename tactic_selector_t::ScoreTimeTacticMap::const_iterator its = ito->second.begin();
              its != ito->second.end(); its++ )
        {
            for ( typename tactic_selector_t::TimeTacticMap::const_iterator itt = its->second.begin();
                  itt != its->second.end(); itt++ )
            {
                bool tactic_id_exists = strategy_traits_t::has_tactic_id( itt->second );
                sane = sane && tactic_id_exists;
                if ( !tactic_id_exists && unregistered_tactics.find( itt->second ) == unregistered_tactics.end() )
                {
                    unregistered_tactics.insert( itt->second );
                }
            }
        }
    }

    if ( !sane )
    {
        std::cerr << "time_opponent_score_tactic_selector SANITY CHECK ERRORS " << std::endl;

        std::cerr << "Unregistered tactics = " << unregistered_tactics.size() << "[ ";
        for ( typename std::set< TacticId >::const_iterator itt = unregistered_tactics.begin();
              itt != unregistered_tactics.end(); itt++ )
        {
            std::cerr << *itt << " ";
        }
        std::cerr << "]" << std::endl;
    }

    return sane;
}

}


namespace boost {

    template <typename Strategy,
              typename StateAssessmentFunctor>
    void put( soccer::state_assessment_tactic_selector<Strategy, StateAssessmentFunctor>& sats,
              const typename soccer::state_assessment_tactic_selector<Strategy, StateAssessmentFunctor>::key_type key,
              const typename soccer::state_assessment_tactic_selector<Strategy, StateAssessmentFunctor>::value_type value)
    {
        sats.m_tactics_by_state_assessment_category.insert( std::make_pair( key, value ));
    }
}
#endif // SOCCER_TACTIC_SELECTOR_HPP
