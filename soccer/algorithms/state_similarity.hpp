#ifndef SOCCER_ALGORITHMS_STATE_SIMILARITY_HPP
#define SOCCER_ALGORITHMS_STATE_SIMILARITY_HPP

//! Include useful macros
#include <soccer/defines.h>

#include <soccer/core/access.hpp>
#include <soccer/geometry.hpp>

#include <soccer/algorithms/state_algorithms.hpp>

#include <soccer/boost/matching.hpp>

#include <boost/concept_check.hpp>

#include <map>
#include <functional>

SOCCER_NAMESPACE_END

/*!
 * \brief Calculate the cost of matching players between states
 * \tparam State Model of a soccer state
 * \tparam PlayerMatchingMap Model of a map matching player ids from state S1 to S2
 * \tparam PlayerMatchingCostFunction Model of a function that calculates a cost of matching players is a state
 * \tparam PlayerIdMatchingMap Model of an output property map that stores cost of matching player ids from S1 and S2
 * \param s1
 * \param s2
 * \param left_player_matching_cost_fn
 * \param right_player_matching_cost_fn
 * \param out_player_matching_pmap
 * \return
 */
template < typename State,
           typename PlayerMatchingMap,
           typename PlayerMatchingCostFunction,
           typename PlayerIdMatchingMap >
long
inter_state_player_matching( typename side_type< typename player_type< State >::type >::type side,
                             State const& s1,
                             State const& s2,
                             PlayerMatchingCostFunction player_matching_cost_fn,
                             PlayerIdMatchingMap out_player_matching_pmap )
{
    BOOST_CONCEPT_CHECK(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    BOOST_CONCEPT_CHECK(( boost::WritablePropertyMapConcept< PlayerIdMatchingMap, player_id_t >));

    auto _s1_team_players_iterators = get_players( side, s1 );
    auto _s2_team_players_iterators = get_players( side, s2 );


    const auto _matching_cost = matching( _s1_team_players_iterators.first, _s1_team_players_iterators.second,
                                          _s2_team_players_iterators.first, _s2_team_players_iterators.second,
                                          player_matching_cost_fn,
                                          out_player_matching_pmap );

    return _matching_cost;
}


/*!
 * \brief Calculate the similarity between 2 states
 * \tparam State Model of a soccer state
 * \tparam BallMatchingCostFunction Cost function for matching the ball
 * \tparam LeftPlayerMatchingCostFunction Cost function for matching players of the left team
 * \tparam RightPlayerMatchingCostFunction Cost function for matching players of the right team
 * \tparam PlayerIdMatchingMap WritablePropertyMap with the resulting matching of players
 * \param s2
 * \param left_player_matching_cost_fn
 * \param right_player_matching_cost_fn
 * \param out_player_matching_pmap
 * \return
 */
template < typename State,
           typename PlayerMatchingMap,
           typename BallMatchingCostFunction,
           typename LeftPlayerMatchingCostFunction,
           typename RightPlayerMatchingCostFunction,
           typename PlayerIdMatchingMap >
// TODO Figure out a way to deduce an adequate return type
auto
state_similarity( State const& s1,
                  State const& s2,
                  BallMatchingCostFunction ball_matching_cost_fn,
                  LeftPlayerMatchingCostFunction left_player_matching_cost_fn,
                  RightPlayerMatchingCostFunction right_player_matching_cost_fn,
                  PlayerIdMatchingMap out_player_matching_pmap )
{
    BOOST_CONCEPT_CHECK(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    BOOST_CONCEPT_CHECK(( boost::WritablePropertyMapConcept< PlayerIdMatchingMap, player_id_t >));

    auto _left_players_matching_cost = inter_state_player_matching( get_left_side< side_t >(), s1, s2, left_player_matching_cost_fn, out_player_matching_pmap );
    auto _right_players_matching_cost = inter_state_player_matching( get_right_side< side_t >(), s1, s2, right_player_matching_cost_fn, out_player_matching_pmap );

    const auto _ball_matching_cost = ball_matching_cost_fn( get_ball( s1 ), get_ball( s2 ) );

    const auto total_cost = _ball_matching_cost +
                            _left_players_matching_cost +
                            _right_players_matching_cost;
    return total_cost;
}

/*!
 * \brief Calculate the similarity between 2 states with a single player matching cost function
 * \tparam State Model of a soccer state
 * \tparam BallMatchingFunction Cost function for matching the ball
 * \tparam PlayerMatchingFunction Cost function for matching players of the right team
 * \tparam PlayerIdMatchingMap WritablePropertyMap with the resulting matching of players
 * \param s1
 * \param s2
 * \param player_matching_cost_fn
 * \param out_player_matching_pmap
 * \return
 */
template < typename State,
           typename PlayerMatchingMap,
           typename PlayerMatchingCostFunction,
           typename PlayerIdMatchingMap >
auto
state_similarity( State const& s1,
                  State const& s2,
                  PlayerMatchingCostFunction player_matching_cost_fn,
                  PlayerIdMatchingMap out_player_matching_pmap )
{
    BOOST_CONCEPT_CHECK(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    BOOST_CONCEPT_CHECK(( boost::WritablePropertyMapConcept< PlayerIdMatchingMap, player_id_t >));

    return state_similarity( s1,
                             s2,
                             player_matching_cost_fn,
                             player_matching_cost_fn,
                             out_player_matching_pmap );
}

SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_SIMILARITY_HPP
