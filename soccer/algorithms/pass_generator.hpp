#ifndef SOCCER_PASS_GENERATOR_HPP
#define SOCCER_PASS_GENERATOR_HPP

namespace soccer {

template < typename TargetPlayerIdInputIterator,
           typename PassGenerator,
           typename PassCheckPredicate,
           typename ActionOutputIterator >
void generate_passes( TargetPlayerIdInputIterator player_begin,
                      TargetPlayerIdInputIterator player_end,
                      PassGenerator pass_generator,
                      PassCheckPredicate pass_checker,
                      ActionOutputIterator out_passes )
{
    for ( auto itp = player_begin; itp != player_end; itp++ ) {
        if ( pass_checker( *itp ) )
        {
            pass_generator( out_passes );
        }
    }
}


} // end namespace soccer

#endif // SOCCER_PASS_GENERATOR_HPP
