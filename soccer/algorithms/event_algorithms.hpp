#ifndef SOCCER_EVENT_ALGORITHMS_HPP
#define SOCCER_EVENT_ALGORITHMS_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import helper class traits
#include <soccer/traits/event_traits.hpp>

//! Import concepts to be checked
#include <soccer/concepts/event_concepts.hpp>

SOCCER_EVENTS_NAMESPACE_BEGIN

/*!
  * Concept interface
  * startTime( const Event& )
  * endTime( const Event& )
  * durationTime( const Event& )
  */
template < typename Event >
typename Event::time_descriptor
duration( const Event& e )
{
    BOOST_CONCEPT_ASSERT(( EventConcept< Event > ));
    return endTime( e ) - startTime( e );
}

SOCCER_EVENTS_NAMESPACE_END

#endif // SOCCER_EVENT_ALGORITHMS_HPP
