#ifndef SOCCER_ACTION_ALGORITHMS_HPP
#define SOCCER_ACTION_ALGORITHMS_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/concepts/action_concepts.hpp>

#include <soccer/traits/action_traits.hpp>

SOCCER_NAMESPACE_BEGIN

/*!
  \brief calculate the duration of the action
  */
template <typename Action>
typename duration_type< Action>::type
duration( const Action& action )
{
    return endTime( action ) - startTime( action );
}

/*!
  \brief calculate the duration of the action
  */
template <typename Action>
typename duration_type< Action>::type
distance( const Action& action )
{
    return distance( startPosition( action ), endPosition( action ) );
}



SOCCER_NAMESPACE_END

#endif // SOCCER_ACTION_ALGORITHMS_HPP
