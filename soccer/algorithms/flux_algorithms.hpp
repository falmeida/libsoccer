#ifndef SOCCER_FLUX_ALGORITHMS_HPP
#define SOCCER_FLUX_ALGORITHMS_HPP

//! Include useful macros
#include <soccer/defines.h>

//! Include necessary concepts for checking
#include <soccer/concepts/flux_concepts.hpp>
#include <soccer/concepts/pitch_concept.hpp>
#include <soccer/concepts/soccer_concepts.hpp>

#include <soccer/core/access.hpp>

#include <boost/type_traits.hpp>
#include <boost/utility/enable_if.hpp>

#include <boost/tuple/tuple.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/concepts/point_concept.hpp>

#include <set>
#include <queue>
#include <algorithm>
#include <functional>

SOCCER_NAMESPACE_BEGIN

template < typename Pitch,
           typename FluxMatrix,
           typename Position >
std::pair< typename matrix_size_type< FluxMatrix >::type,
           typename matrix_size_type< FluxMatrix >::type >
position_to_matrix_index( Pitch const& _pitch,
                          Position const& _position,
                          FluxMatrix const& _flux )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( FluxMatrixConcept<FluxMatrix> ));

    // static field variables
    const auto _pitch_length = get_length( _pitch );
    const auto _pitch_width = get_width( _pitch );

    const auto pitch_left = get_left_goal_line( _pitch );
    const auto pitch_right = get_right_goal_line( _pitch );
    const auto pitch_top = get_top_sideline( _pitch );
    const auto pitch_bottom = get_bottom_sideline( _pitch );

    const auto pos_x = get_x( _position );
    const auto pos_y = get_y( _position );

    // runtime assertions => position must be within field boundaries
    assert( pos_x >= pitch_left && pos_x <= pitch_right );
    assert( pos_y >= pitch_top && pos_y <= pitch_bottom );

    // convert pitch coordinates to flux matrix coordinates
    typedef typename matrix_size_type< FluxMatrix >::type index_t;
    const auto length_index = (index_t) std::floor( std::fabs( pos_x / length( _flux ) * 1.0 ) * _pitch_length );
    const auto width_index = (index_t) std::floor( std::fabs( pos_y / width( _flux ) * 1.0 ) * _pitch_width );

    return std::make_pair( width_index, length_index );
}

template < typename Pitch,
           typename Position,
           typename FluxMatrix >
typename flux_value_type< FluxMatrix >::type
flux_value( Pitch const& pitch,
            Position const& position,
            const FluxMatrix& fm,
            flux_matrix_tag )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( FluxMatrixConcept<FluxMatrix> ));
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    typedef typename matrix_size_type< FluxMatrix >::type index_t;
    index_t _width_idx, _length_idx;
    boost::tie( _width_idx, _length_idx ) = position_to_matrix_index( pitch, position, fm  );
    return flux_value( _width_idx, _length_idx, fm );
}

template < typename Position,
           typename FluxTriangulation >
typename flux_value_type< FluxTriangulation >::type
flux_value( Position const& position,
            FluxTriangulation const& ft,
            flux_triangulation_tag )
{
    BOOST_CONCEPT_ASSERT(( FluxTriangulationConcept<FluxTriangulation> ));

    return flux_position( position, ft );
}

/*!
 * \brief Calculate the value of the flux for a given position for the current state
 */
template < typename Position,
           typename Flux >
typename flux_value_type< Flux >::type
flux_value( Position const& position,
            Flux const& flux )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( FluxConcept<Flux> ));

    typename tag< Flux >::type category;
    return value( position, flux, category );
}

/*!
 * \brief Calculate the value of the flux for a given position for the current state
 */
template < typename Pitch,
           typename Position,
           typename FluxMatrix >
double
flux_direction( Pitch const& _pitch,
                Position const& _position,
                FluxMatrix const& _flux_matrix,
                flux_matrix_tag )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<FluxMatrix> ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( FluxMatrixConcept<FluxMatrix> ));


    // static field variables
    const auto pitch_length = get_pitch_length( _pitch );
    const auto pitch_width = get_pitch_width( _pitch );

    const auto pos_x = get_x(_position);
    const auto pos_y = get_y(_position);

    // convert pitch coordinates to flux matrix coordinates
    int ref_length_index = (int) std::floor( std::fabs( pos_x / length( _flux_matrix ) * 1.0 ) * pitch_length );
    int ref_width_index = (int) std::floor( std::fabs( pos_y / width( _flux_matrix ) * 1.0 ) * pitch_width );

    unsigned int best_flux_length_idx = ref_length_index;
    unsigned int best_flux_width_idx = ref_width_index;
    const auto _best_flux_value = min_value( _flux_matrix );

    const auto _flux_matrix_length = get_length( _flux_matrix ) - 1;
    const auto flux_matrix_width = get_width( _flux_matrix ) - 1;

    const auto min_length_index = std::min( 0, ref_length_index - 1 );
    const auto max_length_index = std::max( _flux_matrix_length, ref_length_index + 1 );

    const auto min_width_index = std::min( 0, ref_width_index - 1 );
    const auto max_width_index = std::max( flux_matrix_width - 1, ref_width_index + 1 );

    //! determine the best flux
    for ( auto length_idx = min_length_index; length_idx < max_length_index ; length_idx++ )
    {
        for ( auto width_idx = min_width_index; width_idx < max_width_index ; width_idx++ )
        {
            if ( length_idx == ref_length_index &&
                 width_idx == ref_width_index )
            {
                continue;
            }


            const auto flux_val = get_flux_value( length_idx, width_idx, _flux_matrix );
            if (  flux_val > _best_flux_value )
            {
                _best_flux_value = flux_val;
                best_flux_length_idx = length_idx;
                best_flux_width_idx = width_idx;
            }
        }
    }

    assert( best_flux_length_idx != ref_length_index
            && best_flux_width_idx != ref_width_index );

    // TODO calculate direction between ref_point and adj_point
    assert( false );
}

template < typename Pitch,
           typename Position,
           typename FluxTriangulation >
double
flux_direction( Pitch const& _pitch,
                Position const& _position,
                FluxTriangulation const& _flux,
                flux_triangulation_tag )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( FluxTriangulationConcept< FluxTriangulation > ));

    // static field variables
    const auto pitch_length = get_pitch_length( _pitch );
    const auto pitch_width = get_pitch_width( _pitch );

    const auto pos_x = get_x(_position);
    const auto pos_y = get_y(_position);

    assert( false );
    return 0.0;
    // convert pitch coordinates to flux triangulation coordinates
    // int ref_length_index = (int) std::floor( std::fabs( pos_x / length( flux_matrix ) * 1.0 ) * pitch_length );
    // int ref_width_index = (int) std::floor( std::fabs( pos_y / width( flux_matrix ) * 1.0 ) * pitch_width );

    /*unsigned int best_flux_length_index = ref_length_index;
    unsigned int best_flux_width_index = ref_width_index;
    typename flux_matrix_traits_t::value_descriptor best_flux = min_value( flux_matrix );

    int flux_matrix_length = length( flux_matrix ) - 1;
    int flux_matrix_width = width( flux_matrix ) - 1;

    unsigned int min_length_index = std::min( 0, ref_length_index - 1 );
    unsigned int max_length_index = std::max( flux_matrix_length, ref_length_index + 1 );

    unsigned int min_width_index = std::min( 0, ref_width_index - 1 );
    unsigned int max_width_index = std::max( flux_matrix_width - 1, ref_width_index + 1 );

    //! determine the best flux
    for ( unsigned int length_index = min_length_index; length_index < max_length_index ; length_index++ )
    {
        for ( unsigned int width_index = min_width_index; width_index < max_width_index ; width_index++ )
        {
            if ( length_index == ref_length_index &&
                 width_index == ref_width_index )
            {
                continue;
            }


            typename flux_matrix_traits_t::value_descriptor flux_val = value( length_index, width_index, flux_matrix );
            if (  flux_val > best_flux )
            {
                best_flux = flux_val;
                best_flux_length_index = length_index;
                best_flux_width_index = width_index;
            }
        }
    }

    assert( best_flux_length_index != ref_length_index
            && best_flux_width_index != ref_width_index );

    // TODO calculate direction between ref_point and adj_point
    assert( false ); */
}

template < typename Pitch,
           typename Position,
           typename Flux >
double
flux_direction( Pitch const& _pitch,
                Position const& _position,
                Flux const& _flux )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( FluxConcept<Flux> ));

    typename tag<Flux>::type category;

    const auto pitch_left = get_left_goal_line( _pitch );
    const auto pitch_right = get_right_goal_line( _pitch );
    const auto pitch_top = get_top_sideline( _pitch );
    const auto pitch_bottom = get_bottom_sideline( _pitch );

    // runtime assertions => position must be within field boundaries
    assert( soccer::get_x( _position ) >= pitch_left && soccer::get_x( _position ) <= pitch_right );
    assert( soccer::get_y( _position ) >= pitch_top && soccer::get_y( _position ) <= pitch_bottom );

    // Truncate position to field and ignore assertions

    return direction( _pitch, _position, _flux, category );
}

template < typename Position,
           typename FluxTriangulation >
typename vertex_type<FluxTriangulation>::type
nearest_vertex( Position const& pos,
                FluxTriangulation const& flux_triang )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));

    BOOST_CONCEPT_ASSERT(( FluxTriangulationConcept<FluxTriangulation> ));
    typedef typename triangulation_type< FluxTriangulation >::type triangulation_t;
    typedef typename vertex_type< triangulation_t >::type vertex_t;
    typedef typename vertex_iterator_type< triangulation_t >::type vertex_iterator_t;

    // TODO should use a more efficient strategy => should wrap this up in my own function
    typedef double distance_descriptor;

    const auto& _triangulation = get_triangulation( flux_triang );
    auto candidate = infinite_vertex( _triangulation );

    // ASSUMING distance_descriptor is a type for which numerical_limits are defined
    auto min_dist = std::numeric_limits<distance_descriptor>::max();

    vertex_iterator_t _vertex_begin, _vertex_end;
    for ( boost::tie(_vertex_begin, _vertex_end) = vertices( _triangulation ) ;
          _vertex_begin != _vertex_end; _vertex_begin++ )
    {
        if ( *_vertex_begin == infinite_vertex( _triangulation ) ) { continue; }

        auto dist = comparable_distance( position( *_vertex_begin, flux_triang ), pos );
        if ( dist < min_dist )
        {
            candidate = *_vertex_begin;
            min_dist = dist;
        }
    }

    return candidate;
}

template < typename Position,
           typename FluxTriangulation >
typename vertex_type< FluxTriangulation >::type
triangle_container( Position const& ,
                    FluxTriangulation const&  )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( FluxTriangulationConcept<FluxTriangulation> ));

    typedef typename triangulation_type< FluxTriangulation >::type triangulation_t;
    typedef typename vertex_type< triangulation_t >::type vertex_t;
    typedef typename vertex_iterator_type< triangulation_t >::type vertex_iterator_t;
    typedef typename face_iterator_type< triangulation_t >::type face_iterator_t;

    assert( false );
}


//
/*template < typename FluxTriangulation >
struct flux_triangulation_vertex_generator
{
    BOOST_CONCEPT_ASSERT(( FluxTriangulationConcept< FluxTriangulation > ));
    typedef typename triangulation_type< FluxTriangulation >::type triangulation_t;
    typedef typename flux_value_type< FluxTriangulation >::type flux_value_descriptor;

    std::set< size_t > M_closed_vertices;
    std::set< size_t > M_open_vertices;

    FluxTriangulation const* M_flux_graph;
    flux_position_descriptor M_start_position;

    flux_vertex_t M_start_vertex;
    flux_value_descriptor M_start_flux_value;

    typedef std::pair< flux_vertex_t, flux_value_descriptor > flux_queue_element_type;
    typedef std::function< bool ( const flux_queue_element_type&,
                                  const flux_queue_element_type&) > flux_queue_element_compare_type;
    typedef std::priority_queue< flux_queue_element_type,
                                 std::vector<flux_queue_element_type>,
                                 flux_queue_element_compare_type > flux_queue_type;
    flux_queue_type M_open;
    std::set< flux_vertex_t > M_closed;

    flux_triangulation_vertex_generator( FluxTriangulation const& flux,
                                         const flux_position_descriptor start_position )
        : M_flux_graph( &flux )
        , M_start_position( start_position )
        , M_open( [] ( const flux_queue_element_type& v1, const flux_queue_element_type& v2 )
                    {
                        return v1.second < v2.second;
                    } )
    {
        // Find vertex nearest to a position
        M_start_vertex = nearest_vertex( start_position, *M_flux_graph );
        // assert( M_start_vertex != flux_triangulation_traits_t::null_vertex() );

        update( M_start_vertex, std::numeric_limits< flux_value_descriptor >::max() );
    }

    flux_vertex_t
    operator()()
    {
        if ( M_open.empty() )
        {
            return flux_position_descriptor();
        }

        flux_queue_element_type _curr_elem = M_open.front();
        M_open.pop();
        M_closed.insert( _curr_elem.first );

        update( _curr_elem.first, _curr_elem.second );

        return _curr_elem.first;
    }

private:
    void update( const flux_vertex_t vd,
                 const flux_value_descriptor score )
    {
        // Update elements
        flux_vertex_iterator vfirst, vlast;
        boost::tie( vfirst, vlast ) = vertices( vd );
        for ( ; vfirst != vlast ; vfirst++ )
        {
            // Append all unvisited vertices
            if ( M_closed.find( *vfirst ) == M_closed.end() )
            {
                M_open.push( flux_queue_element_type( *vfirst, score  - flux_vertex( *vfirst ) ) );
            }
        }
    }
}; */

SOCCER_NAMESPACE_END

#endif // SOCCER_FLUX_ALGORITHMS_HPP
