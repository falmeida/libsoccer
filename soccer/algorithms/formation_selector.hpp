#ifndef SOCCER_FORMATION_SELECTOR_HPP
#define SOCCER_FORMATION_SELECTOR_HPP

//! Include useful macros
#include <soccer/defines.h>

//! Include relevant concepts
#include <soccer/concepts/soccer_concepts.hpp>
#include <soccer/concepts/strategy_concepts.hpp>

#include <soccer/types.h>

#include <soccer/core/access.hpp>

#include <boost/type_traits.hpp>
#include <boost/functional.hpp>

#include <fstream>
#include <sstream>
#include <cassert>
#include <map>

SOCCER_NAMESPACE_BEGIN

/*!
 \brief Formation selection based on state assessment (categorizer) functor
 */
template < typename FormationId,
           typename StateAssessmentFunctor >
struct state_assessment_formation_selector {

    typedef boost::unary_traits< StateAssessmentFunctor > state_assessment_functor_t;
    typedef typename state_assessment_functor_t::result_type state_assessment_category;
    typedef typename state_assessment_functor_t::argument_type state_t;

    BOOST_STATIC_ASSERT_MSG(( boost::is_enum< state_assessment_category >::value),
                             "Result type of StateAssessmentFunctor must be an enum" );

    // BOOST Writable Property Map Traits
    typedef state_assessment_category key_type;
    typedef FormationId value_type;

    typedef std::map< state_assessment_category, FormationId > CategorizedFormations;
    CategorizedFormations M_formations_by_state_assessment_category;

    StateAssessmentFunctor m_state_assessor;

    state_assessment_formation_selector( StateAssessmentFunctor state_assessor )
        : m_state_assessor( state_assessor )
    {

    }

    template < typename State,
               typename Strategy>
    void operator()( State const& state, Strategy& strategy )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< State, state_t >::value),
                                 "StateAssessmentFunctor argument must be of type State" );

        BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));
        typedef typename formation_id_type<Strategy>::type formation_id_t;

        BOOST_STATIC_ASSERT_MSG(( boost::is_same< FormationId, formation_id_t>::value),
                                  "Formation id descriptor types in formation selector and strategy mismatch");

        const state_assessment_category category = m_state_assessor( state );

        typename CategorizedFormations::const_iterator itf = M_formations_by_state_assessment_category.find( category );

        // TODO Allow default formation ?
        assert( itf != M_formations_by_state_assessment_category.end() );

        FormationId new_formation_id = itf->second;
        set_current_formation_id( new_formation_id, strategy );
    }
};

/*!
 \brief tactic_state_assessment_formation_selector
 */
template < typename FormationId,
           typename TacticId,
           typename StateAssessmentFunctor >
struct tactic_state_assessment_formation_selector
{
    typedef boost::unary_traits< StateAssessmentFunctor > state_assessment_functor_type;
    typedef typename state_assessment_functor_type::result_type state_assessment_type;
    typedef typename state_assessment_functor_type::argument_type state_type;

    BOOST_STATIC_ASSERT_MSG(( boost::is_enum< state_assessment_type >::value),
                             "Result type of StateAssessmentFunctor must be an enum" );

    BOOST_CONCEPT_ASSERT(( StateConcept<state_type> ));


    typedef std::map< state_assessment_type, FormationId > StateAssessmentFormationMap;
    typedef std::map< TacticId, StateAssessmentFormationMap > TacticStateAssessmentFormationMap;

    TacticStateAssessmentFormationMap M_tactic_state_assessment_formation_map;
    StateAssessmentFunctor M_state_assessment_functor;

    tactic_state_assessment_formation_selector( StateAssessmentFunctor state_assessment_functor )
        : M_state_assessment_functor( state_assessment_functor )
    {

    }

    template < typename Strategy >
    void operator()( state_type const& state,
                     Strategy& strategy )
    {
        BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));
        typedef typename tactic_id_type<Strategy>::type tactic_id_t;
        typedef typename formation_id_type<Strategy>::type formation_id_t;

        BOOST_STATIC_ASSERT_MSG(( boost::is_same< tactic_id_t, TacticId >::value),
                                  "Tactic id types of tactic selector and store mismatch");

        BOOST_STATIC_ASSERT_MSG(( boost::is_same< formation_id_t, FormationId >::value),
                                  "Formation id types of formation selector and strategy mismatch");

        const auto _tactic_id = get_tactic_id( strategy );
        // assert(( _tactic_id != get_null_value< soccer::tactic_id_tag, Strategy >() ));

        typename TacticStateAssessmentFormationMap::const_iterator ittf = M_tactic_state_assessment_formation_map.find( _tactic_id );
        assert( ittf != M_tactic_state_assessment_formation_map.end() );

        const StateAssessmentFormationMap& state_assessment_formations = ittf->second;
        state_assessment_type state_assessment_value = M_state_assessment_functor( state );
        typename StateAssessmentFormationMap::const_iterator itsf = state_assessment_formations.find( state_assessment_value );
        assert( itsf != state_assessment_formations.end() );

        FormationId new_formation_id = itsf->second;
        set_formation_id( new_formation_id, strategy );
    }

    void add( const TacticId tactic_id,
              const state_assessment_type state_assessment,
              const FormationId formation_id )
    {
        typename TacticStateAssessmentFormationMap::iterator itt = M_tactic_state_assessment_formation_map.find( tactic_id );

        if ( itt == M_tactic_state_assessment_formation_map.end() )
        {
            StateAssessmentFormationMap state_assessment_formations;
            state_assessment_formations.insert( std::make_pair( state_assessment, formation_id ) );
            M_tactic_state_assessment_formation_map.insert( std::make_pair( tactic_id, state_assessment_formations ) );
            return;
        }

        StateAssessmentFormationMap& state_assessment_formations = itt->second;
        // if it fails here, it means a state_assessment has already been mapped with that formation id
        assert( state_assessment_formations.find( state_assessment ) == state_assessment_formations.end() );

        state_assessment_formations.insert( std::make_pair( state_assessment, formation_id ));
    }
};

/*!
  \brief tactic and situation based formation reader
*/
struct tactic_state_assessment_formation_selector_reader
{
private:

    std::string read_line( std::istream& is )
    {
        std::string line_buf;
        while (std::getline( is, line_buf )) {
            if ( line_buf.empty()
                         || line_buf[0] == '#'
                         || ! line_buf.compare( 0, 2, "//" ) )
            {
                // TODO Does this ignore empty spaces?
                continue; // Ignore comments and empty lines
            }
            break;
        }
        return line_buf;
    }

    std::ifstream M_ifs;

public:
    tactic_state_assessment_formation_selector_reader( const std::string& file_name )
        : M_ifs( file_name.c_str(), std::ifstream::in )
    {

    }

    template < typename FormationId,
               typename TacticId,
               typename StateAssessmentFunctor >
    void operator()( tactic_state_assessment_formation_selector< FormationId,
                                                                 TacticId,
                                                                 StateAssessmentFunctor >& formation_selector )
    {
        // BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));

        typedef typename boost::unary_traits< StateAssessmentFunctor >::result_type  state_assessment_type;
        std::istringstream istr( this->read_line( M_ifs ) );
        // Read the number of tactics
        int num_tactics = -1;
        istr >> num_tactics;
        assert( num_tactics != -1 );

        for ( int t = 0 ; t < num_tactics; t++ )
        {
            // 1st line Read tactic id
            istr.clear();
            istr.str( read_line( M_ifs ) );
            int tactic_id = -1;
            istr >> tactic_id;
            assert( tactic_id != -1 );

            // 2nd line Read formation for each situation
            // 1s situation is "default formation"

            istr.clear();
            istr.str( read_line( M_ifs ) );
            static const unsigned int num_situations = 19;
            for ( unsigned int s = 0; s < num_situations ; s++ )
            {
                int formation_id = -1;
                istr >> formation_id;
                assert( formation_id != -1 );

                state_assessment_type state_assessment = soccer::integer_to_situation_map.at( s );
                formation_selector.add( tactic_id, state_assessment, formation_id );
            }

        }

    }

};

/*!
 * \brief check sanity of tactic_state_assessment formation selector based on strategy
 */
template <typename Strategy,
          typename FormationId,
          typename TacticId,
          typename StateAssessmentFunctor,
          typename TacticsAssociativeContainer,
          typename FormationsAssociativeContainer
          >
bool
tactic_state_assessment_formation_selector_sanity_check( tactic_state_assessment_formation_selector< FormationId,
                                                                                                     TacticId,
                                                                                                     StateAssessmentFunctor > const& _formation_selector,
                                                         TacticsAssociativeContainer const& tactics_store,
                                                         FormationsAssociativeContainer const& formations_store)
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));
    typedef typename formation_id_type<Strategy>::type formation_id_t;
    typedef typename tactic_id_type<Strategy>::type tactic_id_t;


    BOOST_CONCEPT_ASSERT(( boost::UniqueAssociativeContainerConcept< FormationsAssociativeContainer > ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< formation_id_t, typename FormationsAssociativeContainer::key_type >::value),
                            "Formation id descriptor types of strategy and formation store mismatch");
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< formation_id_t, FormationId>::value),
                            "Formation id descriptor types of strategy and formation selector mismatch");

    BOOST_CONCEPT_ASSERT(( boost::UniqueAssociativeContainerConcept< TacticsAssociativeContainer > ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< tactic_id_t, typename TacticsAssociativeContainer::key_type>::value),
                            "Tactic id descriptor types of strategy and tactic store mismatch");
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< tactic_id_t, TacticId>::value),
                            "Tactic id descriptor types of strategy and tactic selector mismatch");

    typedef tactic_state_assessment_formation_selector< FormationId,
                                                        TacticId,
                                                        StateAssessmentFunctor
                                                      > formation_selector_t;

    bool sane = true;

    std::set< TacticId > unregistered_tactics;
    std::set< TacticId > unregistered_formations;
    for ( auto itt = _formation_selector.M_tactic_state_assessment_formation_map.begin();
          itt != _formation_selector.M_tactic_state_assessment_formation_map.end(); itt++ )
    {
        bool tactic_id_exists = tactics_store.find( itt->first ) != tactics_store.end();
        sane = sane && tactic_id_exists;
        if ( !tactic_id_exists )
        {
            unregistered_tactics.insert( itt->first );
            continue;
        }

        for ( auto its = itt->second.begin(); its != itt->second.end(); its++ )
        {
            bool formation_id_exists = formations_store.find( its->second ) != formations_store.end();
            sane = sane && formation_id_exists;
            if ( !formation_id_exists && unregistered_formations.find( itt->second ) == unregistered_formations.end() )
            {
                unregistered_formations.insert( itt->second );
            }
        }
    }

    if ( !sane )
    {
        std::cerr << "FormationSelector SANITY CHECK ERRORS " << std::endl;

        if ( !unregistered_tactics.empty() )
        {
            std::cerr << "Unregistered tactics = " << unregistered_tactics.size() << "[ ";
            for ( typename std::set< TacticId>::const_iterator itt = unregistered_tactics.begin();
                  itt != unregistered_tactics.end(); itt++ )
            {
                std::cerr << *itt << " ";
            }
            std::cerr << "]" << std::endl;
        }

        if ( !unregistered_formations.empty() )
        {
            std::cerr << "Unregistered formations= " << unregistered_formations.size() << "[ ";
            for ( typename std::set< FormationId>::const_iterator itt = unregistered_formations.begin();
                  itt != unregistered_formations.end(); itt++ )
            {
                std::cerr << *itt << " ";
            }
            std::cerr << "]" << std::endl;
        }
    }

    return sane;
}


SOCCER_NAMESPACE_BEGIN


namespace boost {

    template < typename FormationId,
               typename StateAssessmentFunctor >
    void put( soccer::state_assessment_formation_selector<FormationId, StateAssessmentFunctor>& safs,
              const typename soccer::state_assessment_formation_selector<FormationId,
                                                                                   StateAssessmentFunctor>::key_type key,
              const typename soccer::state_assessment_formation_selector<FormationId,
                                                                                   StateAssessmentFunctor>::value_type value)
    {
        safs.M_formations_by_state_assessment_category.insert( std::make_pair( key, value ));
    }

    template < typename FormationId,
               typename TacticId,
               typename StateAssessmentFunctor >
    void put( soccer::tactic_state_assessment_formation_selector<FormationId, TacticId, StateAssessmentFunctor>& tsafs,
              const typename soccer::tactic_state_assessment_formation_selector<FormationId, TacticId, StateAssessmentFunctor>::key_type key,
              const typename soccer::tactic_state_assessment_formation_selector<FormationId, TacticId, StateAssessmentFunctor>::value_type value )
    {
        tsafs.M_tactic_state_assessment_formation_map.insert( std::make_pair( key, value ));
    }
}


#endif // FORMATION_SELECTOR_HPP
