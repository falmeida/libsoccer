#ifndef SOCCER_OBJECT_MODEL_ALGORITHMS_HPP
#define SOCCER_OBJECT_MODEL_ALGORITHMS_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import relevant concepts to be checked
#include <soccer/concepts/object_concepts.hpp>

#include <functional>

SOCCER_NAMESPACE_BEGIN

/*!
 * \brief The inertia_effector struct
 * \tparam MobileObject mobile object model
 */
template < typename MobileObject,
           typename ObjectModel >
struct InertiaEffector
    : public std::unary_function< MobileObject, void >
{
    BOOST_CONCEPT_ASSERT(( MobileObjectConcept<MobileObject> ));

    // TODO Assert ball inertia model
    // BOOST_CONCEPT_ASSERT(( soccer::ObjectModelConcept<ObjectModel ));

    ObjectModel const* M_model;

    InertiaEffector( ObjectModel const& model )
        : M_model( &model )
    {

    }

    void operator() ( MobileObject& mobile_object )
    {
        // BOOST_CONCEPT_ASSERT(( soccer::LinearDecayModelConcept< ObjectModel > ));
        const auto old_position = soccer::get_position( mobile_object );
        const auto old_velocity = soccer::get_velocity( mobile_object );
        auto new_position = old_position + old_velocity;
        soccer::set_position( new_position, mobile_object );

        auto new_velocity = old_velocity * soccer::get_decay( *M_model );
        soccer::set_velocity( new_velocity, mobile_object );
    }
};

template < typename MobileObject,
           typename MobileObjectModel >
typename position_type< MobileObject >::type
get_inertia_position( MobileObject const& obj,
                      MobileObjectModel const& model,
                      const unsigned int _num_iterations = 1u )
{
    BOOST_CONCEPT_ASSERT(( MobileObjectConcept< Ball > ));

    auto _inertia_pos = get_position( obj );
    for ( auto i = 0u; i < _num_iterations; i++ )
    {
        set_x( _inertia_pos * get_decay( model ), _inertia_pos );
        set_y( _inertia_pos * get_decay( model ), _inertia_pos );
    }

    return _inertia_pos;
}

template < typename MobileObject,
           typename MobileObjectModel >
typename velocity_type< MobileObject >::type
get_inertia_velocity( MobileObject const& _obj,
                      MobileObjectModel const& _model,
                      const unsigned int _num_iterations = 1 )
{
    // BOOST_CONCEPT_ASSERT(( BallConcept< Ball > ));

    auto _inertia_vel = get_velocity( _obj );
    for ( auto i = 0u; i < _num_iterations; i++ )
    {
        set_x( _inertia_vel * get_decay( _model ), _inertia_vel );
        set_y( _inertia_vel * get_decay( _model ), _inertia_vel );
    }

    return _inertia_vel;
}

SOCCER_NAMESPACE_END

#endif // SOCCER_OBJECT_MODEL_ALGORITHMS_HPP
