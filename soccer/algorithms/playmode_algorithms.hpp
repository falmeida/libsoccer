#ifndef SOCCER_PLAYMODE_ALGORITHMS_HPP
#define SOCCER_PLAYMODE_ALGORITHMS_HPP

#include <soccer/concepts/playmode_concepts.hpp>

namespace soccer {

template < typename PlayMode >
bool is_team_setplay( typename side_type<PlayMode>::type const _side,
                      PlayMode const& _pmode)
{
    BOOST_CONCEPT_ASSERT(( PlayModeConcept<PlayMode> ));

    const auto _match_situation = get_match_situation( _pmode );
    if ( is_kick_off( _match_situation ) ||
         is_throw_in( _match_situation ) ||
         is_corner_kick( _match_situation ) ||
         is_free_kick( _match_situation ) ||
         is_indirect_free_kick( _match_situation ) ||
         is_goalie_catch( _match_situation ) )
    {
        return _side == get_side( _pmode );
    }


    if ( is_offside( _match_situation ) )
    {
        return _side != get_side( _pmode );
    }

    return false;
}


template < typename MatchSituation >
bool is_game_stopped( MatchSituation const& _match_situation )
{
    BOOST_CONCEPT_ASSERT(( MatchSituationConcept<MatchSituation> ));

    return !is_play_on( _match_situation );
}


template < typename MatchSituation >
bool is_penalty_mode( MatchSituation const& _match_situation )
{
    BOOST_CONCEPT_ASSERT(( MatchSituationConcept<MatchSituation> ));

    return is_penalty_setup( _match_situation ) ||
           is_penalty_ready( _match_situation );
}


}

#endif // SOCCER_PLAYMODE_ALGORITHMS_HPP
