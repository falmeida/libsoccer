#ifndef SOCCER_GAME_ALGORITHMS_HPP
#define SOCCER_GAME_ALGORITHMS_HPP

// Include useful macros
#include <soccer/defines.h>

#include <soccer/concepts/game_concepts.hpp>

#include <soccer/geometry.hpp>

#include <functional>

SOCCER_NAMESPACE_BEGIN

template < typename Game >
bool
has_penalty_shootouts( Game const& game )
{
    BOOST_CONCEPT_ASSERT(( GameConcept< Game > ));

    return num_penalties_in_shootout( game ) > 0u;
}

SOCCER_NAMESPACE_END

#endif // SOCCER_GAME_ALGORITHMS_HPP
