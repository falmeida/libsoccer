#ifndef SOCCER_ALGORITHM_CONGESTION_HPP
#define SOCCER_ALGORITHM_CONGESTION_HPP

//! Useful define macros
#include <soccer/defines.h>

//! Import relevant soccer concepts
#include <soccer/concepts/state_concepts.hpp>

//! Import relevant Boost concept checks
#include <boost/concept_check.hpp>

SOCCER_NAMESPACE_BEGIN

/**************/
/* Congestion */
/**************/

/*!
    \brief Calculate the congestion effect a player has on another
    \tparam Position A readable model of the position concept
    \tparam State A readable model of the state concept
*/
template < typename Position,
           typename State >
double
player_congestion_from_position( const typename player_id_type< State >::type player_id,
                                 Position const& position,
                                 State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));

    double result = 1.0 / distance( get_position( player_id, state ), position );
    return result;
}

/*!
    \brief Calculate the congestion effect a player has on another
*/
template < typename State >
double
inter_player_congestion( const typename player_id_type< State >::type player_id1,
                         const typename player_id_type< State >::type player_id2,
                         State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return player_congestion_from_position( player_id1,
                                            get_position( player_id2, state ),
                                            state );
}

/*!
 * \brief Calculate the congestion of a position based on a set of players
 */
template <typename PlayerIdInputIterator,
          typename Position,
          typename State>
double get_position_congestion_from_players( PlayerIdInputIterator first,
                                             const PlayerIdInputIterator last,
                                             State const& state,
                                             Position const& target_pos )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerIdInputIterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value<PlayerIdInputIterator>::type,
                                              typename player_id_type< State >::type >::value ),
                            "Player id types from State and PlayerId mismatch");

    double result = 0.0;

    for ( ; first != last; first++ )
    {
        result += player_congestion_from_position( *first, target_pos, state );
    }

    return result;
}

/*!
 * \brief Calculate the congestion of a player from other players
 */
template < typename PlayerIdInputIterator,
           typename State >
double player_congestion_from_players( PlayerIdInputIterator first,
                                       const PlayerIdInputIterator last,
                                       State const& state,
                                       const typename player_id_type< State >::type target_player_id )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerIdInputIterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value<PlayerIdInputIterator>::type,
                                              typename player_id_type< State >::type >::value ),
                            "Player id types from State and PlayerId mismatch");

    return position_congestion_from_players( first,
                                             last,
                                             state,
                                             get_position( target_player_id, state ) );
}

/*!
  \brief Least congested team player id from a set of player identifiers
  */
template < typename PlayerIdInputIterator,
           typename State >
typename player_id_type< State >::type
least_congested_player( PlayerIdInputIterator first,
                        const PlayerIdInputIterator last,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerIdInputIterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value<PlayerIdInputIterator>::type,
                                              typename player_id_type< State >::type>::value ),
                            "Player id types mismatch in State and PlayerIdInputIterator");

    double least_congestion = std::numeric_limits<double>::max();
    auto least_congested_player = last;

    for ( auto itp = first; itp != last ; itp++ )
    {
        double current_congestion = position_congestion_from_players( first,
                                                                      last,
                                                                      state,
                                                                      get_position( *itp, state ) );
        if ( current_congestion < least_congestion )
        {
            least_congested_player = itp;
            least_congestion = current_congestion;
        }
    }
    return least_congested_player ;
}

/*!
  \brief Least congested team player id from a given side
  */
template < typename Side,
           typename State >
typename player_id_type< State >::type
least_congested_player( const Side side,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< Side, typename side_type< typename player_type< State >::type >::type >::value ),
                            "Side descriptor types mismatch in State and Side");

    //typedef typename player_iterator_type< State >::type player_iterator_t;

    auto team_players_iterators = team_players( side, state );

    return least_congested_player( team_players_iterators.first,
                                   team_players_iterators.last,
                                   state );
}


SOCCER_NAMESPACE_END

#endif // SOCCER_ALGORITHM_CONGESTION_HPP
