#ifndef SOCCER_POSITIONING_ALGORITHMS_HPP
#define SOCCER_POSITIONING_ALGORITHMS_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import relevant concepts to be checked
#include <soccer/concepts/positionings_concepts.hpp>

#include <boost/tuple/tuple.hpp>


SOCCER_NAMESPACE_BEGIN

/*!
 \brief return the number of existing positionings
 \param p an instance of the positionings concept model
 \returns the number of existing positionings
 */
/*template < typename Positionings >
typename std::iterator_traits< typename positioning_iterator_type< Positionings >::type  >::difference_type
num_positionings( const Positionings& p )
{
    BOOST_CONCEPT_ASSERT(( PositioningsConcept<Positionings> ));

    typedef typename positioning_iterator_type< Positionings >::type positioning_id_iterator;
    std::pair< positioning_id_iterator, positioning_id_iterator> iters = positionings( p );
    return std::distance( iters.first, iters.second );
} */

/*!
 \brief return the number of registered swaps in positionings
 \param p an instance of the positionings concept model
 \returns the number of swaps in the current positionings
 */
/*template < typename Positionings >
size_t num_positionings_swaps( const Positionings& p)
{
    BOOST_CONCEPT_ASSERT(( PositioningsConcept<Positionings> ));

    typedef typename positioning_id_type< Positionings >::type positioning_id_descriptor;
    typedef typename positioning_iterator_type< Positionings >::type positioning_id_iterator;

    positioning_id_iterator _positioning_begin, _positioning_end;
    boost::tie( _positioning_begin, _positioning_end ) = get_positionings( p );
    std::size_t count = 0u;
    for ( auto itp = _positioning_begin; itp != _positioning_end; itp++ )
    {
        const auto posit = positioning( *_positioning_begin, p );
        if ( *_positioning_begin != posit )
        {
            count++;
        }
    }

    return count / 2u;
}*/

SOCCER_NAMESPACE_END

#endif // SOCCER_POSITIONING_ALGORITHMS_HPP
