#ifndef SOCCER_ALGORITHMS_OFFSIDE_HPP
#define SOCCER_ALGORITHMS_OFFSIDE_HPP

//! Useful define macros
#include <soccer/defines.h>

//! Import relevant soccer concepts//! Useful define macros
#include <soccer/concepts/state_concepts.hpp>
#include <soccer/concepts/pitch_concept.hpp>

//! import boost::tie
#include <boost/utility.hpp>

SOCCER_NAMESPACE_BEGIN

/*!
 * \brief Check if a given position is offside in a based on a team's side
 * \tparam State
 * \tparam Position
 * \tparam Pitch
 * \returns list of player ids which are offside
 */
template < typename State,
           typename Position,
           typename Pitch >
inline bool
is_offside( Position const& position,
            typename side_type< typename player_type< State >::type >::type const& _side,
            State const& state,
            Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    assert( _side != get_unknown_side< side_t >() );

    // TODO Non-generic (not assessing sign of vertical and horizontal coordinates)
    if ( _side == get_left_side< side_t >() )
    {
        auto _offside_line = get_left_offside_line( pitch, state );
        return get_x( position ) < _offside_line;
    }
    auto _offside_line = get_right_offside_line( pitch, state );
    return get_x( position ) > _offside_line;
}

/////////////////////
// Players Offside //
/////////////////////

/*!
  \brief Determine player id of left team players that are in an offisde situation
  */
template < typename State,
           typename PlayerIdOutputIterator >
void
left_players_offside( State const& state,
                      PlayerIdOutputIterator out_players )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    //typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename side_type< player_t >::type side_t;

    const auto offside_line = left_offside_line( state );

    player_from_side_unary_predicate<player_t> side_pred( get_left_side< side_t >() );
    is_player_pos_after_x<player_t> offside_pred( offside_line );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    for ( auto itp = _player_begin ; itp != _player_end; itp++ )
    {
        const auto _player = get_player( *itp, state );

        if ( side_pred( _player ) && offside_pred( _player ) )
        {
            *out_players = *itp;
        }
    }

}

/*!
  \brief Determine player id of left team players that are in an offisde situation
  */
template < typename State,
           typename PlayerIdOutputIterator >
void
right_players_offside( State const& state,
                       PlayerIdOutputIterator out_players )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    const auto _offside_line = right_offside_line( state );

    player_from_side_unary_predicate<player_t> side_pred( get_right_side< side_t >() );
    is_player_pos_before_x<player_t> offside_pred( _offside_line );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        const auto _player = get_player( *itp, state );

        if ( side_pred( _player ) && offside_pred( _player ) )
        {
            *out_players = *itp;
        }
    }

}

/*!
  \brief Players offside from a given team side
  \returns list of player ids which are offside
  */
template < typename State,
           typename PlayerIdOutputIterator >
inline void
players_offside( typename side_type< typename player_type< State >::type >::type const& side,
                 State const& state,
                 PlayerIdOutputIterator out_players )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return side == get_left_side< side_t >()
            ? left_players_offside( state, out_players )
            : right_players_offside( state, out_players );
}

/*!
  \brief Players offside from a given team side
  \tparam State A model of a state
  \tparam Pitch A model of a pitch
  \returns list of player ids which are offside
  */
template < typename State,
           typename Pitch >
inline bool
is_offside( typename player_id_type< State >::type const& pid,
            State const& state,
            Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    typedef typename side_type< typename player_id_type< State >::type >::type side_t;

    const auto _player = get_player( pid, state );
    const auto _side = get_side( _player );

    assert( _side != get_unknown_side< side_t >() );

    const auto _player_pos = get_position( _player );

    // TODO Non-generic (not assessing sign of vertical and horizontal coordinates)
    if ( _side == get_left_side< side_t >() )
    {
        auto _offside_line = get_left_offside_line( pitch, state );
        return get_x( _player_pos ) < _offside_line;
    }
    auto _offside_line = get_right_offside_line( pitch, state );
    return get_x( _player_pos ) > _offside_line;
}


/************
 * FUNCTORS *
 * **********/
/*!
 \brief State predicate to determine if a player identifier is offside
 */
template < typename Pitch,
           typename State >
struct is_player_id_offside
    : public std::unary_function< typename player_id_type< State >::type, bool>
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    Pitch const* M_pitch;
    State const& M_state;

    is_player_id_offside( Pitch const& pitch,
                          State const& state )
        : M_pitch( &pitch )
        , M_state( state )
    {

    }

    bool operator() ( const player_id_t player_id )
    {
        return get_side( player_id, M_state ) == get_left_side< side_t >() ?
                    get_x( get_position( player_id, M_state ) ) > left_offside_line( *M_pitch, M_state ) :
                    get_x( get_position( player_id, M_state ) ) < right_offside_line( *M_pitch, M_state );
    }
};

SOCCER_NAMESPACE_END

#endif // SOCCER_ALGORITHMS_OFFSIDE_HPP
