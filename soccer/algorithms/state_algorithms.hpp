#ifndef SOCCER_STATE_ALGORITHMS_HPP
#define SOCCER_STATE_ALGORITHMS_HPP

//! Useful define macros
#include <soccer/defines.h>

#include <soccer/geometry.hpp>

#include <soccer/core/access.hpp>
#include <soccer/core/type_generators.hpp>

#include <soccer/traits/state_traits.hpp>
#include <soccer/predicates/player_predicates.hpp>

//! Import relevant concepts
#include <soccer/concepts/state_concepts.hpp>

#include <soccer/algorithms/playmode_algorithms.hpp>
#include <soccer/algorithms/pitch_algorithms.hpp>
#include <soccer/algorithms/game_algorithms.hpp>

#include <soccer/types.h>

#include <boost/tuple/tuple.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/type_traits.hpp>

#include <functional>
#include <algorithm>
#include <iterator>

#include <cassert>

SOCCER_DETAIL_NAMESPACE_BEGIN

template <class ForwardIterator, class BinaryPredicate>
ForwardIterator
find_best_match( ForwardIterator first, ForwardIterator last, BinaryPredicate pred )
{
  if ( first == last)
      return last;

  ForwardIterator best_result = first;
  while (++first!=last)
    if ( pred ( *best_result, *first ) )
    {
        best_result = first;
    }
  return best_result;
}

SOCCER_DETAIL_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*
 *  State Traits SHORTCUT ACCESSORS
 */

template < typename State >
typename team_type< State >::type
get_team( const typename side_type< typename player_type< State >::type >::type _side,
          State const& state )
{
    // compile time assertion
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    // runtime assertion
    assert( _side != get_unknown_side< side_t >() );

    return _side == get_left_side< side_t >()
            ? get_left_team( state )
            : get_right_team( state );
}

template < typename State >
void
set_team( const typename side_type< typename player_type< State >::type >::type _side,
          typename team_type< State >::type _team,
          State& state )
{
    // compile time assertion
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    // runtime assertion
    assert( _side != get_unknown_side< side_t >() );

    if ( _side == get_left_side< side_t >() )
        set_left_team( _team, state );
    else
        set_right_team( _team, state );
}

/*!
  \brief Determine the team score for a particular state
  \tparam State Model of a soccer state
  \param side the team side whose score is to be retrieved
  \param state the state
  \returns the team score
  */
template < typename State >
typename score_type< State >::type
get_score( const typename side_type< typename player_type< State >::type >::type _side,
          State const& state )
{
    // compile time assertion
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    // runtime assertion
    assert( _side != get_unknown_side< side_t >() );

    return _side == get_left_side< side_t >()
            ? get_left_score( state )
            : get_right_score( state );
}

template < typename State >
void
set_score( const typename side_type< typename player_type< State >::type >::type _side,
          typename score_type< State >::type _score,
          State& state )
{
    // compile time assertion
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    // runtime assertion
    assert( _side != get_unknown_side< side_t >() );

    if ( _side == get_left_side< side_t >() )
        set_left_score( _score, state );
    else
        set_right_score( _score, state );
}

template < typename State >
typename side_type< typename player_type< State >::type >::type
get_side( const typename player_id_type< State >::type player_id,
          State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_side( get_player( player_id, state ) );
}

template < typename State >
typename position_type< typename player_type< State >::type >::type
get_player_position( const typename player_id_type< State >::type pid, State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_position( get_player( pid, state ) );
}

template < typename State >
typename velocity_type< typename player_type< State >::type >::type
get_player_velocity( const typename player_id_type< State >::type pid, State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_velocity( get_player( pid, state ) );
}

template < typename State >
typename number_type< typename player_type< State >::type >::type
get_number( const typename player_id_type< State >::type player_id,
            State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_number( get_player( player_id, state ) );
}

template < typename State >
typename velocity_type< typename player_type< State >::type >::type
get_velocity( const typename player_id_type< State >::type player_id,
          State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_velocity( get_player( player_id, state ) );
}

template < typename State >
typename direction_type< typename player_type< State >::type >::type
get_body_direction( typename player_id_type< State >::type const& player_id,
                    State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_body_direction( get_player( player_id, state ) );
}

template < typename State >
typename position_type< typename ball_type< State >::type >::type
get_ball_position( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_position( get_ball( state ) );
}

template < typename State >
inline typename velocity_type< typename ball_type< State >::type >::type
get_ball_velocity( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_velocity( get_ball( state ) );
}

template < typename State >
inline bool
is_goalie( const typename player_id_type< State >::type player_id,
           State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return is_goalie( get_player( player_id, state ) );
}

/*!
  \brief Check if two player identifiers of a state belong to the same side
  \tparam State Model of a soccer state
  \param player_id1 the first player identifier
  \param player_id2 the second player identifier
  \param state the state to resolve the player identifiers
  \returns True if both player identifiers belong to the same team or false otherwise.
  */
template < typename State >
bool same_side( const typename player_id_type< State >::type player_id1,
                const typename player_id_type< State >::type player_id2,
                State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_side( player_id1, state ) == get_side( player_id2, state );
}

/*!
 * \brief Determine the player identifier on a state for a particular number and side descriptors
 * \tparam State Model of a soccer state
  */
template < typename State >
typename player_id_type< State >::type
player_id( typename number_type< typename player_id_type< State >::type >::type const _number,
           typename side_type< typename player_id_type< State >::type >::type const side,
           State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_id_type< State >::type player_id_t;

    player_iterator_t f, l;
    for ( boost::tie( f, l ) = get_players( state ) ; f != l; f++ )
    {
        const auto _player = get_player( *f, state );
        if ( get_side( _player ) == side && get_number( _player ) == _number )
        {
            return *f;
        }
    }

    return get_null_value< player_id_t >();
}

/*!
  \brief Determine the direction from a player position to an arbitrary position
  \param from_pid the source player identifier
  \param to_pid the target player identifier
  \param the state
  \returns direction from a source player to a target player
  */
template < typename State >
typename state_traits< State >::direction_t // TODO Fix Me!!!
direction( const typename player_id_type< State >::type from_pid,
           const typename player_id_type< State >::type to_pid,
           State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return direction( get_position( from_pid, state ), get_position( to_pid, state ) );
}

/*!
  \brief Determine the direction from a player position to an arbitrary position
  \param from_pid the player identifier
  \param to_pos the target position
  \param the state
  \returns direction from a player position to an arbitrary position
  */
template < typename State,
           typename Position >
typename state_traits< State >::direction_t
direction( const typename player_id_type< State >::type from_pid,
           Position const& to_pos,
           State const& state )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return direction( get_position( from_pid, state ), to_pos );
}

template < typename State,
           typename Position >
typename state_traits< State >::direction_t
direction( Position const& from_pos,
           const typename player_id_type< State >::type to_pid,
           State const& state )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return direction( from_pos, get_position( to_pid, state ) );
}

/*!
  \brief Determine the direction from arbitrary to an position player position
  \tparam State Model of a soccer state
  \param from_pos the position
  \param to_pid the player identifier
  \param the state
  \returns direction from an arbitrary position to a player position
  */
template < typename State >
bool same_side( const typename player_id_type< State >::type pid1,
                const typename player_id_type< State >::type pid2,
                State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_side( pid1, state) == get_side( pid2, state );
}

/*!
 \brief distance between two players using their state identifiers
 \param pid1 id of the first player
 \param pid2 id of the second player
 \param state the state in which the players are defined
 \returns distance between the players current position
 */
template < typename State >
typename position_traits< typename state_traits< State >::position_type >::distance_t // TODO Fix ME
distance( const typename player_id_type< State >::type pid1,
          const typename player_id_type< State >::type pid2,
          State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    // typedef player_traits< typename player_type< State >::type > player_traits;
    return distance( get_position( pid1, state ), get_position( pid2, state ) );
}

/*!
 \brief the side of the ball owner
 \param state the state
 \returns the side of the ball owner
 */
template < typename State >
typename side_type< typename player_type< State >::type >::type
ball_owner_side( State const& state )
{
    return get_side( ball_owner_id( state ), state );
}

/*!
\brief Determine the side of the attacking team
\param state The state to analyze
*/
template < typename State >
typename side_type< typename player_type< State >::type >::type
attacking_side( State const& state )
{
    return ball_owner_side( state );
}

/*!
\brief Determine the side of the defending team
\param state The state to analyze
*/
template < typename State >
typename side_type< typename player_type< State >::type >::type
defending_side( State const& state )
{
    return state_traits< State >::opposite_side( attacking_side( state ) );
}

/*!
  \brief Determine the number of players adjacent to another player
  \param player_id the player identifier
  \param state the state
  \returns The number of players adjacent to the player identifier
  */
/* template < typename State >
typename boost::enable_if< detail::has_player_t< State >,
                           typename state_traits< State >::players_size_type >::type
num_adjacent_players( const typename player_id_type< State >::type& player_id,
                      State& state)
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    // Get adjacent vertexes
    typename adjacent_player_iterator_type< State >::type vi, viend;
    boost::tie(vi, viend) = adjacent_players( player_id, state );

    return viend - vi;
} */

/*!
  \brief Get team players iterators based on distance from ball
  \param side the side of the players
  \param state the state to consider
  \returns a pair of iterators for the players
*/
template < typename State >
std::pair< typename State::team_players_distance_from_ball_iterator,
           typename State::team_players_distance_from_ball_iterator> teamPlayersDistanceFromBall( typename side_type< typename player_type< State >::type >::type side,
                                                                                                  State& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return side == get_left_side< side_t >()
            ? leftPlayersDistanceFromBall( state )
            : rightPlayersDistanceFromBall( state );
}

/*!
  \brief Determine whether the ball is kickable in a state
  \param state the state
  \returns True if the ball is kickable, or false otherwise
  */
template < typename State >
bool is_ball_kickable( State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typename player_iterator_type< State >::type _player_begin, _player_end;

    boost::tie(_player_begin,_player_end) = get_players(state);
    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( canKick( *itp, state ) )
        {
            return true;
        }
    }
    return false;
}

/*!
  \brief Determine whether two players are adjacent based on their identifiers in a given state
  \param player_id1 the first player identifier
  \param player_id2 the first player identifier
  \param state the state
  \returns True if players are adjacent or false otherwise
  */
template < typename State >
bool is_adjacent( const typename player_type< State >::type& player_id1,
                  const typename player_type< State >::type& player_id2,
                  State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    // Get adjacent vertexes
    typedef typename player_type< State >::type player_t;

    typename state_traits< State >::adjacency_iterator vi, viend, found;
    boost::tie(vi, viend) = adjacent_players( player_id1, state );

    found = std::find( vi, viend, player_id2 );

    return found != viend;
}

/******************
 * State Functors *
 ******************/

/*!
  \defgroup Player sorting binary function
  */
template < typename State,
           typename StaticObject >
struct player_id_direction_from_object_cmp
        : public std::binary_function< typename player_id_type< State >::type,
                                       typename player_id_type< State >::type,
                                       bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( StaticObjectConcept<StaticObject> ));

    player_id_direction_from_object_cmp( State const& state,
                                         const StaticObject& static_object )
        : M_state( state )
    {

    }

    bool operator() ( const player_id_t pid1,
                      const player_id_t pid2 )
    {
        return direction( get_position( pid1, M_state ), get_position( M_static_object ) ) <
               direction( get_position( pid2, M_state ), get_position( M_static_object ) );
    }

    State const& M_state;
    const StaticObject& M_static_object;
};

/*!
  \defgroup Player sorting binary function
  */
template < typename State >
struct player_id_direction_from_ball_cmp
        : public std::binary_function< typename player_id_type< State >::type,
                                       typename player_id_type< State >::type,
                                       bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    player_id_direction_from_ball_cmp( State const& state )
        : m_state( state )
    {

    }

    bool operator() ( const player_id_t pd1,
                      const player_id_t pd2 )
    {
        return directionFromBall( pd1, m_state ) < directionFromBall( pd2, m_state );
    }

    State const& m_state;
};

/*!
 * \brief compare player id distances
 */
template < typename State >
struct player_id_distance_from_cmp
        : public std::binary_function< typename player_id_type< State >::type,
                                       typename player_id_type< State >::type,
                                       bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    player_id_distance_from_cmp( State const& state )
        : M_state( state )
    {

    }

    bool operator() ( const player_id_t pid1,
                      const player_id_t pid2)
    {
        return distanceFromBall( pid1, M_state ) < distanceFromBall( pid2, M_state );
    }

    State const& M_state;
};

/*!
  \brief Predicate to check if two player identifiers are adjacent in a state
  */
template < typename State >
struct is_adjacent_functor
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    State const& M_state;

    is_adjacent_functor( State const& state )
        : M_state( state )
    {

    }

    bool operator() ( const player_id_t player_id1,
                      const player_id_t player_id2)
    {
        return is_adjacent( player_id1,
                            player_id2,
                            M_state );
    }
};


/*!
  \brief Constant opponent id
  */
template < typename State,
           typename OpponentId >
struct opponent_id_constant
        : public std::unary_function< State, OpponentId>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    OpponentId M_opponent_id;

    opponent_id_constant( OpponentId opponent_id )
        : M_opponent_id( opponent_id )
    {

    }

    OpponentId operator()( State const& )
    {
        return M_opponent_id;
    }
};

/*!
 * \brief Assess the score of a state from the perspective of a given team side
 * \tparam State Model of soccer state
 */
template < typename State >
struct score_assessment
    : public std::unary_function< State, ScoreAssessment >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    typedef typename score_type< State >::type score_t;


    side_t M_team_side;
    side_t M_opponent_side;

    score_assessment( const side_t team_side )
        : M_team_side( team_side )
        , M_opponent_side( get_opposite_side( team_side ) )
    {

    }

    ScoreAssessment operator()( State const& state )
    {
        const auto team_score = get_score( M_team_side, state );
        const auto opponent_score = get_score( M_opponent_side, state );
        const auto relative_score = (int) team_score - (int) opponent_score;

        ScoreAssessment score_assessment = ScoreAssessment::MEDIUM;
        int dif = 2;  //draw
        if ( relative_score > dif) score_assessment = ScoreAssessment::VERY_GOOD;
        else if ( relative_score > 0 ) score_assessment = ScoreAssessment::GOOD;
        else if ( relative_score < -dif ) score_assessment = ScoreAssessment::VERY_BAD;  //loose bad
        else if ( relative_score < 0 ) score_assessment = ScoreAssessment::BAD;

        return score_assessment;
    }
};

/*!
 * \brief Make a filtered player iterator based on an unary player id predicate
 * \tparam State model of soccer state
 * \param state instance of a model of a soccer state
 * \param _pid_skip_fn unary predicate to skip player ids
 */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
make_player_filter_iterator( std::function< bool( typename player_id_type< State >::type ) > _pid_skip_fn,
                             State const& state )
{
    typename player_iterator_type< State >::type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    return std::make_pair( typename player_filter_iterator_generator< State >::type( _pid_skip_fn, _player_begin, _player_end ),
                           typename player_filter_iterator_generator< State >::type( _pid_skip_fn, _player_end, _player_end ));
}

/*!
 * \brief Make a filtered player iterator based on an unary player id predicate
 * \tparam State model of soccer state
 * \param team_side the side of the players to iterate
 * \param state instance of a model of a soccer state
 */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
make_teammate_iterator( typename side_type< typename player_type< State >::type >::type const& team_side,
                        State const& state )
{
    typedef typename player_id_type< State >::type player_id_t;
    std::function< bool(player_id_t const) >
            opponent_player_skip_fn = [&] ( player_id_t const pid )
                                    {
                                        return get_side( get_player( pid, state ) ) == team_side;
                                    };
    return make_player_filter_iterator( opponent_player_skip_fn, state );
}

/*!
 * \brief Get a pair of iterators to left players
 * \tparam State model of soccer state
 * \param state instance of a model of a soccer state
 */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
get_left_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return make_player_filter_iterator( get_left_side< side_t >( ), state );
}

/*!
* \brief Get a pair of iterators to left players
* \tparam State model of soccer state
* \param state instance of a model of a soccer state
*/
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
          typename player_filter_iterator_generator< State >::type >
get_right_players( State const& state )
{
   BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
   typedef typename side_type< typename player_type< State >::type >::type side_t;

   return make_player_filter_iterator( get_right_side< side_t >( ), state );
}


/*!
  \brief Get iterators for players from a given team
  */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
get_players( typename side_type< typename player_type< State >::type >::type const& side,
             State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return make_teammate_iterator( side, state );
}

/*!
  \brief Get adjacent players from the same side of the player
*/
template < typename State >
struct AdjacentTeammateGenerator
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    player_id_t M_sender_id;


    AdjacentTeammateGenerator( player_id_t sender )
        : M_sender_id( sender )
    {

    }

    template < typename PlayerIdOutputIterator >
    void operator() ( State const& /*state*/, PlayerIdOutputIterator /*out_player*/ )
    {
        //typedef boost::detail::iterator_traits< PlayerIdOutputIterator > player_id_output_iterator;
        // BOOST_STATIC_ASSERT_MSG(( !boost::is_same< adjacent_player_iterator, void >::value), "State must provide an adjacent_player_iterator" );
        // Output iterators do not provide value_type???????
        /* BOOST_STATIC_ASSERT_MSG( boost::is_convertible< player_id_t,
                                                        typename player_id_output_iterator::value_type >::value,
                                 "player_id_t do not match in AdjacentTeammateGenerator" ); */

        assert( false );
        /*typename side_type< typename player_type< State >::type >::type const& sender_side = get_side( m_sender_id, state );
        adjacent_player_iterator f, l;
        for ( boost::tie( f, l ) = adjacent_players( m_sender_id, state ); f != l ; f++ )
        {
            if ( side( *f, state ) == sender_side )
            {
                out_player = *f;
            }
        }*/
    }
};



/////////////////
// Goalie info //
/////////////////

/*!
  \brief Determine the goalie player identifier for a given team
  \param side the team side
  \param state the state to consider
  \returns The player id or null_player() if not found
*/
template < typename State >
typename player_id_type< State >::type
goalie_id( typename side_type< typename player_type< State >::type >::type const& goalie_side,
           State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    //typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    assert ( goalie_side == get_left_side< side_t >() ||
             goalie_side == get_right_side< side_t >() );

    if ( goalie_side == get_unknown_side< side_t >() )
    {
        return get_null_value< player_id_tag, State >();
    }

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( get_side( *itp, state ) == goalie_side &&
             is_goalie( *itp, state ) )
        {
            return *itp;
        }
    }

    return get_null_value< player_id_tag, State >();
}

/*!
  \brief Left team goalie id
  */
template < typename State >
typename player_id_type< State >::type
left_goalie_id( State const& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return goalie_id( get_right_side< side_t >(), state );
}

/*!
  \brief Leftmost player in a state
  */
template < typename State >
typename player_id_type< State >::type
right_goalie_id( State const& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return goalie_id( get_right_side< side_t >(), state );
}


//////////////////////
// State Predicates //
//////////////////////

/*!
  \brief Player id unary predicate (proxy for player unary predicates)
  */
template <typename PlayerUnaryPredicate,
          typename State>
struct player_id_unary_predicate
    : public std::unary_function< typename player_id_type< State >::type,
                                  bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_type< State >::type player_t;
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicate< PlayerUnaryPredicate, player_t> ));

    player_id_unary_predicate()
        : M_state( NULL )
    {

    }

    player_id_unary_predicate( const PlayerUnaryPredicate pred,
                               const State* state )
        : M_pred( pred )
        , M_state( state )
    {

    }

    bool operator() ( const player_id_t player_id ) const
    {
        assert( M_state != NULL );

        const auto _player = get_player( player_id, *M_state );
        return M_pred( _player );
    }

    PlayerUnaryPredicate M_pred;
    const State* M_state;
};

/*!
  \brief Player id binary predicate (proxy for player binary predicates)
  */
template <typename PlayerBinaryPredicate,
          typename State>
struct player_id_binary_predicate
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_type< State >::type player_t;
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::BinaryPredicate< PlayerBinaryPredicate, player_t, player_t > ));

    player_id_binary_predicate()
    {

    }

    player_id_binary_predicate( const PlayerBinaryPredicate pred,
                                const State* state  )
        : M_pred( pred )
        , M_state( state )
    {

    }

    bool operator() ( const player_id_t pid1,
                      const player_id_t pid2 ) const
    {
        const auto _p1 = get_player( pid1, *M_state );
        const auto _p2 = get_player( pid2, *M_state );

        return M_pred( _p1, _p2 );
    }

    PlayerBinaryPredicate M_pred;
    const State* M_state;
};


//////////////////
// Defense line //
//////////////////

/*!
  \brief Determine the left team defense line
  */
template < typename State >
typename coordinate_type<
    typename position_type<
        typename player_type< State >::type
    >::type
>::type
left_defense_line( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return get_null_value< player_id_t >();
    }

    auto leftmost_player_id1 = get_null_value< player_id_t >();
    auto leftmost_player_id2 = get_null_value< player_id_t >();
    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( side( *itp, state ) != get_left_side< side_t >() )
        {
            continue;
        }

        if ( leftmost_player_id1 == get_null_value< player_id_t >()
             || get_x( get_position( *itp, state )) < get_x( get_position( leftmost_player_id1, state )) )
        {
            leftmost_player_id2 = leftmost_player_id1;
            leftmost_player_id1 = *itp;
        }
    }

    auto left_defense_line = get_x( get_position( get_ball( state )));
    if ( leftmost_player_id1 != get_null_value< player_id_t >()
         && !isGoalie( leftmost_player_id1, state )
         && get_x( get_position( leftmost_player_id1, state )) < left_defense_line
         && get_x( get_position( leftmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        left_defense_line = get_x( get_position( leftmost_player_id1, state ));
    } else if ( leftmost_player_id1 != get_null_value< player_id_t >()
                && !isGoalie( leftmost_player_id2, state )
                && get_x( get_position( leftmost_player_id1, state )) < left_defense_line
                && get_x( get_position( leftmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        left_defense_line = get_x( get_position( leftmost_player_id2, state ) );
    }

    return left_defense_line;
}

/*!
  \brief Determine the x-axis coordinate of the right team defense line
  */
template < typename State >
typename coordinate_type< typename position_type< typename player_type< State >::type >::type >::type
right_defense_line( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return get_null_value< player_id_t >();
    }

    auto rightmost_player_id1 = get_null_value< player_id_t >();
    auto rightmost_player_id2 = get_null_value< player_id_t >();
    for ( auto itp = _player_begin ; itp != _player_end; itp++ )
    {
        if ( side( *itp, state ) != get_left_side< side_t >() )
        {
            continue;
        }

        if ( rightmost_player_id1 == get_null_value< player_id_t >()
             || get_x( get_position( *itp, state )) > get_x( get_position( rightmost_player_id1, state )) )
        {
            rightmost_player_id2 = rightmost_player_id1;
            rightmost_player_id1 = *itp;
        }
    }

    auto right_defense_line = get_x( get_position( get_ball( state )));
    if ( rightmost_player_id1 != get_null_value< player_id_t >()
         && !isGoalie( rightmost_player_id1, state )
         && get_x( get_position( rightmost_player_id1, state )) > right_defense_line
         && get_x( get_position( rightmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        right_defense_line = get_x( get_position( rightmost_player_id1, state ));
    } else if ( rightmost_player_id1 != get_null_value< player_id_t >()
                && !isGoalie( rightmost_player_id2, state )
                && get_x( get_position( rightmost_player_id1, state )) < right_defense_line
                && get_x( get_position( rightmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        right_defense_line = get_x( get_position( rightmost_player_id2, state ));
    }

    return right_defense_line;
}

/*!
  \brief Determine the defense line (x-axis coordinate) of a given team side
  \param side the side of the team to consider
  \param state the state
  \returns The x-axis coordinate of the team's defense line
  */
template < typename State >
typename coordinate_type< typename position_type< typename player_type< State >::type >::type >::type
defense_line( typename side_type< typename player_type< State >::type >::type const& side,
              State const& state )
{
    return side == get_left_side< typename side_type< typename player_type< State >::type >::type >()
            ? left_defense_line( state )
            : right_defense_line( state );
}


//////////////////
// Offense line //
//////////////////

/*!
  \brief determine the left offense line
  \param state the state to consider
  \returns the coordinate the left team offense line
  */
template < typename State,
           typename Pitch >
typename coordinate_type< typename position_type< typename player_type< State >::type >::type >::type
left_offense_line( State const& state,
                   Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    auto _left_offense_line = get_left_goal_line( pitch );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return _left_offense_line;
    }

    auto _right_defense_line =_right_defense_line( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( side( *itp, state ) != get_left_side< side_t >() )
        {
            continue;
        }

        const auto _player = get_player( *itp, state );
        const auto _player_pos = get_position( _player );
        if ( get_x( _player_pos ) > _right_defense_line )
        {
            continue;
        }

        if ( get_x( _player_pos ) > _left_offense_line )
        {
            _left_offense_line = get_x( _player_pos );
        }
    }

    return _left_offense_line;
}

/*!
 * \brief Determine the offense line (x-axis coordinate) of the right team
 * \tparam Pitch Model of a soccer pitch
 * \tparam State Model of a soccer state
 * \param pitch the soccer pitch model
 * \param state the soccer state model
 * \returns The x-axis coordinate of the right team's offense line
 */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_right_offense_line( Pitch const& pitch,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;


    auto _right_offense_line = get_right_goal_line( pitch );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return _right_offense_line;
    }

    auto _left_defense_line =_left_defense_line( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( get_side( *itp, state ) != get_right_side< side_t >() )
        {
            continue;
        }

        const auto _player = get_player( *itp, state );
        const auto _player_pos = get_position( _player );
        if ( get_x( _player_pos ) < _left_defense_line )
        {
            continue;
        }

        if ( get_x( _player_pos ) < _right_offense_line )
        {
            _right_offense_line = get_x( _player_pos );
        }
    }

    return _right_offense_line;
}

/*!
 * \brief Determine the offense line (x-axis coordinate) of a given team side
 * \tparam Pitch Model of a soccer pitch
 * \tparam State Model of a soccer state
 * \param side the team side whose offense line is to be considered
 * \param pitch the soccer pitch model
 * \param state the soccer state
 * \returns The x-axis coordinate of the team's offense line
 */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_offense_line( typename side_type< typename player_type< State >::type >::type const& side,
                  Pitch const& pitch,
                  State const& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return side == get_left_side< side_t >()
            ? get_left_offense_line( pitch, state )
            : get_right_offense_line( pitch, state );
}

//////////////////////////
// Offside calculations //
//////////////////////////

/*!
 * \brief Determine the left team side offside line (x-axis coordinate)
 * \tparam State Model of a soccer state
 * \tparam Pitch Model of a soccer pitch
 * \param pitch the soccer pitch
 * \param state the soccer state
 * \returns The x-axis coordinate of left team's offside line
 */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_left_offside_line( Pitch const& pitch,
                       State const& state )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    //typedef typename player_iterator_type< State >::type player_iterator_t;
    //typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    typename player_filter_iterator_generator< State >::type _opponent_begin, _opponent_end;
    boost::tie( _opponent_begin, _opponent_end ) = make_teammate_iterator( get_right_side< side_t >(), state );

    const auto _ball_x = get_x( get_position( get_ball( state ) ) );
    const auto _midfield_line = get_midfield_line( pitch );

    if ( _opponent_begin == _opponent_end )
    {
        // TODO HACK Change this. Logic is invalid for all cartesian referentials.
        return _ball_x < _midfield_line
             ? _midfield_line
             : _ball_x;
    }

    auto last_opponent_id = *_opponent_begin;
    auto last_opponent = get_player( last_opponent_id, state );
    auto before_last_opponent_id = get_null_value< player_id_tag, State >();

    auto ito = _opponent_begin;

    for ( ito++; ito != _opponent_end; ito++ )
    {
        const auto _opponent = get_player( *ito, state );
        const auto _opponent_pos = get_position( _opponent );

        if ( get_x( _opponent_pos  ) > get_x( get_position( last_opponent  ) ) )
        {
            before_last_opponent_id = last_opponent_id;
            last_opponent_id = *ito;
            last_opponent  = get_player( last_opponent_id, state );
        }
    }


    if ( before_last_opponent_id == get_null_value< player_id_tag, State >() )
    {
        // TODO HACK Change this. Logic is invalid for all cartesian referentials.
        return _ball_x < _midfield_line
             ? _midfield_line
             : _ball_x;
    }

    auto before_last_opponent  = get_player( before_last_opponent_id, state );
    // TODO HACK Change this. Logic is invalid for all cartesian referentials.
    return _ball_x < get_x( get_position( before_last_opponent ) )
         ? get_x( get_position( before_last_opponent ) )
         : _ball_x;
}

/*!
 * \brief Determine the left right side offside line (x-axis coordinate)
 * \tparam State Model of a soccer state
 * \tparam Pitch Model of a soccer pitch
 * \param pitch the soccer pitch
 * \param state the soccer state
 * \returns The x-axis coordinate of right team's offside line
 */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_right_offside_line( Pitch const& pitch,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    assert( false ); // TODO Fix Me (update according to left_offside_line)
    typedef typename player_iterator_type< State >::type player_iterator_t;
    //typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    if ( _player_begin == _player_end )
    {
        return get_midfield_line( pitch );
    }

    typedef player_id_unary_predicate< player_from_side_unary_predicate< player_t >, State > player_id_predicate;
    player_id_predicate pred( get_right_side< side_t >(), &state );

    typedef boost::filter_iterator< player_id_predicate, player_iterator_t > filter_player_iterator;

    filter_player_iterator cur( pred, _player_begin, _player_end );
    filter_player_iterator end( pred, _player_end, _player_end );

    auto last_player_id = *cur;
    auto last_player = get_player( last_player_id, state );
    auto before_last_player_id = get_null_value< player_id_tag, State >();

    for ( ; cur != end; cur++ )
    {
        const auto _player = get_player( *cur, state );
        const auto _player_pos = get_position( _player );

        if ( get_x( _player_pos  ) < get_x( get_position( last_player ) ) )
        {
            before_last_player_id = last_player_id;
            last_player_id = *cur;
            last_player = get_player( last_player_id, state );
        }
    }

    if ( before_last_player_id == get_null_value< player_id_tag, State >() )
    {
        return get_midfield_line( pitch );
    }

    auto before_last_player = get_player( before_last_player_id, state );
    return get_x( get_position( before_last_player ));
}

/*!
  \brief Determine a team side offside line (x-axis coordinate)
  \tparam State
  \tparam Pitch
  \param side the side of the team to consider
  \param state the state
  \returns The x-axis coordinate of the team's offside line
  */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_offside_line( typename side_type< typename player_type< State >::type >::type const& side,
                  Pitch const& pitch,
                  State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return side == get_left_side< side_t >()
            ? get_left_offside_line( pitch, state )
            : get_right_offside_line( pitch, state );
}

/********************
 * State Predicates *
 ********************/

template < typename State >
struct PlayerIdDistanceToPlayerIdPredicate
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;

    PlayerIdDistanceToPlayerIdPredicate( player_id_t player_id,
                                         State& state )
        : M_ref_player_id( player_id )
        , M_state( state )
    {

    }

    bool operator()( const player_id_t player_id1,
                     const player_id_t player_id2 )
    {
        return distance( M_ref_player_id, player_id1, M_state ) < distance( M_ref_player_id, player_id2, M_state );
    }

    player_id_t M_ref_player_id;
    State& M_state;
};

template < typename State,
           typename Position = typename position_type< typename player_type< State >::type >::type >
struct PlayerIdDistanceToPositionPredicate
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    PlayerIdDistanceToPositionPredicate( Position const& position,
                                         State* state )
        : M_position( position )
        , M_state( state )
    {

    }

    bool operator()( const player_id_t player_id1,
                     const player_id_t player_id2 )
    {
        return distance( M_position, player_id1, M_state ) < distance( M_position, player_id2, M_state );
    }

    Position const& M_position;
    State const& M_state;
};

/*!
 * \brief The direction_from_player_id_cmp struct
 * \tparam State Model of a state
 */
template < typename State >
struct direction_from_player_id_cmp
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;

    direction_from_player_id_cmp( const player_id_t player_id,
                                  State const& state )
        : M_ref_player_id( player_id )
        , M_state( state )
    {

    }

    bool operator()( const player_id_t player_id1,
                     const player_id_t player_id2 )
    {
        return direction( M_ref_player_id, player_id1, M_state ) < direction( M_ref_player_id, player_id2, M_state );
    }

    const player_id_t M_ref_player_id;
    State const& M_state;
};


/*!
 * \brief check if ball is moving to goal
 * \tparam MobileObject Model of a mobile object(e.g. player or ball)
 * \tparam Side Team side of the relevant goal
 * \tparam Pitch Model of a soccer pitch
 * \returns True if the ball is going to the team side goal or false otherwise
 */
template < typename MobileObject,
           typename Side,
           typename Pitch >
bool is_moving_towards_goal( MobileObject const& _mobile_object,
                             Side const& _side,
                             Pitch const& _pitch )
{
    BOOST_CONCEPT_ASSERT(( MobileObjectConcept< MobileObject > ));
    typedef typename soccer::position_type< MobileObject >::type mobile_object_position_t;

    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto _pos = get_position( _mobile_object );
    const auto _vel = get_velocity( _mobile_object );
    const auto _dir = direction( _vel );

    const auto _to_upper_post_direction = direction( _pos, get_goal_upper_post< mobile_object_position_t >( _side, _pitch ) );
    const auto _to_lower_post_direction = direction( _pos, get_goal_lower_post< mobile_object_position_t >( _side, _pitch ) );

    const auto _to_upper_post_diff = _to_upper_post_direction - _dir;
    const auto _to_lower_post_diff = _to_lower_post_direction - _dir;

    return _side == get_left_side< Side >()
            ? _to_upper_post_diff > 0.0 && _to_lower_post_diff < 0.0
            : _to_upper_post_diff < 0.0 && _to_lower_post_diff > 0.0;
}

template < typename State,
           typename Pitch >
bool is_ball_moving_towards_goal( typename side_type< typename player_type< State >::type >::type const side,
                                  State const& state,
                                  Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return is_moving_towards_goal( soccer::get_ball( state ), side, pitch );
}

/******************/
/* Player Queries */
/******************/

/*!
 * \brief Determine the total number of the left team players in a state
 * \tparam State Model of a soccer state
 * \param state the state
 * \returns the total number of players from the left team in the state
 */
template < typename State >
typename state_traits< State >::players_size_type // TODO Fix Me
num_left_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return num_players( get_left_side< side_t >(), state );
}

/*!
 * \brief Determine the total number of the right team players in a state
 * \tparam State Model of a soccer state
 * \param state the state
 * \returns the total number of players from the right team in the state
 */
template < typename State >
typename state_traits< State >::players_size_type // TODO Fix Me
num_right_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return num_players( get_right_side< side_t >(), state );
}

/*!
 * \brief Determine the total number of players in a state
 * \tparam State Model of a soccer state
 * \param state the state
 * \returns the total number of players in the state
 */
template < typename State >
typename std::iterator_traits< typename player_iterator_type< State >::type >::difference_type
num_players( State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typename player_iterator_type< State >::type _player_begin, _player_end;

    boost::tie( _player_begin, _player_end ) = get_players( state );
    return std::distance( _player_begin, _player_end );
}

/*!
 * \brief Determine the player nearest to another position
 * \tparam State
 * \tparam Position
 * \tparam PlayerIdExclusionUnaryPredicate
 */
template < typename State,
           typename Position,
           typename PlayerIdExclusionUnaryPredicate >
typename player_id_type< State >::type
player_nearest_position( State const& state,
                         Position const& position,
                         PlayerIdExclusionUnaryPredicate pid_exclusion_predicate )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;

    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicateConcept<PlayerIdExclusionUnaryPredicate, player_id_t> ));

    player_iterator_t _player_begin, _player_end;
    boost::tie(_player_begin,_player_end) = get_players( state );

    if ( _player_begin == _player_end ) { return get_null_value< player_id_t >(); }

    auto _nearest_player = *_player_begin;
    auto _min_dist = comparable_distance( get_position( *_player_begin, state ), position);

    for ( ++_player_begin; _player_begin != _player_end; _player_begin++ )
    {
        if ( pid_exclusion_predicate( *_player_begin ) ) { continue; }
        const auto _dist = comparable_distance( get_position( *_player_begin, state ), position );
        if ( _dist < _min_dist )
        {
            _min_dist = _dist;
            _nearest_player =*_player_begin;
        }
    }

    return _nearest_player;
}

/*!
 * \brief Determine player id nearest to another player id
 * \tparam State state model
 * \tparam PlayerIdExclusionUnaryPredicate unary predicate model to exclude players from analysis
 * \param state state object
 * \param pid reference player id descriptor
 * \param pid_exclusion_predicate player id exclusion predicate
 */
template < typename State,
           typename PlayerIdExclusionUnaryPredicate >
typename player_id_type< State >::type
player_nearest_player( State const& state,
                       typename player_id_type< State >::type const& pid,
                       PlayerIdExclusionUnaryPredicate pid_exclusion_predicate )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicateConcept<PlayerIdExclusionUnaryPredicate, player_id_t> ));

    auto pid_exclusion_fn = [&] ( const player_id_t _pid )
    {
        return _pid == pid // Excluse own player id (logical, should be handled within the predicate)
                || pid_exclusion_predicate(_pid);
    };
	return player_nearest_position( state, 
                    get_position( pid, state ),
					pid_exclusion_fn );
}

/*!
 * \brief Determine player id nearest to another player id
 * \tparam State state model
 * \param state state object
 * \param pid reference player id descriptor
 */
template < typename State >
typename player_id_type< State >::type
player_nearest_player( State const& state,
                       typename player_id_type< State >::type const& pid )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    auto pid_exclusion_fn = [] ( const typename player_id_type< State >::type ) { return false; };
    return player_nearest_position( state,
                                    get_position( pid, state ),
                                    pid_exclusion_fn );
}


/*!
 * \brief Data structure used for the response of a nearest player query
 * \tparam State Model of a soccer state
 */
template < typename State >
struct nearest_player_functor {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
	
    nearest_player_functor()
        : M_player_id( get_null_value< player_id_t >() )
    {

    }

    player_id_t M_player_id;
    //typename state_traits< State >::distance_t m_distance;
};

/*!
  \brief Determine the player nearest another player
  \param player the reference player
  \param state the state to consider
  \returns A nearest player data structure containing the details of the player found
*/
template < typename State >
typename player_id_type< State >::type
nearest_player( typename player_id_type< State >::type const player_id,
                State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename State::adjacent_players adjacent_player_iterator;

    // Get adjacent players => use adjacent players information
    std::pair< adjacent_player_iterator,
               adjacent_player_iterator > adjacent_players = adjacent_players( player_id, state );

    adjacent_player_iterator best_result = detail::find_best_match( adjacent_players.first,
                                                                    adjacent_players.second,
                                                                    distance_to_player( player_id, state ) );
    if ( best_result == adjacent_players.second )
    {
        return get_null_value< player_id_t >();
    }
    return *best_result;
}

/*!
  \brief Determine the identifier of the player nearest a given position
  \abstract Given a position descriptor, determine the player nearest to that position
  */
template < typename Position,
           typename State >
typename player_id_type< State >::type
nearest_player_from_position( Position const& position,
                              State const& state )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    //typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;

    // Get adjacent players
    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    player_iterator_t best_result = detail::find_best_match( _player_begin,
                                                           _player_end,
                                                           PlayerIdDistanceToPositionPredicate<State, Position>( position, state ) );
    assert ( best_result != _player_end );

    return *best_result;
}

/*!
  \brief Determine the player nearest a given position from a particular side
  \param position the reference position
  \param side the player team side
  \param state the state to consider
  \returns A nearest player data structure containing the details of the player found
*/
template < typename Position,
           typename State >
typename player_id_type< State >::type
nearest_player_from_position( Position const& position,
                              typename side_type< typename player_type< State >::type >::type const& side,
                              State const& state )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    // Get adjacent players
    auto _team_players_iterators = team_players( side, state );

    auto best_result = detail::find_best_match( _team_players_iterators.first,
                                                _team_players_iterators.second,
                                                PlayerIdDistanceToPositionPredicate<State,Position>( position, state ) );

    assert( best_result != _team_players_iterators.second );

    return *best_result;
}


/*!
 \brief Constrain a position behind the team offside line
 \tparam Pitch Model of a soccer pitch
 \tparam State Model of a soccer state
 \tparam CoordinateType Type of coordinate position
 */
template < typename Pitch,
           typename State,
           typename CoordinateType = typename position_type< Pitch >::type >
struct constrain_position_behind_offside_line
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename side_type< typename player_type< State >::type >::type side_t;

    side_t M_team_side;
    Pitch const* M_pitch;
    State const* M_state;
    CoordinateType M_offside_offset;
public:
    constrain_position_behind_offside_line( const side_t team_side,
                                            Pitch const& pitch,
                                            State const& state )
    : M_team_side( team_side )
    , M_pitch( &pitch )
    , M_state( &state )
    , M_offside_offset( 0 )
    {

    }

    constrain_position_behind_offside_line( const side_t team_side,
                                            Pitch const& pitch,
                                            State const& state,
                                            const CoordinateType offside_offset )
        : M_team_side( team_side )
        , M_pitch( &pitch )
        , M_state( &state )
        , M_offside_offset( offside_offset )
    {
        // runtime assertion
        assert( offside_offset >= 0 );
    }

    template < typename Position >
    void operator()( Position& position )
    {
        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<Position> ));

        assert( M_pitch != NULL && M_state != NULL );

        // inneficient => could be initialized on constructor once but could not be safely reused in different times of the game
        const auto _offside_line = get_offside_line( M_team_side, *M_pitch, *M_state );

        if ( M_team_side == get_left_side< side_t >()
             && get_x( position ) > _offside_line )
        {
            set_x( _offside_line - M_offside_offset, position );
        }
        else if ( M_team_side == get_right_side< side_t >() &&
                  get_x( position ) < _offside_line )
        {
            set_x( _offside_line + M_offside_offset, position );
        }
    }
};



/*!
 * \brief get player descriptor reference from state
 * \tparam State Model of a soccer state
 */
template < typename State >
struct get_player_info
    : public std::unary_function< typename player_id_type< State >::type,
                                  typename player_type< State >::type >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;

    State const& M_state;

    get_player_info( State const& state )
        : M_state( state )
    {

    }

    const player_t&
    operator()( const player_id_t pid )
    {
        return get_players( pid, M_state );
    }
};



/*!
 * \brief The player_range_property_map_generator struct
 */
struct player_range_property_map_generator
{

    player_range_property_map_generator()
    {

    }

    template <typename PlayerInputIterator,
              typename PlayerStateFunctor,
              typename State,
              typename PlayerPropertyMap>
    void operator() ( PlayerInputIterator first,
                      const PlayerInputIterator last,
                      const PlayerStateFunctor player_state_binary_functor,
                      State const& state,
                      PlayerPropertyMap pmap ) const
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        typedef typename player_id_type< State >::type state_player_id_t;

        BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< PlayerPropertyMap, state_player_id_t > ));
        BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< PlayerInputIterator > ));
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::detail::iterator_traits< PlayerInputIterator >::value_type,
                                              state_player_id_t >::value ),
                                "PlayerInputIterator and State player id types mismatch");

        typedef boost::binary_traits< PlayerStateFunctor > player_binary_functor_traits_t;
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< PlayerPropertyMap >::value_type,
                                                  typename player_binary_functor_traits_t::result_type >::value ),
                                "Property value types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_binary_functor_traits_t::first_argument_type,
                                                  state_player_id_t >::value ),
                                "PlayerStateFunctor and State player id types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_binary_functor_traits_t::second_argument_type,
                                                  State >::value ),
                                "State types mismatch");

        for ( first ; first != last ; first++ )
        {
            boost::put( pmap,
                        *first,
                        player_state_binary_functor( *first, state ) );
        }
    }
};

/*!
 * \brief state player property map generator
 */
struct all_player_property_map_generator {

    all_player_property_map_generator()
    {

    }

    template <typename PlayerStateFunctor,
              typename State,
              typename PlayerPropertyMap>
    void operator() ( const PlayerStateFunctor player_state_binary_functor,
                      State const& state,
                      PlayerPropertyMap pmap ) const
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        typedef typename player_id_type< State >::type state_player_id_t;
        typedef typename player_iterator_type< State >::type player_iterator_t;
        BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< PlayerPropertyMap, state_player_id_t > ));

        typedef boost::binary_traits< PlayerStateFunctor > player_functor_traits_t;
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_functor_traits_t::first_argument_type,
                                              state_player_id_t >::value ),
                                "Player id types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_functor_traits_t::second_argument_type,
                                              State >::value ),
                                "State types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< PlayerPropertyMap >::value_type,
                                              typename player_functor_traits_t::result_type >::value ),
                                "Property map value types mismatch");


        player_iterator_t _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( state );
        player_range_property_map_generator pmap_range_generator;
        pmap_range_generator( _player_begin,
                              _player_end,
                              player_state_binary_functor,
                              state,
                              pmap );
    }
};


/*!
 * \brief The partition_players_range struct
 */
struct partition_players_range {

    template <typename PlayerInputIterator,
              typename State>
    void operator()( PlayerInputIterator first,
                     const PlayerInputIterator last,
                     State const& state )
    {

    }
};

/*!
 * \brief The partition_players struct
 */
struct partition_players
{

    template < typename State >
    void operator()( State const& state )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    }
};

SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_ALGORITHMS_HPP
