#ifndef SOCCER_ALGORITHM_TIME_HPP
#define SOCCER_ALGORITHM_TIME_HPP

//! Useful define macros
#include <soccer/defines.h>

//! Include relevant concepts
#include <soccer/concepts/state_concepts.hpp>
#include <soccer/concepts/game_concepts.hpp>

SOCCER_NAMESPACE_BEGIN

// State and Game algorithms
/*!
 * \brief check if the game is in the first normal half
 */
template < typename State,
           typename Game >
bool match_in_first_normal_half( State const& _state,
                                 Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    return time( _state ) >= match_start_time( _game ) &&
           time( _state ) <= match_start_time( _game) + normal_half_time_duration( _game );
}

/*!
 * \brief check if the game is in the second normal half
 */
template < typename State,
           typename Game >
bool match_in_second_normal_half( State const& _state,
                                  Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto first_normal_half_end_time = match_start_time( _game ) + normal_half_time_duration( _game );
    const auto normal_full_time = first_normal_half_end_time + normal_half_time_duration( _game );

    return time( _state ) > first_normal_half_end_time &&
           time( _state ) <= normal_full_time;
}

/*!
 * \brief check if the game is in the normal time
 */
template < typename State,
           typename Game >
bool match_in_normal_time( State const& _state,
                           Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto normal_full_time = match_start_time( _game ) + normal_half_time_duration( _game ) * 2;

    return time( _state ) >= match_start_time( _game ) &&
           time( _state ) <= normal_full_time;
}

/*!
 * \brief check if the game is in the first extra half
 */
template < typename State,
           typename Game >
bool match_in_first_extra_half( State const& _state,
                                Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto match_extra_time_start = match_start_time( _game ) + normal_half_time_duration( _game ) * 2;
    const auto extra_time_first_half_end_time = match_extra_time_start + extra_half_time_duration( _game );

    return time( _state ) > match_extra_time_start &&
           time( _state ) <= extra_time_first_half_end_time;
}

/*!
 * \brief check if the game is in the second extra half
 */
template < typename State,
           typename Game >
bool match_in_second_extra_half( State const& _state,
                                 Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto extra_time_first_half_end_time = match_start_time( _game ) + normal_half_time_duration( _game ) * 2 + extra_half_time_duration( _game );
    const auto extra_time_second_half_end_time = extra_time_first_half_end_time + extra_half_time_duration( _game );

    return time( _state ) > extra_time_first_half_end_time &&
           time( _state ) <= extra_time_second_half_end_time;
}

/*!
 * \brief check if the game is in the normal time
 */
template < typename State,
           typename Game >
bool match_in_extra_time( State const& _state,
                          Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto extra_time_start = match_start_time( _game ) + normal_half_time_duration( _game ) * 2;
    const auto extra_time_end = extra_time_start + extra_half_time_duration( _game ) * 2;

    return time( _state ) > extra_time_start( _game ) &&
           time( _state ) <= extra_time_end;
}

// Useful functors

/*!
 * \struct position_within_pitch_pred
 */
template < typename Position,
           typename Game >
struct position_within_pitch_pred
    : public std::binary_function< Position, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    position_within_pitch_pred() { }

    bool operator()( Position const& pos, Game const& _game )
    {
        return is_within_pitch( pos, _game );
    }
};

/*!
 * \struct match_in_first_normal_half_pred
 */
template < typename State,
           typename Game >
struct match_in_first_normal_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_first_normal_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_first_normal_half( _state, _game );
    }
};

/*!
 * \struct match_in_second_normal_half_pred
 */
template < typename State,
           typename Game >
struct match_in_second_normal_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_second_normal_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_second_normal_half( _state, _game );
    }
};

/*!
 * \struct match_in_first_extra_half_pred
 */
template < typename State,
           typename Game >
struct match_in_first_extra_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_first_extra_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_first_extra_half( _state, _game );
    }
};

/*!
 * \struct match_in_second_extra_half_pred
 */
template < typename State,
           typename Game >
struct match_in_second_extra_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_second_extra_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_second_extra_half( _state, _game );
    }
};

SOCCER_NAMESPACE_END

#endif // SOCCER_ALGORITHM_TIME_HPP
