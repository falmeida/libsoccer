#ifndef SOCCER_GEOMETRY_DIRECTION_HPP
#define SOCCER_GEOMETRY_DIRECTION_HPP

//! Useful define macros
#include <soccer/defines.h>

#include <soccer/core/access.hpp>

//! Import relevant soccer concepts//! Useful define macros
#include <soccer/concepts/object_concepts.hpp>

//! Import relevant Boost meta-programming constructs
#include <boost/mpl/if.hpp>
#include <boost/mpl/logical.hpp>

SOCCER_NAMESPACE_BEGIN
namespace core_dispatch {

    template < typename Object1Tag, typename Object2Tag, typename Object1, typename Object2 >
    struct direction_calculation;

    /*!
     * \brief Calculate the direction in degrees between two positions
     * \struct direction_calculation
     */
    template < typename FromPosition, typename ToPosition >
    struct direction_calculation< soccer::position_tag, soccer::position_tag, FromPosition, ToPosition>
    {
        static inline
        double apply( FromPosition const& from_pos, ToPosition const& to_pos )
        {
            BOOST_CONCEPT_ASSERT(( PositionConcept<FromPosition> ));
            BOOST_CONCEPT_ASSERT(( PositionConcept<ToPosition> ));

            // atan2 normalizes angles to the interval [-pi,pi]
            return std::atan2( soccer::get_y( to_pos ) - soccer::get_y( from_pos ),
                               soccer::get_x( to_pos ) - soccer::get_x( from_pos ) )
                    * RAD2DEG;
        }
    };

    template < typename FromStaticObject, typename ToStaticObject >
    /*!
     * \brief Calculate the direction in degrees between two players
     * \struct direction_calculation
     */
    struct direction_calculation< soccer::static_object_tag, soccer::static_object_tag, FromStaticObject, ToStaticObject >
    {
        static inline
        double apply( FromStaticObject const& from_obj , ToStaticObject const& to_obj )
        {
            BOOST_CONCEPT_ASSERT(( StaticObjectConcept< FromStaticObject > ));
            BOOST_CONCEPT_ASSERT(( StaticObjectConcept< ToStaticObject > ));

            return direction_calculation< soccer::position_tag,
                                          soccer::position_tag,
                                          typename position_type< FromStaticObject >::type,
                                          typename position_type< ToStaticObject >::type>
                    ::apply( get_position( from_obj ), get_position( to_obj ) );
        }
    };

    template < typename StaticObject, typename Position >
    /*!
     * \brief Calculate the direction in degrees between a player and a position
     * \struct direction_calculation
     */
    struct direction_calculation< soccer::static_object_tag, soccer::position_tag, StaticObject, Position >
    {
        static inline
        double apply( StaticObject const& obj, Position const& pos )
        {
            BOOST_CONCEPT_ASSERT(( StaticObjectConcept< StaticObject > ));
            BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));

            return direction_calculation< soccer::position_tag,
                                          soccer::position_tag,
                                          typename position_type< StaticObject >::type,
                                          Position>
                    ::apply( get_position( obj ), pos );
        }
    };

    template < typename Position, typename StaticObject >
    /*!
     * \brief Calculate the direction in degrees between a player and a position
     * \struct direction_calculation
     */
    struct direction_calculation< soccer::position_tag, soccer::static_object_tag, Position, StaticObject >
    {
        static inline
        double apply( Position const& pos, StaticObject const& obj )
        {
            BOOST_CONCEPT_ASSERT(( StaticObjectConcept< StaticObject > ));
            BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));

            return direction_calculation< soccer::position_tag,
                                          soccer::position_tag,
                                          Position,
                                          typename position_type< StaticObject >::type >
                    ::apply( pos, get_position( pos ) );
        }
    };


} // end namespace core_dispatch

template < typename Object1, typename Object2 >
struct direction_result {
    typedef double type;
};

template < typename Object1, typename Object2 >
typename direction_result< Object1, Object2 >::type
direction( Object1 const& from, Object2 const& to )
{
    typedef typename boost::mpl::if_c< boost::is_base_of< static_object_tag, typename tag< Object1 >::type >::value,
                                       static_object_tag,
                                       position_tag
            >::type object1_tag;
    typedef typename boost::mpl::if_c< boost::is_base_of< static_object_tag, typename tag< Object2 >::type >::value,
                                       static_object_tag,
                                       position_tag
            >::type object2_tag;

    typedef core_dispatch::direction_calculation< object1_tag, object2_tag, Object1, Object2 > impl;

    return impl::apply( from, to );
}

SOCCER_NAMESPACE_END

#endif // SOCCER_GEOMETRY_DIRECTION_HPP
