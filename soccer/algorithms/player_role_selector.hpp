#ifndef SOCCER_PLAYER_ROLE_SELECTOR_HPP
#define SOCCER_PLAYER_ROLE_SELECTOR_HPP

#include <boost/concept_check.hpp>
#include <boost/property_map/property_map.hpp>

#include <string>
#include <fstream>
#include <iostream>
#include <map>

namespace soccer {


/*!
 \brief Player strategic roles selector
 */
template < typename FormationId,
           typename PlayerId,
           typename PlayerRole >
struct formation_role_selector
{
    typedef std::map< PlayerId, PlayerRole > PlayerRoleMap;
    typedef std::map< FormationId, PlayerRoleMap > FormationPlayerRoleMap;

    FormationPlayerRoleMap M_formation_player_role_map;
    PlayerRoleMap M_default_player_role_map;

    formation_role_selector() { }

    template < typename State,
               typename Strategy,
               typename PlayerRoleMap >
    void operator() ( typename side_type< typename player_type< State >::type >::type const team_side,
                      State const& state,
                      Strategy const& strategy,
                      PlayerRoleMap& player_role_pmap )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept<PlayerRoleMap, PlayerId> ));
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits<PlayerRoleMap>::value_type,
                                  PlayerRole>::value ),
                                "Player role descriptor types mismatch");

        typedef typename formation_id_type< Strategy >::type strategy_formation_id_descriptor;
        typedef typename player_id_type< Strategy >::type strategy_player_id_descriptor;

        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< PlayerRoleMap >::key_type,
                                                  PlayerId>::value ),
                                "PlayerId and PlayerRoleMap::key_type types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< PlayerRoleMap >::value_type,
                                                  PlayerRole>::value ),
                                "PlayerRole and PlayerRoleMap::value_type types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< strategy_formation_id_descriptor,
                                                  FormationId>::value ),
                                "FormationId and strategy::formation_id_descriptor types mismatch ");


        strategy_formation_id_descriptor _curr_form_id = soccer::strategy::current_formation_id( player_role_pmap  );
        FormationPlayerRoleMap::const_iterator itf = M_formation_player_role_map.find( _curr_form_id );
        if ( itf == M_formation_player_role_map.end() )
        {
            std::clog << "Formation " << _curr_form_id
                      << " not found. Using default player roles "
                      << std::endl;

            for ( PlayerRoleMap::const_iterator itp = M_default_player_role_map.begin();
                  itp != M_default_player_role_map.end(); itp++ )
            {
                boost::put( player_role_map,
                            itp->first,
                            itp->second );
            }
            return;
        }

        for ( PlayerRoleMap::const_iterator itp = itf->second.begin();
              itp != itf->second.end(); itp++ )
        {
            boost::put( player_role_map,
                        itp->first,
                        itp->second );
        }
    }

    /*!
     * \brief add_role
     * \param player_id_t
     * \param player_role_descriptor
     */
    void add_role( PlayerId player_id_t,
                   PlayerRole player_role_descriptor )
    {
        M_default_player_role_map.insert( std::make_pair( player_id_t, player_role_descriptor ));
    }

    /*!
     * \brief add_role
     * \param formation_id
     * \param player_id_t
     * \param player_role_descriptor
     */
    void add_role( FormationId formation_id,
                   PlayerId player_id_t,
                   PlayerRole player_role_descriptor )
    {
        typename FormationPlayerRoleMap::iterator itf = M_formation_player_role_map.find( formation_id );
        if ( itf != M_formation_player_role_map.end() )
        {
            PlayerRoleMap& player_role_map = itf->second;
            assert( player_role_map.find( player_id_t ) == player_role_map.end() );
            player_role_map.insert( std::make_pair( player_id_t, player_role_descriptor ));
            return;
        }

        PlayerRoleMap new_player_role_map;
        new_player_role_map.insert( std::make_pair( player_id_t, player_role_descriptor ));
        M_formation_player_role_map.insert( std::make_pair( formation_id, new_player_role_map ) );
    }
};

/*!
 * \brief formation_role_selector_single_line_reader
 * \struct formation_role_selector_single_line_reader
 */
template < typename FormationId,
           typename PlayerId,
           typename PlayerRole >
struct formation_role_selector_reader
{
    std::string read_line( std::istream& is )
    {
        std::string line_buf;
        while (std::getline( is, line_buf )) {
            if ( line_buf.empty()
                         || line_buf[0] == '#'
                         || ! line_buf.compare( 0, 2, "//" ) )
            {
                // TODO Does this ignore empty spaces?
                continue; // Ignore comments and empty lines
            }
            break;
        }
        return line_buf;
    }

    std::ifstream M_ifs;

public:
    formation_role_selector_reader( const std::string& file_name )
        : M_ifs( file_name.c_str(), std::ifstream::in )
    {

    }

    template < typename FormationId,
               typename PlayerId,
               typename PlayerRole >
    void operator()( formation_roles_selector<FormationId, PlayerId, PlayerRole>& role_selector )
    {

        assert( M_ifs.is_open() );

        while ( true )
        {
            std::istringstream istr( this->read_line( M_ifs ) );
            if ( M_ifs.eof() )
            {
                break;
            }
            // Read formation id, player id descriptor, role descriptor
            FormationId formation_id_descriptor;
            istr >> formation_id_descriptor;
            PlayerId player_id_t;
            istr >> player_id_t;
            PlayerRole role_descriptor;
            istr >> role_descriptor;

            // register role
            role_selector.add_role( formation_id_descriptor, player_id_t, role_descriptor );
        }
    }
};

} // end namespace soccer

#endif // SOCCER_PLAYER_ROLE_SELECTOR_HPP
