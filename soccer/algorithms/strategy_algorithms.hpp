#ifndef SOCCER_STRATEGY_ALGORITHMS_HPP
#define SOCCER_STRATEGY_ALGORITHMS_HPP

//! Include useful macros
#include <soccer/defines.h>

//! Include relevant algorithms
#include <soccer/algorithms/soccer_algorithms.hpp>
#include <soccer/formation_algorithms.hpp>

#include <soccer/predicates/state_predicates.hpp>

//! Include relevant traits
#include <soccer/traits/tactic_traits.hpp>
#include <soccer/traits/formation_traits.hpp>

//! Include relevant concepts to be checked
#include <soccer/concepts/soccer_concepts.hpp>
#include <soccer/concepts/positionings_concepts.hpp>
#include <soccer/concepts/strategy_concepts.hpp>


#include <boost/property_map/property_map.hpp>
#include <boost/type_traits.hpp>
#include <boost/functional.hpp>
#include <boost/tuple/tuple.hpp>

#include <iostream>
#include <map>
#include <algorithm>


SOCCER_NAMESPACE_BEGIN

// end shortcut accessors

/* template < typename Strategy,
           typename Positionings >
typename position_type< Strategy >::type
position( const typename role_id_type<Strategy>::type pid,
          const Positionings& posits,
          const Strategy& strategy  )
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));
    BOOST_CONCEPT_ASSERT(( PositioningsConcept<Positionings> ));

    const auto posit_pid = get_positioning( pid, posits );
    return position( posit_pid, strategy );
} */

template < typename Position,
           typename Strategy >
inline typename position_type< Strategy >::type
nearest_position( Position const& _pos,
                  Strategy const& _strategy )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));

    typedef typename role_iterator_type< Strategy >::type role_iterator_t;
    role_iterator_t _role_begin, _role_end;
    boost::tie( _role_begin, _role_end ) = get_roles( _strategy );

    assert( _role_begin != _role_end );
    auto itp = _role_begin;
    auto _nearest_role_id = *itp;
    auto _nearest_dist = comparable_distance( _pos, get_position( _nearest_role_id, _strategy ));

    for ( itp++ ; itp != _role_end; itp++ )
    {
        auto _dist = comparable_distance( _pos, get_position( *itp, _strategy ));
        if ( _dist < _nearest_dist )
        {
            _nearest_role_id = *itp;
            _nearest_dist = _dist;
        }
    }

    return get_position( _nearest_role_id, _strategy );
}



/*!
 \brief Player strategic positions updater
 */
template < typename Pitch,
           typename State >
struct strategy_positions_updater
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    Pitch const* M_pitch;
    State const* M_state;

    strategy_positions_updater( const Pitch& _pitch ,
                                State const& _state )
        : M_pitch( &_pitch )
        , M_state( &_state )
    {

    }

    template < typename Strategy,
               typename FormationPropertyMap >
    void operator() ( typename side_type< typename player_type< State >::type >::type const& team_side,
                      Strategy& strategy,
                      FormationPropertyMap _formation_store )
    {   
        BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));

        typedef typename role_id_type<Strategy>::type role_id_t;
        typedef typename formation_id_type<Strategy>::type formation_id_t;

        BOOST_CONCEPT_ASSERT(( boost::ReadablePropertyMapConcept< FormationPropertyMap, formation_id_t > ));

        typedef typename boost::property_traits<Strategy>::value_type formation_t;

        BOOST_CONCEPT_ASSERT(( FormationConcept< formation_t> ));

        typedef typename player_iterator_type< formation_t >::type formation_player_iterator_t;
        typedef typename player_id_type< formation_t >::type  formation_player_id_t;
        typedef typename position_type< formation_t >::type formation_position_t;

        BOOST_STATIC_ASSERT_MSG(( boost::is_same< role_id_t, formation_player_id_t >::value ),
                                "Player id types mismatch");


        constrain_position_to_field<Pitch> _constrain_position_to_field( *M_pitch );
        constrain_position_behind_offside_line<Pitch,State> _constrain_position_behind_offside_line( team_side, *M_pitch, *M_state );

        const auto _formation_id = get_formation( strategy );
        const auto& _formation = boost::get( _formation_store, _formation_id );

        formation_player_iterator_t _player_begin, _player_end;
        boost::tie(_player_begin, _player_end) = soccer::get_players( _formation );
        for( auto itfp = _player_begin; itfp != _player_end; itfp++ )
        {
            // HACK Assumes role_descriptor for all formations is the same as the player_id_t in strategy
            // TODO Should a mapping function be introduced?
            const auto _player_position = get_position( *itfp, *M_state, _formation );

            // apply positioning constraints
            _constrain_position_to_field( _player_position );
            _constrain_position_behind_offside_line( _player_position );

            set_position( *itfp, _player_position, strategy );
        }
    }
};

/*!
 * \brief number of players in a strategy
 * \return number of players in a strategy
 */
template < typename Strategy >
typename std::iterator_traits< typename player_iterator_type< Strategy >::type >::difference_type
num_players( const Strategy& _strategy )
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));

    typedef typename role_iterator_type< Strategy >::type role_iterator_t;

    role_iterator_t _role_begin, _role_end;
    boost::tie(_role_begin, _role_end) = get_roles( _strategy );
    return std::distance( _role_begin, _role_end );
}

/*!
 * \brief number of tactics in a strategy
 * \return number of tactics in a strategy
 */
template <typename Strategy>
std::size_t
num_tactics( const Strategy& st )
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));
    typedef strategy_traits<Strategy> strategy_traits_t;
    typedef typename strategy_traits_t::tactic_iterator tactic_iterator;

    tactic_iterator f,l;
    boost::tie(f,l) = get_tactics( st );
    return std::distance(f,l);
}

/*!
 * \brief number of formations in a strategy
 * \return number of formations in a strategy
 */
template <typename Strategy>
std::size_t
num_formations( const Strategy& st )
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));
    typedef strategy_traits<Strategy> strategy_traits_t;
    typedef typename strategy_traits_t::formation_iterator formation_iterator;

    formation_iterator f,l;
    boost::tie(f,l) = get_formations( st );
    return std::distance(f,l);
}

/*!
 * \brief number of fluxes in a strategy
 * \return number of fluxes in a strategy
 */
template <typename Strategy>
std::size_t
num_fluxes( const Strategy& st )
{
    BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));
    typedef strategy_traits<Strategy> strategy_traits_t;
    typedef typename strategy_traits_t::flux_iterator flux_iterator;

    flux_iterator f,l;
    boost::tie(f,l) = fluxes( st );
    return std::distance(f,l);
}

/*!
 * \brief The strategy_printer struct
 */
struct strategy_printer
{
    std::ostream& M_os;
    strategy_printer()
        : M_os( std::cout )
    {

    }

    template <typename Strategy>
    void operator()( const Strategy& st ) const
    {
        BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));

        auto tactics_iterators = tactics( st );
        print( M_os, "Tactics", tactics_iterators.first, tactics_iterators.second );

        auto formations_iterators = formations( st );
        print( M_os, "Formations", formations_iterators.first, formations_iterators.second );

        auto fluxes_iterators = fluxes( st );
        print( M_os, "Fluxes", fluxes_iterators.first, fluxes_iterators.second );

        auto players_iterators = players( st );
        print( M_os, "Players", players_iterators.first, players_iterators.second );
    }

private:
    template <typename OutputStream,
              typename Iterator>
    void print( OutputStream& os, const char* label, Iterator first, Iterator last )
    {
        os << label << "=[";
        if ( first != last )
        {
            os << *first;
            for ( ; first != last ; first++ )
            {
                os << "," << *first;
            }
        }
        os << "]" << std::endl;
    }
};

/*!
  \brief Strategic information updater
*/
template < typename TacticSelector,
           typename FormationSelector,
           typename PositionModifier >
struct strategy_updater {

    TacticSelector M_tactic_selector;
    FormationSelector M_formation_selector;
    PositionModifier M_position_modifier;

    strategy_updater( const TacticSelector tactic_selector,
                      const FormationSelector formation_selector,
                      const PositionModifier position_updater )
        : M_tactic_selector( tactic_selector )
        , M_formation_selector( formation_selector )
        , M_position_modifier( position_updater )
    {

    }

    template <typename State,
              typename Strategy>
    void operator()( State const& _state,
                     Strategy& _strategy )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));

        typedef strategy_traits<Strategy> strategy_traits_t;
        typedef typename tactic_id_type<Strategy>::type tactic_id_t;
        typedef typename formation_id_type<Strategy>::type formation_id_t;

        // updateTactic( wm );
        const auto old_tactic_id = current_tactic_id( _strategy );
        M_tactic_selector( _strategy );
        if ( old_tactic_id != current_tactic_id( _strategy ) )
        {
            // tactic changed
        }

        // updateFormation ( wm );
        const auto old_formation_id = current_formation_id( _strategy );
        M_formation_selector( _strategy );
        const auto new_formation_id = current_formation_id( _strategy );
        if ( old_formation_id != new_formation_id )
        {
            // formation changed
        }

        // updatePositions( wm );
        M_position_modifier( _strategy );
    }
};

/*!
 \brief reset player positionings in strategy
 */
template < typename Strategy >
void reset_positionings( Strategy& strategy)
{
    BOOST_CONCEPT_ASSERT(( MutableStrategyConcept<Strategy> ));

    typedef typename role_iterator_type< Strategy >::type role_iterator_t;

    role_iterator_t _role_begin,_role_end;
    boost::tie( _role_begin, _role_end ) = get_roles( strategy );
    for ( auto itp = _role_begin; itp != _role_end; itp++ )
    {
        set_positioning( *itp, *itp, strategy );
    }
}

SOCCER_NAMESPACE_END

#endif // SOCCER_STRATEGY_ALGORITHMS_HPP
