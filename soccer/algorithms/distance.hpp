#ifndef SOCCER_GEOMETRY_DISTANCE_HPP
#define SOCCER_GEOMETRY_DISTANCE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/concepts/object_concepts.hpp>

#include <soccer/core/position_type.hpp>
#include <soccer/core/tag.hpp>
#include <soccer/core/tags.hpp>


#include <boost/geometry.hpp>

#include <boost/mpl/if.hpp>
#include <boost/mpl/and.hpp>
#include <boost/mpl/or.hpp>
#include <boost/mpl/not.hpp>
#include <boost/mpl/has_xxx.hpp>
#include <boost/mpl/assert.hpp>


SOCCER_NAMESPACE_BEGIN

namespace dispatch
{
    template < typename Object1Tag, typename Object2Tag, typename Object1, typename Object2 >
    struct distance {
        BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object1Tag,Object2Tag, Object1, Object2>) );
    };

    template < typename Point1, typename Point2 >
    struct distance < point_tag, point_tag, Point1, Point2 >
    {
        static
        inline typename boost::geometry::default_distance_result< Point1, Point2 >::type
        apply( Point1 const& pos1, Point2 const& pos2 )
        {
            return boost::geometry::distance( pos1, pos2 );
        }
    };

    template < typename Point1, typename Object2 >
    struct distance < point_tag, typename boost::is_base_of< static_object_tag, Object2 >::type, Point1, Object2 >
    {
        static
        inline typename boost::geometry::default_distance_result< Point1, typename position_type< Object2 >::type >::type
        apply( Point1 const& pos1, Object2 const& obj2 )
        {
            return distance< position_tag,
                             typename tag< typename position_type< Object2 >::type >::type,
                             Point1,
                             typename position_type< Object2 >::type
                    >::apply( pos1, get_position( obj2 ));
        }
    };

    template < typename Object1, typename Point2 >
    struct distance < typename boost::is_base_of< static_object_tag, Object1 >::type, point_tag, Object1, Point2 >
    {
        static
        inline typename boost::geometry::default_distance_result< typename position_type< Object1 >::type, Point2 >::type
        apply( Object1 const& obj1, Point2 const& pos2 )
        {
            return distance< typename tag< typename position_type< Object1 >::type >::type,
                             point_tag,
                             typename position_type< Object1 >::type,
                             Point2
                    >::apply( get_position( obj1 ), pos2 );
        }
    };

    template < typename Object1, typename Object2 >
    struct distance< typename boost::is_base_of< static_object_tag, Object1 >::type,
                     typename boost::is_base_of< static_object_tag, Object2 >::type,
                     Object1,
                     Object2 >
    {
       // BOOST_CONCEPT_ASSERT(( StaticObject< Object1 > ));
       // BOOST_CONCEPT_ASSERT(( StaticObject< Object2 > ));

        static
        inline typename boost::geometry::default_distance_result< typename position_type< Object1 >::type,
                                                                  typename position_type< Object2 >::type >::type
        apply( Object1 const& obj1, Object2 const& obj2 )
        {
            return distance< typename tag< typename position_type< Object1 >::type >::type,
                             typename tag< typename position_type< Object2 >::type >::type,
                             typename position_type< Object1 >::type,
                             typename position_type< Object2 >::type
                    >::apply( get_position( obj1 ), get_position( obj2 ) );
        }
    };

} // end namespace dispatch

/*!
\brief Get the distance between two position concepts
\note proxy method for boost::geometry::distance
\tparam Object1 Position descriptor
\tparam Object2 Position descriptor
\param obj1
\param obj2
\return the distance between the two objects

*/
// HACK Should be something like typename default_distance_result<Object1, Object2>::type
template < typename Object1,
           typename Object2 >
double
distance( Object1 const& obj1,
          Object2 const& obj2 )
{

   return dispatch::distance< typename tag< Object1 >::type,
                              typename tag< Object2 >::type,
                              Object1,
                              Object2
                            >::apply( obj1, obj2 );
}


SOCCER_NAMESPACE_END

#endif // SOCCER_GEOMETRY_DISTANCE_HPP
