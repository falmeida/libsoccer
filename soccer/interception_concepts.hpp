#ifndef SOCCER_INTERCEPTION_CONCEPTS_HPP
#define SOCCER_INTERCEPTION_CONCEPTS_HPP

#include <soccer/core/tags.hpp>
#include <soccer/interception_traits.hpp>

#include <soccer/action_concepts.hpp>
#include <soccer/soccer_concepts.hpp>
#include <soccer/position_concepts.hpp>

#include <boost/concept_check.hpp>
#include <boost/static_assert.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(InterceptionConcept,(I))
    : public ActionConcept<I>
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< interception_tag, typename tag< I >::type >::value ),
                              "Type is not categorized with the expected category tag");
    typedef typename position_type< I >::type position_t;
    typedef typename speed_type< I >::type speed_t;

    BOOST_CONCEPT_ASSERT(( PositionConcept<position_t> ));


    BOOST_CONCEPT_USAGE(InterceptionConcept)
    {
        const_constraints( _interception );
    }
private:
    void const_constraints( const I& const_interception )
    {
        _pos = get_position( const_interception );
        _speed = get_first_speed( const_interception );
    }

    I _interception;

    position_t _pos;
    speed_t _speed;
};

BOOST_concept(MutableInterceptionConcept,(I))
    : public InterceptionConcept<I>
{
     typedef typename position_type< I >::type position_t;
     typedef typename speed_type< I >::type speed_t;

    BOOST_CONCEPT_USAGE(MutableInterceptionConcept)
    {
        set_position( _pos, _interception );
        set_first_speed( _speed, _interception );
    }

    I _interception;
    position_t _pos;
    speed_t _speed;
};



BOOST_concept(InterceptionTableConcept,(IT))
{
    /* typedef typename interception_id_type< IT >::type interception_id_t;
    typedef typename interception_type< IT >::type interception_t;
    typedef typename interception_iterator_type<IT>::type interception_iterator; */

    // BOOST_CONCEPT_ASSERT(( boost::Comparable<player_id_t> ));
    /* BOOST_CONCEPT_ASSERT(( boost::Comparable<interception_id_t> ));
    BOOST_CONCEPT_ASSERT(( InterceptionConcept<interception_t> ));
    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructibleConcept<interception_t> ));
    BOOST_CONCEPT_ASSERT(( boost::CopyConstructibleConcept<interception_t> )); */

    BOOST_CONCEPT_USAGE(InterceptionTableConcept)
    {
        const_constraints( _it );
    }

protected:
    void const_constraints( const IT& const_it )
    {
        // _interception_iterators = interceptions( const_it );
        // const interception_t& _int = get_interception( _int_id, const_it );
    }

    IT _it;
    /* std::pair< interception_iterator,
               interception_iterator> _interception_iterators;
    interception_id_t _int_id;*/
};


BOOST_concept(MutableInterceptionTableConcept,(IT))
    : public InterceptionTableConcept<IT>
{
     /*typedef typename interception_id_type< IT >::type interception_id_t;
     typedef typename interception_type< IT >::type interception_t;
     typedef typename interception_iterator_type< IT >::type interception_iterator; */

     BOOST_CONCEPT_USAGE(MutableInterceptionTableConcept)
     {
         // _int_id = add_interception( _int, _it );
     }

private:
     IT _it;
     /* interception_t _int;
     interception_id_t _int_id;*/
};


}

#endif // SOCCER_INTERCEPTION_CONCEPTS_HPP
