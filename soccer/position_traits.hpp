#ifndef SOCCER_POSITION_TRAITS_HPP
#define SOCCER_POSITION_TRAITS_HPP

namespace soccer {

template <typename Position>
struct position_traits {
    // typedef typename Position::distance_descriptor distance_descriptor;
    typedef typename Position::coordinate_descriptor coordinate_descriptor;
    // typedef typename Position::direction_descriptor direction_descriptor;

    static Position unknown_position()
    {
        return Position::unknown_position();
    }
};

//!< Position::distance_type distance( Position const& p1, Position const& p2 )
//!< Angle::angle_type angle( Position const& p1, Position const& p2 )
//!< Position::cordinate_descriptor getX( Position const& p )
//!< Position::cordinate_descriptor getY( Position const& p )

}

#endif // SOCCER_POSITION_TRAITS_HPP
