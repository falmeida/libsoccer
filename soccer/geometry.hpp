#ifndef SOCCER_GEOMETRY_HPP
#define SOCCER_GEOMETRY_HPP

//! Useful define macros
#include <soccer/defines.h>

//! Relevant type definitions
#include <soccer/core/coordinate_type.hpp>
#include <soccer/core/direction_type.hpp>

//! Import concepts to be checked
#include <soccer/concepts/position_concepts.hpp>


SOCCER_NAMESPACE_BEGIN

static const double PI = 3.14159265359;
static const double TWO_PI = PI * 2.0;
static const double DEG2RAD = PI / 180.0;
static const double RAD2DEG = 180.0 / M_PI;

/*!
\brief Get the value of a point coordinate
\tparam Position Position descriptor
\param pos
\return the value of the x-coordinate
*/
template < typename Position >
typename boost::enable_if< boost::is_same< typename boost::geometry::tag< Position >::type,
                                           boost::geometry::point_tag >,
                           typename boost::geometry::coordinate_type< Position >::type
                         >::type
get_x( Position const& pos )
{
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::ConstPoint<Position> ));

    return boost::geometry::get<0>( pos );
}

/*!
\brief Get the value of a point coordinate
\tparam Position Position descriptor
\param pos
\return the value of the y-coordinate
*/
template < typename Position >
typename boost::enable_if< boost::is_same< typename boost::geometry::tag< Position >::type,
                                           boost::geometry::point_tag >,
                           typename boost::geometry::coordinate_type< Position >::type
                         >::type
get_y( Position const& pos )
{
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::ConstPoint<Position> ));

    return boost::geometry::get<1>( pos );
}

/*!
\brief Set the value of a point coordinate
\tparam Position Position descriptor
\param pos
\param value
\return the value of the x-coordinate
*/
template <typename Position>
typename boost::enable_if< boost::is_same< typename boost::geometry::tag< Position >::type,
                                           boost::geometry::point_tag >,
                           void
                         >::type
set_x( const typename boost::geometry::coordinate_type< Position >::type& coord,
       Position& point )
{
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::Point<Position> ));

    boost::geometry::set<0>( point, coord );
}

/*!
\brief Get the value of a point coordinate
\tparam Position Position descriptor
\param pos
\param value
\return the value of the y-coordinate
*/
template <typename Position>
typename boost::enable_if< boost::is_same< typename boost::geometry::tag< Position >::type,
                                           boost::geometry::point_tag >,
                           void
                         >::type
set_y( const typename boost::geometry::coordinate_type< Position >::type& coord,
       Position& point )
{
    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::Point<Position> ));

    boost::geometry::set<1>( point, coord );
}

//
// Get direction
//
/*!
\brief get direction value of velocity object
\details \details_get_set
\tparam VelocityObject \tparam_velocity_object
\param object \param_object
\return velocity value
\ingroup get
*/
template < typename Vector >
inline typename coordinate_type< typename soccer::util::bare_type< Vector >::type >::type
direction( Vector const& _vec )
{
    // TODO Assert that object is registered with a velocity tag
    auto _vx = get_x( _vec );
    auto _vy = get_y( _vec );
    return _vy / _vx;
}

//
// Get magnitude
//
/*!
\brief get magnitude value of velocity object
\details \details_get_set
\tparam VelocityObject \tparam_velocity_object
\param object \param_object
\return velocity value
\ingroup get
*/
template < typename Vector >
inline typename coordinate_type< typename soccer::util::bare_type< Vector >::type >::type
magnitude( Vector const& _vec )
{
    // TODO Assert that object is registered with a velocity tag
    auto _vx = get_x( _vec );
    auto _vy = get_y( _vec );
    auto _mag = std::sqrt( std::pow( _vx, 2 ) + std::pow( _vy, 2 ));
    return _mag;
}

template <typename OutputPosition,
          typename Direction,
          typename Magnitude>
OutputPosition
vector_from_polar_degrees( const Direction dir, const Magnitude mag)
{
    return OutputPosition( std::cos( dir * DEG2RAD ) * mag,
                           std::sin( dir * DEG2RAD ));
}

/*
 *    inline
    void set_from_polar( double magnitude, rcsc::AngleDeg& direction, rcsc::Vector2D& v)
    {
        v = rcsc::Vector2D::from_polar( magnitude, direction );
    }

    // velocity model functions
    inline
    double get_speed( const rcsc::Vector2D& v )
    {
        return v.r();
    }

    inline
    rcsc::AngleDeg direction( const rcsc::Vector2D& v )
    {
        return v.th();
    }

    /// position model functions
    inline
    double distance ( const rcsc::Vector2D& v1,
                      const rcsc::Vector2D& v2 )
    {
        return v1.dist( v2 );
    }

    // position model functions
    inline
    double direction ( const rcsc::Vector2D& from,
                       const rcsc::Vector2D& to )
    {
        rcsc::Vector2D res = to - from;
        return res.th().degree();
    }

    }
 **/
SOCCER_NAMESPACE_END

#endif // SOCCER_GEOMETRY_HPP
