#ifndef SOCCER_ARCHETYPES_HPP
#define SOCCER_ARCHETYPES_HPP

#include <soccer/soccer.hpp>
#include <boost/concept_archetype.hpp>
#include <boost/graph/graph_archetypes.hpp>

namespace soccer {

namespace detail {
  struct null_scene_archetype :
          public boost::null_archetype<> {
    struct scene_category { };
  };
}

//===========================================================================
template <typename Player,
          typename Ball,
          typename Time,
          typename GameMode,
          typename Base = detail::null_scene_archetype >
struct static_scene_archetype
        : public Base
{
    typedef typename Base::scene_category base_scene_category;
    struct scene_category
        : public static_scene_tag,
            public base_scene_category { };

  typedef Player player_t;
  typedef Ball ball_t;
  typedef Time time_descriptor;
  typedef GameMode game_mode_descriptor;

  typedef void players_iterator;
};

template <typename P, typename B, typename T, typename G, typename Base>
T time(const typename static_scene_archetype<P,B,T,G,Base>& )
{
    T t();
    return t;
}

template <typename P, typename B, typename T, typename G, typename Base>
typename static_scene_archetype<P,B,T,G,Base>::players_size_type
numPlayers(const static_scene_archetype<P,B,T,G,Base>& )
{
  return 0;
}

template <typename P, typename B, typename T, typename G, typename Base>
typename static_scene_archetype<P,B,T,G,Base>::players_size_type
num_left_players(const static_scene_archetype<P,B,T,G,Base>& )
{
  return 0;
}

template <typename P, typename B, typename T, typename G, typename Base>
typename static_scene_archetype<P,B,T,G,Base>::players_size_type
num_right_players(const static_scene_archetype<P,B,T,G,Base>& )
{
  return 0;
}

template <typename P, typename B, typename T, typename G, typename Base>
std::pair< typename static_scene_archetype<P,B,T,G,Base>::adjacent_player_iterator,
           typename static_scene_archetype<P,B,T,G,Base>::adjacent_player_iterator >
adjacent_players(const P&, const static_scene_archetype<P,B,T,G,Base>& )
{
  typedef typename static_scene_archetype<P,B,T,G,Base>::adjacent_player_iterator Iter;
  return std::make_pair(Iter(), Iter());
}

template <typename P, typename B, typename T, typename G, typename Base>
std::pair< typename static_scene_archetype<P,B,T,G,Base>::all_players_iterator,
           typename static_scene_archetype<P,B,T,G,Base>::all_players_iterator >
allPlayersDistanceFromBall(const P&, const static_scene_archetype<P,B,T,G,Base>& )
{
  typedef typename static_scene_archetype<P,B,T,G,Base>::all_players_iterator Iter;
  return std::make_pair(Iter(), Iter());
}

template <typename P, typename B, typename T, typename G, typename Base>
std::pair< typename static_scene_archetype<P,B,T,G,Base>::all_players_iterator,
           typename static_scene_archetype<P,B,T,G,Base>::all_players_iterator >
allPlayersAngleFromBall(const P&, const static_scene_archetype<P,B,T,G,Base>& )
{
  typedef typename static_scene_archetype<P,B,T,G,Base>::all_players_iterator Iter;
  return std::make_pair(Iter(), Iter());
}

template <typename P, typename B, typename T, typename G, typename Base>
typename static_scene_archetype<P,B,T,G,Base>::player_t
ballHolder(const V&, const static_scene_archetype<P,B,T,G,Base>&  )
{
    return static_scene_archetype<P,B,T,G,Base>::null_player();
}

//===========================================================================
template <typename Player,
          typename Ball,
          typename Time,
          typename GameMode,
          typename Base = detail::null_scene_archetype >
struct dynamic_scene_archetype
        : public Base
{
    typedef typename Base::scene_category base_scene_category;
    struct scene_category
        : public dynamic_scene_tag,
            public base_scene_category { };

  typedef Player player_t;
  typedef Ball ball_t;
  typedef Time time_descriptor;
  typedef GameMode game_mode_descriptor;

  typedef void players_iterator;
};

}

#endif // SOCCER_ARCHETYPES_HPP
