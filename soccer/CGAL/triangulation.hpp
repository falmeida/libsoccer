#ifndef SOCCER_CGAL_TRIANGULATION_HPP
#define SOCCER_CGAL_TRIANGULATION_HPP

//! Import useful macros
#include <soccer/defines.h>

//! CGAL relevant includes
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Projection_traits_xy_3.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/boost/graph/graph_traits_Triangulation_2.h>

//! Access functions to soccer objects
#include <soccer/core/access.hpp>

#include <iterator>

#include <algorithm>
#include <utility>

SOCCER_TRAITS_NAMESPACE_BEGIN

template <>
template < typename Traits, typename Td >
struct tag< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef delaunay_triangulation_tag type; };

// Data structure types
template <>
template < typename Traits, typename Td >
struct vertex_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex type; };

template <>
template < typename Traits, typename Td >
struct edge_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Edge type; };

template <>
template < typename Traits, typename Td >
struct face_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Face type; };

// Ids
template <>
template < typename Traits, typename Td >
struct vertex_id_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_handle type; };

template <>
template < typename Traits, typename Td >
struct edge_id_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Edge type; };

template <>
template < typename Traits, typename Td >
struct face_id_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Face_handle type; };

// Iterators
template <>
template < typename Traits, typename Td >
struct vertex_iterator_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Finite_vertices_iterator type; };

template <>
template < typename Traits, typename Td >
struct edge_iterator_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::All_edges_iterator type; };

template <>
template < typename Traits, typename Td >
struct face_iterator_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::All_faces_iterator type; };

// Circulators
template <>
template < typename Traits, typename Td >
struct vertex_circulator_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_circulator type; };

template <>
template < typename Traits, typename Td >
struct edge_circulator_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Edge_circulator type; };

template <>
template < typename Traits, typename Td >
struct face_circulator_type< CGAL::Delaunay_triangulation_2< Traits, Td > >
    { typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Face_circulator type; };

//
// Data Access
//

template <>
template < typename Traits, typename Td >
struct vertex_iterators_access< CGAL::Delaunay_triangulation_2< Traits, Td > >
{
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::All_vertices_iterator vertex_iterator_t;
    static inline std::pair< vertex_iterator_t, vertex_iterator_t >
    get( CGAL::Delaunay_triangulation_2< Traits, Td > const& _dt )
    {
        return std::make_pair( _dt.all_vertices_begin(), _dt.all_vertices_end() );
    }
};

template <>
template < typename Traits, typename Td >
struct edge_iterators_access< CGAL::Delaunay_triangulation_2< Traits, Td > >
{
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::All_edges_iterator edge_iterator_t;
    static inline std::pair< edge_iterator_t, edge_iterator_t >
    get( CGAL::Delaunay_triangulation_2< Traits, Td > const& _dt )
    {
        return std::make_pair( _dt.all_edges_begin(), _dt.all_edges_end() );
    }
};

template <>
template < typename Traits, typename Td >
struct face_iterators_access< CGAL::Delaunay_triangulation_2< Traits, Td > >
{
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::All_faces_iterator face_iterator_t;
    static inline std::pair< face_iterator_t, face_iterator_t >
    get( CGAL::Delaunay_triangulation_2< Traits, Td > const& _dt )
    {
        return std::make_pair( _dt.all_faces_begin(), _dt.all_faces_end() );
    }
};

// Circulators access
template <>
template < typename Traits, typename Td >
struct incident_vertices_access< CGAL::Delaunay_triangulation_2< Traits, Td > >
{
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_circulator vertex_circulator_t;
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_handle vertex_id_t;

    static inline
    std::pair< vertex_circulator_t, vertex_circulator_t >
    get( vertex_id_t const _vid,
         CGAL::Delaunay_triangulation_2< Traits, Td > const& _dt )
    {
        // assert( _dt.number_of_vertices() >= 3 );
        auto _vend = _dt.incident_vertices( _vid );
        auto _vbegin = _vend;
        _vbegin++;
        return std::make_pair( _vbegin, _vend );
    }
};

template <>
template < typename Traits, typename Td >
struct incident_edges_access< CGAL::Delaunay_triangulation_2< Traits, Td > >
{
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Edge_circulator edge_circulator_t;
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_handle vertex_id_t;

    static inline
    std::pair< edge_circulator_t, edge_circulator_t >
    get( vertex_id_t const _vid,
         CGAL::Delaunay_triangulation_2< Traits, Td > const& _dt )
    {
        auto _vend = _dt.incident_edges( _vid );
        auto _vbegin = _vend;
        _vbegin++;
        return std::make_pair( _vbegin, _vend );
    }
};

template <>
template < typename Traits, typename Td >
struct incident_faces_access< CGAL::Delaunay_triangulation_2< Traits, Td > >
{
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Face_circulator face_circulator_t;
    typedef typename CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_handle vertex_id_t;

    static inline
    std::pair<  face_circulator_t, face_circulator_t >
    get( vertex_id_t const _vid,
         CGAL::Delaunay_triangulation_2< Traits, Td > const& _dt )
    {
        auto _vend = _dt.incident_faces( _vid );
        auto _vbegin = _vend;
        _vbegin++;

        return std::make_pair( _vbegin, _vend );
    }
};

SOCCER_TRAITS_NAMESPACE_END


// Algorithms
SOCCER_NAMESPACE_BEGIN

template < typename Position,
           typename Traits,
           typename Td
         >
inline
typename ::CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_handle
insert( Position const& position,
        ::CGAL::Delaunay_triangulation_2< Traits, Td >& dt )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position> ));

    const auto _point = typename CGAL::Delaunay_triangulation_2< Traits, Td >::Point( soccer::get_x( position ),
                                                                                      soccer::get_y( position ));
    return dt.push_back( _point );
}

template < typename Position,
           typename Traits,
           typename Td >
inline
typename ::CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_handle
nearest_vertex( Position const& position,
                ::CGAL::Delaunay_triangulation_2< Traits, Td > const& dt )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));

    const auto _point = typename CGAL::Delaunay_triangulation_2< Traits, Td >::Point( soccer::get_x( position ),
                                                                                      soccer::get_y( position ));

    return dt.nearest_vertex( _point );
}

template < typename Position,
           typename Traits,
           typename Td >
inline
typename ::CGAL::Delaunay_triangulation_2< Traits, Td >::Face_handle
find_face( Position const& position,
           ::CGAL::Delaunay_triangulation_2< Traits, Td > const& dt )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position> ));


    const auto _point = typename CGAL::Delaunay_triangulation_2< Traits, Td >::Point( soccer::get_x( position ),
                                                                                      soccer::get_y( position ));

    return dt.locate( _point );
}

template < typename Position1,
           typename Position2,
           typename Traits,
           typename Td >
inline typename ::CGAL::Delaunay_triangulation_2< Traits, Td >::Line_face_circulator
find_face( Position1 const& position1,
           Position2 const& position2,
           ::CGAL::Delaunay_triangulation_2< Traits, Td > const& dt )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position1 > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position2 > ));

    const auto _point1 = typename CGAL::Delaunay_triangulation_2< Traits, Td >::Point( soccer::get_x( position1 ),
                                                                                       soccer::get_y( position1 ));


    const auto _point2 = typename CGAL::Delaunay_triangulation_2< Traits, Td >::Point( soccer::get_x( position2 ),
                                                                                       soccer::get_y( position2 ));

    return dt.line_walk( _point1, _point2 );
}

/*********************
 *  HELPER FUNCTIONS *
 *********************/
template < typename Position,
           typename Traits,
           typename Td,
           typename OutputPosition = Position >
inline
typename ::CGAL::Delaunay_triangulation_2< Traits, Td >::Vertex_handle
nearest_position( Position const& position,
                  ::CGAL::Delaunay_triangulation_2< Traits, Td > const& dt )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept< OutputPosition > ));

    auto _vh = nearest_vertex( position, dt );

    auto _vh_point = _vh->point();

    return OutputPosition( _vh_point.x(), _vh_point.y() );
}

template < typename Position,
           typename Traits,
           typename Td,
           typename OutputPositionIterator >
void
nearest_vertices( Position const& position,
                  ::CGAL::Delaunay_triangulation_2< Traits, Td > const& dt,
                  OutputPositionIterator out_positions,
                  bool cumulative = true,
                  unsigned int search_depth = 1u ) {
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));

    typedef ::CGAL::Delaunay_triangulation_2< Traits, Td > triangulation_t;
    typedef typename triangulation_t::Vertex_handle vertex_id_t;
    typedef typename triangulation_t::Vertex_circulator vertex_circulator_t;

    typedef typename std::iterator_traits< OutputPositionIterator >::value_type position_t;

    auto start_vh = nearest_vertex( position, dt );

    std::set< vertex_id_t > open_vertices;
    std::set< vertex_id_t > closed_vertices;
    open_vertices.push_back( start_vh );

    while ( !open_vertices.empty() && search_depth > 0u )
    {
        std::clog << "Search vertices at depth " << search_depth << std::endl;

        std::list< vertex_id_t > neighbor_vertices;

        for ( auto itv = open_vertices.begin(); itv != open_vertices.end(); itv++ )
        {
            // Add to open vertices only if the vertex has not yet been explored
            if  ( closed_vertices.find( *itv ) == closed_vertices.end() )
            {
                neighbor_vertices.push_back( *itv );
                closed_vertices.insert( *itv );
                if ( cumulative || search_depth == 1u )
                {
                    position_t vertex_pos( itv->point().x(), itv->point().y() );
                    std::clog << "Found position vertex " << *itv << " position " << vertex_pos
                              << " at search depth = " << search_depth << std::endl;
                    *out_positions = vertex_pos;
                }
            }
        }

        // Update control variables
        open_vertices.clear();
        open_vertices.insert( open_vertices.begin(), neighbor_vertices.begin(), neighbor_vertices.end() );

        // update search depth stop variable
        search_depth--;
    }

}

SOCCER_NAMESPACE_END

#endif // SOCCER_CGAL_TRIANGULATION_HPP
