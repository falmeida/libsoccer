#ifndef SOCCER_BALL_HPP
#define SOCCER_BALL_HPP

#include <soccer/object.hpp>

namespace soccer {

template < typename Position,
           typename Velocity = Position >
struct ball
    : public dynamic_object< Position, Velocity >
{

};

} // end namespace soccer

#include <soccer/register/ball.hpp>
#include <soccer/core/tags.hpp>

NAMESPACE_SOCCER_TRAITS_BEGIN

//
// ball TYPE REGISTRATION
//
template <>
template < typename Position,
           typename Velocity >
struct tag< ball< Position, Velocity > > { typedef ball_tag type; };

NAMESPACE_SOCCER_TRAITS_END

#endif // SOCCER_BALL_HPP
