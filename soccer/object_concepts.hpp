#ifndef SOCCER_OBJECT_CONCEPTS_HPP
#define SOCCER_OBJECT_CONCEPTS_HPP

#include <soccer/object_traits.hpp>
#include <soccer/position_concepts.hpp>

#include <boost/type_traits.hpp>
#include <boost/concept/detail/concept_def.hpp>
#include <boost/concept_check.hpp>

namespace soccer {

BOOST_concept(StaticObjectConcept,(TT))
{
    typedef typename position_type< TT >::type position_t;

    BOOST_CONCEPT_USAGE(StaticObjectConcept)
    {
        const_constraints( _obj );
    }

private:
    void const_constraints( const TT& const_obj )
    {
        _pos = get_position( const_obj);
    }

    TT _obj;
    position_t _pos;
};

BOOST_concept(MutableStaticObjectConcept,(TT))
    : public StaticObjectConcept<TT>
{
    typedef typename position_type< TT >::type position_t;

    BOOST_CONCEPT_USAGE(MutableStaticObjectConcept)
    {
        set_position( _pos, _obj );
    }

    TT _obj;
    position_t _pos;
};


BOOST_concept(MobileObjectConcept,(TT))
    : public StaticObjectConcept<TT>
{
    typedef typename velocity_type< TT >::type velocity_t;

    BOOST_CONCEPT_USAGE(MobileObjectConcept)
    {
        const_constraints( _obj );
    }

private:
    void const_constraints( const TT& const_obj )
    {
        _vel = get_velocity( const_obj);
    }

    TT _obj;
    velocity_t _vel;
};

BOOST_concept(MutableMobileObjectConcept,(TT))
    : public MutableStaticObjectConcept<TT>
{
    typedef typename velocity_type< TT >::type velocity_t;

    BOOST_CONCEPT_USAGE(MutableMobileObjectConcept)
    {
        set_velocity( _vel, _obj );
    }

    TT _obj;
    velocity_t _vel;
};


}

#endif // SOCCER_OBJECT_CONCEPTS_HPP
