#ifndef SOCCER_DEFINES_H
#define SOCCER_DEFINES_H

// NAMESPACE USE
#define SOCCER_NAMESPACE_USE using soccer;
#define SOCCER_TRAITS_NAMESPACE_USE using soccer::traits;
#define SOCCER_CONCEPTS_NAMESPACE_USE using soccer::concepts;
#define SOCCER_EVENTS_NAMESPACE_USE using soccer::event;
#define SOCCER_ALGORITHM_NAMESPACE_USE using soccer::algorithm;
#define SOCCER_CORE_DISPATCH_NAMESPACE_USE using soccer::core_dispatch;
#define SOCCER_UTIL_NAMESPACE_USE using soccer::util;

// NAMESPACE BEGIN_END
#define SOCCER_CONCEPTS_NAMESPACE_BEGIN namespace soccer { namespace concepts {
#define SOCCER_CONCEPTS_NAMESPACE_END }}

#define SOCCER_TRAITS_NAMESPACE_BEGIN namespace soccer { namespace traits {
#define SOCCER_TRAITS_NAMESPACE_END }}

#define SOCCER_EVENTS_NAMESPACE_BEGIN namespace soccer { namespace event {
#define SOCCER_EVENTS_NAMESPACE_END }}

#define SOCCER_NAMESPACE_BEGIN namespace soccer {
#define SOCCER_NAMESPACE_END }

#define SOCCER_DETAIL_NAMESPACE_BEGIN namespace soccer { namespace detail {
#define SOCCER_DETAIL_NAMESPACE_END }}

#define SOCCER_ALGORITHM_NAMESPACE_BEGIN namespace soccer { namespace algorithm {
#define SOCCER_ALGORITHM_NAMESPACE_END }}

#define SOCCER_CORE_DISPATCH_NAMESPACE_BEGIN namespace soccer { namespace core_dispatch {
#define SOCCER_CORE_DISPATCH_NAMESPACE_END }}

#define SOCCER_UTIL_NAMESPACE_BEGIN namespace soccer { namespace util {
#define SOCCER_UTIL_NAMESPACE_END }}

#endif // SOCCER_DEFINES_H
