#ifndef SOCCER_RCSC_GEOM_SEGMENT_2D_H
#define SOCCER_RCSC_GEOM_SEGMENT_2D_H

#include <rcsc/geom/segment_2d.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/segment.hpp>

BOOST_GEOMETRY_REGISTER_SEGMENT( rcsc::Segment2D, rcsc::Vector2D, M_origin, M_terminal )

#endif // SOCCER_RCSC_GEOM_SEGMENT_2D_H
