#ifndef SOCCER_RCSC_PLAYER_TYPE_H
#define SOCCER_RCSC_PLAYER_TYPE_H

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/common/player_type.h>

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// Type Registration
//
template < >
struct tag < rcsc::PlayerType > { typedef player_object_model_tag type; };

template < >
struct decay_type< rcsc::PlayerType > { typedef double type; };

template < >
struct speed_type< rcsc::PlayerType > { typedef double type; };

template < >
struct stamina_type< rcsc::PlayerType > { typedef double type; };

template < >
struct power_type< rcsc::PlayerType > { typedef double type; };

template < >
struct size_type< rcsc::PlayerType > { typedef double type; };

template < >
struct effort_type< rcsc::PlayerType > { typedef double type; };

template < >
struct percentage_type< rcsc::PlayerType > { typedef double type; };

template < >
struct direction_type< rcsc::PlayerType > { typedef double type; };

template < >
struct length_type< rcsc::PlayerType > { typedef double type; };

//
// Data Access Registration
//

template < >
struct speed_max_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.playerSpeedMax(); }
};

template < >
struct effort_max_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.effortMax(); }
};

template < >
struct effort_min_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.effortMin(); }
};

template < >
struct stamina_inc_max_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.staminaIncMax(); }
};

template < >
struct extra_stamina_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.extraStamina(); }
};

template < >
struct kick_power_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.kickPowerRate(); }
};

template < >
struct dash_power_rate_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.dashPowerRate(); }
};

template < >
struct size_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.playerSize(); }
};

template < >
struct inertia_moment_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.inertiaMoment(); }
};

template < >
struct kickable_distance_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.kickableArea(); }
};

template < >
struct catchable_distance_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.catchAreaLengthStretch(); }
};

template < >
struct foul_probability_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.foulDetectProbability(); }
};

template < >
struct decay_access< rcsc::PlayerType >
{
    static inline
    double get( rcsc::PlayerType const& ptype ) { return ptype.playerDecay(); }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_PLAYER_TYPE_H
