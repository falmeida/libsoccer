#ifndef SOCCER_RCSC_INTERCEPT_INFO_H
#define SOCCER_RCSC_INTERCEPT_INFO_H

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/player/intercept_table.h>

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

template < >
struct tag< rcsc::InterceptInfo > { typedef interception_tag type; };


template < >
struct player_id_type< rcsc::InterceptInfo > { typedef void type; };

template < >
struct position_type< rcsc::InterceptInfo > { typedef rcsc::Vector2D type; };

template < >
struct speed_type< rcsc::InterceptInfo > { typedef double type; };

template < >
struct velocity_type< rcsc::InterceptInfo > { typedef rcsc::Vector2D type; };

template < >
struct duration_type< rcsc::InterceptInfo > { typedef int type; };

// TODO Incomplete definition (NO DATA ACCESS REGISTRATION)

template < >
struct duration_access< rcsc::InterceptInfo >
{
    static inline
    int get( rcsc::InterceptInfo const& _interception ) { return _interception.reachCycle(); }
};

template < >
struct position_access< rcsc::InterceptInfo >
{
    static inline
    rcsc::Vector2D const& get( rcsc::InterceptInfo const& _interception ) { return _interception.selfPos(); }
};


SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_INTERCEPT_INFO_H
