#ifndef SOCCER_RCSC_PLAYER_WORLD_MODEL_HPP
#define SOCCER_RCSC_PLAYER_WORLD_MODEL_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/player/world_model.h>

#include <soccer/core/access.hpp>

#include <soccer/register/state.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Type Registration
template <>
struct tag< rcsc::WorldModel > { typedef state_tag type; };

template <>
struct ball_type< rcsc::WorldModel > { typedef rcsc::BallObject type; };

template <>
struct player_type< rcsc::WorldModel > { typedef rcsc::AbstractPlayerObject type; };

template <>
struct player_iterator_type< rcsc::WorldModel > { typedef rcsc::AbstractPlayerCont::const_iterator type; };

template <>
struct player_id_type< rcsc::WorldModel > { typedef rcsc::AbstractPlayerObject const* type; };

template <>
struct time_type< rcsc::WorldModel > { typedef /*rcsc::GameTime*/ long type; };

template <>
struct playmode_type< rcsc::WorldModel > { typedef rcsc::GameMode type; };

template <>
struct score_type< rcsc::WorldModel > { typedef int type; };

template <>
struct team_type< rcsc::WorldModel > { typedef std::string type; };

// Access Registration
template <>
struct ball_access< rcsc::WorldModel >
{
    static inline rcsc::BallObject const&
    get( rcsc::WorldModel const& wm ) { return wm.ball(); }
};

template <>
struct player_access< rcsc::WorldModel >
{
    static inline rcsc::AbstractPlayerObject const&
    get( rcsc::AbstractPlayerObject const* pid, rcsc::WorldModel const& )
    {
        return *pid;
    }
};

template <>
struct player_iterators_access< rcsc::WorldModel >
{
    static inline std::pair< rcsc::AbstractPlayerCont::const_iterator, rcsc::AbstractPlayerCont::const_iterator >
    get( rcsc::WorldModel const& wm )
    {
        return std::make_pair( wm.allPlayers().begin(),
                               wm.allPlayers().end() );
    }
};

template <>
struct time_access< rcsc::WorldModel >
{
    static inline long
    get( rcsc::WorldModel const& wm ) { return wm.time().cycle(); }
};

template <>
struct playmode_access< rcsc::WorldModel >
{
    static inline rcsc::GameMode
    get( rcsc::WorldModel const& wm ) { return wm.gameMode(); }
};


template <>
struct left_team_access< rcsc::WorldModel >
{
    static inline std::string
    get( rcsc::WorldModel const& wm ) { return wm.ourSide() == rcsc::LEFT ? wm.teamName() : wm.opponentTeamName(); }
};

template <>
struct right_team_access< rcsc::WorldModel >
{
    static inline std::string
    get( rcsc::WorldModel const& wm ) { return wm.ourSide() == rcsc::RIGHT ? wm.teamName() : wm.opponentTeamName(); }
};


template <>
struct left_score_access< rcsc::WorldModel >
{
    static inline int
    get( rcsc::WorldModel const& wm ) { return wm.gameMode().scoreLeft(); }
};

template <>
struct right_score_access< rcsc::WorldModel >
{
    static inline int
    get( rcsc::WorldModel const& wm ) { return wm.gameMode().scoreRight(); }
};

template <>
struct null_value_access< soccer::player_id_tag, rcsc::WorldModel >
{
    static inline
    rcsc::AbstractPlayerObject const* get( ) { return NULL; }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_PLAYER_WORLD_MODEL_HPP
