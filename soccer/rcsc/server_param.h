#ifndef SOCCER_RCSC_SERVER_PARAM_HPP
#define SOCCER_RCSC_SERVER_PARAM_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/common/server_param.h>

#include <soccer/register/pitch.hpp>

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Type Registration
// TODO Use different tagging mechanism rcsc::ServerParam represents more than pitch
template <>
struct tag< rcsc::ServerParam > { typedef pitch_tag type; };

template <>
struct coordinate_type< rcsc::ServerParam > { typedef double type; };

template <>
struct length_type< rcsc::ServerParam > { typedef double type; };

// Access Registration
template <>
struct pitch_right_sign_access< rcsc::ServerParam >
{
    static inline double
    get ( ) { return 1.0; }
};

template <>
struct pitch_bottom_sign_access< rcsc::ServerParam >
{
    static inline double
    get ( ) { return 1.0; }
};

template <>
struct pitch_top_left_x_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return -sp.pitchHalfLength(); }
};

template <>
struct pitch_top_left_y_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return -sp.pitchHalfWidth(); }
};

template <>
struct pitch_length_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.pitchLength(); }
};

template <>
struct pitch_width_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.pitchWidth(); }
};

template <>
struct goal_area_length_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.goalAreaLength(); }
};

template <>
struct goal_area_width_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.goalAreaWidth(); }
};

template <>
struct penalty_area_length_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.penaltyAreaLength(); }
};

template <>
struct penalty_area_width_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.penaltyAreaWidth(); }
};

template <>
struct penalty_circle_radius_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.penaltyCircleR(); }
};

template <>
struct penalty_spot_distance_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.penaltySpotDist(); }
};

template <>
struct goal_depth_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.goalDepth(); }
};

template <>
struct goal_width_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.goalWidth(); }
};

template <>
struct center_circle_radius_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.centerCircleR(); }
};

template <>
struct corner_arc_radius_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.cornerArcR(); }
};

template <>
struct goal_post_radius_access< rcsc::ServerParam >
{
    static inline double
    get ( rcsc::ServerParam const& sp ) { return sp.goalPostRadius(); }
};

SOCCER_TRAITS_NAMESPACE_END

#include <rcsc/geom/rect_2d.h>

// custom algorithms
namespace rcsc {
    template < typename OutputPosition,
               typename InputPosition = OutputPosition >
    OutputPosition nearest_position( const InputPosition& position,
                                     rcsc::ServerParam const& sp ) {
        static const rcsc::Rect2D pitch_rect( -sp.pitchHalfLength(),
                                              -sp.pitchHalfWidth(),
                                              sp.pitchLength(),
                                              sp.pitchWidth() );

        rcsc::Vector2D pos( soccer::get_x( position ), soccer::get_y( position ));

        if ( pitch_rect.contains( pos ) )
            {
                return pos;
            }

            if ( pos.x < pitch_rect.left() )
            {
                if ( pos.y < pitch_rect.top() )
                {
                    return pitch_rect.topLeft();
                }
                else if ( pos.y > pitch_rect.bottom() )
                {
                    return pitch_rect.bottomLeft();
                }
                return OutputPosition( pitch_rect.left(), pos.y );
            }
            else if ( pos.x > pitch_rect.right() )
            {
                if ( pos.y < pitch_rect.top() )
                {
                    return pitch_rect.topRight();
                }
                else if ( pos.y > pitch_rect.bottom() )
                {
                    return pitch_rect.bottomRight();
                }
                return OutputPosition( pitch_rect.right(), pos.y);
            }

            // left() <= point.x <= right()
            return pos.y < pitch_rect.top()
                    ? OutputPosition(pos.x, pitch_rect.top() )
                    : OutputPosition(pos.x, pitch_rect.bottom() );


    }

} // end namespace rcsc

#endif // SOCCER_RCSC_SERVER_PARAM_HPP
