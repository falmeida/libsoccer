#ifndef SOCCER_RCSC_GEOM_RECT_2D_H
#define SOCCER_RCSC_GEOM_RECT_2D_H

#include <rcsc/geom/rect_2d.h>

// BOOST Geometry
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/box.hpp>

BOOST_GEOMETRY_REGISTER_BOX(rcsc::Rect2D, rcsc::Vector2D, topLeft, bottomRight);

#endif // SOCCER_RCSC_GEOM_RECT_2D_H
