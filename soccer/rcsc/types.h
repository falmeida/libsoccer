#ifndef SOCCER_RCSC_TYPES_H
#define SOCCER_RCSC_TYPES_H

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/types.h>

#include <soccer/core/tags.hpp>

#include <cassert>

SOCCER_TRAITS_NAMESPACE_BEGIN

template <>
struct unknown_side_access< rcsc::SideID >
    { static inline rcsc::SideID get( ) { return rcsc::NEUTRAL; } };

template <>
struct left_side_access< rcsc::SideID >
    { static inline rcsc::SideID get( ) { return rcsc::LEFT; } };

template <>
struct right_side_access< rcsc::SideID >
    { static inline rcsc::SideID get( ) { return rcsc::RIGHT; } };


SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_TYPES_H
