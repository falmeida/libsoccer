#ifndef SOCCER_RCSC_ABSTRACT_PLAYER_OBJECT_H
#define SOCCER_RCSC_ABSTRACT_PLAYER_OBJECT_H

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/player/abstract_player_object.h>

#include <soccer/traits/player_traits.hpp>
#include <soccer/register/player.hpp>

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Types Registration
template <>
struct tag< rcsc::AbstractPlayerObject > { typedef dynamic_player_tag type; };

template <>
struct number_type< rcsc::AbstractPlayerObject > { typedef int type; };

template <>
struct side_type< rcsc::AbstractPlayerObject > { typedef rcsc::SideID type; };

template <>
struct position_type< rcsc::AbstractPlayerObject > { typedef rcsc::Vector2D type; };

template <>
struct velocity_type< rcsc::AbstractPlayerObject > { typedef rcsc::Vector2D type; };

template <>
struct direction_type< rcsc::AbstractPlayerObject > { typedef double type; };

template <>
struct stamina_type< rcsc::AbstractPlayerObject > { typedef double type; };

//
// Access Registration
//

template <>
struct number_access< rcsc::AbstractPlayerObject >
{
    static inline int
    get( rcsc::AbstractPlayerObject const& player ) { return player.unum(); }
};

template <>
struct side_access< rcsc::AbstractPlayerObject >
{
    static inline rcsc::SideID
    get( rcsc::AbstractPlayerObject const& player ) { return player.side(); }
};

template <>
struct position_access< rcsc::AbstractPlayerObject >
{
    static inline rcsc::Vector2D
    get( rcsc::AbstractPlayerObject const& player ) { return player.pos(); }

    static inline void
    set( rcsc::Vector2D const& pos, rcsc::AbstractPlayerObject& player )
        { player.M_pos = pos; }
};


template <>
struct velocity_access< rcsc::AbstractPlayerObject >
{
    static inline rcsc::Vector2D
    get( rcsc::AbstractPlayerObject const& player ) { return player.vel(); }

    static inline void
    set( rcsc::Vector2D const& vel, rcsc::AbstractPlayerObject& player )
        { player.M_vel = vel; }
};

template <>
struct body_direction_access< rcsc::AbstractPlayerObject >
{
    static inline double
    get( rcsc::AbstractPlayerObject const& player ) { return player.body().degree(); }
};

template <>
struct face_direction_access< rcsc::AbstractPlayerObject >
{
    static inline double
    get( rcsc::AbstractPlayerObject const& player ) { return player.face().degree(); }
};

/* template <>
struct stamina_access< rcsc::AbstractPlayerObject >
{
    static inline double
    get( rcsc::AbstractPlayerObject const& player ) { return player.M_stamina.stamina(); }
};*/

template <>
struct is_goalie_access< rcsc::AbstractPlayerObject >
{
    static inline bool
    get( rcsc::AbstractPlayerObject const& player ) { return player.goalie(); }
};

template <>
struct has_yellow_card_access< rcsc::AbstractPlayerObject >
{
    static inline bool
    get( rcsc::AbstractPlayerObject const& player ) { return player.card() == rcsc::YELLOW; }
};


template <>
struct unknown_number_access< rcsc::AbstractPlayerObject >  { static inline int get( ) { return rcsc::Unum_Unknown; } };

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_ABSTRACT_PLAYER_OBJECT_HPP
