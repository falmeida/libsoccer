#ifndef SOCCER_RCSC_FORMATION_DT_H
#define SOCCER_RCSC_FORMATION_DT_H

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/formation/formation_dt.h>
#include <rcsc/common/server_param.h>

#include <boost/iterator/counting_iterator.hpp>

#include <soccer/concepts/state_concepts.hpp>
#include <soccer/core/access.hpp>
#include <soccer/core/tags.hpp>

// #include <soccer/algorithms/pitch_algorithms.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// Types registration
//
template <>
struct tag< rcsc::FormationDT > { typedef formation_dynamic_tag type; };

template <>
struct role_id_type< rcsc::FormationDT > { typedef int type; };

template <>
struct role_iterator_type< rcsc::FormationDT > { typedef boost::counting_iterator< int > type; };

template <>
struct position_type< rcsc::FormationDT > { typedef rcsc::Vector2D type; };


//
// Data Access registration
//

template <>
struct role_iterators_access< rcsc::FormationDT >
{
    static inline
    std::pair< boost::counting_iterator< int >,
               boost::counting_iterator< int >
             >
    get( rcsc::FormationDT const& )
    {
        return std::make_pair( boost::make_counting_iterator( 1 ),
                               boost::make_counting_iterator( rcsc::ServerParam::i().maxPlayer() + 1 ) );
    }
};

template <>
struct position_access< rcsc::FormationDT >
{
    template < typename State >
    static inline
    rcsc::Vector2D get( int const role_id,
                        State const& _state,
                        rcsc::FormationDT const& _formation )
    {
        BOOST_CONCEPT_ASSERT(( soccer::StateConcept< State> ));
        const auto _ball = get_ball( _state );
        const auto _ball_pos = get_position( _ball );

        /* if ( soccer::is_within_pitch( ) )
        {*/
        rcsc::Vector2D aux_bpos( get_x( _ball_pos ), get_y( _ball_pos ) );
        return _formation.getPosition( role_id, aux_bpos );
    }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_FORMATION_DT_H
