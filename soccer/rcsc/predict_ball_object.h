// -*-c++-*-

/*!
  \file predict_ball_object.h
  \brief predict ball object class Header File
*/

/*
 *Copyright:

 Copyright (C) Hidehisa AKIYAMA, Hiroki SHIMORA

 This code is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifndef SOCCER_RCSC_PREDICT_BALL_OBJECT_H
#define SOCCER_RCSC_PREDICT_BALL_OBJECT_H

//! Import ueful macros
#include <soccer/defines.h>

#include <rcsc/player/predict_ball_object.h>

#include <soccer/core/access.hpp>
#include <soccer/register/ball.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Types Registration
template <>
struct tag< rcsc::PredictBallObject > { typedef dynamic_ball_tag type; };

template <>
struct position_type< rcsc::PredictBallObject > { typedef rcsc::Vector2D type; };

template <>
struct velocity_type< rcsc::PredictBallObject > { typedef rcsc::Vector2D type; };

//
// Access Registration
//

template <>
struct position_access< rcsc::PredictBallObject >
{
    static inline rcsc::Vector2D
    get( rcsc::PredictBallObject const& ball ) { return ball.pos(); }

    static inline void
    set( rcsc::Vector2D const& pos, rcsc::PredictBallObject& ball )
    {
        ball.setPos( pos );
    }
};


template <>
struct velocity_access< rcsc::PredictBallObject >
{
    static inline rcsc::Vector2D
    get( rcsc::PredictBallObject const& ball ) { return ball.vel(); }

    static inline void
    set( rcsc::Vector2D const& vel, rcsc::PredictBallObject& ball )
    {
        ball.setVel( vel );
    }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_PREDICT_BALL_OBJECT_H
