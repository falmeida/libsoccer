#ifndef SOCCER_RCSC_VECTOR2D_AS_POINT_HPP
#define SOCCER_RCSC_VECTOR2D_AS_POINT_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/geom/vector_2d.h>

// #include <soccer/register/point.hpp>

#include <soccer/core/speed_type.hpp>
#include <soccer/core/direction_type.hpp>
#include <soccer/core/coordinate_type.hpp>
#include <soccer/core/tag.hpp>

#include <soccer/core/tags.hpp>
// #include <soccer/core/access.hpp>

// BOOST Geometry
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>

BOOST_GEOMETRY_REGISTER_POINT_2D(rcsc::Vector2D, double, boost::geometry::cs::cartesian, x, y)

SOCCER_TRAITS_NAMESPACE_BEGIN

template <>
struct tag< rcsc::Vector2D > { typedef point_tag type; };

template <>
struct coordinate_type< rcsc::Vector2D > { typedef double type; };

template <>
struct direction_type< rcsc::Vector2D > { typedef double type; };

template <>
struct speed_type< rcsc::Vector2D > { typedef double type; };


/* template <>
struct coordinate_access< rcsc::Vector2D >
{
    static inline double get_x( rcsc::Vector2D const& p ) { return p.x; }
    static inline double& get_x( rcsc::Vector2D& p ) { return p.x; }

    static inline void set_x( double const val, rcsc::Vector2D& p ) { p.x = val; }


    static inline double get_y( rcsc::Vector2D const& p ) { return p.y; }
    static inline double& get_y( rcsc::Vector2D& p ) { return p.y; }

    static inline void set_y( double const val, rcsc::Vector2D& p ) { p.y = val; }
}; */

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_VECTOR2D_AS_POINT_HPP
