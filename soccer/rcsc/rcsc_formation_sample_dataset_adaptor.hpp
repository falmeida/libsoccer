#ifndef RCSC_FORMATION_SAMPLE_DATASET_ADAPTOR_HPP
#define RCSC_FORMATION_SAMPLE_DATASET_ADAPTOR_HPP

#include <rcsc/formation/sample_data.h>

#include <soccer/models/object.hpp>
#include <soccer/geometry.hpp>

#include <boost/iterator/transform_iterator.hpp>
#include <boost/iterator/counting_iterator.hpp>

#include <utility>
#include <functional>

namespace rcsc {
namespace formation {

// HACK Should not need to add this here!!! Bad coding in SampleDataSet::addData => Checks for existNearData
static const double RCSC_FORMATION_SAMPLE_MAX_DIST2_THR = rcsc::formation::SampleDataSet::NEAR_DIST_THR * rcsc::formation::SampleDataSet::NEAR_DIST_THR;

}}

namespace soccer {
namespace rcsc {
namespace formation {

    // SampleData access
    typedef soccer::static_object< ::rcsc::Vector2D > ball_type;
    typedef soccer::static_object< ::rcsc::Vector2D > player_type;
    typedef std::size_t player_id_type;
    typedef boost::counting_iterator< std::size_t > player_iterator_type;

    ball_type get_ball( const ::rcsc::formation::SampleData& sample ) { return ball_type( sample.ball_ ); }
    void set_ball( ball_type const& ball, ::rcsc::formation::SampleData& sample ) { sample.ball_ = ball.M_position; }

    player_type get_player( player_id_type const pid, const ::rcsc::formation::SampleData& sample )
        { return player_type( sample.players_.at( pid ) );}
    void set_player( player_id_type const pid, player_type const& player, ::rcsc::formation::SampleData& sample )
        { sample.players_[pid] = player.M_position; }

    std::pair< player_iterator_type, player_iterator_type >
    get_players( const ::rcsc::formation::SampleData& )
    {
        return std::make_pair( player_iterator_type( 0u ), player_iterator_type( 11u ) );
    }

    // HACK to prevent error in macro usage!! Bad coding => Make macro usage for modular
    void* get_info( const ::rcsc::formation::SampleData& ) { assert( false ); return NULL; }
    void set_info( void* , ::rcsc::formation::SampleData& ) { assert( false ); }


    // SampleDataSet access
    typedef boost::transform_iterator< std::function< int(const ::rcsc::formation::SampleData&) >,
                                       ::rcsc::formation::SampleDataSet::DataCont::const_iterator > sample_iterator;

    const ::rcsc::formation::SampleData&
    get_sample( size_t const sid, ::rcsc::formation::SampleDataSet const& storage )
    {
        assert( sid < storage.dataCont().size() );

        auto _sample_ptr = storage.data( sid );
        assert( _sample_ptr != NULL );

        return *_sample_ptr;
    }

    ::rcsc::formation::SampleData&
    get_sample( size_t const sid, ::rcsc::formation::SampleDataSet& storage )
    {
        assert( sid < storage.dataCont().size() );

        auto _sample_ptr = const_cast< ::rcsc::formation::SampleData*>( storage.data( sid ) );
        assert( _sample_ptr != NULL );

        return *_sample_ptr;
    }

    std::pair< sample_iterator, sample_iterator >
    get_samples( ::rcsc::formation::SampleDataSet const& storage )
    {
        auto _get_sample_id_fn = [] ( const ::rcsc::formation::SampleData& sample ) -> int
            {
                typedef size_t result_type;
                return sample.index_;
            };
        return std::make_pair( sample_iterator( storage.dataCont().begin(), _get_sample_id_fn ),
                               sample_iterator( storage.dataCont().end(), _get_sample_id_fn ) );
    }

}
}
}

#include <soccer/defines.h>
#include "register/formation_sample.hpp"
#include "register/formation_sample_storage.hpp"
#include <soccer/core/access.hpp>
#include "Core/access.hpp"

SOCCER_REGISTER_FORMATION_SAMPLE_GET_SET_FREE( ::rcsc::formation::SampleData, \
                                               soccer::rcsc::formation::ball_type, soccer::rcsc::formation::get_ball, soccer::rcsc::formation::set_ball, \
                                               soccer::rcsc::formation::player_id_type, soccer::rcsc::formation::player_iterator_type, soccer::rcsc::formation::get_players, \
                                               soccer::rcsc::formation::player_type, soccer::rcsc::formation::get_player, soccer::rcsc::formation::set_player, \
                                               void*, soccer::rcsc::formation::get_info, soccer::rcsc::formation::set_info )

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// rcsc::formation::SampleData
//

// TYPE REGISTRATION
/* template <>
struct tag< ::rcsc::formation::SampleData > { typedef soccer::formation_sample_tag type; };

template <>
struct ball_type< ::rcsc::formation::SampleData > { typedef soccer::static_object< ::rcsc::Vector2D > type; };

template <>
struct player_type< ::rcsc::formation::SampleData > { typedef soccer::static_object< ::rcsc::Vector2D > type; };

template <>
struct player_iterator_type< ::rcsc::formation::SampleData > { typedef boost::counting_iterator< std::size_t > type; };

// ACCESS REGISTRATION
template <>
struct ball_access
{
    static inline soccer::static_object< ::rcsc::Vector2D >
    get( )

}; */



//
// rcsc::formation::SampleDataSet
//

// TYPE REGISTRATION
template <>
struct tag< ::rcsc::formation::SampleDataSet > { typedef soccer::formation_sample_storage_tag type; };

template <>
struct formation_sample_type< ::rcsc::formation::SampleDataSet > { typedef ::rcsc::formation::SampleData type; };

template <>
struct formation_sample_id_type< ::rcsc::formation::SampleDataSet > { typedef std::size_t type; };

template <>
struct formation_sample_iterator_type< ::rcsc::formation::SampleDataSet >
    { typedef boost::transform_iterator< std::function< int(const ::rcsc::formation::SampleData&) >,
                                        ::rcsc::formation::SampleDataSet::DataCont::const_iterator > type; };

// ACCESS REGISTRATION
template <>
struct formation_sample_access< ::rcsc::formation::SampleDataSet >
{
    static inline ::rcsc::formation::SampleData const&
    get( std::size_t const sid, ::rcsc::formation::SampleDataSet const& storage )
    {
        auto _sample_ptr = storage.data( sid );
        assert( _sample_ptr != NULL );
        return *_sample_ptr;
    }

    static inline ::rcsc::formation::SampleData&
    get( std::size_t const sid, ::rcsc::formation::SampleDataSet& storage )
    {
        auto _sample_ptr = const_cast< ::rcsc::formation::SampleData*>( storage.data( sid ) );
        assert( _sample_ptr != NULL );
        return *_sample_ptr;
    }
};

template <>
struct formation_sample_iterators_access< ::rcsc::formation::SampleDataSet >
{
    typedef boost::transform_iterator< std::function< int(const ::rcsc::formation::SampleData&) >,
                                       ::rcsc::formation::SampleDataSet::DataCont::const_iterator > sample_iterator_t;
    static inline std::pair< sample_iterator_t, sample_iterator_t>
    get( ::rcsc::formation::SampleDataSet const& storage )
    {
        auto _get_sample_id_fn = [] ( const ::rcsc::formation::SampleData& sample ) -> int
            {
                typedef size_t result_type;
                return sample.index_;
            };
        return std::make_pair( sample_iterator_t( storage.dataCont().begin(), _get_sample_id_fn ),
                               sample_iterator_t( storage.dataCont().end(), _get_sample_id_fn));
    }
};

/* SOCCER_REGISTER_FORMATION_SAMPLE_STORAGE_GET_SET_FREE( ::rcsc::formation::SampleDataSet, \
                                                       ::rcsc::formation::SampleData, soccer::rcsc::formation::get_sample, \
                                                       std::size_t, soccer::rcsc::formation::sample_iterator, soccer::rcsc::formation::get_samples )*/

SOCCER_TRAITS_NAMESPACE_END
#endif // RCSC_FORMATION_SAMPLE_DATASET_ADAPTOR_HPP
