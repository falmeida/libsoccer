#ifndef SOCCER_RCSC_GAME_MODE_H
#define SOCCER_RCSC_GAME_MODE_H

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/game_mode.h>

// #include <soccer/register/playmode.hpp>

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// Types Registration
//
template <>
struct tag< rcsc::GameMode > { typedef playmode_tag type; };

template <>
struct side_type< rcsc::GameMode > { typedef rcsc::SideID type; };

template <>
struct time_type< rcsc::GameMode > { typedef rcsc::GameTime type; };

template <>
struct match_situation_type< rcsc::GameMode > { typedef rcsc::GameMode::Type type; };

//
// Data Access Registration
//

template <>
struct side_access< rcsc::GameMode >
{
    static inline rcsc::SideID get( rcsc::GameMode const& pmode ) { return pmode.side(); }
};

template <>
struct time_access< rcsc::GameMode >
{
    static inline rcsc::GameTime const& get( rcsc::GameMode const& pmode ) { return pmode.time(); }
};

template <>
struct match_situation_access< rcsc::GameMode >
{
    static inline rcsc::GameMode::Type get( rcsc::GameMode const& pmode ) { return pmode.type(); }
};

template < >
struct is_kick_off_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::KickOff_; }

};

template < >
struct is_throw_in_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::KickIn_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_goal_kick_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::GoalKick_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_corner_kick_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::CornerKick_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_free_kick_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::FreeKick_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_indirect_free_kick_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::IndFreeKick_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_goalie_catch_kick_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::GoalieCatch_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_offside_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::OffSide_; }

    static inline void set( bool const , rcsc::GameMode::Type const&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_play_on_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::PlayOn; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_penalty_setup_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::PenaltySetup_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};


template < >
struct is_penalty_ready_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::PenaltyReady_; }

    static inline void set( bool const , rcsc::GameMode::Type&  )
    {
        assert( false ); // TODO
    }
};

template < >
struct is_before_kick_off_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::BeforeKickOff; }
};

template < >
struct is_time_over_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::TimeOver; }
};

template < >
struct is_goal_access< rcsc::GameMode::Type >
{
    static inline bool get( rcsc::GameMode::Type const& _situation ) { return _situation == rcsc::GameMode::AfterGoal_; }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSC_GAME_MODE_H
