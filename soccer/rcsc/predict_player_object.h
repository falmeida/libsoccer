// -*-c++-*-

/*!
  \file predict_player_object.h
  \brief predict player object class Header File
*/

/*
 *Copyright:

 Copyright (C) Hidehisa AKIYAMA, Hiroki SHIMORA

 This code is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifndef SOCCER_RCSC_PREDICT_PLAYER_OBJECT_H
#define SOCCER_RCSC_PREDICT_PLAYER_OBJECT_H

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/player/predict_player_object.h>

#include <soccer/player_traits.hpp>
#include <soccer/register/player.hpp>

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Types Registration
template <>
struct tag< rcsc::PredictPlayerObject > { typedef dynamic_player_tag type; };

template <>
struct number_type< rcsc::PredictPlayerObject > { typedef int type; };

template <>
struct side_type< rcsc::PredictPlayerObject > { typedef rcsc::SideID type; };

template <>
struct position_type< rcsc::PredictPlayerObject > { typedef rcsc::Vector2D type; };

template <>
struct velocity_type< rcsc::PredictPlayerObject > { typedef rcsc::Vector2D type; };

template <>
struct direction_type< rcsc::PredictPlayerObject > { typedef double type; };

template <>
struct stamina_type< rcsc::PredictPlayerObject > { typedef double type; };

//
// Access Registration
//

template <>
struct number_access< rcsc::PredictPlayerObject >
{
    static inline int
    get( rcsc::PredictPlayerObject const& player ) { return player.unum(); }
};

template <>
struct side_access< rcsc::PredictPlayerObject >
{
    static inline rcsc::SideID
    get( rcsc::PredictPlayerObject const& player ) { return player.side(); }
};

template <>
struct position_access< rcsc::PredictPlayerObject >
{
    static inline rcsc::Vector2D
    get( rcsc::PredictPlayerObject const& player ) { return player.pos(); }

    static inline void
    set( rcsc::Vector2D const& pos, rcsc::PredictPlayerObject& player )
        { player.M_pos = pos; }
};


template <>
struct velocity_access< rcsc::PredictPlayerObject >
{
    static inline rcsc::Vector2D
    get( rcsc::PredictPlayerObject const& player ) { return player.vel(); }

    static inline void
    set( rcsc::Vector2D const& vel, rcsc::PredictPlayerObject& player )
        { player.M_vel = vel; }
};

template <>
struct body_direction_access< rcsc::PredictPlayerObject >
{
    static inline double
    get( rcsc::PredictPlayerObject const& player ) { return player.body().degree(); }
};

template <>
struct face_direction_access< rcsc::PredictPlayerObject >
{
    static inline double
    get( rcsc::PredictPlayerObject const& player ) { return player.face().degree(); }
};

/* template <>
struct stamina_access< rcsc::PredictPlayerObject >
{
    static inline double
    get( rcsc::PredictPlayerObject const& player ) { return player.M_stamina.stamina(); }
};*/

template <>
struct is_goalie_access< rcsc::PredictPlayerObject >
{
    static inline bool
    get( rcsc::PredictPlayerObject const& player ) { return player.goalie(); }
};

template <>
struct has_yellow_card_access< rcsc::PredictPlayerObject >
{
    static inline bool
    get( rcsc::PredictPlayerObject const& player ) { return player.card() == rcsc::YELLOW; }
};


template <>
struct unknown_number_access< rcsc::PredictPlayerObject >  { static inline int get( ) { return rcsc::Unum_Unknown; } };

SOCCER_TRAITS_NAMESPACE_END

#endif
