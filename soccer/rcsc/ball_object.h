#ifndef SOCCER_RCSC_BALL_OBJECT_HPP
#define SOCCER_RCSC_BALL_OBJECT_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <rcsc/player/ball_object.h>

#include <soccer/core/access.hpp>
#include <soccer/register/ball.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Types Registration
template <>
struct tag< rcsc::BallObject > { typedef dynamic_ball_tag type; };

template <>
struct position_type< rcsc::BallObject > { typedef rcsc::Vector2D type; };

template <>
struct velocity_type< rcsc::BallObject > { typedef rcsc::Vector2D type; };

//
// Access Registration
//

template <>
struct position_access< rcsc::BallObject >
{
    static inline rcsc::Vector2D
    get( rcsc::BallObject const& ball ) { return ball.pos(); }

    static inline void
    set( rcsc::Vector2D const& pos, rcsc::BallObject& ball )
    {
        rcsc::BallObject::State& _state = const_cast< rcsc::BallObject::State& >( ball.state() );
        _state.pos_ = pos;
    }
};


template <>
struct velocity_access< rcsc::BallObject >
{
    static inline rcsc::Vector2D
    get( rcsc::BallObject const& ball ) { return ball.vel(); }

    static inline void
    set( rcsc::Vector2D const& vel, rcsc::BallObject& ball )
    {
        rcsc::BallObject::State& _state = const_cast< rcsc::BallObject::State& >( ball.state() );
        _state.vel_ = vel;
    }
};

SOCCER_TRAITS_NAMESPACE_END


#endif // SOCCER_RCSC_BALL_OBJECT_HPP
