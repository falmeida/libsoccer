#ifndef SOCCER_GAME_HPP
#define SOCCER_GAME_HPP

namespace soccer
{

template < typename Time >
struct game
{

    Time M_match_start_time;
    Time M_normal_half_time;
    Time M_extra_half_time;
    Time M_drop_ball_time;
    bool M_has_extra_time;
    bool M_has_golden_goal;
    unsigned short M_num_penalties_in_shootout;
};

} // end namespace soccer

#include <soccer/register/util.hpp>

NAMESPACE_SOCCER_TRAITS_BEGIN

//
// game TYPE REGISTRATION
//
template <>
template < typename Time >
struct tag < game< Time > > { typedef game_tag type; };

template <>
template < typename Time >
struct time_type < game< Time > > { typedef Time type; };


//
// game DATA ACCESS REGISTRATION
//
template <>
template < typename Time >
struct match_start_time_access < game< Time > >
{
    static inline
    Time const& get( game< Time > const& _game )
        { return _game.M_match_start_time; }

    static inline
    void set( Time const& _time, game< Time > const& _game )
        { _game.M_match_start_time = _time; }
};

template <>
template < typename Time >
struct normal_half_time_access < game< Time > >
{
    static inline
    Time const& get( game< Time > const& _game )
        { return _game.M_normal_half_time; }

    static inline
    void set( Time const& _time, game< Time > const& _game )
        { _game.M_normal_half_time = _time; }
};

template <>
template < typename Time >
struct extra_half_time_access < game< Time > >
{
    static inline
    Time const& get( game< Time > const& _game )
        { return _game.M_extra_half_time; }

    static inline
    void set( Time const& _time, game< Time > const& _game )
        { _game.M_extra_half_time = _time; }
};

template <>
template < typename Time >
struct drop_ball_time_access < game< Time > >
{
    static inline
    Time const& get( game< Time > const& _game )
        { return _game.M_drop_ball_time; }

    static inline
    void set( Time const& _time, game< Time > const& _game )
        { _game.M_drop_ball_time = _time; }
};

template <>
template < typename Time >
struct has_extra_time_access < game< Time > >
{
    static inline
    bool get( game< Time > const& _game )
        { return _game.M_has_extra_time; }

    static inline
    void set( bool const _val, game< Time > const& _game )
        { _game.M_has_extra_time = _val; }
};

template <>
template < typename Time >
struct has_golden_goal_access < game< Time > >
{
    static inline
    bool get( game< Time > const& _game )
        { return _game.M_has_golden_goal; }

    static inline
    void set( bool const _val, game< Time > const& _game )
        { _game.M_has_golden_goal = _val; }
};


template <>
template < typename Time >
struct num_penalties_in_shootout_access< game< Time > >
{
    static inline
    unsigned short get( game< Time > const& _game )
        { return _game.M_num_penalties_in_shootout; }

    static inline
    void set( unsigned short _val, game< Time > const& _game )
        { _game.M_num_penalties_in_shootout = _val; }
};

NAMESPACE_SOCCER_TRAITS_END

#endif // SOCCER_GAME_HPP
