#ifndef SOCCER_FLUX_TRIANGULATION_HPP
#define SOCCER_FLUX_TRIANGULATION_HPP

#include <soccer/core/access.hpp>

#include <soccer/position_concepts.hpp>

#include <map>

namespace soccer {

template < typename FluxValue = double,
           typename Triangulation >
struct flux_triangulation {
    BOOST_CONCEPT_ASSERT(( soccer::TriangulationConcept< Triangulation > ));


    std::map< typename vertex_id_type< Triangulation >::type, FluxValue > M_flux_values;
    Triangulation M_triangulation;
};

} // end namespace soccer

namespace soccer { namespace traits {

template < >
template < typename FluxValue, typename Triangulation >
struct triangulation_access< flux_triangulation< FluxValue, Triangulation >  >
{
    typedef triangulation_type< flux_triangulation< FluxValue, Triangulation > >::type triangulation_t;

    static inline
    Triangulation const&
    get( flux_triangulation< FluxValue, Triangulation > const& ft )
    {
        return ft.M_triangulation;
    }

    template < typename Position >
    static inline
    void
    set( Triangulation const& _triangulation,
         flux_triangulation< FluxValue, Triangulation > const& ft )
    {
        ft.M_triangulation = _triangulation;
    }
};

// Data access registration
template < >
template < typename FluxValue, typename Triangulation >
struct flux_value_access< flux_triangulation< FluxValue, Triangulation >  >
{
    typedef triangulation_type< flux_triangulation< FluxValue, Triangulation > >::type triangulation_t;
    static inline
    FluxValue
    get( typename vertex_id_type< triangulation_t >::type const vid,
         flux_triangulation< FluxValue, Triangulation > const& ft )
    {
        return ft.M_flux_values.at( vid );
    }

    template < typename Position >
    static inline
    typename vertex_id_type< triangulation_t >::type
    set( Position const& pos,
         flux_triangulation< FluxValue, Triangulation > const& ft )
    {
        BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
        insert( pos, ft.M_triangulation );
    }
};

/* get_triangulation( const_flux );
get_value( _vid, const_flux );
vd = get_vertex( _vid, const_flux );
_vid = get_infinite_vertex( const_flux );
vertices_iterators = get_vertices( const_flux );
pos = get_position( vd, const_flux );
val = get_flux( vd, const_flux );
// get_face( _fid, const_flux );
face_iterators = get_faces( const_flux );
vertex_circulators = get_incident_vertices( _vid, const_flux );*/

}} // end namespace soccer::traits

#endif // SOCCER_FLUX_TRIANGULATION_HPP
