#ifndef SOCCER_ACTION_TRAITS_HPP
#define SOCCER_ACTION_TRAITS_HPP

#include <soccer/core/player_id_type.hpp>
#include <soccer/core/duration_type.hpp>
#include <soccer/core/position_type.hpp>
#include <soccer/core/velocity_type.hpp>
#include <soccer/core/speed_type.hpp>
#include <soccer/core/direction_type.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/core/tags.hpp>

namespace soccer {

/*!
  \brief action traits
  * FUNCTIONS:
  * startTime( action )
  * endTime( action)
  * duration( action ) => base implementation
  */
template < typename Action >
struct action_traits
{
    typedef typename player_id_type< Action >::type player_id_type;
    typedef typename duration_type< Action >::type duration_type;
    typedef typename tag< Action >::type category;
};

/*!
  \brief individual action traits.
  * FUNCTIONS
  * ballStartPosition( action )
  * ballEndPosition( action )
  * ballTravelledDistance( action )
  */
template < typename Action >
struct action_with_ball_traits
    : public action_traits<Action>
{
    typedef typename position_type< Action >::type position_type;
    typedef typename velocity_type< Action >::type velocity_type;
    typedef typename direction_type< Action >::type direction_type;
};

template < typename Action >
struct individual_action_with_ball_traits
    : public action_with_ball_traits<Action>
{

};


/*!
  \brief cooperative action traits.
  * bool participates( player_id, action )
  * bool initiatedBy( player_id, action )
  * bool endedBy( player_id, action )
  * player_id initiator( action )
  * player_id initiator( action )
  */
/*template <typename CooperativeAction>
struct cooperative_action_traits
    : public action_traits< CooperativeAction>
{
    // start_player_id
    // end_player_id
    // players
}; */

template < typename ActionGenerator >
struct action_generator_raits {
    typedef typename ActionGenerator::action_base_type action_base_type;
    typedef typename ActionGenerator::pass_type pass_type;
    typedef typename ActionGenerator::shoot_type shoot_type;
    typedef typename ActionGenerator::dribble_type dribble_type;
    typedef typename ActionGenerator::move_type move_type;
    typedef typename ActionGenerator::tackle_type tackle_type;
    typedef typename ActionGenerator::clear_ball_type clear_ball_type;
    typedef typename ActionGenerator::hold_ball_type hold_ball_type;
    typedef typename ActionGenerator::intercept_ball_type intercept_ball_type;
};

template < typename ActionGenerator >
struct action_base_type { typedef typename ActionGenerator::action_base_type type; };

template < typename ActionGenerator >
struct pass_type { typedef typename ActionGenerator::pass_type type; };

template < typename ActionGenerator >
struct shoot_type { typedef typename ActionGenerator::shoot_type type; };

template < typename ActionGenerator >
struct dribble_type { typedef typename ActionGenerator::dribble_type type; };

template < typename ActionGenerator >
struct move_type { typedef typename ActionGenerator::move_type type; };

template < typename ActionGenerator >
struct tackle_type { typedef typename ActionGenerator::tackle_type type; };

template < typename ActionGenerator >
struct clear_ball_type { typedef typename ActionGenerator::clear_ball_type type; };

template < typename ActionGenerator >
struct intercept_ball_type { typedef typename ActionGenerator::intercept_ball_type type; };

template < typename ActionGenerator >
struct hold_ball_type { typedef typename ActionGenerator::hold_ball_type type; };


namespace detail {
    inline bool is_pass( pass_tag ) { return true; }
    template <typename T>
    inline bool is_pass(T ) { return false; }

    inline bool is_dribble( dribble_tag ) { return true; }
    template <typename T>
    inline bool is_dribble(T ) { return false; }

    inline bool is_shoot( shoot_tag ) { return true; }
    template <typename T>
    inline bool is_shoot(T ) { return false; }

    inline bool is_move( move_tag ) { return true; }
    template <typename T>
    inline bool is_move(T ) { return false; }

    inline bool is_hold( hold_ball_tag ) { return true; }
    template <typename T>
    inline bool is_hold(T ) { return false; }

    inline bool is_catch( catch_tag ) { return true; }
    template <typename T>
    inline bool is_catch(T ) { return false; }

    inline bool is_intercept( interception_tag ) { return true; }
    template <typename T>
    inline bool is_intercept(T ) { return false; }
}

/*!
\brief check if the action is a pass
\returns true if the action is a pass, or false otherwise.
*/
template <typename Action>
bool is_pass(const Action&)
{
    typedef typename tag<Action>::type Cat;
    return detail::is_pass( Cat() );
}

/*!
\brief check if the action is a dribble
\returns true if the action is a dribble, or false otherwise.
*/
template <typename Action>
bool is_dribble(const Action&)
{
    typedef typename tag<Action>::type Cat;
    return detail::is_dribble( Cat() );
}

/*!
\brief check if the action is a shoot
\returns true if the action is a shoot, or false otherwise.
*/
template <typename Action>
bool is_shoot(const Action&)
{
    typedef typename tag<Action>::type Cat;
    return detail::is_shoot( Cat() );
}

/*!
\brief check if the action is a move
\returns true if the action is a move, or false otherwise.
*/
template <typename Action>
bool is_move(const Action&)
{
    typedef typename tag<Action>::type Cat;
    return detail::is_move( Cat() );
}

/*!
\brief check if the action is a ball hold
\returns true if the action is a ball hold, or false otherwise.
*/
template <typename Action>
bool is_hold(const Action&)
{
    typedef typename tag<Action>::type Cat;
    return detail::is_hold( Cat() );
}

/*!
\brief check if the action is a ball catch
\returns true if the action is a ball catch, or false otherwise.
*/
template <typename Action>
bool is_catch(const Action&)
{
    typedef typename tag<Action>::type Cat;
    return detail::is_catch( Cat() );
}

/*!
\brief check if the action is an interception
\returns true if the action is an interception, or false otherwise.
*/
template <typename Action>
bool is_intercept(const Action&)
{
    typedef typename tag<Action>::type Cat;
    return detail::is_intercept( Cat() );
}

} // end namespace soccer

#endif // SOCCER_ACTION_TRAITS_HPP
