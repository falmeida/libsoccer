#ifndef SOCCER_REGION_CONCEPTS_HPP
#define SOCCER_REGION_CONCEPTS_HPP

#include <region_traits.hpp>
#include <boost/concept/assert.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(RegionConcept,(R))
{

    BOOST_CONCEPT_USAGE(RegionConcept)
    {

    }

    R reg;
};

}

#endif // SOCCER_REGION_CONCEPTS_HPP
