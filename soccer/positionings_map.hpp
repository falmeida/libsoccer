#ifndef POSITIONINGS_MAP_HPP
#define POSITIONINGS_MAP_HPP

#include <soccer/positionings_traits.hpp>
#include <boost/iterator/transform_iterator.hpp>

#include <map>
#include <limits>
#include <functional>

namespace soccer {

template <typename PositioningId>
struct positionings_map {
    typedef std::map< PositioningId, PositioningId > PositioningMap;
    typedef PositioningId positioning_id_descriptor;
    typedef boost::transform_iterator< std::function<positioning_id_descriptor(const typename PositioningMap::value_type)>,
                                       typename PositioningMap::const_iterator > positioning_id_iterator;
    typedef typename PositioningMap::size_type positionings_size_type;

    positionings_map() { }
    positionings_map( const positionings_map& other)
        : m_positioning_map( other.m_positioning_map )
    {

    }

    positionings_map& operator=( const positionings_map& other )
    {
        if ( this != &other )
        {
            this->m_positioning_map = other.m_positioning_map;
        }
        return *this;
    }

    PositioningMap m_positioning_map;
};

// PositioningConcept implementation
template <typename PositioningId>
std::pair< typename positionings_map<PositioningId>::positioning_id_iterator,
           typename positionings_map<PositioningId>::positioning_id_iterator >
get_positionings( const positionings_map<PositioningId>& positioning_map_obj )
{
    static std::function<typename positionings_map<PositioningId>::positioning_id_descriptor(const typename positionings_map<PositioningId>::PositioningMap::value_type)>
            map_key_getter = [](const typename positionings_map<PositioningId>::PositioningMap::value_type& v){ return v.first; };
    return std::make_pair( boost::make_transform_iterator( positioning_map_obj.m_positioning_map.begin(), map_key_getter ),
                           boost::make_transform_iterator( positioning_map_obj.m_positioning_map.end(), map_key_getter ));
}

template <typename PositioningId>
typename positionings_map<PositioningId>::positionings_size_type
num_positionings( const positionings_map<PositioningId>& positioning_map_obj )
{
    return positioning_map_obj.m_positioning_map.size();
}

template <typename PositioningId>
typename positionings_map<PositioningId>::positioning_id_descriptor
get_positioning( const typename positionings_map<PositioningId>::positioning_id_descriptor positioning_id,
             const positionings_map<PositioningId>& positioning_map_obj )
{
    return positioning_map_obj.m_positioning_map.at( positioning_id );
}

// MutablePositioningConcept implementation
template <typename PositioningId>
void set_positioning( const typename positionings_map<PositioningId>::positioning_id_descriptor positioning_id1,
                      const typename positionings_map<PositioningId>::positioning_id_descriptor positioning_id2,
                      positionings_map<PositioningId>& positioning_map_obj)
{
    positioning_map_obj.m_positioning_map[positioning_id1] = positioning_id2;
}

template <typename PositioningId>
void swap_positioning( const typename positionings_map<PositioningId>::positioning_id_descriptor src_positioning_id,
                       const typename positionings_map<PositioningId>::positioning_id_descriptor tgt_positioning_id,
                       positionings_map<PositioningId>& positioning_map_obj)
{
    typename positionings_map<PositioningId>::positioning_id_descriptor tmp_positioning_id = positioning_map_obj.m_positioning_map.at(src_positioning_id);
    positioning_map_obj.m_positioning_map[src_positioning_id] = positioning_map_obj.m_positioning_map[tgt_positioning_id];
    positioning_map_obj.m_positioning_map[tgt_positioning_id] = tmp_positioning_id ;
}

}

#endif // POSITIONINGS_MAP_HPP
