#ifndef SOCCER_PASS_CONCEPT_HPP
#define SOCCER_PASS_CONCEPT_HPP

#include <soccer/pass_traits.hpp>

#include <soccer/action_concepts.hpp>
#include <soccer/position_concepts.hpp>

#include <boost/type_traits.hpp>

namespace soccer {

BOOST_concept(PassConcept,(P))
    : public ActionConcept<P>
{
     BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< pass_tag, typename tag< P >::type >::value ),
                               "Type is not categorized with the expected category tag");
    typedef typename player_id_type< P >::type player_id_t;
    typedef typename duration_type<P>::type duration_t;
    typedef typename position_type< P >::type position_t;
    typedef typename speed_type< P >::type speed_t;

    BOOST_CONCEPT_ASSERT(( PositionConcept<position_t> ));

    BOOST_CONCEPT_USAGE(PassConcept)
    {
        const_constraints( _pass );
    }

private:
    void const_constraints ( const P& const_pass )
    {
        _pid = get_receiver_id( const_pass );
        _pos = get_position( const_pass );
        _speed = get_ball_first_speed( const_pass );
        _dur = get_duration( const_pass );
    }

    P _pass;
    player_id_t _pid;
    duration_t _dur;
    position_t _pos;
    speed_t _speed;
};

BOOST_concept(MutablePassConcept,(P))
    : public PassConcept<P>
{
    typedef typename player_id_type< P >::type player_id_t;
    typedef typename duration_type<P>::type duration_t;
    typedef typename position_type< P >::type position_t;
    typedef typename speed_type< P >::type speed_t;

    BOOST_CONCEPT_USAGE(MutablePassConcept)
    {
        set_receiver_id( _pid, _pass );
        set_position( _pos, _pass );
        set_ball_first_speed( _speed, _pass );
        set_duration( _dur, _pass );
    }

private:
    P _pass;
    player_id_t _pid;
    duration_t _dur;
    position_t _pos;
    speed_t _speed;
};

}

#endif // SOCCER_PASS_CONCEPT_HPP
