#ifndef SOCCER_TACKLE_HPP
#define SOCCER_TACKLE_HPP

#include <soccer/action.hpp>

#include <soccer/core/tags.hpp>

namespace soccer {

template < typename PlayerId,
           typename Duration,
           typename Direction,
           typename Foul = bool>
struct tackle 
    : public action<PlayerId,Duration>
{
    typedef tackle_tag action_category;

    typedef Direction position_descriptor;
    typedef bool foul_descriptor;

    tackle() : action<PlayerId,Duration>(ActionType::Tackle) { }

    Direction M_direction;
    Foul M_use_foul;
};

} // end namespace soccer


namespace soccer { namespace traits {

//
// tackle TYPE TRAITS REGISTRATION
//
template <>
template < typename PlayerId,
           typename Duration,
           typename Direction,
           typename Foul >
struct tag< soccer::tackle< PlayerId, Duration, Direction, Foul > > { typedef tackle_tag type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Direction,
         typename Foul >
struct player_id_type< soccer::tackle< PlayerId, Duration, Direction, Foul > > { typedef PlayerId type; };

template <>
template < typename PlayerId,
          typename Duration,
          typename Direction,
          typename Foul >
struct duration_type< soccer::tackle< PlayerId, Duration, Direction, Foul > > { typedef Duration type; };


template <>
template < typename PlayerId,
         typename Duration,
         typename Direction,
         typename Foul >
struct direction_type< soccer::tackle< PlayerId, Duration, Direction, Foul > > { typedef Direction type; };

/* template <>
template < typename PlayerId,
         typename Duration,
         typename Direction,
         typename Foul >
struct use_foul_type< soccer::tackle< PlayerId, Duration, Direction, Foul > > { typedef Foul type; }; */


//
// tackle DATA ACCESS REGISTRATION
//

template <>
template < typename PlayerId,
         typename Duration,
         typename Direction,
         typename Foul >
struct executor_id_access< soccer::tackle< PlayerId, Duration, Direction, Foul > >
{
    typedef soccer::tackle< PlayerId, Duration, Direction, Foul > tackle_t;

    static inline
    PlayerId const& get( tackle_t const& _tackle ) { return _tackle.M_executor_id; }

    static inline
    void set( PlayerId const& pid, tackle_t& _tackle ) { _tackle.M_executor_id = pid; }
};

template <>
template < typename PlayerId,
          typename Duration,
          typename Direction,
          typename Foul >
struct duration_access< soccer::tackle< PlayerId, Duration, Direction, Foul > >
{
    typedef soccer::tackle< PlayerId, Duration, Direction, Foul > tackle_t;

    static inline
    Duration const& get( tackle_t const& _tackle ) { return _tackle.M_duration; }

    static inline
    void set( Duration const& dur, tackle_t& _tackle ) { _tackle.M_duration = dur; }
};


/* template <>
template < typename PlayerId,
         typename Duration,
         typename Direction,
         typename Foul >
struct use_foul_access< soccer::tackle< PlayerId, Duration, Direction, Foul > >
{
    typedef soccer::tackle< PlayerId, Duration, Direction, Foul > tackle_t;

    static inline
    Foul const& get( tackle_t const& _tackle ) { return _tackle.M_use_foul; }

    static inline
    void set( Foul const& _use_foul, tackle_t& _tackle ) { _tackle.M_use_foul = _use_foul; }
}; */

}} // end namespace soccer::traits

#endif // SOCCER_TACKLE_HPP
