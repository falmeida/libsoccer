#ifndef SOCCER_EFFECTORS_HPP
#define SOCCER_EFFECTORS_HPP

#include <soccer/ball_model_algorithms.hpp>

namespace soccer {

template < typename Object,
           typename ObjectModel >
void inertia( Object& _obj,
              ObjectModel const& _model,
              unsigned int _num_iterations = 1u )
{
    set_position( get_inertia_position( _obj, _model, _num_iterations ), _obj );
    set_velocity( get_inertia_velocity( _obj, _model, _num_iterations ), _obj );
}

template < typename Power,
           typename Ball >
struct BallKickEffector
{
    BOOST_CONCEPT_ASSERT(( BallConcept<Ball> ));
    BOOST_CONCEPT_ASSERT(( BallModelConcept<BallModel> ));

    Power M_power;

    BallKickEffector( Power power )
        : M_power( power )
    {

    }

    void operator()( Ball& _ball )
    {
        // kick power rate
    }
};

template < typename Ball >
struct StopEffector
{
    BOOST_CONCEPT_ASSERT(( BallConcept<Ball> ));
    BOOST_CONCEPT_ASSERT(( BallModelConcept<BallModel> ));

    void operator() ( Ball& _ball )
    {
        typename velocity_type< Ball >::type _ball_vel;
        set_x( 0.0, _ball_vel );
        set_y( 0.0, _ball_vel );
    }
};


}

#endif // SOCCER_BALL_EFFECTOR_HPP
