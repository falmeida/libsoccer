#ifndef SOCCER_STATE_ALGORITHMS_HPP
#define SOCCER_STATE_ALGORITHMS_HPP

#include <soccer/geometry.hpp>

#include <soccer/core/access.hpp>

#include <soccer/state_traits.hpp>
#include <soccer/player_predicates.hpp>

#include <soccer/state_concepts.hpp>
#include <soccer/playmode_algorithms.hpp>

#include <soccer/pitch_algorithms.hpp>
#include <soccer/game_algorithms.hpp>

#include <soccer/types.h>

#include <boost/tuple/tuple.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/type_traits.hpp>

#include <functional>
#include <algorithm>
#include <iterator>

namespace soccer {

namespace detail {

template <class ForwardIterator, class BinaryPredicate>
ForwardIterator
find_best_match( ForwardIterator first, ForwardIterator last, BinaryPredicate pred )
{
  if ( first == last)
      return last;

  ForwardIterator best_result = first;
  while (++first!=last)
    if ( pred ( *best_result, *first ) )
    {
        best_result = first;
    }
  return best_result;
}

}

// state_traits shortcut accesors
template < typename State >
typename side_type< typename player_type< State >::type >::type
get_side( const typename player_id_type< State >::type player_id,
      State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_side( get_player( player_id, state ) );
}

template < typename State >
typename position_type< typename player_type< State >::type >::type
get_player_position( const typename player_id_type< State >::type pid, State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_position( get_player( pid, state ) );
}

template < typename State >
typename velocity_type< typename player_type< State >::type >::type
get_player_velocity( const typename player_id_type< State >::type pid, State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_velocity( get_player( pid, state ) );
}

template < typename State >
typename number_type< typename player_type< State >::type >::type
get_number( const typename player_id_type< State >::type player_id,
            State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_number( get_player( player_id, state ) );
}

template < typename State >
typename velocity_type< typename player_type< State >::type >::type
get_velocity( const typename player_id_type< State >::type player_id,
          State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_velocity( get_player( player_id, state ) );
}

template < typename State >
typename direction_type< typename player_type< State >::type >::type
get_body_direction( typename player_id_type< State >::type const& player_id,
                    State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_body_direction( get_player( player_id, state ) );
}

template < typename State >
typename position_type< typename ball_type< State >::type >::type
get_ball_position( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_position( get_ball( state ) );
}

template < typename State >
inline typename velocity_type< typename ball_type< State >::type >::type
get_ball_velocity( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_velocity( get_ball( state ) );
}

template < typename State >
inline bool
is_goalie( const typename player_id_type< State >::type player_id,
           State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return is_goalie( get_player( player_id, state ) );
}

/*!
  \brief Determine the team score for a particular state
  \param side the team side whose score is to be retrieved
  \param state the state
  \returns the team score
  */
template < typename State >
inline typename score_type< State >::type
get_score( typename side_type< typename player_type< State >::type >::type side,
           State const& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return side == get_left_side< side_t >()
            ? get_left_score( state )
            : get_right_score( state );
}

/*!
  \brief Check if two player identifiers of a state belong to the same side
  \param player_id1 the first player identifier
  \param player_id2 the second player identifier
  \param state the state to resolve the player identifiers
  \returns True if both player identifiers belong to the same team or false otherwise.
  */
template < typename State >
bool same_side( const typename player_id_type< State >::type player_id1,
                const typename player_id_type< State >::type player_id2,
                State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return get_side( player_id1, state ) == get_side( player_id2, state );
}

/*!
  \brief Determine the player identifier on a state for a particular number and side descriptors
  */
template < typename State >
typename player_id_type< State >::type
player_id( const typename number_type< typename player_type< State >::type >::type _number,
           typename side_type< typename player_type< State >::type >::type const& _side,
           State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_id_type< State >::type player_id_t;

    player_iterator_t f, l;
    for ( boost::tie( f, l ) = get_players( state ) ; f != l; f++ )
    {
        const auto _player = get_player( *f, state );
        if ( get_side( _player ) == _side && get_number( _player ) == _number )
        {
            return *f;
        }
    }

    return get_null_value< player_id_t >();
}

/*!
  \brief Determine the direction from a player position to an arbitrary position
  \param from_pid the source player identifier
  \param to_pid the target player identifier
  \param the state
  \returns direction from a source player to a target player
  */
template < typename State >
typename state_traits< State >::direction_t // TODO Fix Me!!!
direction( const typename player_id_type< State >::type from_pid,
           const typename player_id_type< State >::type to_pid,
           State const& state )
{
    return direction( get_position( from_pid, state ), get_position( to_pid, state ) );
}

/*!
  \brief Determine the direction from a player position to an arbitrary position
  \param from_pid the player identifier
  \param to_pos the target position
  \param the state
  \returns direction from a player position to an arbitrary position
  */
template < typename State,
           typename Position >
typename state_traits< State >::direction_t
direction( const typename player_id_type< State >::type from_pid,
           Position const& to_pos,
           State const& state )
{
    return direction( get_position( from_pid, state ), to_pos );
}

template < typename State,
           typename Position >
typename state_traits< State >::direction_t
direction( Position const& from_pos,
           const typename player_id_type< State >::type to_pid,
           State const& state )
{
    return direction( from_pos, get_position( to_pid, state ) );
}

/*!
  \brief Determine the direction from arbitrary to an position player position
  \param from_pos the position
  \param to_pid the player identifier
  \param the state
  \returns direction from an arbitrary position to a player position
  */
template < typename State >
bool same_side( const typename player_id_type< State >::type pid1,
                const typename player_id_type< State >::type pid2,
                State& state )
{
    return get_side( pid1, state) == get_side( pid2, state );
}

/*!
 \brief distance between two players using their state identifiers
 \param pid1 id of the first player
 \param pid2 id of the second player
 \param state the state in which the players are defined
 \returns distance between the players current position
 */
template < typename State >
typename position_traits< typename state_traits< State >::position_type >::distance_t // TODO Fix ME
distance( const typename player_id_type< State >::type pid1,
          const typename player_id_type< State >::type pid2,
          State const& state )
{
    // typedef player_traits< typename player_type< State >::type > player_traits;
    return distance( get_position( pid1, state ), get_position( pid2, state ) );
}

/*!
 \brief the side of the ball owner
 \param state the state
 \returns the side of the ball owner
 */
template < typename State >
typename side_type< typename player_type< State >::type >::type
ball_owner_side( State const& state )
{
    return get_side( ball_owner_id( state ), state );
}

/*!
\brief Determine the side of the attacking team
\param state The state to analyze
*/
template < typename State >
typename side_type< typename player_type< State >::type >::type
attacking_side( State const& state )
{
    return ball_owner_side( state );
}

/*!
\brief Determine the side of the defending team
\param state The state to analyze
*/
template < typename State >
typename side_type< typename player_type< State >::type >::type
defending_side( State const& state )
{
    return state_traits< State >::opposite_side( attacking_side( state ) );
}

/*!
  \brief Determine the number of players adjacent to another player
  \param player_id the player identifier
  \param state the state
  \returns The number of players adjacent to the player identifier
  */
/* template < typename State >
typename boost::enable_if< detail::has_player_t< State >,
                           typename state_traits< State >::players_size_type >::type
num_adjacent_players( const typename player_id_type< State >::type& player_id,
                      State& state)
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    // Get adjacent vertexes
    typename adjacent_player_iterator_type< State >::type vi, viend;
    boost::tie(vi, viend) = adjacent_players( player_id, state );

    return viend - vi;
} */

/*!
  \brief Get team players iterators based on distance from ball
  \param side the side of the players
  \param state the state to consider
  \returns a pair of iterators for the players
*/
template < typename State >
std::pair< typename State::team_players_distance_from_ball_iterator,
           typename State::team_players_distance_from_ball_iterator> teamPlayersDistanceFromBall( typename side_type< typename player_type< State >::type >::type side,
                                                                                                  State& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return side == get_left_side< side_t >()
            ? leftPlayersDistanceFromBall( state )
            : rightPlayersDistanceFromBall( state );
}

/*!
  \brief Determine whether the ball is kickable in a state
  \param state the state
  \returns True if the ball is kickable, or false otherwise
  */
template < typename State >
bool is_ball_kickable( State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typename player_iterator_type< State >::type _player_begin, _player_end;

    boost::tie(_player_begin,_player_end) = get_players(state);
    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( canKick( *itp, state ) )
        {
            return true;
        }
    }
    return false;
}

/*!
  \brief Determine whether two players are adjacent based on their identifiers in a given state
  \param player_id1 the first player identifier
  \param player_id2 the first player identifier
  \param state the state
  \returns True if players are adjacent or false otherwise
  */
template < typename State >
bool is_adjacent( const typename player_type< State >::type& player_id1,
                  const typename player_type< State >::type& player_id2,
                  State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    // Get adjacent vertexes
    typedef typename player_type< State >::type player_t;

    typename state_traits< State >::adjacency_iterator vi, viend, found;
    boost::tie(vi, viend) = adjacent_players( player_id1, state );

    found = std::find( vi, viend, player_id2 );

    return found != viend;
}

/****************
 * State Functors
 */

/*!
  \defgroup Player sorting binary function
  */
template < typename State,
           typename StaticObject >
struct player_id_direction_from_object_cmp
        : public std::binary_function< typename player_id_type< State >::type,
                                       typename player_id_type< State >::type,
                                       bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( StaticObjectConcept<StaticObject> ));

    player_id_direction_from_object_cmp( State const& state,
                                         const StaticObject& static_object )
        : M_state( state )
    {

    }

    bool operator() ( const player_id_t pid1,
                      const player_id_t pid2 )
    {
        return direction( get_position( pid1, M_state ), get_position( M_static_object ) ) <
               direction( get_position( pid2, M_state ), get_position( M_static_object ) );
    }

    State const& M_state;
    const StaticObject& M_static_object;
};

/*!
  \defgroup Player sorting binary function
  */
template < typename State >
struct player_id_direction_from_ball_cmp
        : public std::binary_function< typename player_id_type< State >::type,
                                       typename player_id_type< State >::type,
                                       bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    player_id_direction_from_ball_cmp( State const& state )
        : m_state( state )
    {

    }

    bool operator() ( const player_id_t pd1,
                      const player_id_t pd2 )
    {
        return directionFromBall( pd1, m_state ) < directionFromBall( pd2, m_state );
    }

    State const& m_state;
};

/*!
 * \brief compare player id distances
 */
template < typename State >
struct player_id_distance_from_cmp
        : public std::binary_function< typename player_id_type< State >::type,
                                       typename player_id_type< State >::type,
                                       bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    player_id_distance_from_cmp( State const& state )
        : M_state( state )
    {

    }

    bool operator() ( const player_id_t pid1,
                      const player_id_t pid2)
    {
        return distanceFromBall( pid1, M_state ) < distanceFromBall( pid2, M_state );
    }

    State const& M_state;
};

/*!
  \brief Predicate to check if two player identifiers are adjacent in a state
  */
template < typename State >
struct is_adjacent_functor
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    State const& M_state;

    is_adjacent_functor( State const& state )
        : M_state( state )
    {

    }

    bool operator() ( const player_id_t player_id1,
                      const player_id_t player_id2)
    {
        return is_adjacent( player_id1,
                            player_id2,
                            M_state );
    }
};


/*!
  \brief Constant opponent id
  */
template < typename State,
           typename OpponentId >
struct opponent_id_constant
        : public std::unary_function< State, OpponentId>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    OpponentId M_opponent_id;

    opponent_id_constant( OpponentId opponent_id )
        : M_opponent_id( opponent_id )
    {

    }

    OpponentId operator()( State const& )
    {
        return M_opponent_id;
    }
};

/*!
 \brief Assess the score of a state
 */
template < typename State >
struct score_assessment
    : public std::unary_function< State, ScoreAssessment>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    typedef typename score_type< State >::type score_t;


    side_t M_team_side;
    side_t M_opponent_side;

    score_assessment( const side_t team_side )
        : M_team_side( team_side )
        , M_opponent_side( opposite_side( team_side ) )
    {

    }

    ScoreAssessment operator()( State const& state )
    {
        const auto team_score = get_score( M_team_side, state );
        const auto opponent_score = get_score( M_opponent_side, state );
        const auto relative_score = (int) team_score - (int) opponent_score;

        ScoreAssessment score_assessment = ScoreAssessment::MEDIUM;
        int dif = 2;  //draw
        if ( relative_score > dif) score_assessment = ScoreAssessment::VERY_GOOD;
        else if ( relative_score > 0 ) score_assessment = ScoreAssessment::GOOD;
        else if ( relative_score < -dif ) score_assessment = ScoreAssessment::VERY_BAD;  //loose bad
        else if ( relative_score < 0 ) score_assessment = ScoreAssessment::BAD;

        return score_assessment;
    }
};


/*!
 * \brief Generate a player iterator that skips player ids according to a unary predicate
 * \tparam State state model
 */
template < typename State >
struct player_filter_iterator_generator
{
    typedef boost::filter_iterator< std::function< bool( typename player_id_type< State >::type ) >,
                                    typename player_iterator_type< State >::type > type;
};

/*!
 * \brief Make a filtered player iterator based on an unary player id predicate
 * \tparam State model of soccer state
 * \param state instance of a model of a soccer state
 * \param _pid_skip_fn unary predicate to skip player ids
 */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
make_player_filter_iterator( std::function< bool( typename player_id_type< State >::type ) > _pid_skip_fn,
                             State const& state )
{
    typename player_iterator_type< State >::type _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    return std::make_pair( typename player_filter_iterator_generator< State >::type( _pid_skip_fn, _player_begin, _player_end ),
                           typename player_filter_iterator_generator< State >::type( _pid_skip_fn, _player_end, _player_end ));
}

/*!
 * \brief Make a filtered player iterator based on an unary player id predicate
 * \tparam State model of soccer state
 * \param team_side the side of the players to iterate
 * \param state instance of a model of a soccer state
 */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
make_teammate_iterator( typename side_type< typename player_type< State >::type >::type const& team_side,
                        State const& state )
{
    std::function< bool(typename player_id_type< State >::type const) >
            opponent_player_skip_fn = [&] ( typename player_id_type< State >::type const pid )
                                    {
                                        return get_side( get_player( pid, state ) ) == team_side;
                                    };
    return make_player_filter_iterator( opponent_player_skip_fn, state );
}

/*!
 * \brief Get a pair of iterators to left players
 * \tparam State model of soccer state
 * \param state instance of a model of a soccer state
 */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
get_left_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return make_player_filter_iterator( get_left_side< side_t >( ), state );
}

/*!
* \brief Get a pair of iterators to left players
* \tparam State model of soccer state
* \param state instance of a model of a soccer state
*/
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
          typename player_filter_iterator_generator< State >::type >
get_right_players( State const& state )
{
   BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
   typedef typename side_type< typename player_type< State >::type >::type side_t;

   return make_player_filter_iterator( get_right_side< side_t >( ), state );
}


/*!
  \brief Get iterators for players from a given team
  */
template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
get_players( typename side_type< typename player_type< State >::type >::type const& side,
             State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return make_teammate_iterator( side, state );
}

template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
opposite_players( typename side_type< typename player_type< State >::type >::type const& side,
                  State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return make_teammate_iterator( get_opposite_side< side_t >( side ), state );
}

template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
our_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return make_teammate_iterator( our_side( state ), state );
}

template < typename State >
std::pair< typename player_filter_iterator_generator< State >::type,
           typename player_filter_iterator_generator< State >::type >
their_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return make_teammate_iterator( their_side( state ), state );
}

/*!
  \brief Get adjacent players from the same side of the player
*/
template < typename State >
struct AdjacentTeammateGenerator
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;

    player_id_t M_sender_id;


    AdjacentTeammateGenerator( player_id_t sender )
        : M_sender_id( sender )
    {

    }

    template < typename PlayerIdOutputIterator >
    void operator() ( State const& state, PlayerIdOutputIterator out_player )
    {
        typedef boost::detail::iterator_traits< PlayerIdOutputIterator > player_id_output_iterator;
        // BOOST_STATIC_ASSERT_MSG(( !boost::is_same< adjacent_player_iterator, void >::value), "State must provide an adjacent_player_iterator" );
        // Output iterators do not provide value_type???????
        /* BOOST_STATIC_ASSERT_MSG( boost::is_convertible< player_id_t,
                                                        typename player_id_output_iterator::value_type >::value,
                                 "player_id_t do not match in AdjacentTeammateGenerator" ); */

        assert( false );
        /*typename side_type< typename player_type< State >::type >::type const& sender_side = get_side( m_sender_id, state );
        adjacent_player_iterator f, l;
        for ( boost::tie( f, l ) = adjacent_players( m_sender_id, state ); f != l ; f++ )
        {
            if ( side( *f, state ) == sender_side )
            {
                out_player = *f;
            }
        }*/
    }
};

/////////////////////
// Players Offside //
/////////////////////

/*!
  \brief Determine player id of left team players that are in an offisde situation
  */
template < typename State,
           typename PlayerIdOutputIterator >
void
left_players_offside( State const& state, PlayerIdOutputIterator out_players )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename side_type< player_t >::type side_t;

    const auto offside_line = left_offside_line( state );

    player_from_side_unary_predicate<player_t> side_pred( get_left_side< side_t >() );
    is_player_pos_after_x<player_t> offside_pred( offside_line );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    for ( auto itp = _player_begin ; itp != _player_end; itp++ )
    {
        const auto _player = get_player( *itp, state );

        if ( side_pred( _player ) && offside_pred( _player ) )
        {
            *out_players = *itp;
        }
    }

}

/*!
  \brief Determine player id of left team players that are in an offisde situation
  */
template < typename State,
           typename PlayerIdOutputIterator >
void
right_players_offside( State const& state,
                       PlayerIdOutputIterator out_players )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    const auto _offside_line = right_offside_line( state );

    player_from_side_unary_predicate<player_t> side_pred( get_right_side< side_t >() );
    is_player_pos_before_x<player_t> offside_pred( _offside_line );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        const auto _player = get_player( *itp, state );

        if ( side_pred( _player ) && offside_pred( _player ) )
        {
            *out_players = *itp;
        }
    }

}

/*!
  \brief Players offside from a given team side
  \returns list of player ids which are offside
  */
template < typename State,
           typename PlayerIdOutputIterator >
inline void
players_offside( typename side_type< typename player_type< State >::type >::type const& side,
                 State const& state,
                 PlayerIdOutputIterator out_players )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return side == get_left_side< side_t >()
            ? left_players_offside( state, out_players )
            : right_players_offside( state, out_players );
}

/*!
  \brief Players offside from a given team side
  \returns list of player ids which are offside
  */
template < typename State,
           typename Pitch >
inline bool
is_offside( typename player_id_type< State >::type const& pid,
            State const& state,
            Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    const auto _player = get_player( pid, state );
    const auto _side = get_side( _player );

    assert( _side != get_unknown_side< side_t >() );

    const auto _player_pos = get_position( _player );

    // TODO Non-generic (not assessing sign of vertical and horizontal coordinates)
    if ( _side == get_left_side< side_t >() )
    {
        auto _offside_line = get_left_offside_line( pitch, state );
        return get_x( _player_pos ) < _offside_line;
    }
    auto _offside_line = get_right_offside_line( pitch, state );
    return get_x( _player_pos ) > _offside_line;
}


/////////////////
// Goalie info //
/////////////////

/*!
  \brief Determine the goalie player identifier for a given team
  \param side the team side
  \param state the state to consider
  \returns The player id or null_player() if not found
*/
template < typename State >
typename player_id_type< State >::type
goalie_id( typename side_type< typename player_type< State >::type >::type const& goalie_side,
           State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    assert ( goalie_side == get_left_side< side_t >() ||
             goalie_side == get_right_side< side_t >() );

    if ( goalie_side == get_unknown_side< side_t >() )
    {
        return get_null_value< player_id_tag, State >();
    }

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( get_side( *itp, state ) == goalie_side &&
             is_goalie( *itp, state ) )
        {
            return *itp;
        }
    }

    return get_null_value< player_id_tag, State >();
}

/*!
  \brief Left team goalie id
  */
template < typename State >
typename player_id_type< State >::type
left_goalie_id( State const& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return goalie_id( get_right_side< side_t >(), state );
}

/*!
  \brief Leftmost player in a state
  */
template < typename State >
typename player_id_type< State >::type
right_goalie_id( State const& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    return goalie_id( get_right_side< side_t >(), state );
}


//////////////////////
// State Predicates //
//////////////////////

/*!
  \brief Player id unary predicate (proxy for player unary predicates)
  */
template <typename PlayerUnaryPredicate,
          typename State>
struct player_id_unary_predicate
    : public std::unary_function< typename player_id_type< State >::type,
                                  bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_type< State >::type player_t;
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicate< PlayerUnaryPredicate, player_t> ));

    player_id_unary_predicate()
        : M_state( NULL )
    {

    }

    player_id_unary_predicate( const PlayerUnaryPredicate pred,
                               const State* state )
        : M_pred( pred )
        , M_state( state )
    {

    }

    bool operator() ( const player_id_t player_id ) const
    {
        assert( M_state != NULL );

        const auto _player = get_player( player_id, *M_state );
        return M_pred( _player );
    }

    PlayerUnaryPredicate M_pred;
    const State* M_state;
};

/*!
  \brief Player id binary predicate (proxy for player binary predicates)
  */
template <typename PlayerBinaryPredicate,
          typename State>
struct player_id_binary_predicate
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_type< State >::type player_t;
    typedef typename player_id_type< State >::type player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::BinaryPredicate< PlayerBinaryPredicate, player_t, player_t > ));

    player_id_binary_predicate()
    {

    }

    player_id_binary_predicate( const PlayerBinaryPredicate pred,
                                const State* state  )
        : M_pred( pred )
        , M_state( state )
    {

    }

    bool operator() ( const player_id_t pid1,
                      const player_id_t pid2 ) const
    {
        const auto _p1 = get_player( pid1, *M_state );
        const auto _p2 = get_player( pid2, *M_state );

        return M_pred( _p1, _p2 );
    }

    PlayerBinaryPredicate M_pred;
    const State* M_state;
};


//////////////////
// Defense line //
//////////////////

/*!
  \brief Determine the left team defense line
  */
template < typename State >
typename coordinate_type<
    typename position_type<
        typename player_type< State >::type
    >::type
>::type
left_defense_line( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return get_null_value< player_id_t >();
    }

    auto leftmost_player_id1 = get_null_value< player_id_t >();
    auto leftmost_player_id2 = get_null_value< player_id_t >();
    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( side( *itp, state ) != get_left_side< side_t >() )
        {
            continue;
        }

        if ( leftmost_player_id1 == get_null_value< player_id_t >()
             || get_x( get_position( *itp, state )) < get_x( get_position( leftmost_player_id1, state )) )
        {
            leftmost_player_id2 = leftmost_player_id1;
            leftmost_player_id1 = *itp;
        }
    }

    auto left_defense_line = get_x( get_position( get_ball( state )));
    if ( leftmost_player_id1 != get_null_value< player_id_t >()
         && !isGoalie( leftmost_player_id1, state )
         && get_x( get_position( leftmost_player_id1, state )) < left_defense_line
         && get_x( get_position( leftmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        left_defense_line = get_x( get_position( leftmost_player_id1, state ));
    } else if ( leftmost_player_id1 != get_null_value< player_id_t >()
                && !isGoalie( leftmost_player_id2, state )
                && get_x( get_position( leftmost_player_id1, state )) < left_defense_line
                && get_x( get_position( leftmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        left_defense_line = get_x( get_position( leftmost_player_id2, state ) );
    }

    return left_defense_line;
}

/*!
  \brief Determine the x-axis coordinate of the right team defense line
  */
template < typename State >
typename coordinate_type< typename position_type< typename player_type< State >::type >::type >::type
right_defense_line( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return get_null_value< player_id_t >();
    }

    auto rightmost_player_id1 = get_null_value< player_id_t >();
    auto rightmost_player_id2 = get_null_value< player_id_t >();
    for ( auto itp = _player_begin ; itp != _player_end; itp++ )
    {
        if ( side( *itp, state ) != get_left_side< side_t >() )
        {
            continue;
        }

        if ( rightmost_player_id1 == get_null_value< player_id_t >()
             || get_x( get_position( *itp, state )) > get_x( get_position( rightmost_player_id1, state )) )
        {
            rightmost_player_id2 = rightmost_player_id1;
            rightmost_player_id1 = *itp;
        }
    }

    auto right_defense_line = get_x( get_position( get_ball( state )));
    if ( rightmost_player_id1 != get_null_value< player_id_t >()
         && !isGoalie( rightmost_player_id1, state )
         && get_x( get_position( rightmost_player_id1, state )) > right_defense_line
         && get_x( get_position( rightmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        right_defense_line = get_x( get_position( rightmost_player_id1, state ));
    } else if ( rightmost_player_id1 != get_null_value< player_id_t >()
                && !isGoalie( rightmost_player_id2, state )
                && get_x( get_position( rightmost_player_id1, state )) < right_defense_line
                && get_x( get_position( rightmost_player_id1, state )) < get_x( get_position( get_ball( state ))) )
    {
        right_defense_line = get_x( get_position( rightmost_player_id2, state ));
    }

    return right_defense_line;
}

/*!
  \brief Determine the defense line (x-axis coordinate) of a given team side
  \param side the side of the team to consider
  \param state the state
  \returns The x-axis coordinate of the team's defense line
  */
template < typename State >
typename coordinate_type< typename position_type< typename player_type< State >::type >::type >::type
defense_line( typename side_type< typename player_type< State >::type >::type const& side,
              State const& state )
{
    return side == get_left_side< typename side_type< typename player_type< State >::type >::type >()
            ? left_defense_line( state )
            : right_defense_line( state );
}


//////////////////
// Offense line //
//////////////////

/*!
  \brief determine the left offense line
  \param state the state to consider
  \returns the coordinate the left team offense line
  */
template < typename State,
           typename Pitch >
typename coordinate_type< typename position_type< typename player_type< State >::type >::type >::type
left_offense_line( State const& state,
                   Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    auto _left_offense_line = get_left_goal_line( pitch );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return _left_offense_line;
    }

    auto _right_defense_line =_right_defense_line( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( side( *itp, state ) != get_left_side< side_t >() )
        {
            continue;
        }

        const auto _player = get_player( *itp, state );
        const auto _player_pos = get_position( _player );
        if ( get_x( _player_pos ) > _right_defense_line )
        {
            continue;
        }

        if ( get_x( _player_pos ) > _left_offense_line )
        {
            _left_offense_line = get_x( _player_pos );
        }
    }

    return _left_offense_line;
}


/*!
 * \brief Team goal center position
 * \returns position of the goal center for the team side
 */
template < typename OutputPosition,
           typename Side,
           typename Pitch >
OutputPosition
get_goal_center_position( Side const& side,
                          Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<OutputPosition> ));

        return side == get_left_side< Side >()
                ? get_left_goal_center_position<OutputPosition>( pitch )
                : get_right_goal_center_position<OutputPosition>( pitch );
}


/*!
 * \brief Team goal upper post position
 * \returns position of the goal upper post for the team side
 */ 
template < typename OutputPosition,
           typename Pitch,
           typename Side >
OutputPosition
get_goal_upper_post( Side const& side,
                     Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

	BOOST_CONCEPT_ASSERT(( MutablePositionConcept<OutputPosition> ));

    return side == get_left_side< Side >()
        ? get_left_goal_upper_post<OutputPosition>( pitch )
        : get_right_goal_upper_post<OutputPosition>( pitch );
}

/*
 * \brief Team goal lower post position
 * \returns position of the goal lower post for the team side
 */
template < typename OutputPosition,
           typename Pitch,
           typename Side >
OutputPosition
get_goal_lower_post( Side const& side,
                     Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<OutputPosition> ));

        return side == get_left_side< Side >()
                ? get_left_goal_lower_post<OutputPosition>( pitch )
                : get_right_goal_lower_post<OutputPosition>( pitch );
}


/*
 * \brief Team goal line coordinate
 * \returns coordinate of the team goal line
 */
template < typename Pitch,
           typename Side >
typename coordinate_type< Pitch >::type
get_goal_line( Side const& side,
               Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        return side == get_left_side< Side >()
                ? get_left_goal_line( pitch )
                : get_right_goal_line( pitch );
}

/*
 * \brief Team goal area line coordinate
 * \returns coordinate of the team goal area line
 */
template < typename Pitch,
           typename Side >
typename coordinate_type< Pitch >::type
get_goal_area_line( Side const& side,
                    Pitch const& pitch )
{
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        return side == get_left_side< Side >()
                ? get_left_goal_area_line( pitch )
                : get_right_goal_area_line( pitch );
}


/*
 * \brief Team goal area line coordinate
 * \returns coordinate of the team goal area line
 */
template < typename Pitch,
           typename Side >
typename coordinate_type< Pitch >::type
get_penalty_area_line( Side const& side,
                       Pitch const& pitch )
{         
        BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

        return side == get_left_side< Side >()
                ? get_left_penalty_area_line( pitch )
                : get_right_penalty_area_line( pitch );
}


/*!
  \brief determine the right offense line
  \param state the state to consider
  \returns the coordinate the right team offense line
  */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_right_offense_line( Pitch const& pitch,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;


    auto _right_offense_line = get_right_goal_line( pitch );

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );
    if ( _player_begin == _player_end )
    {
        return _right_offense_line;
    }

    auto _left_defense_line =_left_defense_line( state );

    for ( auto itp = _player_begin; itp != _player_end; itp++ )
    {
        if ( get_side( *itp, state ) != get_right_side< side_t >() )
        {
            continue;
        }

        const auto _player = get_player( *itp, state );
        const auto _player_pos = get_position( _player );
        if ( get_x( _player_pos ) < _left_defense_line )
        {
            continue;
        }

        if ( get_x( _player_pos ) < _right_offense_line )
        {
            _right_offense_line = get_x( _player_pos );
        }
    }

    return _right_offense_line;
}

/*!
  \brief Determine the offense line (x-axis coordinate) of a given team side
  \param side the team side whose offense line is to be considered
  \param state the state
  \returns The x-axis coordinate of the team's offense line
  */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_offense_line( typename side_type< typename player_type< State >::type >::type const& side,
                  Pitch const& pitch,
                  State const& state )
{
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return side == get_left_side< side_t >()
            ? get_left_offense_line( pitch, state )
            : get_right_offense_line( pitch, state );
}

//////////////////////////
// Offside calculations //
//////////////////////////

template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_left_offside_line( Pitch const& pitch,
                       State const& state )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    typename player_filter_iterator_generator< State >::type _opponent_begin, _opponent_end;
    boost::tie( _opponent_begin, _opponent_end ) = make_teammate_iterator( get_right_side< side_t >(), state );

    const auto _ball_x = get_x( get_position( get_ball( state ) ) );
    const auto _midfield_line = get_midfield_line( pitch );

    if ( _opponent_begin == _opponent_end )
    {
        // TODO HACK Change this. Logic is invalid for all cartesian referentials.
        return _ball_x < _midfield_line
             ? _midfield_line
             : _ball_x;
    }

    auto last_opponent_id = *_opponent_begin;
    auto last_opponent = get_player( last_opponent_id, state );
    auto before_last_opponent_id = get_null_value< player_id_tag, State >();

    auto ito = _opponent_begin;

    for ( ito++; ito != _opponent_end; ito++ )
    {
        const auto _opponent = get_player( *ito, state );
        const auto _opponent_pos = get_position( _opponent );

        if ( get_x( _opponent_pos  ) > get_x( get_position( last_opponent  ) ) )
        {
            before_last_opponent_id = last_opponent_id;
            last_opponent_id = *ito;
            last_opponent  = get_player( last_opponent_id, state );
        }
    }


    if ( before_last_opponent_id == get_null_value< player_id_tag, State >() )
    {
        // TODO HACK Change this. Logic is invalid for all cartesian referentials.
        return _ball_x < _midfield_line
             ? _midfield_line
             : _ball_x;
    }

    auto before_last_opponent  = get_player( before_last_opponent_id, state );
    // TODO HACK Change this. Logic is invalid for all cartesian referentials.
    return _ball_x < get_x( get_position( before_last_opponent ) )
         ? get_x( get_position( before_last_opponent ) )
         : _ball_x;
}

template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_right_offside_line( Pitch const& pitch,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    assert( false ); // TODO Fix Me (update according to left_offside_line)
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    if ( _player_begin == _player_end )
    {
        return get_midfield_line( pitch );
    }

    typedef player_id_unary_predicate< player_from_side_unary_predicate< player_t >, State > player_id_predicate;
    player_id_predicate pred( get_right_side< side_t >(), &state );

    typedef boost::filter_iterator< player_id_predicate, player_iterator_t > filter_player_iterator;

    filter_player_iterator cur( pred, _player_begin, _player_end );
    filter_player_iterator end( pred, _player_end, _player_end );

    auto last_player_id = *cur;
    auto last_player = get_player( last_player_id, state );
    auto before_last_player_id = get_null_value< player_id_tag, State >();

    for ( ; cur != end; cur++ )
    {
        const auto _player = get_player( *cur, state );
        const auto _player_pos = get_position( _player );

        if ( get_x( _player_pos  ) < get_x( get_position( last_player ) ) )
        {
            before_last_player_id = last_player_id;
            last_player_id = *cur;
            last_player = get_player( last_player_id, state );
        }
    }

    if ( before_last_player_id == get_null_value< player_id_tag, State >() )
    {
        return get_midfield_line( pitch );
    }

    auto before_last_player = get_player( before_last_player_id, state );
    return get_x( get_position( before_last_player ));
}

/*!
  \brief Determine a team side offside line (x-axis coordinate)
  \param side the side of the team to consider
  \param state the state
  \returns The x-axis coordinate of the team's offside line
  */
template < typename Pitch,
           typename State >
typename coordinate_type< Pitch >::type
get_offside_line( typename side_type< typename player_type< State >::type >::type const& side,
                  Pitch const& pitch,
                  State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return side == get_left_side< side_t >()
            ? get_left_offside_line( pitch, state )
            : get_right_offside_line( pitch, state );
}



/*!
  \brief Check if a given position is offside based on a team pein a state
  \returns list of player ids which are offside
  */
template < typename State,
           typename Position,
           typename Pitch >
inline bool
is_offside( Position const& position,
            typename side_type< typename player_type< State >::type >::type const& _side,
            State const& state,
            Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    assert( _side != get_unknown_side< side_t >() );

    // TODO Non-generic (not assessing sign of vertical and horizontal coordinates)
    if ( _side == get_left_side< side_t >() )
    {
        auto _offside_line = get_left_offside_line( pitch, state );
        return get_x( position ) < _offside_line;
    }
    auto _offside_line = get_right_offside_line( pitch, state );
    return get_x( position ) > _offside_line;
}


/// End offside algorithms

/*!
  \brief Check if a position is within a team's penalty area
  \param side the side of the team to consider
  \param position the position to check
  \param pitch the pitch
  \returns true if the position is within the team's penalty area or false otherwise
  */
template < typename Side,
           typename Pitch,
           typename Position >
bool
is_within_penalty_area( Side const& side,
                             Position const& position,
                             Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return side == get_left_side< Side >()
            ? is_within_left_penalty_area( position, pitch )
            : is_within_right_penalty_area( position, pitch );
}

/*!
  \brief Check if a position is within a team's goal area
  \param side the side of the team to consider
  \param position the position to check
  \param pitch the pitch
  \returns true if the position is within the team's goal area or false otherwise
  */
template < typename Side,
           typename Pitch,
           typename Position >
bool
is_within_goal_area( Side const& side,
                          Position const& position,
                          Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return side == get_left_side< Side >()
            ? is_within_left_goal_area( position, pitch )
            : is_within_right_goal_area( position, pitch );
}

/*!
  \brief Check if a position is within a team's goal area
  \param side the side of the team to consider
  \param position the position to check
  \param pitch the pitch
  \returns true if the position is within the team's goal area or false otherwise
  */
template < typename Side,
           typename Pitch,
           typename Position >
bool
is_within_midfield_area( Side const& side,
                              Position const& position,
                              Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return side == get_left_side< Side >()
            ? is_within_left_midfield_area( position, pitch )
            : is_within_right_midfield_area( position, pitch );
}


/************
 * State Predicates
 */

template < typename State >
struct PlayerIdDistanceToPlayerIdPredicate
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;

    PlayerIdDistanceToPlayerIdPredicate( player_id_t player_id,
                                         State& state )
        : M_ref_player_id( player_id )
        , M_state( state )
    {

    }

    bool operator()( const player_id_t player_id1,
                     const player_id_t player_id2 )
    {
        return distance( M_ref_player_id, player_id1, M_state ) < distance( M_ref_player_id, player_id2, M_state );
    }

    player_id_t M_ref_player_id;
    State& M_state;
};

template < typename State,
           typename Position >
struct PlayerIdDistanceToPositionPredicate
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;

    PlayerIdDistanceToPositionPredicate( Position const& position,
                                         State* state )
        : M_position( position )
        , M_state( state )
    {

    }

    bool operator()( const player_id_t player_id1,
                     const player_id_t player_id2 )
    {
        return distance( M_position, player_id1, M_state ) < distance( M_position, player_id2, M_state );
    }

    Position M_position;
    State* M_state;
};

template < typename State >
struct direction_from_player_id_cmp
    : public std::binary_function< typename player_id_type< State >::type,
                                   typename player_id_type< State >::type,
                                   bool >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;

    direction_from_player_id_cmp( const player_id_t player_id,
                                  State const& state )
        : M_ref_player_id( player_id )
        , M_state( state )
    {

    }

    bool operator()( const player_id_t player_id1,
                     const player_id_t player_id2 )
    {
        return direction( M_ref_player_id, player_id1, M_state ) < direction( M_ref_player_id, player_id2, M_state );
    }

    const player_id_t M_ref_player_id;
    State const& M_state;
};

/*!
 \brief State predicate to determine if a player identifier is offside
 */
template < typename Pitch,
           typename State >
struct is_player_id_offside
    : public std::unary_function< typename player_id_type< State >::type, bool>
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    Pitch const* M_pitch;
    State const& M_state;

    is_player_id_offside( Pitch const& pitch,
                          State const& state )
        : M_pitch( &pitch )
        , M_state( state )
    {

    }

    bool operator() ( const player_id_t player_id )
    {
        return get_side( player_id, M_state ) == get_left_side< side_t >() ?
                    get_x( get_position( player_id, M_state ) ) > left_offside_line( *M_pitch, M_state ) :
                    get_x( get_position( player_id, M_state ) ) < right_offside_line( *M_pitch, M_state );
    }
};

/*!
  \brief check if ball is moving to goal
  */
template < typename MobileObject,
           typename Side,
           typename Pitch >
bool is_moving_towards_goal( MobileObject const& _mobile_object,
                             Side const& _side,
                             Pitch const& _pitch )
{
    BOOST_CONCEPT_ASSERT(( MobileObjectConcept< MobileObject > ));
    typedef typename soccer::position_type< MobileObject >::type mobile_object_position_t;

    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto _pos = get_position( _mobile_object );
    const auto _vel = get_velocity( _mobile_object );
    const auto _dir = direction( _vel );

    const auto _to_upper_post_direction = direction( _pos, get_goal_upper_post< mobile_object_position_t >( _side, _pitch ) );
    const auto _to_lower_post_direction = direction( _pos, get_goal_lower_post< mobile_object_position_t >( _side, _pitch ) );

    const auto _to_upper_post_diff = _to_upper_post_direction - _dir;
    const auto _to_lower_post_diff = _to_lower_post_direction - _dir;

    return _side == get_left_side< Side >()
            ? _to_upper_post_diff > 0.0 && _to_lower_post_diff < 0.0
            : _to_upper_post_diff < 0.0 && _to_lower_post_diff > 0.0;
}

template < typename State,
           typename Pitch >
bool is_ball_moving_towards_goal( typename side_type< typename player_type< State >::type >::type const side,
                                  State const& state,
                                  Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return is_moving_towards_goal( soccer::get_ball( state ),
                                   side,
                                   pitch );
}

/******************/
/* Player Queries */
/******************/

template < typename State >
typename state_traits< State >::players_size_type // TODO Fix Me
num_left_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return num_players( get_left_side< side_t >(), state );
}

template < typename State >
typename state_traits< State >::players_size_type // TODO Fix Me
num_right_players( State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    return num_players( get_right_side< side_t >(), state );
}

/*!
  \brief Determine the total number of players in a state
  \param state the state
  \returns the total number of players in the state
  */
template < typename State >
typename std::iterator_traits< typename player_iterator_type< State >::type >::difference_type
num_players( State& state )
{
    typename player_iterator_type< State >::type _player_begin, _player_end;

    boost::tie( _player_begin, _player_end ) = get_players( state );
    return std::distance( _player_begin, _player_end );
}

/*!
 * \brief Determine the player nearest to another position
 */
template < typename State,
           typename Position,
           typename PlayerIdExclusionUnaryPredicate >
typename player_id_type< State >::type
player_nearest_position( State const& state,
                         Position const& position,
                         PlayerIdExclusionUnaryPredicate pid_exclusion_predicate )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;

    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicateConcept<PlayerIdExclusionUnaryPredicate, player_id_t> ));

    player_iterator_t _player_begin, _player_end;
    boost::tie(_player_begin,_player_end) = get_players( state );

    if ( _player_begin == _player_end ) { return get_null_value< player_id_t >(); }

    auto _nearest_player = *_player_begin;
    auto _min_dist = comparable_distance( get_position( *_player_begin, state ), position);

    for ( ++_player_begin; _player_begin != _player_end; _player_begin++ )
    {
        if ( pid_exclusion_predicate( *_player_begin ) ) { continue; }
        const auto _dist = comparable_distance( get_position( *_player_begin, state ), position );
        if ( _dist < _min_dist )
        {
            _min_dist = _dist;
            _nearest_player =*_player_begin;
        }
    }

    return _nearest_player;
}

/*!
 * \brief Determine player id nearest to another player id
 * \tparam State state model
 * \tparam PlayerIdExclusionUnaryPredicate unary predicate model to exclude players from analysis
 * \param state state object
 * \param pid reference player id descriptor
 * \param pid_exclusion_predicate player id exclusion predicate
 */
template < typename State,
           typename PlayerIdExclusionUnaryPredicate >
typename player_id_type< State >::type
player_nearest_player( State const& state,
                       const typename player_id_type< State >::type& pid,
                       PlayerIdExclusionUnaryPredicate pid_exclusion_predicate )
{
    auto pid_exclusion_fn = [&] ( const typename player_id_type< State >::type _pid ) { return _pid == pid || pid_exclusion_predicate(_pid); };
	return player_nearest_position( state, 
                    get_position( pid, state ),
					pid_exclusion_fn );
}

/*!
 * \brief Determine player id nearest to another player id
 * \tparam State state model
 * \param state state object
 * \param pid reference player id descriptor
 */
template < typename State >
typename player_id_type< State >::type
player_nearest_player( State const& state,
                       const typename player_id_type< State >::type& pid )
{
        auto pid_exclusion_fn = [] ( const typename player_id_type< State >::type ) { return false; };
        return player_nearest_position( state,
                                        get_position( pid, state ),
                                        pid_exclusion_fn );
}


/*!
  \brief Data structure used for the response of a nearest player query
*/
template < typename State >
struct nearest_player_functor {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
	
    nearest_player_functor()
        : M_player_id( get_null_value< player_id_t >() )
    {

    }

    player_id_t M_player_id;
    //typename state_traits< State >::distance_t m_distance;
};

/*!
  \brief Determine the player nearest another player
  \param player the reference player
  \param state the state to consider
  \returns A nearest player data structure containing the details of the player found
*/
template < typename State >
typename player_id_type< State >::type
nearest_player( typename player_id_type< State >::type player_id,
                State const& state )
{
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename State::adjacent_players adjacent_player_iterator;

    // Get adjacent players => use adjacent players information
    std::pair< adjacent_player_iterator,
               adjacent_player_iterator > adjacent_players = adjacent_players( player_id, state );

    adjacent_player_iterator best_result = detail::find_best_match( adjacent_players.first,
                                                                    adjacent_players.second,
                                                                    distance_to_player( player_id, state ) );
    if ( best_result == adjacent_players.second )
    {
        return get_null_value< player_id_t >();
    }
    return *best_result;
}

/*!
  \brief Determine the identifier of the player nearest a given position
  \abstract Given a position descriptor, determine the player nearest to that position
  */
template < typename Position,
           typename State >
typename player_id_type< State >::type
nearest_player_from_position( Position const& position,
                              State const& state )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;

    // Get adjacent players
    player_iterator_t _player_begin, _player_end;
    boost::tie( _player_begin, _player_end ) = get_players( state );

    player_iterator_t best_result = detail::find_best_match( _player_begin,
                                                           _player_end,
                                                           PlayerIdDistanceToPositionPredicate<State, Position>( position, state ) );
    assert ( best_result != _player_end );

    return *best_result;
}

/*!
  \brief Determine the player nearest a given position from a particular side
  \param position the reference position
  \param side the player team side
  \param state the state to consider
  \returns A nearest player data structure containing the details of the player found
*/
template < typename Position,
           typename State>
typename player_id_type< State >::type
nearest_player_from_position( Position const& position,
                              typename side_type< typename player_type< State >::type >::type const& side,
                              State const& state )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< Position > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    // Get adjacent players
    auto _team_players_iterators = team_players( side, state );

    auto best_result = detail::find_best_match( _team_players_iterators.first,
                                                _team_players_iterators.second,
                                                PlayerIdDistanceToPositionPredicate<State,Position>( position, state ) );

    assert( best_result != _team_players_iterators.second );

    return *best_result;
}



/*!
 * \brief Determine the left team's goal post position nearest to a position
 */
template < typename Position,
           typename Pitch >
Position
get_left_team_near_goal_post( Position const& pos,
                              Pitch const& pitch)
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    auto _left_goal_upper_post = get_left_goal_upper_post< Position >( pitch );
    auto _left_goal_lower_post = get_left_goal_lower_post< Position >( pitch );
    auto left_goal_upper_post_dist = distance( _left_goal_upper_post, pos );
    auto left_goal_lower_post_dist = distance( _left_goal_lower_post, pos );
    return left_goal_upper_post_dist < left_goal_lower_post_dist
            ? _left_goal_upper_post
            : _left_goal_lower_post;
}

/*!
 * \brief Determine the right team's goal post position nearest to a position
 */
template < typename Position,
           typename Pitch >
Position
get_right_team_near_goal_post( Position const& pos,
                               Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    auto _right_goal_upper_post = get_right_goal_upper_post< Position >( pitch );
    auto _right_goal_lower_post = get_right_goal_lower_post< Position >( pitch );
    auto right_goal_upper_post_dist = distance( _right_goal_upper_post, pos );
    auto right_goal_lower_post_dist = distance( _right_goal_lower_post, pos );
    return right_goal_upper_post_dist < right_goal_lower_post_dist
            ? _right_goal_upper_post
            : _right_goal_lower_post;
}

/*!
 * \brief Determine the team's goal post nearest to a position
 */
template < typename State,
           typename Position >
typename position_type< State >::type
get_team_near_goal_post( typename side_type< typename player_type< State >::type >::type const& side,
                         Position const& pos )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename side_type< typename player_type< State >::type >::type side_t;
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));

    return side == get_left_side< side_t >()
            ? get_left_team_near_goal_post< State >( pos )
            : get_right_team_near_goal_post< State >( pos );
}

/*
 *
 */

/*!
 \brief Constrain a position to the soccer field
 */
template < typename Pitch >
struct constrain_position_to_field
{
private:
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    typedef typename coordinate_type< Pitch >::type field_coordinate_t;

    Pitch const* M_pitch;
    field_coordinate_t M_pitch_left_offset;
    field_coordinate_t M_pitch_right_offset;
    field_coordinate_t M_pitch_top_offset;
    field_coordinate_t M_pitch_bottom_offset;

public:
    constrain_position_to_field( Pitch const& pitch )
        : M_pitch( &pitch )
        , M_pitch_left_offset( 0 )
        , M_pitch_right_offset( 0 )
        , M_pitch_top_offset( 0 )
        , M_pitch_bottom_offset( 0 )
    {

    }

    constrain_position_to_field( Pitch const& pitch,
                                 const field_coordinate_t offset )
        : M_pitch( &pitch )
        , M_pitch_left_offset( offset )
        , M_pitch_right_offset( offset )
        , M_pitch_top_offset( offset )
        , M_pitch_bottom_offset( offset )
    {
        assert( offset >= 0 );
    }

    constrain_position_to_field( Pitch const& pitch,
                                 const field_coordinate_t horizontal_offset,
                                 const field_coordinate_t vertical_offset)
        : M_pitch( &pitch )
        , M_pitch_left_offset( horizontal_offset )
        , M_pitch_right_offset( horizontal_offset )
        , M_pitch_top_offset( vertical_offset )
        , M_pitch_bottom_offset( vertical_offset )
    {
        assert( horizontal_offset >= 0 );
        assert( vertical_offset >= 0 );
    }

    constrain_position_to_field( Pitch const& pitch,
                                 const field_coordinate_t left_offset,
                                 const field_coordinate_t right_offset,
                                 const field_coordinate_t top_offset,
                                 const field_coordinate_t bottom_offset)
        : M_pitch( &pitch )
        , M_pitch_left_offset( left_offset )
        , M_pitch_right_offset( right_offset )
        , M_pitch_top_offset( top_offset )
        , M_pitch_bottom_offset( bottom_offset )
    {
        assert( left_offset >= 0 );
        assert( right_offset >= 0 );
        assert( top_offset >= 0 );
        assert( bottom_offset >= 0 );
    }

    template <typename Position>
    void operator()( Position& position )
    {
        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<Position> ));

        // static field variables
        const auto pitch_left = get_left_goal_line( *M_pitch );
        const auto pitch_right = get_right_goal_line( *M_pitch );
        const auto pitch_top = get_top_sideline( *M_pitch );
        const auto pitch_bottom = get_bottom_sideline( *M_pitch );

        if ( get_x( position ) < pitch_left )
        {
            set_x( pitch_left + M_pitch_left_offset, position );
        }
        else if( get_x( position ) > pitch_right )
        {
            set_x( pitch_right - M_pitch_right_offset, position);
        }

        if ( get_y( position ) < pitch_top )
        {
            set_y( pitch_top + M_pitch_top_offset, position);
        }
        else if ( get_y( position ) > pitch_bottom )
        {
            set_y( pitch_bottom - M_pitch_bottom_offset, position);
        }
    }
};

template < typename Position,
           typename Pitch >
void constrain_position_to_pitch( Position& position,
                                  Pitch const& pitch,
                                  typename coordinate_type< Pitch >::type left_offset = 0,
                                  typename coordinate_type< Pitch >::type right_offset  = 0,
                                  typename coordinate_type< Pitch >::type top_offset = 0,
                                  typename coordinate_type< Pitch >::type bottom_offset = 0 )
{
    BOOST_CONCEPT_ASSERT(( MutablePositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    // static field variables
    const auto pitch_left = get_left_goal_line( pitch );
    const auto pitch_right = get_right_goal_line( pitch );
    const auto pitch_top = get_top_sideline( pitch );
    const auto pitch_bottom = get_bottom_sideline( pitch );

    if ( get_x( position ) < pitch_left )
    {
        set_x( pitch_left + left_offset, position );
    }
    else if( get_x( position ) > pitch_right )
    {
        set_x( pitch_right - right_offset, position);
    }

    if ( get_y( position ) < pitch_top )
    {
        set_y( pitch_top + top_offset, position);
    }
    else if ( get_y( position ) > pitch_bottom )
    {
        set_y( pitch_bottom - bottom_offset, position);
    }
}

/*!
 \brief Constrain a position behind the team offside line
 */
template < typename Pitch,
           typename State,
           typename CoordinateType = float >
struct constrain_position_behind_offside_line
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename side_type< typename player_type< State >::type >::type side_t;

    side_t M_team_side;
    Pitch const* M_pitch;
    State const* M_state;
    CoordinateType M_offside_offset;
public:
    constrain_position_behind_offside_line( const side_t team_side,
                                            Pitch const& pitch,
                                            State const& state )
    : M_team_side( team_side )
    , M_pitch( &pitch )
    , M_state( &state )
    , M_offside_offset( 0 )
    {

    }

    constrain_position_behind_offside_line( const side_t team_side,
                                            Pitch const& pitch,
                                            State const& state,
                                            const CoordinateType offside_offset )
        : M_team_side( team_side )
        , M_pitch( &pitch )
        , M_state( &state )
        , M_offside_offset( offside_offset )
    {
        // runtime assertion
        assert( offside_offset >= 0 );
    }

    template < typename Position >
    void operator()( Position& position )
    {
        BOOST_CONCEPT_ASSERT(( MutablePositionConcept<Position> ));

        assert( M_pitch != NULL && M_state != NULL );

        // inneficient => could be initialized on constructor once but could not be safely reused in different times of the game
        const auto _offside_line = get_offside_line( M_team_side, *M_pitch, *M_state );

        if ( M_team_side == get_left_side< side_t >()
             && get_x( position ) > _offside_line )
        {
            set_x( _offside_line - M_offside_offset, position );
        }
        else if ( M_team_side == get_right_side< side_t >() &&
                  get_x( position ) < _offside_line )
        {
            set_x( _offside_line + M_offside_offset, position );
        }
    }
};


/**************/
/* Congestion */
/**************/

/*!
    \brief Calculate the congestion effect a player has on another
*/
template < typename Position,
           typename State >
double
player_congestion_from_position( const typename player_id_type< State >::type player_id,
                                 Position const& position,
                                 State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));

    double result = 1.0 / distance( get_position( player_id, state ), position );
    return result;
}

/*!
    \brief Calculate the congestion effect a player has on another
*/
template < typename State >
double
inter_player_congestion( const typename player_id_type< State >::type player_id1,
                         const typename player_id_type< State >::type player_id2,
                         State& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    return player_congestion_from_position( player_id1,
                                            get_position( player_id2, state ),
                                            state );
}

/*!
 * \brief Calculate the congestion of a position based on a set of players
 */
template <typename PlayerIdInputIterator,
          typename Position,
          typename State>
double get_position_congestion_from_players( PlayerIdInputIterator first,
                                             const PlayerIdInputIterator last,
                                             State const& state,
                                             Position const& target_pos )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerIdInputIterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value<PlayerIdInputIterator>::type,
                                              typename player_id_type< State >::type >::value ),
                            "Player id types from State and PlayerId mismatch");

    double result = 0.0;

    for ( ; first != last; first++ )
    {
        result += player_congestion_from_position( *first, target_pos, state );
    }

    return result;
}


/*!
 * \brief get player descriptor reference from state
 */
template < typename State >
struct get_player_t
    : public std::unary_function< typename player_id_type< State >::type,
                                  typename player_type< State >::type >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;

    const State* M_state;

    get_player_t( State const& state )
        : M_state( &state )
    {

    }

    const player_t&
    operator()( const player_id_t pid )
    {
        return get_players( pid, *M_state );
    }
};

/*!
 * \brief Calculate the congestion of a player from other players
 */
template <typename PlayerIdInputIterator,
          typename State>
double player_congestion_from_players( PlayerIdInputIterator first,
                                       const PlayerIdInputIterator last,
                                       State const& state,
                                       const typename player_id_type< State >::type target_player_id )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerIdInputIterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value<PlayerIdInputIterator>::type,
                                              typename player_id_type< State >::type >::value ),
                            "Player id types from State and PlayerId mismatch");

    return position_congestion_from_players( first,
                                             last,
                                             state,
                                             get_position( target_player_id, state ) );
}

/*!
  \brief Least congested team player id from a set of player identifiers
  */
template < typename PlayerIdInputIterator,
           typename State >
typename player_id_type< State >::type
least_congested_player( PlayerIdInputIterator first,
                        const PlayerIdInputIterator last,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<PlayerIdInputIterator> ));
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::iterator_value<PlayerIdInputIterator>::type,
                                              typename player_id_type< State >::type>::value ),
                            " Player id types mismatch in State and PlayerIdInputIterator");

    double least_congestion = std::numeric_limits<double>::max();
    auto least_congested_player = last;

    for ( auto itp = first; itp != last ; itp++ )
    {
        double current_congestion = position_congestion_from_players( first,
                                                                      last,
                                                                      state,
                                                                      get_position( *itp, state ) );
        if ( current_congestion < least_congestion )
        {
            least_congested_player = itp;
            least_congestion = current_congestion;
        }
    }
    return least_congested_player ;
}

/*!
  \brief Least congested team player id from a given side
  */
template < typename Side,
           typename State >
typename player_id_type< State >::type
least_congested_player( const Side side,
                        State const& state )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< Side, typename side_type< typename player_type< State >::type >::type >::value ),
                            "Side descriptor types mismatch in State and Side");

    typedef typename player_iterator_type< State >::type player_iterator_t;

    auto team_players_iterators = team_players( side, state );

    return least_congested_player( team_players_iterators.first,
                                   team_players_iterators.last,
                                   state );
}


/*!
 * \brief The player_range_property_map_generator struct
 */
struct player_range_property_map_generator
{

    player_range_property_map_generator()
    {

    }

    template <typename PlayerInputIterator,
              typename PlayerStateFunctor,
              typename State,
              typename PlayerPropertyMap>
    void operator() ( PlayerInputIterator first,
                      const PlayerInputIterator last,
                      const PlayerStateFunctor player_state_binary_functor,
                      State const& state,
                      PlayerPropertyMap pmap ) const
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        typedef typename player_id_type< State >::type state_player_id_t;

        BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< PlayerPropertyMap, state_player_id_t > ));
        BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< PlayerInputIterator > ));
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::detail::iterator_traits< PlayerInputIterator >::value_type,
                                              state_player_id_t >::value ),
                                "PlayerInputIterator and State player id types mismatch");

        typedef boost::binary_traits< PlayerStateFunctor > player_binary_functor_traits_t;
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< PlayerPropertyMap >::value_type,
                                                  typename player_binary_functor_traits_t::result_type >::value ),
                                "Property value types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_binary_functor_traits_t::first_argument_type,
                                                  state_player_id_t >::value ),
                                "PlayerStateFunctor and State player id types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_binary_functor_traits_t::second_argument_type,
                                                  State >::value ),
                                "State types mismatch");

        for ( first ; first != last ; first++ )
        {
            boost::put( pmap,
                        *first,
                        player_state_binary_functor( *first, state ) );
        }
    }
};

/*!
 * \brief state player property map generator
 */
struct all_player_property_map_generator {

    all_player_property_map_generator()
    {

    }

    template <typename PlayerStateFunctor,
              typename State,
              typename PlayerPropertyMap>
    void operator() ( const PlayerStateFunctor player_state_binary_functor,
                      State const& state,
                      PlayerPropertyMap pmap ) const
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
        typedef typename player_id_type< State >::type state_player_id_t;
        typedef typename player_iterator_type< State >::type player_iterator_t;
        BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept< PlayerPropertyMap, state_player_id_t > ));

        typedef boost::binary_traits< PlayerStateFunctor > player_functor_traits_t;
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_functor_traits_t::first_argument_type,
                                              state_player_id_t >::value ),
                                "Player id types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename player_functor_traits_t::second_argument_type,
                                              State >::value ),
                                "State types mismatch");
        BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< PlayerPropertyMap >::value_type,
                                              typename player_functor_traits_t::result_type >::value ),
                                "Property map value types mismatch");


        player_iterator_t _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( state );
        player_range_property_map_generator pmap_range_generator;
        pmap_range_generator( _player_begin,
                              _player_end,
                              player_state_binary_functor,
                              state,
                              pmap );
    }
};


/*!
 * \brief The partition_players_range struct
 */
struct partition_players_range {

    template <typename PlayerInputIterator,
              typename State>
    void operator()( PlayerInputIterator first,
                     const PlayerInputIterator last,
                     State const& state )
    {

    }
};

/*!
 * \brief The partition_players struct
 */
struct partition_players
{

    template < typename State >
    void operator()( State const& state )
    {
        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    }
};


// State and Game algorithms
/*!
 * \brief check if the game is in the first normal half
 */
template < typename State,
           typename Game >
bool match_in_first_normal_half( State const& _state,
                                 Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    return time( _state ) >= match_start_time( _game ) &&
           time( _state ) <= match_start_time( _game) + normal_half_time_duration( _game );
}

/*!
 * \brief check if the game is in the second normal half
 */
template < typename State,
           typename Game >
bool match_in_second_normal_half( State const& _state,
                                  Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto first_normal_half_end_time = match_start_time( _game ) + normal_half_time_duration( _game );
    const auto normal_full_time = first_normal_half_end_time + normal_half_time_duration( _game );

    return time( _state ) > first_normal_half_end_time &&
           time( _state ) <= normal_full_time;
}

/*!
 * \brief check if the game is in the normal time
 */
template < typename State,
           typename Game >
bool match_in_normal_time( State const& _state,
                           Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto normal_full_time = match_start_time( _game ) + normal_half_time_duration( _game ) * 2;

    return time( _state ) >= match_start_time( _game ) &&
           time( _state ) <= normal_full_time;
}

/*!
 * \brief check if the game is in the first extra half
 */
template < typename State,
           typename Game >
bool match_in_first_extra_half( State const& _state,
                                Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto match_extra_time_start = match_start_time( _game ) + normal_half_time_duration( _game ) * 2;
    const auto extra_time_first_half_end_time = match_extra_time_start + extra_half_time_duration( _game );

    return time( _state ) > match_extra_time_start &&
           time( _state ) <= extra_time_first_half_end_time;
}

/*!
 * \brief check if the game is in the second extra half
 */
template < typename State,
           typename Game >
bool match_in_second_extra_half( State const& _state,
                                 Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto extra_time_first_half_end_time = match_start_time( _game ) + normal_half_time_duration( _game ) * 2 + extra_half_time_duration( _game );
    const auto extra_time_second_half_end_time = extra_time_first_half_end_time + extra_half_time_duration( _game );

    return time( _state ) > extra_time_first_half_end_time &&
           time( _state ) <= extra_time_second_half_end_time;
}

/*!
 * \brief check if the game is in the normal time
 */
template < typename State,
           typename Game >
bool match_in_extra_time( State const& _state,
                          Game const& _game )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    const auto extra_time_start = match_start_time( _game ) + normal_half_time_duration( _game ) * 2;
    const auto extra_time_end = extra_time_start + extra_half_time_duration( _game ) * 2;

    return time( _state ) > extra_time_start( _game ) &&
           time( _state ) <= extra_time_end;
}

// Useful functors

/*!
 * \struct position_within_pitch_pred
 */
template < typename Position,
           typename Game >
struct position_within_pitch_pred
    : public std::binary_function< Position, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( PositionConcept<Position> ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    position_within_pitch_pred() { }

    bool operator()( Position const& pos, Game const& _game )
    {
        return is_within_pitch( pos, _game );
    }
};

/*!
 * \struct match_in_first_normal_half_pred
 */
template < typename State,
           typename Game >
struct match_in_first_normal_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_first_normal_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_first_normal_half( _state, _game );
    }
};

/*!
 * \struct match_in_second_normal_half_pred
 */
template < typename State,
           typename Game >
struct match_in_second_normal_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_second_normal_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_second_normal_half( _state, _game );
    }
};

/*!
 * \struct match_in_first_extra_half_pred
 */
template < typename State,
           typename Game >
struct match_in_first_extra_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_first_extra_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_first_extra_half( _state, _game );
    }
};

/*!
 * \struct match_in_second_extra_half_pred
 */
template < typename State,
           typename Game >
struct match_in_second_extra_half_pred
    : public std::binary_function< State, Game, bool>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( GameConcept<Game> ));

    match_in_second_extra_half_pred() { }

    bool operator()( State const& _state,
                     Game const& _game )
    {
        return match_in_second_extra_half( _state, _game );
    }
};

} // end namespace soccer

#endif // SOCCER_STATE_ALGORITHMS_HPP
