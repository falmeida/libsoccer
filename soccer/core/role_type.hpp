#ifndef SOCCER_CORE_ROLE_TYPE_HPP
#define SOCCER_CORE_ROLE_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the player contained in a state
\ingroup traits
\par Object:
    - all objects that represent a soccer state
\par Specializations should provide:
    - typedef P type (where P should fulfill the Object concept)
\tparam Object player_object
*/
template < typename Object >
struct role_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>)
        );
};


} // end namespace traits


/*!
\brief \brief_meta{type, object_type, \meta_object_type}
\tparam Object \tparam_object_object
\ingroup core

\qbk{[include reference/core/role_type.qbk]}
*/
template < typename Object >
struct role_type
{
    typedef typename traits::role_type
        <
            typename soccer::util::bare_type<Object>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_ROLE_TYPE_HPP
