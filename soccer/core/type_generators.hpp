#ifndef SOCCER_TYPE_GENERATORS_HPP
#define SOCCER_TYPE_GENERATORS_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/core/access.hpp>

//! Import concepts to be checked
#include <soccer/concepts/state_concepts.hpp>
#include <soccer/concepts/strategy_concepts.hpp>
#include <soccer/concepts/action_concepts.hpp>

#include <functional>


SOCCER_NAMESPACE_BEGIN


/*!
 * \brief Generate the type traits of a tactic selector
 * \tparam State Model of a soccer state
 * \tparam Strategy Model of a soccer strategy
 */
template < typename State, typename Strategy >
struct tactic_selector_generator {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( StrategyConcept< Strategy > ));

    typedef std::function< typename tactic_id_type< Strategy >::type( State const&, Strategy const& ) > type;
};

/*!
 * \brief Generate the type traits of a formation selector
 * \tparam State Model of a soccer state
 * \tparam Strategy Model of a soccer strategy
 */
template < typename State, typename Strategy >
struct formation_selector_generator {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( StrategyConcept< Strategy > ));

    typedef std::function< typename formation_id_type< Strategy >::type( State const&, Strategy const& ) > type;
};

/*!
 * \brief Generate the type traits of a flux selector
 * \tparam State Model of a soccer state
 * \tparam Strategy Model of a soccer strategy
 */
template < typename State, typename Strategy >
struct flux_selector_generator {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( StrategyConcept< Strategy > ));

    typedef std::function< typename flux_id_type< Strategy >::type( State const&, Strategy const& ) > type;
};

/*!
 * \brief Generate the type traits of an action generator
 * \tparam State Model of a soccer state
 * \tparam Action Model of a soccer action
 */
template < typename State, typename Action >
struct action_generator {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef std::function< Action*( State const& ) > type;
};

/*!
 * \brief Generate the type traits of a state generator
 * \tparam State Model of a soccer state
 * \tparam Action Model of a soccer action
 */
template < typename State, typename Action >
struct state_generator {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( ActionConcept< Action > ));

    typedef std::function< State*( State const&, Action const& ) > type;
};


/*!
 * \brief Generate a player iterator that skips player ids using a unary predicate
 * \tparam State state model
 */
template < typename State >
struct player_filter_iterator_generator
{
    typedef boost::filter_iterator< std::function< bool( typename player_id_type< State >::type ) >,
                                    typename player_iterator_type< State >::type > type;
};


SOCCER_NAMESPACE_END

#endif // SOCCER_TYPE_GENERATORS_HPP
