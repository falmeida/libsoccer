#ifndef SOCCER_CORE_MATCH_SITUATION_TYPE_HPP
#define SOCCER_CORE_MATCH_SITUATION_TYPE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>


SOCCER_TRAITS_NAMESPACE_BEGIN

/*!
\brief Traits class which indicate the match situation type of a playmode model
\ingroup traits
*/
template <typename PlayMode, typename Enable = void>
struct match_situation_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<PlayMode>)
        );
};

SOCCER_TRAITS_NAMESPACE_END



SOCCER_NAMESPACE_BEGIN

/*!
\brief \brief_meta{type, decay type, \meta_match_situation_type}
\tparam PlayMode \tparam_playmode
\ingroup core

\qbk{[include reference/core/match_situation_type.qbk]}
*/
template <typename PlayMode>
struct match_situation_type
{
    typedef typename traits::match_situation_type
                <
                    // typename tag<VectorOrPoint>::type,
                    typename soccer::util::bare_type<PlayMode>::type
                >::type type;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_CORE_MATCH_SITUATION_TYPE_HPP
