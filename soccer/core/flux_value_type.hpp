#ifndef SOCCER_CORE_FLUX_VALUE_TYPE_HPP
#define SOCCER_CORE_FLUX_VALUE_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the flux id contained in a strategy
\ingroup traits
\par Flux:
    - all objects that represent a soccer strategy
\par Specializations should provide:
    - typedef P type (where P should fulfill the Flux concept)
\tparam Flux player_object
*/
template < typename Flux >
struct flux_value_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Flux>)
        );
};


} // end namespace traits


/*!
\brief \brief_meta{type, strategy_type, \meta_strategy_type}
\tparam Flux \tparam_strategy_object
\ingroup core

\qbk{[include reference/core/flux_value_type.qbk]}
*/
template < typename Flux >
struct flux_value_type
{
    typedef typename traits::flux_value_type
        <
            typename soccer::util::bare_type<Flux>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_FLUX_VALUE_TYPE_HPP
