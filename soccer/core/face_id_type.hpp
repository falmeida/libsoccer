#ifndef SOCCER_CORE_FACE_ID_TYPE_HPP
#define SOCCER_CORE_FACE_ID_TYPE_HPP

//! Import useful macross
#include <soccer/defines.h>

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

/*!
\brief Traits class indicating the type of the vertex id contained in a geometry
\ingroup traits
\par Geometry:
    - all objects that represent a soccer geometry
\par Specializations should provide:
    - typedef P type (where P should fulfill the Geometry concept)
\tparam Geometry player_object
*/
template < typename Geometry >
struct face_id_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Geometry>)
        );
};


SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*!
\brief \brief_meta{type, geometry_type, \meta_geometry_type}
\tparam Geometry \tparam_geometry_object
\ingroup core

\qbk{[include reference/core/face_id_type.qbk]}
*/
template < typename Geometry >
struct face_id_type
{
    typedef typename traits::face_id_type
        <
            typename soccer::util::bare_type<Geometry>::type
        >::type type;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_CORE_FACE_ID_TYPE_HPP
