#ifndef SOCCER_CORE_LEFT_STRATEGY_TYPE_HPP
#define SOCCER_CORE_LEFT_STRATEGY_TYPE_HPP

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class which indicates the type of strategy being used by the left team
\ingroup traits
*/
template <typename State, typename Enable = void>
struct left_strategy_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>)
        );
};

} // end namespace traits

/*!
\brief \brief_meta{type, strategy type, \meta_state_type}
\tparam State \tparam_state
\ingroup core

\qbk{[include reference/core/strategy_type.qbk]}
*/
template <typename State>
struct left_strategy_type
{
    typedef typename traits::left_strategy_type
                <
                    typename soccer::util::bare_type<State>::type
                >::type type;
};

} // end namespace soccer

#endif // SOCCER_CORE_LEFT_STRATEGY_TYPE_HPP
