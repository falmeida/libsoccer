#ifndef SOCCER_CORE_FACE_CIRCULATOR_TYPE_HPP
#define SOCCER_CORE_FACE_CIRCULATOR_TYPE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>


SOCCER_TRAITS_NAMESPACE_BEGIN

/*!
\brief Traits class indicating the type of the face contained in a geometry
\ingroup traits
\par Geometry:
    - all objects that represent a soccer geometry
\tparam Geometry geometry_object
*/
template < typename Geometry >
struct face_circulator_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Geometry>)
        );
};


SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*!
\brief \brief_meta{type, geometry_type, \meta_geometry_type}
\tparam Geometry \tparam_geometry_object
\ingroup core

\qbk{[include reference/core/face_circulator_type.qbk]}
*/
template < typename Geometry >
struct face_circulator_type
{
    typedef typename traits::face_circulator_type
        <
            typename soccer::util::bare_type<Geometry>::type
        >::type type;
};

SOCCER_NAMESPACE_END


#endif // SOCCER_CORE_FACE_CIRCULATOR_TYPE_HPP
