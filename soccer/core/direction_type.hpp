#ifndef SOCCER_CORE_DIRECTION_TYPE_HPP
#define SOCCER_CORE_DIRECTION_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {


/*!
\brief Traits class indicating the type of the object contained position
\ingroup traits
\par Objects:
    - all objects that have a position
\par Specializations should provide:
    - typedef P type (where P should fulfill the StaticObject concept)
\tparam StaticObject static_object
*/
template < typename StaticObject >
struct direction_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StaticObject>)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, static_object_type, \meta_static_object_type}
\tparam StaticObject \tparam_static_object
\ingroup core

\qbk{[include reference/core/position_type.qbk]}
*/
template < typename StaticObject >
struct direction_type
{
    typedef typename traits::direction_type
        <
            typename soccer::util::bare_type<StaticObject>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_DIRECTION_TYPE_HPP
