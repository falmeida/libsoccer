#ifndef SOCCER_CORE_TEAMMATE_ITERATOR_TYPE_HPP
#define SOCCER_CORE_TEAMMATE_ITERATOR_TYPE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

/*!
\brief Traits class indicating the type of the player contained in a state
\ingroup traits
\par State:
    - all objects that represent a soccer state
\par Specializations should provide:
    - typedef P type (where P should fulfill the State concept)
\tparam State player_object
*/
template < typename State >
struct teammate_iterator_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>)
        );
};


SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*!
\brief \brief_meta{type, state_type, \meta_state_type}
\tparam State \tparam_state_object
\ingroup core

\qbk{[include reference/core/teammate_iterator_type.qbk]}
*/
template < typename State >
struct teammate_iterator_type
{
    typedef typename traits::teammate_iterator_type
        <
            typename soccer::util::bare_type<State>::type
        >::type type;
};

SOCCER_NAMESPACE_END


#endif // SOCCER_CORE_TEAMMATE_ITERATOR_TYPE_HPP
