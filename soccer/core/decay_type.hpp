#ifndef SOCCER_DECAY_TYPE_HPP
#define SOCCER_DECAY_TYPE_HPP

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class which indicate the decay type (double,float,...) of an object (motion) model
\ingroup traits
*/
template <typename Model, typename Enable = void>
struct decay_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Model>)
        );
};

} // end namespace traits


namespace core_dispatch {


}

/*!
\brief \brief_meta{type, decay type, \meta_decay_type}
\tparam Model \tparam_model
\ingroup core

\qbk{[include reference/core/decay_type.qbk]}
*/
template <typename Model>
struct decay_type
{
    typedef typename traits::decay_type
                <
                    // typename tag<VectorOrPoint>::type,
                    typename soccer::util::bare_type<Model>::type
                >::type type;
};

} // end namespace soccer

#endif // SOCCER_DECAY_TYPE_HPP
