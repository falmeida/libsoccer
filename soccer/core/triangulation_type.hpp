#ifndef SOCCER_CORE_TRIANGULATION_TYPE_HPP
#define SOCCER_CORE_TRIANGULATION_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {


/*!
\brief Traits class indicating the type of the object contained triangulation
\ingroup traits
\par Objects:
    - all objects that have a triangulation
\par Specializations should provide:
    - typedef P type (where P should fulfill the Object concept)
\tparam Object object
*/
template < typename Object >
struct triangulation_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, object_type, \meta_object_type}
\tparam Object \tparam_object
\ingroup core

\qbk{[include reference/core/triangulation_type.qbk]}
*/
template < typename Object >
struct triangulation_type
{
    typedef typename traits::triangulation_type
        <
            typename soccer::util::bare_type<Object>::type
        >::type type;
};


} // end namespace soccer


#endif // SOCCER_CORE_TRIANGULATION_TYPE_HPP
