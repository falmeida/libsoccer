#ifndef SOCCER_CORE_VELOCITY_TYPE_HPP
#define SOCCER_CORE_VELOCITY_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {


/*!
\brief Traits class indicating the type of the object contained velocity
\ingroup traits
\par Objects:
    - all objects that have a velocity
\par Specializations should provide:
    - typedef P type (where P should fulfill the DynamicObject concept)
\tparam DynamicObject dynamic_object
*/
template < typename DynamicObject >
struct velocity_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types< DynamicObject >)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, dynamic_object_type, \meta_dynamic_object_type}
\tparam DynamicObject \tparam_dynamic_object
\ingroup core

\qbk{[include reference/core/velocity_type.qbk]}
*/
template < typename DynamicObject >
struct velocity_type
{
    typedef typename traits::velocity_type
        <
            typename soccer::util::bare_type<DynamicObject>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_VELOCITY_TYPE_HPP
