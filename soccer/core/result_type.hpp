#ifndef SOCCER_CORE_RESULT_TYPE_HPP
#define RESULT_TYPE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

/*!
\brief Traits class indicating the type used to represent speed
\ingroup traits
\tparam Object object
*/
template < typename Object >
struct result_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types< Object >)
        );
};


SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*!
\brief \brief_meta{type, object_type, \meta_object_type}
\tparam Object \tparam_object
\ingroup core

\qbk{[include reference/core/result_type.qbk]}
*/
template < typename Object >
struct result_type
{
    typedef typename traits::result_type
        <
            typename soccer::util::bare_type<Object>::type
        >::type type;
};


SOCCER_NAMESPACE_END

#endif // SOCCER_CORE_RESULT_TYPE_HPP
