#ifndef SOCCER_CORE_MATRIX_SIZE_TYPE_HPP
#define SOCCER_CORE_MATRIX_SIZE_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the number contained in a matrix
\ingroup traits
\par Matrix:
    - all objects that represent a matrix
\par Specializations should provide:
    - typedef P type (where P should fulfill the Matrix concept)
\tparam Matrix matrix_object
*/
template < typename Matrix >
struct matrix_size_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Matrix>)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, matrix_type, \meta_matrix_type}
\tparam Matrix \tparam_matrix_object
\ingroup core

\qbk{[include reference/core/matrix_size_type.qbk]}
*/
template < typename Matrix >
struct matrix_size_type
{
    typedef typename traits::matrix_size_type
        <
            typename soccer::util::bare_type<Matrix>::type
        >::type type;
};


} // end namespace soccer


#endif // SOCCER_CORE_MATRIX_SIZE_TYPE_HPP
