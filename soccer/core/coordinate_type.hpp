#ifndef SOCCER_CORE_COORDINATE_TYPE_HPP
#define SOCCER_CORE_COORDINATE_TYPE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

/*!
\brief Traits class which indicate the coordinate type (double,float,...) of a point
\ingroup traits
\par Geometries:
    - point
\par Specializations should provide:
    - typedef T type; (double,float,int,etc)
*/
template <typename VectorOrPoint, typename Enable = void>
struct coordinate_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<VectorOrPoint>)
        );
};

SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*!
\brief \brief_meta{type, coordinate type (int\, float\, double\, etc), \meta_point_type}
\tparam Point \tparam_geometry
\ingroup core

\qbk{[include reference/core/coordinate_type.qbk]}
*/
template <typename VectorOrPoint>
struct coordinate_type
{
    typedef typename traits::coordinate_type
                <
                    // typename tag<VectorOrPoint>::type,
                    typename soccer::util::bare_type<VectorOrPoint>::type
                >::type type;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_CORE_COORDINATE_TYPE_HPP
