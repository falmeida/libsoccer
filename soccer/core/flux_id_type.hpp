#ifndef SOCCER_CORE_FLUX_ID_TYPE_HPP
#define SOCCER_CORE_FLUX_ID_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the flux id contained in a strategy
\ingroup traits
\par Strategy:
    - all objects that represent a soccer strategy
\par Specializations should provide:
    - typedef P type (where P should fulfill the Strategy concept)
\tparam Strategy player_object
*/
template < typename Strategy >
struct flux_id_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Strategy>)
        );
};


} // end namespace traits


/*!
\brief \brief_meta{type, strategy_type, \meta_strategy_type}
\tparam Strategy \tparam_strategy_object
\ingroup core

\qbk{[include reference/core/flux_id_type.qbk]}
*/
template < typename Strategy >
struct flux_id_type
{
    typedef typename traits::flux_id_type
        <
            typename soccer::util::bare_type<Strategy>::type
        >::type type;
};

} // end namespace soccer

#endif // SOCCER_CORE_FLUX_ID_TYPE_HPP
