#ifndef SOCCER_CORE_TAG_HPP
#define SOCCER_CORE_TAG_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/util/bare_type.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

/*!
\brief Traits class to attach a tag to a soccer object
\details All soccer objects should implement a traits::tag<G>::type metafunction to indicate their own type.
\ingroup traits
\par SoccerObjects:
    - all soccer objects
\par Specializations should provide:
    - typedef XXX_tag type; (player_tag, ball_tag, ...)
\tparam Object object
*/
template < typename Object, typename Enable = void >
struct tag
{
    typedef void type;
};

SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*!
\brief \brief_meta{type, tag, \meta_object_type}
\details With soccer objects, tags are the driving force of the tag dispatching mechanism.
The tag metafunction is therefore used in every free function.
\tparam Object \tparam_object
\ingroup core

\qbk{[include reference/core/tag.qbk]}
*/
template < typename Object >
struct tag
{

    typedef typename traits::tag
        <
            typename soccer::util::bare_type<Object>::type
        >::type type;
};


SOCCER_NAMESPACE_END


#endif // SOCCER_CORE_TAG_HPP
