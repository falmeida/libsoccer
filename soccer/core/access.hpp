#ifndef SOCCER_CORE_ACCESS_HPP
#define SOCCER_CORE_ACCESS_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/core/coordinate_type.hpp>
#include <soccer/core/length_type.hpp>
#include <soccer/core/speed_type.hpp>
#include <soccer/core/acceleration_type.hpp>
#include <soccer/core/size_type.hpp>

#include <soccer/core/position_type.hpp>
#include <soccer/core/direction_type.hpp>
#include <soccer/core/velocity_type.hpp>

#include <soccer/core/number_type.hpp>
#include <soccer/core/side_type.hpp>

#include <soccer/core/player_id_type.hpp>
#include <soccer/core/player_type.hpp>
#include <soccer/core/player_iterator_type.hpp>
#include <soccer/core/ball_type.hpp>
#include <soccer/core/playmode_type.hpp>
#include <soccer/core/time_type.hpp>
#include <soccer/core/score_type.hpp>
#include <soccer/core/team_type.hpp>

#include <soccer/core/duration_type.hpp>

#include <soccer/core/decay_type.hpp>
#include <soccer/core/effort_type.hpp>
#include <soccer/core/stamina_type.hpp>
#include <soccer/core/power_type.hpp>

#include <soccer/core/percentage_type.hpp>

#include <soccer/core/matrix_size_type.hpp>
#include <soccer/core/key_type.hpp>
#include <soccer/core/value_type.hpp>

#include <soccer/core/role_type.hpp>
#include <soccer/core/role_id_type.hpp>
#include <soccer/core/role_iterator_type.hpp>
#include <soccer/core/formation_id_type.hpp>
#include <soccer/core/tactic_id_type.hpp>
#include <soccer/core/flux_id_type.hpp>

#include <soccer/core/strategy_type.hpp>
#include <soccer/core/left_strategy_type.hpp>
#include <soccer/core/right_strategy_type.hpp>

#include <soccer/core/match_situation_type.hpp>

#include <soccer/core/adjacent_player_iterator_type.hpp>

//! Import relevant type decutions classe for the triangulations
#include <soccer/core/triangulation_type.hpp>
#include <soccer/core/vertex_type.hpp>
#include <soccer/core/edge_type.hpp>
#include <soccer/core/face_type.hpp>
#include <soccer/core/vertex_id_type.hpp>
#include <soccer/core/edge_id_type.hpp>
#include <soccer/core/face_id_type.hpp>
#include <soccer/core/vertex_iterator_type.hpp>
#include <soccer/core/edge_iterator_type.hpp>
#include <soccer/core/face_iterator_type.hpp>
#include <soccer/core/vertex_circulator_type.hpp>
#include <soccer/core/edge_circulator_type.hpp>
#include <soccer/core/face_circulator_type.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/core/tags.hpp>

#include <soccer/geometry.hpp>

//! Import relevant concepts to be checked
/*#include <soccer/concepts/pitch_concept.hpp>
#include <soccer/concepts/state_concepts.hpp>
#include <soccer/concepts/tactic_concepts.hpp>
#include <soccer/concepts/formation_concepts.hpp>
#include <soccer/concepts/strategy_concepts.hpp> */

#include <soccer/util/bare_type.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

template < typename Object >
struct key_value_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Position >
struct position_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Position>) ); };

template < typename Direction >
struct direction_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Direction>) ); };

template < typename Direction >
struct body_direction_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Direction>) ); };

template < typename Direction >
struct face_direction_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Direction>) ); };

template < typename Velocity >
struct velocity_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Velocity>) ); };

//
// Player model
//
template < typename Player >
struct side_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Player>) ); };

template < typename Player >
struct number_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Player>) ); };

template < typename Player >
struct is_goalie_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Player>) ); };

template < typename Player >
struct has_yellow_card_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Player>) ); };


template < typename Player >
struct stamina_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Player>) ); };

//
// State model access base
//
template < typename State >
struct ball_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct player_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct player_iterators_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct adjacent_player_iterators_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct time_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct playmode_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct left_team_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct right_team_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct left_score_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

template < typename State >
struct right_score_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>) ); };

//
// State with team affiliation
//

template < typename StateWithTeamAffiliation >
struct our_side_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

template < typename StateWithTeamAffiliation >
struct their_side_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

template < typename StateWithTeamAffiliation >
struct our_players_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

template < typename StateWithTeamAffiliation >
struct their_players_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

template < typename StateWithTeamAffiliation >
struct our_score_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

template < typename StateWithTeamAffiliation >
struct their_score_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

template < typename StateWithTeamAffiliation >
struct our_team_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

template < typename StateWithTeamAffiliation >
struct their_team_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StateWithTeamAffiliation>) ); };

//
// Pitch model access base
//

template < typename Pitch >
struct pitch_right_sign_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct pitch_bottom_sign_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct pitch_top_left_x_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct pitch_top_left_y_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct pitch_length_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct pitch_width_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct goal_area_length_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct goal_area_width_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct penalty_area_length_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct penalty_area_width_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct penalty_circle_radius_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct penalty_spot_distance_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct goal_depth_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct goal_width_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct center_circle_radius_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct corner_arc_radius_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

template < typename Pitch >
struct goal_post_radius_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Pitch>) ); };

//
// action model access base
//
template < typename Action >
struct executor_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Action>) ); };

template < typename Action >
struct receiver_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Action>) ); };

template < typename Action >
struct duration_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Action>) ); };

template < typename Action >
struct speed_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Action>) ); };

template < typename Action >
struct ball_first_speed_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Action>) ); };

template < typename Action >
struct first_speed_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Action>) ); };

template < typename Direction >
struct tackle_direction_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Direction>) ); };

//
// tactic model access base
//
template < typename Tactic >
struct tactic_weight_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Tactic>) ); };

//
// strategy model access database
//
template < typename Strategy >
struct role_iterators_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Strategy>) ); };

template < typename Strategy >
struct tactic_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Strategy>) ); };

template < typename Strategy >
struct flux_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Strategy>) ); };

template < typename Strategy >
struct formation_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Strategy>) ); };

template < typename Strategy >
struct positioning_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Strategy>) ); };

//
// ball model
//
template < typename Object >
struct speed_max_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct accel_max_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct effort_max_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct effort_min_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct stamina_inc_max_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct extra_stamina_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct kick_power_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct dash_power_rate_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct speed_min_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct kickable_distance_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct catchable_distance_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct foul_probability_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct inertia_moment_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct decay_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct size_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct unknown_number_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Side >
struct unknown_side_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Side>) ); };

template < typename Side >
struct opposite_side_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Side>) ); };

template < typename Side >
struct right_side_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Side>) ); };

template < typename Side >
struct left_side_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Side>) ); };

template < typename ContextTag, typename Type >
struct null_value_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<ContextTag, Type>) ); };

template < typename TacticId >
struct null_tactic_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<TacticId>) ); };

template < typename FormationId >
struct null_formation_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<FormationId>) ); };

template < typename FluxId >
struct null_flux_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<FluxId>) ); };

template < typename StrategyId >
struct null_strategy_id_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<StrategyId>) ); };

//
// playmode
//
template < typename PlayMode >
struct match_situation_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<PlayMode>) ); };

template < typename MatchSituation >
struct is_kick_off_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_throw_in_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_goal_kick_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_corner_kick_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_free_kick_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_indirect_free_kick_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_goalie_catch_kick_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_offside_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_play_on_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_penalty_setup_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_penalty_ready_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_before_kick_off_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_time_over_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

template < typename MatchSituation >
struct is_goal_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<MatchSituation>) ); };

//
// triangulation access
//
template < typename Object >
struct triangulation_access
    { BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct vertex_iterators_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct edge_iterators_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct face_iterators_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct incident_vertices_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct incident_edges_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct incident_faces_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };

template < typename Object >
struct infinite_vertex_access
    {  BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>) ); };


SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

//
// Get/Set position of object
//

/*!
\brief get position value of static or dynamic object
\details \details_get_set
\tparam StaticObject \tparam_static_object
\param object \param_object
\return position value
\ingroup get
\qbk{[include reference/core/get_position.qbk]}
*/
template < typename StaticObject >
inline typename position_type< typename soccer::util::bare_type< StaticObject >::type >::type
get_position( StaticObject const& obj )
{
    return traits::position_access< typename soccer::util::bare_type< StaticObject >::type >::get( obj );
}

/*!
\brief set position value of static or dynamic object
\details \details_get_set
\tparam StaticObject \tparam_static_object
\param object \param_object
\ingroup set
\qbk{[include reference/core/get_position.qbk]}
*/
template < typename StaticObject >
inline void
set_position( typename position_type< typename soccer::util::bare_type< StaticObject >::type >::type const& value,
              StaticObject& obj)
{
    traits::position_access< typename soccer::util::bare_type< StaticObject >::type >::set( value, obj );
}

//
// Get/Set velocity of object
//

/*!
\brief get velocity value of dynamic object
\details \details_get_set
\tparam DynamicObject \tparam_dynamic_object
\param object \param_object
\return velocity value
\ingroup get
*/
template < typename DynamicObject >
inline typename velocity_type< typename soccer::util::bare_type< DynamicObject >::type >::type
get_velocity( DynamicObject const& obj )
{
    return traits::velocity_access< typename soccer::util::bare_type< DynamicObject> ::type >::get( obj );
}

/*!
\brief set velocity of dynamic object
\details \details_get_set
\tparam DynamicObject \tparam_dynamic_object
\param object \param_object
\ingroup set
*/
template < typename DynamicObject >
inline void
set_velocity( typename velocity_type< typename soccer::util::bare_type< DynamicObject >::type >::type const& value,
              DynamicObject& obj )
{
    traits::velocity_access< typename soccer::util::bare_type< DynamicObject >::type >::set( value, obj );
}


//
// Get speed
//
/*!
\brief get speed value of velocity object
\details \details_get_set
\tparam VelocityObject \tparam_velocity_object
\param object \param_object
\return velocity value
\ingroup get
*/
template < typename VelocityObject >
inline typename coordinate_type< typename soccer::util::bare_type< VelocityObject >::type >::type
get_speed( VelocityObject const& _vel )
{
    // TODO Assert that object is registered with a velocity tag
    auto _vx = get_x( _vel );
    auto _vy = get_y( _vel );
    auto _speed = std::sqrt( std::pow( _vx + _vx, 2 ) + std::pow( _vy + _vy, 2) );
    return _speed;
}

/*!
\brief check if player is goalie
\details \details_get_set
\tparam Player \tparam_player_object
\param player \param_player
\return bool
\ingroup get
*/
template < typename Player >
inline bool
is_goalie( Player const& player )
{
    return traits::is_goalie_access< typename soccer::util::bare_type< Player >::type >::get( player );
}

/*!
\brief check if player has a yellow card
\details \details_get_set
\tparam Player \tparam_player_object
\param player \param_player
\return bool
\ingroup get
*/
template < typename Player >
inline bool
has_yellow_card( Player const& player )
{
    return traits::has_yellow_card_access< typename soccer::util::bare_type<Player>::type >::get( player );
}

//
// Get/Set side of player
//

/*!
\brief get side of player
\details \details_get_set
\tparam Player \tparam_player_object
\param player \param_player
\return side
\ingroup get
*/
template < typename Player >
inline typename side_type< typename soccer::util::bare_type< Player >::type >::type
get_side( Player const& player )
{
    return traits::side_access< typename soccer::util::bare_type< Player >::type >::get( player );
}

/*!
\brief set side of player
\details \details_get_set
\tparam Player \tparam_player
\param value \param_value
\param player \param_player
\ingroup set
*/
template < typename Player >
inline void
set_side( typename side_type< typename soccer::util::bare_type< Player >::type >::type const& value,
          Player& player )
{
    traits::side_access< typename soccer::util::bare_type< Player >::type >::set( value, player);
}


//
// Get/Set number of player
//

/*!
\brief get number of player
\details \details_get_set
\tparam Player \tparam_player_object
\param player \param_player
\return jersey number of player
\ingroup get
*/
template < typename Player >
inline typename number_type< typename soccer::util::bare_type< Player >::type >::type
get_number( Player const& player )
{
    return traits::number_access< typename soccer::util::bare_type< Player >::type >::get( player );
}

/*!
\brief set jersey number of player
\details \details_get_set
\tparam Player \tparam_player
\param value \param_value
\param player \param_player
\ingroup set
*/
template < typename Player >
inline void
set_number( typename number_type< typename soccer::util::bare_type< Player >::type >::type const& value,
            Player& player )
{
    traits::number_access< typename soccer::util::bare_type< Player >::type >::set( value, player );
}

//
// Get/Set body direction of player
//

/*!
\brief get number of player
\details \details_get_set
\tparam Player \tparam_player_object
\param player \param_player
\return jersey number of player
\ingroup get
*/
template < typename Player >
inline typename direction_type< typename soccer::util::bare_type< Player >::type >::type
get_body_direction( Player const& player )
{
    return traits::body_direction_access< typename soccer::util::bare_type< Player >::type >::get( player );
}

/*!
\brief set jersey number of player
\details \details_get_set
\tparam Player \tparam_player
\param value \param_value
\param player \param_player
\ingroup set
*/
template < typename Player >
inline void
set_body_direction( typename direction_type< typename soccer::util::bare_type< Player >::type >::type const& value,
                    Player& player )
{
    traits::body_direction_access< typename soccer::util::bare_type< Player >::type >::set( value, player );
}

//
// Get/Set face direction of player
//

/*!
\brief get number of player
\details \details_get_set
\tparam Player \tparam_player_object
\param player \param_player
\return jersey number of player
\ingroup get
*/
template < typename Player >
inline typename direction_type< typename soccer::util::bare_type< Player >::type >::type
get_face_direction( Player const& player )
{
    return traits::face_direction_access< typename soccer::util::bare_type< Player >::type >::get( player );
}

/*!
\brief set jersey number of player
\details \details_get_set
\tparam Player \tparam_player
\param value \param_value
\param player \param_player
\ingroup set
*/
template < typename Player >
inline void
set_face_direction( typename direction_type< typename soccer::util::bare_type< Player >::type >::type const& value,
                    Player& player )
{
    traits::face_direction_access< typename soccer::util::bare_type< Player >::type >::set( value, player );
}


//
// Get players in state
//
/*!
\brief get player iterators pair from state object
\details \details_get_set
\tparam StateObject \tparam_state
\param state \param_state
\return pair of player id iterators object
\ingroup get
*/
namespace dispatch
{
    template < typename Tag, typename Object >
    struct player_iterators_access;

    template < typename Formation >
    struct player_iterators_access< formation_tag, Formation >
    {
        static inline
        std::pair< typename player_iterator_type< Formation >::type,
                   typename player_iterator_type< Formation >::type >
        get( Formation const& _formation )
        {
            // BOOST_CONCEPT_ASSERT(( FormationConcept<Formation> ));
            return traits::player_iterators_access< typename soccer::util::bare_type<Formation>::type >::get( _formation );
        }
    };

    template < typename State >
    struct player_iterators_access< state_tag, State >
    {
        static inline
        std::pair< typename player_iterator_type< State >::type,
                   typename player_iterator_type< State >::type >
        get( State const& _state )
        {
            // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
            return traits::player_iterators_access< typename soccer::util::bare_type<State>::type >::get( _state );
        }
    };

    template < typename FormationSample >
    struct player_iterators_access< formation_sample_tag, FormationSample >
    {
        static inline
        std::pair< typename player_iterator_type< FormationSample >::type,
                   typename player_iterator_type< FormationSample >::type >
        get( FormationSample const& _sample )
        {
            // BOOST_CONCEPT_ASSERT(( FormationSampleConcept<FormationSample> ));
            return traits::player_iterators_access< typename soccer::util::bare_type<FormationSample>::type >::get( _sample );
        }\
    };

    template < typename Strategy >
    struct player_iterators_access< strategy_tag, Strategy >
    {
        static inline
        std::pair< typename player_iterator_type< Strategy >::type,
                   typename player_iterator_type< Strategy >::type >
        get( Strategy const& _strategy )
        {
            // BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));
            return traits::player_iterators_access< typename soccer::util::bare_type<Strategy>::type >::get( _strategy );
        }
    };

}

template < typename Object >
inline
std::pair< typename player_iterator_type< typename soccer::util::bare_type< Object >::type >::type,
           typename player_iterator_type< typename soccer::util::bare_type< Object >::type >::type >
get_players( Object const& _object )
{
    return dispatch::player_iterators_access< typename tag< Object >::type, typename soccer::util::bare_type< Object >::type >::get( _object );
}

template < typename Object >
inline
std::pair< typename role_iterator_type< typename soccer::util::bare_type< Object >::type >::type,
           typename role_iterator_type< typename soccer::util::bare_type< Object >::type >::type >
get_roles( Object const& _object )
{
    return traits::role_iterators_access< typename soccer::util::bare_type< Object >::type >::get( _object );
}

template < typename ContextTag, typename Type >
inline decltype(traits::null_value_access< ContextTag, Type >::get())
get_null_value()
{
    return traits::null_value_access< ContextTag, Type >::get();
}


//
// Get adjacent players in state
//
/*!
\brief get player iterators pair from state object
\details \details_get_set
\tparam StateObject \tparam_state
\param state \param_state
\return pair of player id iterators object
\ingroup get
*/
template < typename State >
inline
std::pair< typename adjacent_player_iterator_type< typename soccer::util::bare_type< State >::type >::type,
           typename adjacent_player_iterator_type< typename soccer::util::bare_type< State >::type >::type >
get_adjacent_players( typename player_id_type< State >::type const pid,
                      State const& state )
{
    // OOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::adjacent_player_iterators_access< typename soccer::util::bare_type< State >::type >::get( pid, state );
}

//
// Get/Set ball in state
//

/* namespace dispatch
{
    template < typename Tag, typename Object >
    struct ball_access { };

    template < typename State >
    struct ball_access < state_tag, State >
    {

        static inline
        typename ball_type< State >::type
        get( State const& _state )
        {
            BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
            return traits::ball_access< typename soccer::util::bare_type<State>::type >::get( _state );
        }

        void
        set( typename ball_type< State >::type const& _ball, State const& _state )
        {
            BOOST_CONCEPT_ASSERT(( MutableStateConcept< State > ));
            traits::ball_access< typename soccer::util::bare_type<State>::type >::set( _ball, _state );
        }
    };

    template < typename FormationSample >
    struct ball_access < formation_sample_tag, FormationSample >
    {
        static inline
        typename ball_type< FormationSample >::type
        get( FormationSample const& _sample )
        {
            // BOOST_CONCEPT_ASSERT(( FormationSampleConcept<FormationSample> ));
            return traits::ball_access< typename soccer::util::bare_type<FormationSample>::type >::get( _sample );
        }

        void
        set( typename ball_type< FormationSample >::type const& _ball, FormationSample const& _sample )
        {
            // BOOST_CONCEPT_ASSERT(( MutableFormationSampleConcept<FormationSample> ));
            traits::ball_access< typename soccer::util::bare_type<FormationSample>::type >::set( _ball, _sample );
        }
    };
} */

/*!
\brief get ball object from state
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return ball object
\ingroup get
*/
template < typename State >
inline typename ball_type< typename soccer::util::bare_type< State >::type >::type
get_ball( State const& _state, state_tag)
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::ball_access< typename soccer::util::bare_type<State>::type >::get( _state );
}

template < typename FormationSample >
inline typename ball_type< typename soccer::util::bare_type< FormationSample >::type >::type
get_ball( FormationSample const& _sample, formation_sample_tag )
{
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));
    return traits::ball_access< typename soccer::util::bare_type< FormationSample >::type >::get( _sample );
}

template < typename Object >
inline typename ball_type< Object >::type
get_ball( Object const& _obj )
{
    return get_ball( _obj, typename tag< typename soccer::util::bare_type< Object >::type >::type() );
}

/*!
\brief set ball object of state object
\details \details_get_set
\tparam State \tparam_state
\param value \param_value
\param state \param_state
\ingroup set
*/
template < typename State >
inline void
set_ball( typename ball_type< typename soccer::util::bare_type< State >::type >::type const& value, State& _state, state_tag)
{
    // BOOST_CONCEPT_ASSERT(( MutableStateConcept< State > ));
    traits::ball_access< typename soccer::util::bare_type<State>::type >::set( value, _state );
}

template < typename FormationSample >
inline void
set_ball( typename ball_type< typename soccer::util::bare_type< FormationSample >::type >::type const& value, FormationSample& _sample, formation_sample_tag )
{
    // BOOST_CONCEPT_ASSERT(( MutableFormationSampleConcept< FormationSample > ));
    traits::ball_access< typename soccer::util::bare_type<FormationSample>::type >::set( value, _sample );
}

template < typename Object >
inline void
set_ball( typename ball_type< typename soccer::util::bare_type< Object >::type >::type const& value,
          Object& _object )
{
    // BOOST_CONCEPT_ASSERT(( MutableStateConcept< State > ));
    set_ball( value, _object, typename tag< typename soccer::util::bare_type< Object >::type >::type() );
}


//
// Get/Set player in state
//

/*!
\brief get player from state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return player object
\ingroup get
*/
template < typename State >
inline typename player_type< typename soccer::util::bare_type< State >::type >::type const&
get_player( typename player_id_type< typename soccer::util::bare_type< State >::type >::type const& pid,
            State const& _state,
            state_tag )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::player_access< typename soccer::util::bare_type< State >::type >::get( pid, _state);
}

template < typename FormationSample >
inline typename player_type< typename soccer::util::bare_type< FormationSample >::type >::type
get_player( typename player_id_type< typename soccer::util::bare_type< FormationSample >::type >::type const& pid,
            FormationSample const& _sample ,
            formation_sample_tag )
{
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));
    return traits::player_access< typename soccer::util::bare_type< FormationSample >::type >::get( pid, _sample );
}

template < typename Strategy >
inline typename player_type< Strategy >::type
get_player( typename player_id_type< typename soccer::util::bare_type< Strategy >::type >::type const& pid,
            Strategy const& _strategy,
            strategy_tag )
{
    // BOOST_CONCEPT_ASSERT(( FormationSampleConcept< FormationSample > ));
    return traits::player_access< typename soccer::util::bare_type< Strategy >::type >::get( pid, _strategy );
}

template < typename Object >
inline typename player_type< typename soccer::util::bare_type< Object >::type >::type const&
get_player( typename player_id_type< typename soccer::util::bare_type< Object >::type >::type const& pid,
            Object const& _object )
{
    return get_player( pid, _object, typename tag< typename soccer::util::bare_type< Object >::type >::type() );
}

/*!
\brief set player object of state object
\details \details_get_set
\tparam State \tparam_state
\param value \param_value
\param state \param_state
\ingroup set
*/
template < typename State >
inline  void
set_player( typename player_id_type< typename soccer::util::bare_type< State >::type >::type const& pid,
            typename player_type< typename soccer::util::bare_type< State >::type >::type const& value,
            State& _state,
            state_tag )
{
    // BOOST_CONCEPT_ASSERT(( MutableStateConcept< State > ));
    traits::player_access< typename soccer::util::bare_type< State >::type >::set( pid, value, _state);
}

template < typename FormationSample >
inline void
set_player( typename player_id_type< typename soccer::util::bare_type< FormationSample >::type  >::type const& pid,
            typename player_type< typename soccer::util::bare_type< FormationSample >::type  >::type const& value,
            FormationSample& _sample ,
            formation_sample_tag )
{
    // BOOST_CONCEPT_ASSERT(( MutableFormationSampleConcept< FormationSample > ));
    traits::player_access< typename soccer::util::bare_type<FormationSample>::type >::set( pid, value, _sample );
}

template < typename Object >
inline void
set_player( typename player_id_type< typename soccer::util::bare_type< Object >::type >::type const& pid,
            typename player_type< typename soccer::util::bare_type< Object >::type >::type const& value,
            Object& _object )
{
    set_player( pid, value, _object, typename tag< typename soccer::util::bare_type< Object >::type >::type() );
}

//
// Get/Set time in state
//

/*!
\brief get time from state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return time object
\ingroup get
*/
template < typename State >
inline typename time_type< typename soccer::util::bare_type< State >::type >::type
get_time( State const& state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::time_access< typename soccer::util::bare_type< State >::type >::get( state );
}

/*!
\brief set time object of state object
\details \details_get_set
\tparam State \tparam_state
\param value \param_value
\param state \param_state
\ingroup set
*/
template < typename State >
inline void
set_time( typename time_type< typename soccer::util::bare_type< State >::type >::type const& value,
          State& state )
{
    // BOOST_CONCEPT_ASSERT(( MutableStateConcept< State > ));
    traits::time_access< typename soccer::util::bare_type< State >::type >::set( value, state );
}


//
// Get/Set game mode in state
//

/*!
\brief get game mode from state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return playmode object
\ingroup get
*/
template < typename State >
inline typename playmode_type< typename soccer::util::bare_type< State >::type >::type
get_playmode( State const& state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::playmode_access< typename soccer::util::bare_type< State >::type >::get( state );
}

/*!
\brief set game mode object of state object
\details \details_get_set
\tparam State \tparam_state
\param value \param_value
\param state \param_state
\ingroup set
*/
template < typename State >
inline void
set_playmode( typename playmode_type< typename soccer::util::bare_type< State >::type >::type const& value,
              State& state )
{
    // BOOST_CONCEPT_ASSERT(( MutableStateConcept< State > ));
    traits::playmode_access< typename soccer::util::bare_type< State >::type >::set( value, state );
}


/*!
\brief get left score of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return score object
\ingroup get
*/
template < typename State >
inline typename score_type< typename soccer::util::bare_type< State >::type >::type
get_left_score( State const& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::left_score_access< typename soccer::util::bare_type< State >::type >::get( _state );
}


/*!
\brief set left score of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\ingroup set
*/
template < typename State >
inline void
set_left_score( typename score_type< typename soccer::util::bare_type< State >::type >::type const& _score,
                State& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    traits::left_score_access< typename soccer::util::bare_type< State >::type >::set( _score, _state );
}

/*!
\brief get left score of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return score object
\ingroup get
*/
template < typename State >
inline typename score_type< typename soccer::util::bare_type< State >::type >::type
get_right_score( State const& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::right_score_access< typename soccer::util::bare_type< State >::type >::get( _state );
}

/*!
\brief set right score of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\ingroup set
*/
template < typename State >
inline void
set_right_score( typename score_type< State >::type const& _score,
                 State& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    traits::right_score_access< typename soccer::util::bare_type< State >::type >::set( _score, _state );
}


/*!
\brief get left team of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return team object
\ingroup get
*/
template < typename State >
inline typename team_type< typename soccer::util::bare_type< State >::type >::type
get_left_team( State const& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::left_team_access< typename soccer::util::bare_type< State >::type >::get( _state );
}

/*!
\brief set left team of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return team object
\ingroup set
*/
template < typename State >
inline void
set_left_team( typename team_type< typename soccer::util::bare_type< State >::type >::type const& _team,
               State& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    traits::left_team_access< typename soccer::util::bare_type< State >::type >::set( _team, _state );
}

/*!
\brief get right team of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return team object
\ingroup get
*/
template < typename State >
inline typename team_type< typename soccer::util::bare_type< State >::type >::type
get_right_team( State const& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    return traits::right_team_access< typename soccer::util::bare_type< State >::type >::get( _state );
}

/*!
\brief set right team of state object
\details \details_get_set
\tparam State \tparam_state
\param state \param_state
\return team object
\ingroup set
*/
template < typename State >
inline void
set_right_team( typename team_type< typename soccer::util::bare_type< State >::type >::type const& _team,
                State& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    traits::right_team_access< typename soccer::util::bare_type< State >::type >::set( _team, _state );
}


/*!
\brief get our side from state with team affiliation object
\details \details_get_set
\tparam StateWithTeamAffiliation \tparam_state
\param state \param_state
\return side object
\ingroup get
*/
template < typename StateWithTeamAffiliation >
inline typename side_type< typename player_type< typename soccer::util::bare_type< StateWithTeamAffiliation >::type >::type >::type
get_our_side( StateWithTeamAffiliation const& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateWithTeamAffiliationConcept< State > ));
    return traits::our_side_access< typename soccer::util::bare_type< StateWithTeamAffiliation >::type >::get( _state );
}

/*!
\brief set get our side in a state with team affiliation object
\details \details_get_set
\tparam StateWithTeamAffiliation \tparam_state
\param state \param_state
\return side object
\ingroup set
*/
template < typename StateWithTeamAffiliation >
inline void
set_our_side( typename side_type< typename player_type< typename soccer::util::bare_type< StateWithTeamAffiliation >::type >::type >::type const& _side,
              StateWithTeamAffiliation& _state )
{
    // BOOST_CONCEPT_ASSERT(( StateWithTeamAffiliationConcept< State > ));
    traits::our_side_access< typename soccer::util::bare_type< StateWithTeamAffiliation >::type >::set( _side, _state );
}

//
// Pitch model access base
//
/*!
\brief get pitch right sign
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return pitch right sign
\ingroup get
*/
template < typename Pitch >
inline typename coordinate_type< typename soccer::util::bare_type< Pitch >::type >::type
get_pitch_right_sign( )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::pitch_right_sign_access< typename soccer::util::bare_type<Pitch>::type >::get( );
}

/*!
\brief get pitch right sign
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return pitch right sign
\ingroup get
*/
template < typename Pitch >
inline typename coordinate_type< typename soccer::util::bare_type< Pitch >::type >::type
get_pitch_bottom_sign( )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::pitch_bottom_sign_access< typename soccer::util::bare_type<Pitch>::type >::get( );
}

/*!
\brief get pitch top left x
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return pitch coordinate
\ingroup get
*/
template < typename Pitch >
inline typename coordinate_type< typename soccer::util::bare_type< Pitch >::type >::type
get_pitch_top_left_x( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::pitch_top_left_x_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get pitch top left y
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return pitch coordinate
\ingroup get
*/
template < typename Pitch >
inline typename coordinate_type< typename soccer::util::bare_type< Pitch >::type >::type
get_pitch_top_left_y( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::pitch_top_left_y_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get pitch length
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return pitch length
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_pitch_length( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::pitch_length_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get pitch width
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return pitch width
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_pitch_width( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::pitch_width_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}


/*!
\brief get goal area length
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return goal area length
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_goal_area_length( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::goal_area_length_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get goal area width
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return goal area width
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_goal_area_width( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::goal_area_width_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}


/*!
\brief get penalty area length
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return penalty area length
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_penalty_area_length( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::penalty_area_length_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get penalty area width
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return penalty area width
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_penalty_area_width( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::penalty_area_width_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get penalty circle radius
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return penalty circle radius
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_penalty_circle_radius( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::penalty_circle_radius_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get penalty spot distance
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return penalty spot distance
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_penalty_spot_distance( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::penalty_spot_distance_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get goal depth
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return goal depth
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_goal_depth( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::goal_depth_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get goal width
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return goal width
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_goal_width( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::goal_width_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get center circle radius
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return center circle radius
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_center_circle_radius( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::center_circle_radius_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get corner arc radius
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return corner arc radius
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_corner_arc_radius( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::corner_arc_radius_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

/*!
\brief get goal post radius
\details \details_get_set
\tparam Pitch \tparam_pitch
\param pitch \param_pitch
\return goal post radius
\ingroup get
*/
template < typename Pitch >
inline typename length_type< typename soccer::util::bare_type< Pitch >::type >::type
get_goal_post_radius( Pitch const& pitch )
{
    // BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));
    return traits::goal_post_radius_access< typename soccer::util::bare_type<Pitch>::type >::get( pitch );
}

//
// strategy access
//
/*!
\brief get formation id used in strategy access
\details \details_get_set
\tparam Strategy \tparam_strategy
\param strategy \param_strategy
\return formation used in strategy
\ingroup get
*/
template < typename Strategy >
inline typename formation_id_type< typename soccer::util::bare_type< Strategy >::type >::type
get_formation_id( Strategy const& _strategy )
{
    // BOOST_CONCEPT_ASSERT(( StrategyConcept< Strategy > ));
    return traits::formation_id_access< typename soccer::util::bare_type<Strategy>::type >::get( _strategy );
}

/*!
\brief set formation id used in strategy access
\details \details_get_set
\tparam Strategy \tparam_strategy
\param strategy \param_strategy
\ingroup get
*/
template < typename Strategy >
inline void
set_formation_id( typename formation_id_type< typename soccer::util::bare_type< Strategy >::type >::type const& _formation_id,
                  Strategy& _strategy )
{
    // BOOST_CONCEPT_ASSERT(( MutableStrategyConcept< Strategy > ));
    traits::formation_id_access< typename soccer::util::bare_type<Strategy>::type >::set( _formation_id, _strategy );
}

/*!
\brief get tactic id used in strategy access
\details \details_get_set
\tparam Strategy \tparam_strategy
\param strategy \param_strategy
\return tactic used in strategy
\ingroup get
*/
template < typename Strategy >
inline typename tactic_id_type< typename soccer::util::bare_type< Strategy >::type >::type
get_tactic_id( Strategy const& _strategy )
{
    // BOOST_CONCEPT_ASSERT(( StrategyConcept< Strategy > ));
    return traits::tactic_id_access< typename soccer::util::bare_type<Strategy>::type >::get( _strategy );
}

/*!
\brief set tactic id used in strategy access
\details \details_get_set
\tparam Strategy \tparam_strategy
\param strategy \param_strategy
\ingroup get
*/
template < typename Strategy >
inline void
set_tactic_id( typename tactic_id_type< typename soccer::util::bare_type< Strategy >::type >::type const& _tactic_id,
               Strategy& _strategy )
{
    // BOOST_CONCEPT_ASSERT(( MutableStrategyConcept< Strategy > ));
    traits::tactic_id_access< typename soccer::util::bare_type<Strategy>::type >::set( _tactic_id, _strategy );
}

/*!
\brief get flux id used in strategy access
\details \details_get_set
\tparam Strategy \tparam_strategy
\param strategy \param_strategy
\return formation used in strategy
\ingroup get
*/
template < typename Strategy >
inline typename flux_id_type< typename soccer::util::bare_type< Strategy >::type >::type

get_flux_id( Strategy const& _strategy )
{
    // BOOST_CONCEPT_ASSERT(( StrategyConcept< Strategy > ));
    return traits::flux_id_access< typename soccer::util::bare_type<Strategy>::type >::get( _strategy );
}

/*!
\brief set flux id used in strategy access
\details \details_get_set
\tparam Strategy \tparam_strategy
\param strategy \param_strategy
\ingroup get
*/
template < typename Strategy >
inline void
set_flux_id( typename flux_id_type< typename soccer::util::bare_type< Strategy >::type >::type const& _flux_id,
             Strategy& _strategy )
{
    // BOOST_CONCEPT_ASSERT(( MutableStrategyConcept< Strategy > ));
    traits::flux_id_access< typename soccer::util::bare_type<Strategy>::type >::set( _flux_id, _strategy );
}

//
// Strategy access base for positions
//

namespace dispatch {

    template < typename Tag, typename Strategy >
    struct position_access;

    template < typename Strategy >
    struct position_access < strategy_tag, Strategy >
    {
        // BOOST_CONCEPT_ASSERT(( StrategyConcept<Strategy> ));

        static inline
        typename position_type< Strategy >::type
        get( typename player_id_type< Strategy >::type const& pid,
             Strategy const& _strategy )
        {
            return traits::position_access< Strategy >::get( pid, _strategy );
        }

        static inline void
        set( typename player_id_type< Strategy >::type const& pid,
             typename position_type< Strategy >::type const& pos,
             Strategy& _strategy )
        {
            traits::position_access< Strategy >::set( pid, pos, _strategy );
        }
    };

    template < typename State >
    struct position_access < state_tag, State >
    {
        // BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

        static inline
        typename position_type< State >::type
        get( typename player_id_type< State >::type const& pid,
             State const& _state )
        {
            return get_position( traits::player_access< State >::get( pid, _state ) );
        }

        static inline void
        set( typename player_id_type< State >::type const& ,
             typename position_type< State >::type const& ,
             State&  )
        {
            assert( false ); // TODO
        }
    };

}

/*!
\brief get position value of static or dynamic object
\details \details_get_set
\tparam StaticObject \tparam_static_object
\param object \param_object
\return position value
\ingroup get
\qbk{[include reference/core/get_position.qbk]}
*/
template < typename Object >
inline typename position_type< typename soccer::util::bare_type< Object >::type >::type
get_position( typename player_id_type< typename soccer::util::bare_type< Object >::type >::type const& pid,
              Object const& obj )
{
    return dispatch::position_access< typename tag< typename soccer::util::bare_type< Object >::type >::type,
                                      typename soccer::util::bare_type< Object >::type >::get( pid, obj );
}

/*!
\brief set position value of static or dynamic object
\details \details_get_set
\tparam StaticObject \tparam_static_object
\param object \param_object
\ingroup set
\qbk{[include reference/core/get_position.qbk]}
*/
template < typename Object >
inline void
set_position( typename player_id_type< typename soccer::util::bare_type< Object >::type >::type const& pid,
              typename position_type< typename soccer::util::bare_type< Object >::type >::type const& pos,
              Object& obj)
{
    dispatch::position_access< typename tag< typename soccer::util::bare_type< Object >::type >::type,
                               typename soccer::util::bare_type< Object >::type >::set( pid, pos, obj );
}

/*!
\brief get position value of static or dynamic object
\details \details_get_set
\tparam StaticObject \tparam_static_object
\param object \param_object
\return position value
\ingroup get
\qbk{[include reference/core/get_position.qbk]}
*/
template < typename Object >
inline typename role_id_type< typename soccer::util::bare_type< Object >::type >::type
get_positioning( typename player_id_type< typename soccer::util::bare_type< Object >::type >::type const& pid,
                 Object const& obj )
{
    return traits::positioning_access< typename soccer::util::bare_type< Object >::type >::get( pid, obj );
}

/*!
\brief set position value of static or dynamic object
\details \details_get_set
\tparam StaticObject \tparam_static_object
\param object \param_object
\ingroup set
\qbk{[include reference/core/get_position.qbk]}
*/
template < typename Object >
inline void
set_positioning( typename player_id_type< typename soccer::util::bare_type< Object >::type >::type const& pid,
                 typename player_id_type< typename soccer::util::bare_type< Object >::type >::type const& _posit_id,
                 Object& obj)
{
    traits::positioning_access< typename soccer::util::bare_type< Object >::type >::set( pid, _posit_id, obj );
}

//
// player id model access
//
/*!
\brief get unknown number value
\details \details_get
\tparam Object \tparam_object
\param object \param_object
\return unknown number value
\ingroup get
\qbk{[include reference/core/get_unknown_number.qbk]}
*/
template < typename Player >
typename number_type< typename soccer::util::bare_type< Player >::type >::type
get_unknown_number( )
{
    return traits::unknown_number_access< typename soccer::util::bare_type< Player >::type >::get( );
}

//
// side model access
//
/*!
\brief get unknown side value
\details \details_get
\tparam Side \tparam_side
\param side \param_side
\return unknown side value
\ingroup get
\qbk{[include reference/core/get_unknown_side.qbk]}
*/
template < typename Side >
Side
get_unknown_side( )
{
    return traits::unknown_side_access< typename soccer::util::bare_type< Side >::type >::get( );
}


/*!
\brief get left side value
\details \details_get
\tparam Side \tparam_side
\param side \param_side
\return left side value
\ingroup get
\qbk{[include reference/core/get_left_side.qbk]}
*/
template < typename Side >
Side
get_left_side( )
{
    return traits::left_side_access< typename soccer::util::bare_type< Side >::type >::get( );
}

/*!
\brief get right side value
\details \details_get
\tparam Side \tparam_side
\param side \param_side
\return right side value
\ingroup get
\qbk{[include reference/core/get_right_side.qbk]}
*/
template < typename Side >
Side
get_right_side( )
{
    return traits::right_side_access< typename soccer::util::bare_type< Side >::type >::get( );
}

/*!
\brief get opposite side value
\details \details_get
\tparam Side \tparam_side
\param side \param_side
\return opposite side value
\ingroup get
\qbk{[include reference/core/get_opposite_side.qbk]}
*/
template < typename Side >
Side
get_opposite_side( Side const& _side )
{
    typedef typename soccer::util::bare_type< Side >::type bare_side_t;
    assert(( _side != get_unknown_side< bare_side_t >() ));

    return _side == get_left_side< bare_side_t >()
            ? get_right_side< bare_side_t >()
            : get_left_side< bare_side_t >();
}

//
// Action/Event access
//
template < typename Event >
inline typename player_id_type< typename soccer::util::bare_type< Event >::type >::type
get_executor_id( Event const& _event )
{
    return traits::executor_id_access< typename soccer::util::bare_type< Event >::type >::get( _event );
}

template < typename Event >
inline void
set_executor_id( typename player_id_type< typename soccer::util::bare_type< Event >::type >::type pid,
                 Event& _event )
{
    traits::executor_id_access< typename soccer::util::bare_type< Event >::type >::set( pid, _event );
}

template < typename Pass >
inline typename player_id_type< typename soccer::util::bare_type< Pass >::type >::type
get_receiver_id( Pass const& _pass )
{
    return traits::receiver_id_access< typename soccer::util::bare_type< Pass >::type >::get( _pass );
}

template < typename Event >
inline typename direction_type< typename soccer::util::bare_type< Event >::type >::type
get_tackle_direction( Event const& _event )
{
    return traits::tackle_direction_access< typename soccer::util::bare_type< Event >::type >::get( _event );
}

template < typename Action >
inline typename speed_type< typename soccer::util::bare_type< Action >::type >::type
get_ball_first_speed( Action const& _action )
{
    return traits::ball_first_speed_access< typename soccer::util::bare_type< Action >::type >::get( _action );
}

template < typename Action >
inline void
set_ball_first_speed( typename speed_type< Action >::type const& _value, Action& _action )
{
    traits::ball_first_speed_access< typename soccer::util::bare_type< Action >::type >::set( _value, _action );
}

template < typename Action >
inline typename speed_type< typename soccer::util::bare_type< Action >::type >::type
get_first_speed( Action const& _action )
{
    return traits::first_speed_access< typename soccer::util::bare_type< Action >::type >::get( _action );
}

template < typename Action >
inline void
set_first_speed( typename speed_type< typename soccer::util::bare_type< Action >::type >::type const& _value,
                        Action& _action )
{
    traits::first_speed_access< typename soccer::util::bare_type< Action >::type >::set( _value, _action );
}

template < typename Event >
inline typename duration_type< typename soccer::util::bare_type< Event >::type >::type
get_duration( Event const& _event )
{
    return traits::duration_access< typename soccer::util::bare_type< Event >::type >::get( _event );
}

template < typename ActionOrEvent >
inline void
set_duration( typename duration_type< typename soccer::util::bare_type< ActionOrEvent >::type >::type const& _value,
              ActionOrEvent& _event )
{
    traits::duration_access< typename soccer::util::bare_type< ActionOrEvent >::type >::set( _value, _event );
}

//
// Object Model
//
template < typename Model >
inline typename decay_type< typename soccer::util::bare_type< Model >::type >::type
get_decay( Model const& _model )
{
    return traits::decay_access< typename soccer::util::bare_type< Model >::type >::get( _model  );
}

//
// Playmode
//
template < typename PlayMode >
inline typename match_situation_type< typename soccer::util::bare_type< PlayMode >::type >::type
get_match_situation( PlayMode const& _pmode )
{
    return traits::match_situation_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_kick_off( PlayMode const& _pmode )
{
    return traits::is_kick_off_access < typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_goal_kick( PlayMode const& _pmode )
{
    return traits::is_goal_kick_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_throw_in( PlayMode const& _pmode )
{
    return traits::is_throw_in_access < typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_corner_kick( PlayMode const& _pmode )
{
    return traits::is_corner_kick_access < typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_free_kick( PlayMode const& _pmode )
{
    return traits::is_free_kick_access < typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_indirect_free_kick( PlayMode const& _pmode )
{
    return traits::is_indirect_free_kick_access < typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_goalie_catch( PlayMode const& _pmode )
{
    return traits::is_goalie_catch_kick_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_offside( PlayMode const& _pmode )
{
    return traits::is_offside_access < typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_play_on( PlayMode const& _pmode )
{
    return traits::is_play_on_access < typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_penalty_setup( PlayMode const& _pmode )
{
    return traits::is_penalty_setup_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_penalty_ready( PlayMode const& _pmode )
{
    return traits::is_penalty_ready_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_before_kick_off( PlayMode const& _pmode )
{
    return traits::is_before_kick_off_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_time_over( PlayMode const& _pmode )
{
    return traits::is_time_over_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

template < typename PlayMode >
inline bool
is_goal( PlayMode const& _pmode )
{
    return traits::is_goal_access< typename soccer::util::bare_type< PlayMode >::type >::get( _pmode );
}

//
// triangulation access
//
template < typename Object >
inline typename triangulation_type< typename soccer::util::bare_type< Object >::type >::type
get_triangulation( Object const& _object )
{
    return traits::triangulation_access< typename soccer::util::bare_type< Object >::type >::get( _object );
}

template < typename Object >
inline std::pair< typename vertex_iterator_type< typename soccer::util::bare_type< Object >::type >::type,
                  typename vertex_iterator_type< typename soccer::util::bare_type< Object >::type >::type >
get_vertices( Object const& _object )
{
    return traits::vertex_iterators_access< typename soccer::util::bare_type< Object >::type >::get( _object );
}

template < typename Object >
inline std::pair< typename edge_iterator_type< typename soccer::util::bare_type< Object >::type >::type,
                  typename edge_iterator_type< typename soccer::util::bare_type< Object >::type >::type >
edges( Object const& _object )
{
    return traits::edge_iterators_access< typename soccer::util::bare_type< Object >::type >::get( _object );
}

template < typename Object >
inline std::pair< typename face_iterator_type< typename soccer::util::bare_type< Object >::type >::type,
                  typename face_iterator_type< typename soccer::util::bare_type< Object >::type >::type >
faces( Object const& _object )
{
    return traits::face_iterators_access< typename soccer::util::bare_type< Object >::type >::get( _object );
}

template < typename Object >
inline std::pair< typename vertex_circulator_type< typename soccer::util::bare_type< Object >::type >::type,
                  typename vertex_circulator_type< typename soccer::util::bare_type< Object >::type >::type >
incident_vertices( typename vertex_id_type< Object >::type vid,
                   Object const& _object )
{
    return traits::vertex_iterators_access< typename soccer::util::bare_type< Object >::type >::get( vid, _object );
}

template < typename Object >
inline std::pair< typename edge_circulator_type< typename soccer::util::bare_type< Object >::type >::type,
                  typename edge_circulator_type< typename soccer::util::bare_type< Object >::type >::type >
incident_edges( typename vertex_id_type< Object >::type vid,
                Object const& _object )
{
    return traits::edge_iterators_access< typename soccer::util::bare_type< Object >::type >::get( vid, _object );
}

template < typename Object >
inline std::pair< typename face_circulator_type< typename soccer::util::bare_type< Object >::type >::type,
                  typename face_circulator_type< typename soccer::util::bare_type< Object >::type >::type >
incident_faces( typename vertex_id_type< Object >::type vid,
                Object const& _object )
{
    return traits::face_iterators_access< typename soccer::util::bare_type< Object >::type >::get( vid, _object );
}


template < typename Object >
inline typename vertex_id_type< typename soccer::util::bare_type< Object > >::type
infinite_vertex( Object const& _object )
{
    return traits::infinite_vertex_access< typename soccer::util::bare_type< Object >::type >::get( _object );
}


SOCCER_NAMESPACE_END


#endif // SOCCER_CORE_ACCESS_HPP
