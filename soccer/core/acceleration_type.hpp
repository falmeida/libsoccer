#ifndef SOCCER_ACCELERATION_TYPE_HPP
#define SOCCER_ACCELERATION_TYPE_HPP


#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type used to represent speed in the object contained
\ingroup traits
\tparam Object object
*/
template < typename Object >
struct acceleration_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types< Object >)
        );
};


} // end namespace traits


/*!
\brief \brief_meta{type, object_type, \meta_object_type}
\tparam Object \tparam_object
\ingroup core

\qbk{[include reference/core/acceleration_type.qbk]}
*/
template < typename Object >
struct acetype
{
    typedef typename traits::acceleration_type
        <
            typename soccer::util::bare_type<Object>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_ACCELERATION_TYPE_HPP
