#ifndef SOCCER_CORE_ROLE_ID_TYPE_HPP
#define SOCCER_CORE_ROLE_ID_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the role id contained in a formation
\ingroup traits
\tparam Formation player_object
*/
template < typename Formation >
struct role_id_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Formation>)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, formation_type, \meta_formation_type}
\tparam Formation \tparam_formation_object
\ingroup core

\qbk{[include reference/core/role_id_type.qbk]}
*/
template < typename Formation >
struct role_id_type
{
    typedef typename traits::role_id_type
        <
            typename soccer::util::bare_type<Formation>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_ROLE_ID_TYPE_HPP
