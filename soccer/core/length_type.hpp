#ifndef SOCCER_CORE_LENGTH_TYPE_HPP
#define SOCCER_CORE_LENGTH_TYPE_HPP

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class which indicate the length of an object
\ingroup traits
*/
template <typename Object, typename Enable = void>
struct length_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>)
        );
};

} // end namespace traits

/*!
\brief \brief_meta{type, length type, \meta_length_type}
\tparam Object \tparam_object
\ingroup core

\qbk{[include reference/core/length_type.qbk]}
*/
template <typename Object>
struct length_type
{
    typedef typename traits::length_type
                <
                    // typename tag<Object>::type,
                    typename soccer::util::bare_type<Object>::type
                >::type type;
};

} // end namespace soccer

#endif // SOCCER_CORE_LENGTH_TYPE_HPP
