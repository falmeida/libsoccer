#ifndef SOCCER_CORE_TAGS_HPP
#define SOCCER_CORE_TAGS_HPP

namespace soccer {

struct use_default {};

/// "default" tags
struct position_not_recognized_tag {};
struct state_not_recognized_tag {};
struct player_not_recognized_tag {};
struct ball_not_recognized_tag {};

struct point_tag { };
struct position_tag : public point_tag { };
struct velocity_tag : public point_tag { };
struct direction_tag { };
struct time_tag { };
struct score_tag { };
struct team_tag { };
struct playmode_tag { };

// motiom model
struct object_model_tag { };
struct dynamic_object_model_tag : public object_model_tag { };
struct player_object_model_tag : public dynamic_object_model_tag { };
struct ball_object_model_tag : public dynamic_object_model_tag { };

struct inertia_model_tag { };

// null value tags
struct null_value_tag { };
struct null_player_id_tag : public null_value_tag { };
struct null_tactic_id_tag : public null_value_tag { };
struct null_formation_id_tag : public null_value_tag  { };
struct null_flux_id_tag : public null_value_tag  { };

// generic object tags
struct static_object_tag { };
struct dynamic_object_tag : public static_object_tag { };

// player tags
struct side_tag { };
struct player_id_tag { };
struct player_tag { };
struct static_player_tag
        : public static_object_tag
        , public player_tag
{ };
struct dynamic_player_tag
        : public dynamic_object_tag
        , public static_player_tag
{ };

// ball tag
struct ball_tag { };
struct static_ball_tag
        : public static_object_tag
        , public ball_tag
{ };
struct dynamic_ball_tag
        : public dynamic_object_tag
        , public static_ball_tag
{ };

// action tags
struct action_tag { };
struct individual_action_tag : public action_tag { };
struct cooperative_action_tag : public action_tag { };
struct dribble_tag : public action_tag { };
struct pass_tag : public action_tag { };
struct move_tag : public action_tag { };
struct shoot_tag : public action_tag { };
struct hold_ball_tag : public action_tag { };
struct tackle_tag : public action_tag { };
struct clear_tag : public action_tag { };
struct catch_tag : public action_tag { };
struct interception_tag : public action_tag { };

// strategy tags
struct strategy_tag { };

struct tactic_id_tag { };
struct flux_id_tag { };
struct formation_id_tag { };

struct formation_tag { };
struct formation_static_tag : public formation_tag { };
struct formation_dynamic_tag : public formation_tag { };

struct formation_sample_tag { };
struct formation_sample_storage_tag { };

// event tags
struct event_tag { };
struct dribble_event_tag : public event_tag { };
struct shoot_event_tag : public event_tag { };
struct ball_dispute_event_tag : public event_tag { };
struct pass_event_tag : public event_tag { };
struct move_event_tag : public event_tag { };
struct goal_kick_event_tag : public event_tag { };
struct kick_off_event_tag : public event_tag { };
struct throw_in_event_tag : public event_tag { };
struct corner_kick_event_tag : public event_tag { };
struct indirect_kick_event_tag : public event_tag { };
struct free_kick_event_tag : public event_tag { };
struct offside_event_tag : public event_tag { };


// Triangulations
struct triangulation_tag { };
struct regular_triangulation_tag : public triangulation_tag { };
struct delaunay_triangulation_tag : public regular_triangulation_tag { };

struct vertex_id_tag { };
struct face_id_tag { };
struct edge_id_tag { };

struct vertex_tag { };
struct edge_tag { };
struct face_tag { };

// flux
struct flux_tag { };
struct flux_matrix_tag : public flux_tag { };
struct flux_triangulation_tag : public flux_tag { };

// state
struct state_tag { };
struct state_with_team_affiliation_tag : public state_tag { };

// tactic
struct tactic_tag { };

// pitch
struct pitch_tag { };

// game
struct game_tag { };

} // end namespace soccer

#endif // SOCCER_CORE_TAGS_HPP
