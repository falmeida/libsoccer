#ifndef SOCCER_CORE_KEY_TYPE_HPP
#define SOCCER_CORE_KEY_TYPE_HPP

#include <boost/mpl/assert.hpp>

#include <soccer/core/tag.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class which indicate the key of an object
\ingroup traits
*/
template <typename Object, typename Enable = void>
struct key_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object>)
        );
};

} // end namespace traits

/*!
\brief \brief_meta{type, key type, \meta_key_type}
\tparam Object \tparam_object
\ingroup core

\qbk{[include reference/core/key_type.qbk]}
*/
template <typename Object>
struct key_type
{
    typedef typename traits::key_type
                <
                    // typename tag<Object>::type,
                    typename soccer::util::bare_type<Object>::type
                >::type type;
};

} // end namespace soccer

#endif // SOCCER_CORE_KEY_TYPE_HPP
