#ifndef SOCCER_CORE_TIME_TYPE_HPP
#define SOCCER_CORE_TIME_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the time contained in a state
\ingroup traits
\par State:
    - all objects that represent a state
\par Specializations should provide:
    - typedef P type (where P should fulfill the State concept)
\tparam State state_object
*/
template < typename State >
struct time_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<State>)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, state_type, \meta_state_type}
\tparam State \tparam_state_object
\ingroup core

\qbk{[include reference/core/time_type.qbk]}
*/
template < typename State >
struct time_type
{
    typedef typename traits::time_type
        <
            typename soccer::util::bare_type<State>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_TIME_TYPE_HPP
