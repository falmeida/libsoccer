#ifndef SOCCER_CORE_NUMBER_TYPE_HPP
#define SOCCER_CORE_NUMBER_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the number contained in a player
\ingroup traits
\par Player:
    - all objects that represent a player
\par Specializations should provide:
    - typedef P type (where P should fulfill the Player concept)
\tparam Player player_object
*/
template < typename Player >
struct number_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Player>)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, player_type, \meta_player_type}
\tparam Player \tparam_player_object
\ingroup core

\qbk{[include reference/core/number_type.qbk]}
*/
template < typename Player >
struct number_type
{
    typedef typename traits::number_type
        <
            typename soccer::util::bare_type<Player>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_NUMBER_TYPE_HPP
