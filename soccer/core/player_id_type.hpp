#ifndef SOCCER_CORE_PLAYER_ID_TYPE_HPP
#define SOCCER_CORE_PLAYER_ID_TYPE_HPP

#include <boost/mpl/assert.hpp>
#include <soccer/util/bare_type.hpp>

namespace soccer {
namespace traits {

/*!
\brief Traits class indicating the type of the player identifier
\ingroup traits
\par PlayerId:
    - object that represents a player identifier
\par Specializations should provide:
    - typedef PlayerId type (where P should fulfill the PlayerId concept)
\tparam PlayerId player id object
*/
template < typename PlayerId >
struct player_id_type
{
    BOOST_MPL_ASSERT_MSG
        (
            false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<PlayerId>)
        );
};


} // end namespace traits



/*!
\brief \brief_meta{type, state_type, \meta_state_type}
\tparam PlayerId \tparam_player_id_object
\ingroup core

\qbk{[include reference/core/player_id_type.qbk]}
*/
template < typename PlayerId >
struct player_id_type
{
    typedef typename traits::player_id_type
        <
            typename soccer::util::bare_type<PlayerId>::type
        >::type type;
};


} // end namespace soccer

#endif // SOCCER_CORE_PLAYER_ID_TYPE_HPP
