#ifndef SOCCER_PITCH_CONCEPT_HPP
#define SOCCER_PITCH_CONCEPT_HPP

#include <soccer/core/coordinate_type.hpp>
#include <soccer/core/length_type.hpp>
#include <soccer/core/tags.hpp>

#include <boost/concept/detail/concept_def.hpp>
#include <boost/concept_check.hpp>

#include <boost/type_traits.hpp>

namespace soccer {

BOOST_concept(PitchConcept,(P))
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< pitch_tag, typename tag< P >::type >::value ),
                            "Type category mismatch");

    typedef typename coordinate_type< P >::type coordinate_t;
    typedef typename length_type< P >::type length_t;

    BOOST_STATIC_ASSERT_MSG(( boost::is_floating_point<coordinate_t>::value),
                            "Coordinate descriptor type is not a floating number");
    BOOST_STATIC_ASSERT_MSG(( boost::is_floating_point<length_t>::value),
                            "Length descriptor type is not a floating number");

    BOOST_CONCEPT_USAGE(PitchConcept)
    {
        const_constraints( p );
    };

private:
    void const_constraints( const P& const_p )
    {
        _coord = get_pitch_top_left_x( const_p );
        _coord = get_pitch_top_left_y( const_p );
        _len = get_pitch_length( const_p );
        _len = get_pitch_width( const_p );

        _len = get_goal_area_width( const_p );
        _len = get_goal_area_length( const_p );

        _len = get_penalty_area_width( const_p );
        _len = get_penalty_area_length( const_p );
        _len = get_penalty_circle_radius( const_p );

        _len = get_goal_width( const_p );
        _len = get_goal_depth( const_p );

        _len = get_penalty_spot_distance( const_p );
        _len = get_center_circle_radius( const_p );
        _len = get_corner_arc_radius( const_p );
        _len = get_goal_post_radius( const_p );
    }

    P p;
    coordinate_t _coord;
    length_t _len;
};

} // end namespace soccer

#endif // SOCCER_PITCH_CONCEPT_HPP
