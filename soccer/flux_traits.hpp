#ifndef SOCCER_FLUX_TRAITS_HPP
#define SOCCER_FLUX_TRAITS_HPP

#include <soccer/core/access.hpp>

namespace soccer {

template < typename Flux >
struct flux_traits {
    typedef typename value_type< Flux >::type value_type;
    typedef typename position_type< Flux >::type position_type;
    typedef typename tag< Flux >::type flux_category;
};

template < typename Flux >
struct flux_triangulation_traits
    : public flux_traits<Flux>
{
    typedef typename triangulation_type< Flux >::type triangulation_type;
};

template < typename Flux >
struct flux_matrix_traits
    : public flux_traits<Flux>
{
    typedef typename size_type< Flux >::type size_type;

};

namespace detail {
    inline bool is_flux_matrix( flux_triangulation_tag ) { return false; }
    inline bool is_flux_matrix( flux_matrix_tag ) { return true; }

    inline bool is_flux_triangulation( flux_matrix_tag ) { return false; }
    inline bool is_flux_triangulation( flux_triangulation_tag ) { return true; }
}

template < typename Flux >
inline
bool is_flux_matrix(const Flux&) {
    typedef typename tag<Flux>::type Cat;
    return detail::is_flux_matrix(Cat());
}

template < typename Flux >
inline
bool is_flux_triangulation(const Flux&) {
    typedef typename tag<Flux>::type Cat;
    return detail::is_flux_triangulation(Cat());
}

} // end namespace soccer

#endif // SOCCER_FLUX_TRAITS_HPP
