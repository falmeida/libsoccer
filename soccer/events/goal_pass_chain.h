#ifndef SOCCER_EVENTS_GOAL_PASS_CHAIN_H
#define SOCCER_EVENTS_GOAL_PASS_CHAIN_H

#include <soccer/events/event_chain.h>

namespace soccer {
namespace event {

template < typename Time,
           typename TimeInterval = Time >
class GoalPassChain
        : public EventChain< Time, TimeInterval >
{
public:
    GoalPassChain()
    {

    }

    template <typename EventInputIterator>
    GoalPassChain( EventInputIterator first,
                   const EventInputIterator last)
        : EventChain< Time, TimeInterval >( first, last )
    {

    }

    GoalPassChain( const GoalPassChain& goal_pass_chain )
    {

    }

    EventType type() const { return EventType::GoalPassChain; }
};

}
}

#endif // SOCCER_EVENTS_GOAL_PASS_CHAIN_H
