#ifndef SOCCER_EVENTS_BALL_LOSS_H
#define SOCCER_EVENTS_BALL_LOSS_H

#include <soccer/events/player_event.hpp>

namespace soccer {
namespace event {

    template < typename PlayerId,
               typename Time,
               typename TimeInterval = Time >
    class BallLoss
            : public PlayerEvent< PlayerId, Time, TimeInterval >
    {
    public:
        BallLoss( Time time, PlayerId pid)
            : PlayerEvent< PlayerId, Time, TimeInterval >( time, pid ) { }
    };


}
}
#endif // SOCCER_EVENTS_BALL_LOSS_H

