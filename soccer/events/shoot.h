#ifndef SOCCER_EVENTS_SHOOT_H
#define SOCCER_EVENTS_SHOOT_H

#include <soccer/events/player_event.hpp>

namespace soccer {
namespace event {
	
        template < typename PlayerId,
                   typename Time,
                   typename TimeInterval = Time >
        class Shoot
                : public PlayerEvent<PlayerId,Time,TimeInterval> {
		public:
            Shoot( Time start_time, PlayerId shooter)
                : PlayerEvent<PlayerId,Time,TimeInterval>( start_time, shooter )
            {

            }

            EventType type() const { return EventType::Shoot; }
		};

}
}


#endif
