#ifndef SOCCER_EVENTS_BALL_KICK_H
#define SOCCER_EVENTS_BALL_KICK_H

#include <soccer/events/player_event.hpp>

namespace soccer {
namespace event {

    template < typename PlayerId,
               typename Time,
               typename TimeInterval = Time >
    class BallKick
            : public PlayerEvent< PlayerId, Time, TimeInterval>
    {
    public:
        BallKick( Time start_time, PlayerId pid)
            : PlayerEvent<PlayerId, Time,TimeInterval >( start_time, pid )
        {

        }

        EventType type() const { return EventType::BallKick; }
    };

}
}

#endif // SOCCER_EVENTS_BALL_KICK_H

