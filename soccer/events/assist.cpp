#include "assist.h"

namespace soccer {
namespace event {


    Assist::Assist(unsigned int start_time, unsigned int end_time,
                   TeamSide team_side,
                   soccer::Unum sender, soccer::Unum receiver)
        : Pass(start_time, end_time, team_side, sender, receiver)
	{

	}

}
}
