#ifndef SOCCER_EVENTS_PASS_CHAIN_H
#define SOCCER_EVENTS_PASS_CHAIN_H

#include <soccer/events/event_chain.h>

namespace soccer {
namespace event {

    /* template <typename Time,
              typename TimeInterval = Time>
    class PassChain
            : public EventChain<Time, TimeInterval>
    {
    public:
        PassChain();
        PassChain( const std::vector< std::tr1::shared_ptr<IEvent> >& events);
        PassChain(const PassChain& pass_chain);

        EventType type() const;

        void visit(EventVisitor& visitor) const;
    }; */

    // TODO Check if an event chain is a pass chain
    struct PassChainChecker {

        template <typename EventInputIterator>
        bool operator()( EventInputIterator first,
                         const EventInputIterator last )
        {
            /* Pass chain regexp => Pass (Pass|Dribble|Hold)+

            */
            for ( ; first != last ; first++ )
            {

            }
        }
    };
	
}
}

#endif
