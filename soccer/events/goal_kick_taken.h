#ifndef SOCCER_EVENTS_GOAL_KICK_TAKEN_H
#define SOCCER_EVENTS_GOAL_KICK_TAKEN_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {

        template < typename TeamSide,
                   typename Time,
                   typename TimeInterval = Time >
        class GoalKickTaken
                : public TeamEvent< TeamSide, Time, TimeInterval > {
        public:
            GoalKickTaken( Time time, TeamSide side)
                : TeamEvent< TeamSide, Time, TimeInterval >( time, side )
            {

            }

            EventType type() const { return EventType::GoalKickTaken; }

        };

}
}


#endif // SOCCER_EVENTS_GOAL_KICK_TAKEN_H
