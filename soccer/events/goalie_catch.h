#ifndef SOCCER_EVENTS_GOALIE_CATCH_H
#define SOCCER_EVENTS_GOALIE_CATCH_H

#include <soccer/events/team_event.hpp>
#include <soccer/types.h>

namespace soccer {
namespace event {

    template < typename TeamSide,
               typename Time,
               typename TimeInterval = Time >
    class GoalieCatch
            : public TeamEvent< TeamSide, Time, TimeInterval> {
    public:
        GoalieCatch(Time time, TeamSide side)
            : TeamEvent< TeamSide, Time,TimeInterval >( time, side )
        {

        }

        EventType type() const { return EventType::GoalieCatch; }

    };

}
}

#endif
