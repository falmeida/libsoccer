#ifndef SOCCER_EVENTS_GOAL_H
#define SOCCER_EVENTS_GOAL_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {

        template <typename TeamSide,
                  typename Time,
                  typename TimeInterval = Time>
        class Goal
                : public TeamEvent< TeamSide, Time, TimeInterval> {
		public:
            Goal(Time time, TeamSide side)
                : TeamEvent<TeamSide, Time, TimeInterval>( time, side )
            {

            }

            Goal(const Goal& other)
                : TeamEvent<TeamSide, Time, TimeInterval>( other )
            {

            }

            EventType type() const { return EventType::Goal; }

		};

}
}

#endif
