#ifndef SOCCER_EVENTS_KICK_OFF_TAKEN_H
#define SOCCER_EVENTS_KICK_OFF_TAKEN_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {


        template <typename TeamSide,
                  typename Time,
                  typename TimeInterval = Time>
        class KickOffTaken
                : public TeamEvent< TeamSide, Time, TimeInterval> {
        public:
            KickOffTaken(Time start_time, TeamSide side )
                : TeamEvent< TeamSide, Time, TimeInterval>( start_time, side )
            {

            }

            EventType type() const { return EventType::KickOffTaken; }

        };

}
}

#endif // KICK_OFF_TAKEN_H
