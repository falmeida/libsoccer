#ifndef SOCCER_EVENTS_POSSESSION_CHANGED_H
#define SOCCER_EVENTS_POSSESSION_CHANGED_H

#include <soccer/events/event.h>
#include <soccer/types.h>

namespace soccer {
namespace event {

    template < typename TeamSide,
               typename Time,
               typename TimeInterval = Time >
    class PossessionChanged
            : public Event<Time, TimeInterval>
    {
    public:
        PossessionChanged(Time start_time, TeamSide team_in_possession)
            : Event<Time,TimeInterval>( start_time )
            , M_team_in_possession( team_in_possession )
        {

        }

        EventType type() const { return EventType::PossessionChanged; }
        TeamSide teamInPossession() const { return M_team_in_possession; }
    private:
        TeamSide M_team_in_possession;
    };

}
}

#endif // POSSESSION_CHANGED_H
