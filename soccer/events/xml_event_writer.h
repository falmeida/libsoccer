#ifndef SOCCER_EVENTS_XML_EVENT_WRITER_H
#define SOCCER_EVENTS_XML_EVENT_WRITER_H

// Mandatory for using any feature of Xerces.
#include <xercesc/util/PlatformUtils.hpp>

// For character transcoding operations
#include <xercesc/util/XMLString.hpp>

#include <xercesc/util/OutOfMemoryException.hpp>

// Required for outputing a Xerces DOMDocument
// to a standard output stream (Also see: XMLFormatTarget)
#include <xercesc/framework/StdOutFormatTarget.hpp>

// Required for outputing a Xerces DOMDocument
// to the file system (Also see: XMLFormatTarget)
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <soccer/events/xml_event_common.h>
#include <soccer/events/ievent_visitor.h>

#include <sstream>

#include <stack>
#include <string>

// ---------------------------------------------------------------------------
//  Includes
// ---------------------------------------------------------------------------
#include <xercesc/dom/DOM.hpp>

namespace soccer {
namespace event {

class XmlEventWriter
        : public IEventVisitor< std::pair< int, TeamSide >,
                                TeamSide,
                                int >
{
public:
    XmlEventWriter( )
    {
        try
        {
            XMLPlatformUtils::Initialize();
        }
        catch (const XMLException& toCatch)
        {
            // Do your failure processing here
            std::cerr << "Could not initialize platform utils" << std::endl;
            assert( false );
        }

        DOMImplementation* dom_impl = DOMImplementationRegistry::getDOMImplementation( X("Core") );

        if ( dom_impl == NULL)
        {
            XERCES_STD_QUALIFIER cerr << "Requested implementation is not supported" << XERCES_STD_QUALIFIER endl;
            // errorCode = 4;
            assert( false );
        }

        try
        {
            M_dom_doc = dom_impl->createDocument( 0,                    // root element namespace URI.
                                                  X("events"),          // root element name
                                                  0);                   // document type object (DTD).
            M_root_elem = M_dom_doc->getDocumentElement();
        }
        catch (const OutOfMemoryException&)
        {
            XERCES_STD_QUALIFIER cerr << "OutOfMemoryException" << XERCES_STD_QUALIFIER endl;
            // errorCode = 5;
            assert(false);
        }
        catch (const DOMException& e)
        {
            XERCES_STD_QUALIFIER cerr << "DOMException code is:  " << e.code << XERCES_STD_QUALIFIER endl;
            // errorCode = 2;
            assert(false);
        }
        catch (...)
        {
            XERCES_STD_QUALIFIER cerr << "An error occurred creating the document" << XERCES_STD_QUALIFIER endl;
            // errorCode = 3;
            assert(false);
        }
    }

    ~XmlEventWriter()
    {
        // Cleanup
        M_dom_doc->release();
        XMLPlatformUtils::Terminate();
    }

    void operator()(const kick_off_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("kick-off", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const kick_off_taken_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("kick-off-taken", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const throw_in_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("throw-in", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const throw_in_taken_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("throw-in-taken", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const goalie_catch_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("goal-kick", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const goal_kick_taken_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("goal-kick-taken", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const corner_kick_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("corner-kick", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const corner_kick_taken_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("corner-kick-taken", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const free_kick_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("free-kick", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const free_kick_taken_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("free-kick-taken", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const indirect_free_kick_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("indirect-free-kick", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const indirect_free_kick_taken_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("indirect-free-kick-taken", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const goal_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("goal", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const soccer_moment_changed_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("soccer-moment-changed", event );
        appendTeamSideAttribute( elem, "teamSide", event.teamMoment().teamSide() );
        elem->setAttribute( X("moment"),
                            X( SoccerMomentChanged::TeamSideMoment::moment( event.teamMoment().moment()).c_str() ) );
        appendChildElement( elem );
    }

    void operator()(const possession_changed_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("possession-changed", event );
        appendTeamSideAttribute( elem, "teamInPossession", event.teamInPossession() );
        appendChildElement( elem );
    }

    void operator()(const shoot_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("shoot", event );
        appendTeamSideAttribute( elem, "teamSide", event.shooter().side() );
        appendPlayerUnumAttribute( elem, "number", event.shooter().number() );
        appendChildElement( elem );
    }

    void operator()(const dribble_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("dribble", event );
        appendTeamSideAttribute( elem, "teamSide", event.dribbler().side() );
        appendPlayerUnumAttribute( elem, "number", event.dribbler().number() );
        appendChildElement( elem );
    }

    void operator()(const pass_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("pass", event );
        appendTeamSideAttribute( elem, "teamSide", event.teamSide() );
        appendPlayerUnumAttribute( elem, "sender", event.sender() );
        appendPlayerUnumAttribute( elem, "receiver", event.receiver() );
        appendChildElement( elem );
    }

    void operator()(const interception_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("interception", event );
        appendTeamSideAttribute( elem, "teamSide", event.interceptor().side() );
        appendPlayerUnumAttribute( elem, "number", event.interceptor().number() );
        appendChildElement( elem );
    }
    // void operator()(const Turnover& event);

    void operator()(const player_move_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("player-move", event );
        appendTeamSideAttribute( elem, "teamSide", event.player().side() );
        appendPlayerUnumAttribute( elem, "number", event.player().number() );
        appendChildElement( elem );
        appendVector( elem, "from", event.from() );
        appendVector( elem, "to", event.to() );
    }

    void operator()(const goalie_catch_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("goalie-catch", event );
        appendTeamSideAttribute( elem, "teamSide", event.teamSide() );
        appendChildElement( elem );
    }

    void operator()(const ball_kick_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("ball-kick", event );
        appendTeamSideAttribute( elem, "teamSide", event.kicker().side() );
        appendPlayerUnumAttribute( elem, "number", event.kicker().number() );
        appendChildElement( elem );
    }

    void operator()(const ball_dispute_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("ball-dispute", event );
        for ( std::vector<player_id>::const_iterator itp = event.players().begin();
              itp != event.players().end() ; itp++ )
        {
            DOMElement* player_elem = M_dom_doc->createElement( X("player") );
            appendTeamSideAttribute( player_elem, "teamSide", (*itp).side() );
            appendPlayerUnumAttribute( player_elem, "number", (*itp).number() );
            elem->appendChild( player_elem );
        }
        appendChildElement( elem );
    }
    void operator()(const ball_move_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("ball-move", event );
        appendChildElement( elem );
        appendVector( elem, "from", event.from() );
        appendVector( elem, "to", event.to() );
    }

    void operator()(const ball_hold_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("ball-hold", event );
        appendTeamSideAttribute( elem, "teamSide", event.holder().side() );
        appendPlayerUnumAttribute( elem, "number", event.holder().number() );
        appendChildElement( elem );
    }

    void operator()(const offside_t& event)
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes("offside", event );
        appendTeamSideAttribute( elem, "teamSide", event.side() );
        appendChildElement( elem );
    }

    void operator()(const pass_chain_t& event)
    {
        createEventChain( "pass-chain", event);
    }

    void operator()(const goal_pass_chain_t& event )
    {
        createEventChain( "goal-pass-chain", event);
    }

    void write( const std::string& file_name,
                const std::string& source = "")
    {
        XERCES_CPP_NAMESPACE::DOMImplementation* pImplement  = NULL;
        XERCES_CPP_NAMESPACE::DOMLSSerializer*   pSerializer = NULL;

        M_root_elem->setAttribute( X("source"), X( source.c_str() ) );

        /*
        Return the first registered implementation that has
        the desired features. In this case, we are after
        a DOM implementation that has the LS feature... or Load/Save.
        */
        pImplement = XERCES_CPP_NAMESPACE::DOMImplementationRegistry::getDOMImplementation( X("LS") );

        /*
        From the DOMImplementation, create a DOMSerializer.
        DOMSerializer are used to serialize a DOM tree [back] into an XML document.
        */
        pSerializer = pImplement->createLSSerializer();

        /*
        This line is optional. It just sets a feature
        of the Serializer to make the output
        more human-readable by inserting line-feeds and tabs,
        without actually inserting any new nodes
        into the DOM tree. (There are many different features to set.)
        Comment it out and see the difference.
        */
        pSerializer->getDomConfig()->setParameter( XERCES_CPP_NAMESPACE::XMLUni::fgDOMWRTFormatPrettyPrint, true );

        /*
        Choose a location for the serialized output. The 3 options are:
            1) StdOutFormatTarget     (std output stream -  good for debugging)
            2) MemBufFormatTarget     (to Memory)
            3) LocalFileFormatTarget  (save to file)
            (Note: You'll need a different header file for each one)
        */
        // pTarget = new LocalFileFormatTarget( X(file_name.c_str()) );

        // Write the serialized output to the target.
        pSerializer->writeToURI( M_dom_doc, X(file_name.c_str()) );
    }

private:
    void createEventChain( const std::string& label,
                           const EventChain &event )
    {
        XERCES_CPP_NAMESPACE::DOMElement* elem = createElementWithBaseAttributes( label, event );
        /// TODO Echo all inner elements
        appendChildElement( elem );
        M_stack.push( elem );

        for ( auto ite = event.begin(); ite != event.end(); ite++ )
        {
            (*ite)->visit( *this );
        }
        M_stack.pop();
    }
    void appendChildElement( xercesc::DOMElement* elem )
    {
        if ( !M_stack.empty() )
        {
            XERCES_CPP_NAMESPACE::DOMElement* parent = M_stack.top();
            parent->appendChild( elem );
            return;
        }

        M_root_elem->appendChild( elem );
    }

    xercesc::DOMElement* createElementWithBaseAttributes( const std::string type_attr_value,
                                                          const ievent_t& event )
    {
        // XERCES_CPP_NAMESPACE::DOMElement* element = M_dom_doc->createElement( X("event") );
        // element->setAttribute( X("type") , X(type_attr_value.c_str()) );
        XERCES_CPP_NAMESPACE::DOMElement* element = M_dom_doc->createElement( XERCES_CPP_NAMESPACE::X(type_attr_value.c_str()) );

        std::std::stringstream ss_start_time;
        ss_start_time << event.startTime();

        element->setAttribute( XERCES_CPP_NAMESPACE::X("startTime") ,
                               XERCES_CPP_NAMESPACE::X( ss_start_time.str().c_str()) );
        if ( event.duration() > 0 )
        {
            std::std::stringstream ss_end_time;
            ss_end_time << event.endTime();

            element->setAttribute( XERCES_CPP_NAMESPACE::X("endTime") ,
                                   XERCES_CPP_NAMESPACE::X( ss_end_time.str().c_str() ) );
        }
        return element;
    }

    void appendTeamSideAttribute( xercesc::DOMElement* element,
                                  const std::string& label,
                                  const TeamSide& side )
    {
        element->setAttribute( XERCES_CPP_NAMESPACE::X(label.c_str()), XERCES_CPP_NAMESPACE::X( teamSide( side ).c_str() ));
    }

    void appendPlayerUnumAttribute( xercesc::DOMElement* element,
                                  const std::string& label,
                                  const unsigned int& unum )
    {
        std::stringstream ss;
        ss << unum;
        element->setAttribute( X(label.c_str()), X( ss.str().c_str() ));
    }

    void appendVector( xercesc::DOMElement* element,
                       const std::string& label,
                       const rcsc::Vector2D& vector )
    {
        XERCES_CPP_NAMESPACE::DOMElement* vector_elem = M_dom_doc->createElement( X(label.c_str()) );
        element->appendChild( vector_elem );

        std::stringstream ssx;
        ssx << vector.x;
        vector_elem->setAttribute( XERCES_CPP_NAMESPACE::X("x"),
                                   XERCES_CPP_NAMESPACE::X( ssx.str().c_str() ) );

        std::stringstream ssy;
        ssy << vector.y;
        vector_elem->setAttribute( XERCES_CPP_NAMESPACE::X("y"),
                                   XERCES_CPP_NAMESPACE::X( ssy.str().c_str() ) );
    }

    xercesc::DOMDocument* M_dom_doc;
    xercesc::DOMElement* M_root_elem;
    std::stack< xercesc::DOMElement* > M_stack;

};

}
}

#endif // XML_EVENT_WRITER_H
