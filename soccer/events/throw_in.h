#ifndef SOCCER_EVENTS_THROW_IN_H
#define SOCCER_EVENTS_THROW_IN_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {

        template < typename TeamSide,
                   typename Time,
                   typename TimeInterval = Time >
        class ThrowIn
                : public TeamEvent< TeamSide, Time, TimeInterval> {
		public:
            ThrowIn( Time start_time, TeamSide side)
                : TeamEvent< TeamSide, Time, TimeInterval>( start_time, side )
            {

            }


            EventType type() const { return EventType::ThrowIn; }

		};

}
}

#endif
