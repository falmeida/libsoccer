#ifndef SOCCER_EVENTS_IEVENT_H
#define SOCCER_EVENTS_IEVENT_H

#include <map>
#include <string>

namespace soccer  {
namespace event {

        enum class EventType {
            Null,
            EventChain,
            ThrowIn,
            ThrowInTaken,
            KickOff,
            KickOffTaken,
            CornerKick,
            CornerKickTaken,
            FreeKick,
            FreeKickTaken,
            IndirectFreeKick,
            IndirectFreeKickTaken,
            GoalKick,
            GoalKickTaken,
            Goal,
            GoalOpportunity,
            Offside,
            Dribble,
            Pass,
            Shoot,
            BallDispute,
            BallKick,
            BallHold,
            GoalieCatch,
            Interception,
            BallMove,
            PlayerMove,
            PossessionChanged,
            Turnover,
            SoccerMomentChanged,
            GoalPassChain,
            PassChain
        };

        template < typename Time,
                   typename TimeInterval = Time >
		class IEvent {
		public:
            typedef Time time_descriptor;
            typedef TimeInterval time_interval_descriptor;

			virtual ~IEvent() { }

            TimeInterval duration() const { return endTime() - startTime(); }
            virtual Time startTime() const = 0;
            virtual Time endTime() const = 0;

            virtual EventType type() const = 0;

            /// For event comparison
            bool operator<( const IEvent& other )
            {
                if ( startTime() == other.startTime() )
                {
                    return endTime() < other.endTime();
                }

                return startTime() < other.startTime();
            }

            virtual bool isEventChain() { return false; }

		};

        template < typename Time,
                   typename TimeInterval = Time >
        class IEventChain
            : public IEvent< Time,TimeInterval > {
        public:
            typedef IEvent< Time,TimeInterval > event_type;
            typedef IEvent< Time,TimeInterval >* event_pointer;

            virtual Time startTime() const = 0;
            virtual Time endTime() const = 0;
            virtual unsigned int size() const = 0;

            virtual EventType type() const { return EventType::EventChain; }
            virtual bool isEventChain() { return true; }

            virtual void push_back( event_pointer ) = 0;
        };

        std::map<EventType, std::string> createEventTypeToStringMap();
        std::map<std::string, EventType> createStringToEventTypeMap();

        std::string eventType( const EventType& event_type );
        EventType eventType( const std::string& event_type_str );

}
}
#endif // SOCCER_EVENTS_IEVENT_H
