#include "xml_event_parser.h"

// Mandatory for using any feature of Xerces.
#include <xercesc/util/PlatformUtils.hpp>

#include <xercesc/sax2/XMLReaderFactory.hpp>

#include <xercesc/sax2/Attributes.hpp>

#include <soccer/events/xml_event_common.h>

#include <soccer/register/player_id.hpp>
// #include <soccer/stl/pair_as_player_id.hpp>


typedef std::pair< unsigned int, soccer::TeamSide > player_id_t;
SOCCER_DETAIL_SPECIALIZE_PLAYER_ID_TRAITS( player_id_t, soccer::TeamSide, unsigned int )


#include <iostream>

using namespace std::tr1;

XERCES_CPP_NAMESPACE_USE

namespace soccer {
namespace event {

XmlEventParser::XmlEventParser()
{
    XMLPlatformUtils::Initialize();

    M_parser = XMLReaderFactory::createXMLReader();
    M_parser->setContentHandler( this );

}

XmlEventParser::~XmlEventParser()
{

    XMLPlatformUtils::Terminate();
}

void XmlEventParser::parse( const std::string& file_name )
{
    M_parser->parse( file_name.c_str() );
}

const std::string& XmlEventParser::source() const
{
    return M_source;
}


// -----------------------------------------------------------------------
//  Implementations of the SAX DocumentHandler interface
// -----------------------------------------------------------------------
void XmlEventParser::characters( const XMLCh* const chars,
                                 const XMLSize_t length)
{

}

void XmlEventParser::endDocument()
{

}

void XmlEventParser::endElement ( const XMLCh* const uri,
                                  const XMLCh* const localname,
                                  const XMLCh* const qname)
{
    std::string elem_name = S(localname);
    if ( elem_name == "from" || elem_name == "to" )
     {
        return;
    }

    if ( !M_stack.empty() )
    {
        EventType event_type = eventType( elem_name );

        shared_ptr< ievent_visitor_t::ievent_t > last_event = M_stack.top();
        if ( last_event->type() == event_type )
        {
            M_stack.pop();
        }
    }
}

void XmlEventParser::ignorableWhitespace ( const XMLCh* const chars,
                                           const XMLSize_t length )
{

}

void XmlEventParser::processingInstruction ( const XMLCh* const target,
                                             const XMLCh* const data )
{

}

void XmlEventParser::setDocumentLocator(const Locator* const locator)
{

}

void XmlEventParser::startDocument()
{

}

void XmlEventParser::startElement ( const XMLCh* const uri,
                                    const XMLCh* const localname,
                                    const XMLCh* const qname,
                                    const Attributes& attrs )
{
    unsigned int start_time = 0u;
    unsigned int end_time = 0u;
    std::string elem_name( S( localname ) );

    if ( elem_name == "events" )
    {
        const XMLCh* source_value_ch = attrs.getValue( X("source") );
        if ( source_value_ch != NULL )
        {
            M_source = std::string( S(source_value_ch) );
        }
        // Ignore events element
        return;
    }

    if ( elem_name == "player" )
    {
        shared_ptr< ievent_visitor_t::ievent_t > last_event = M_events.back();
        // TODO HACK !!! Check type of last event quickly

        // Continue the instantiation of a previous event
        switch ( last_event->type() )
        {
            case EventType::BallDispute:
            {
                typename ievent_visitor_t::ball_dispute_t* ball_dispute = static_cast< typename ievent_visitor_t::ball_dispute_t* >( last_event.get() );

                /// Parse a player element
                TeamSide team_side = TeamSide::Unknown;
                parseTeamSideAttribute( attrs, "teamSide", team_side);
                unsigned int number = 0u;
                parsePlayerNumberAttribute( attrs, "number", number );

                ball_dispute->addPlayer( ievent_visitor_t::player_id_t( number, team_side ) );
            }
            break;
            default:
            {
                std::cerr << "XmlEventParser: Unhandled/Illegal event type "
                          << eventType( last_event->type() )  << " continued instantiation " << std::endl;
                assert(false);
            }
            break;
        }
        return;
    }

    if ( elem_name == "from" )
    {
        typename ievent_visitor_t::position_t pos;
        parseVector( attrs, pos );
        shared_ptr< typename ievent_visitor_t::ievent_t> event = M_stack.top();

        switch ( event->type() )
        {
            case EventType::BallMove:
            {
                typename ievent_visitor_t::ball_move_t* ball_move = static_cast<ievent_visitor_t::ball_move_t*>( event.get() );
                ball_move->setFrom( pos );
            }
            break;
            case EventType::PlayerMove:
            {
                ievent_visitor_t::player_move_t* player_move = static_cast< ievent_visitor_t::player_move_t*>( event.get() );
                player_move->setFrom( pos );
            }
            break;
            default:
                assert(false);
                break;
        }

        return;
    }

    if ( elem_name == "to" )
    {
        typename ievent_visitor_t::position_t pos;
        parseVector( attrs, pos );
        shared_ptr<ievent_visitor_t::ievent_t> event = M_stack.top();

        switch ( event->type() )
        {
            case EventType::BallMove:
            {
                ievent_visitor_t::ball_move_t* ball_move = static_cast<ievent_visitor_t::ball_move_t*>( event.get() );
                ball_move->setTo( pos );
            }
            break;
            case EventType::PlayerMove:
            {
                ievent_visitor_t::player_move_t* player_move = static_cast<ievent_visitor_t::player_move_t*>( event.get() );
                player_move->setTo( pos );
            }
            break;
            default:
                assert(false);
                break;
        }

        return;
    }

    EventType event_type = eventType( elem_name );

    // Parse individual event
    parseEventBaseAttributes( attrs, start_time, end_time);

    shared_ptr< typename ievent_visitor_t::ievent_t > new_event;

    switch ( event_type )
    {
        case EventType::KickOff:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::kick_off_t( start_time, team_side ));
        }
        break;
        case EventType::KickOffTaken:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::kick_off_t( start_time, team_side ));
        }
        break;
        case EventType::ThrowIn:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::throw_in_t( start_time, team_side ));
        }
        break;
        case EventType::ThrowInTaken:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::throw_in_taken_t( start_time, team_side ));
        }
        break;
        case EventType::GoalKick:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::goal_kick_t( start_time, team_side ));
        }
        break;
        case EventType::GoalKickTaken:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::goal_kick_taken_t( start_time, team_side ));
        }
        break;
        case EventType::CornerKick:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::corner_kick_t( start_time, team_side ));
        }
        break;
        case EventType::CornerKickTaken:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::corner_kick_taken_t( start_time, team_side ));
        }
        break;
        case EventType::FreeKick:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::free_kick_t( start_time, team_side ));
        }
        break;
        case EventType::FreeKickTaken:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::free_kick_taken_t( start_time, team_side ));
        }
        break;
        case EventType::IndirectFreeKick:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::indirect_free_kick_t( start_time, team_side ));
        }
        break;
        case EventType::IndirectFreeKickTaken:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::indirect_free_kick_taken_t( start_time, team_side ));
        }
        break;
        case EventType::SoccerMomentChanged:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side );
            const XMLCh* moment_ch = attrs.getValue( X("moment") );
            assert( moment_ch != NULL );
            SoccerMoment moment = soccerMoment( std::string( S( moment_ch ) ) );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::soccer_moment_changed_t( start_time,
                                                                     ievent_visitor_t::soccer_moment_changed_t::TeamSideMoment( team_side, moment) ));
        }
        break;
        case EventType::PossessionChanged:
        {
            TeamSide team_in_possession = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamInPossession", team_in_possession  );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::possession_changed_t( start_time, team_in_possession ));
        }
        break;
        case EventType::Interception:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side   );
            unsigned int number = 0u;
            parsePlayerNumberAttribute( attrs, "number", number );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::interception_t( start_time,
                                                                                                         ievent_visitor_t::player_id_t( number, team_side ) ));
        }
        break;
        /* case Turnover:
        {
            TeamSide team_in_possession = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamInPossession", team_in_possession  );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new Turnover( start_time, team_in_possession ));
        }*/
        break;
        case EventType::BallDispute:
        {
            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::ball_dispute_t( start_time ));
        }
        break;
        case EventType::BallHold:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);
            unsigned int number = 0u;
            parsePlayerNumberAttribute( attrs, "number", number );

            new_event = shared_ptr< typename ievent_visitor_t::ievent_t>( new ievent_visitor_t::ball_hold_t( start_time, end_time,
                                                                                typename ievent_visitor_t::player_id_t( number, team_side )));
        }
        break;
        case EventType::GoalieCatch:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::goalie_catch_t( start_time, team_side ));
        }
        break;
        case EventType::BallKick:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);
            unsigned int number = 0u;
            parsePlayerNumberAttribute( attrs, "number", number );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::ball_kick_t( start_time,
                                                            typename ievent_visitor_t::player_id_t( number, team_side )));
        }
        break;
        case EventType::Goal:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::goal_t( start_time, team_side ));
        }
        break;
        case EventType::Shoot:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);
            unsigned int number = 0u;
            parsePlayerNumberAttribute( attrs, "number", number );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::shoot_t( start_time,
                                                                                                  typename ievent_visitor_t::player_id_t( number, team_side)) );
        }
        break;
        case EventType::Dribble:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);
            unsigned int number = 0u;
            parsePlayerNumberAttribute( attrs, "number", number );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::dribble_t( start_time,
                                                                                                    end_time,
                                                                                                    typename ievent_visitor_t::player_id_t( number, team_side)));
        }
        break;
        case EventType::PlayerMove:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);
            unsigned int number = 0u;
            parsePlayerNumberAttribute( attrs, "number", number );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::player_move_t( start_time,
                                                                                                        end_time,
                                                                                                        typename ievent_visitor_t::player_id_t( number, team_side) ));
            M_stack.push( new_event );
            M_events.push_back( new_event );
            return;
        }
        break;
        case EventType::BallMove:
        {
            new_event = shared_ptr< ievent_visitor_t::ievent_t >( new ievent_visitor_t::ball_move_t( start_time, end_time) );
            M_stack.push( new_event );
            M_events.push_back( new_event );
            return;
        }
        break;
        case EventType::Pass:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);
            typename soccer::traits::player_id_type< ievent_visitor_t::player_id_t >::number_type
                    receiver_unum = 0u;
            parsePlayerNumberAttribute( attrs, "receiver", receiver_unum );
            typename soccer::traits::player_id_type< ievent_visitor_t::player_id_t >::number_type
                    sender_unum = 0u;
            parsePlayerNumberAttribute( attrs, "sender", sender_unum );
            typename ievent_visitor_t::player_id_t receiver( receiver_unum, team_side );
            typename ievent_visitor_t::player_id_t sender( sender_unum, team_side );

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::pass_t( start_time, end_time, sender, receiver));
        }
            break;
        case EventType::Offside:
        {
            TeamSide team_side = TeamSide::Unknown;
            parseTeamSideAttribute( attrs, "teamSide", team_side);

            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::offside_t( start_time, team_side));
        }
        break;
        case EventType::PassChain:
        {
            std::clog << "Not handling Pass-Chain Event" << std::endl;
            assert(false );
            // TODO Handle pass-chain events
            /*new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::pass_chain_t( ) );
            if ( M_stack.empty() )
            {
                M_events.push_back( new_event );
            }
            else
            {
                ievent_visitor_t::ievent_chain_t* event_chain = static_cast<ievent_visitor_t::ievent_chain_t*>( M_stack.top().get() );
                event_chain->push_back( new_event );
            }
            M_stack.push(new_event);
            return;*/
        }
        break;
        case EventType::GoalPassChain:
        {
            // TODO Handle pass-chain events
            new_event = shared_ptr< ievent_visitor_t::ievent_t > ( new ievent_visitor_t::goal_pass_chain_t( ) );
            M_stack.push( new_event );
            M_events.push_back( new_event );
            return;
        }
        break;
        default:
        {
            std::cerr << "XmlEventParser: Unhandled/Illegal event type"
                      << elem_name << std::endl;
            assert(false);
        }
        break;
    }

    if ( !M_stack.empty() )
    {
        /// Get the a reference to the last element inserted
        shared_ptr< typename ievent_visitor_t::ievent_t > last_event = M_stack.top();
        switch ( last_event->type() )
        {
            case EventType::PassChain:
            case EventType::GoalPassChain:
            {
                ievent_visitor_t::ievent_chain_t* event_chain = static_cast< ievent_visitor_t::ievent_chain_t* >( last_event.get() );
                event_chain->push_back( new_event.get() );
            }
            break;
            default:
            {
                std::cerr << "XmlEventParser: Unexpected element at the top of the stack "
                          << eventType( last_event->type() )
                          << std::endl;
                assert( false );
            }
            break;
        }
        return;
    }

    M_events.push_back( new_event );
}

void XmlEventParser::startPrefixMapping( const XMLCh* const prefix,
                                         const XMLCh* const uri )
{

}

void XmlEventParser::endPrefixMapping( const XMLCh* const prefix)
{

}

void XmlEventParser::skippedEntity( const XMLCh* const name )
{
    std::cerr << "Skipped parsing " << name << std::endl;
}

bool XmlEventParser::parseEventBaseAttributes( const Attributes& attrs,
                                               unsigned int& start_time,
                                               unsigned int& end_time )
{
    const XMLCh* start_time_ch = attrs.getValue( X("startTime" ));
    assert( start_time_ch != NULL );
    start_time = atoi( S(start_time_ch) );

    const XMLCh* end_time_ch = attrs.getValue( X("endTime" ));
    if ( end_time_ch != NULL )
    {
        end_time = atoi( S(end_time_ch) );
    }
    else
    {
        end_time = start_time;
        return false;
    }
    return true;
}

bool XmlEventParser::parseTeamSideAttribute( const Attributes& attrs,
                                             const char* attr_name,
                                             TeamSide& team_side )
{
    const XMLCh* team_side_ch = attrs.getValue( X(attr_name) );

    if ( team_side_ch == NULL )
    {
        return false;
    }

    team_side = teamSide( S( team_side_ch ) );

    return true;
}

bool XmlEventParser::parsePlayerNumberAttribute( const Attributes& attrs,
                                                 const char* attr_name,
                                                 unsigned int& number)
{
    const XMLCh* number_ch = attrs.getValue( X(attr_name) );

    if ( number_ch == NULL )
    {
        return false;
    }

    number = atoi( S( number_ch ) );

    return true;
}

bool XmlEventParser::parseVector( const xercesc::Attributes& attrs,
                                  ievent_visitor_t::position_t& pos)
{
    const XMLCh* coordx_ch = attrs.getValue( X("x") );
    if ( coordx_ch == NULL )
    {
        return false;
    }

    float x = atof( S( coordx_ch ) );

    const XMLCh* coordy_ch = attrs.getValue( X("y") );
    if ( coordy_ch == NULL )
    {
        return false;
    }

    float y = atof( S( coordy_ch ) );

    pos.first = x;
    pos.second = y;

    return true;
}

}
}
