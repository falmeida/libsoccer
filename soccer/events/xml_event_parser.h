#ifndef SOCCER_EVENTS_XML_EVENT_PARSER_H
#define SOCCER_EVENTS_XML_EVENT_PARSER_H

#include <soccer/events/ievent_visitor.h>
#include <soccer/types.h>

#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/ContentHandler.hpp>

#include <tr1/memory>

#include <string>
#include <vector>

#include <stack>
#include <utility>

namespace soccer {
namespace event {


class XmlEventParser
        : protected xercesc::ContentHandler
{
public:
    typedef IEventVisitor< std::pair< float, float >,
                           std::pair< unsigned int, TeamSide >,
                           TeamSide,
                           int > ievent_visitor_t;

    XmlEventParser();
    ~XmlEventParser();

    void parse( const std::string& file_name );

    std::vector< std::tr1::shared_ptr< typename ievent_visitor_t::ievent_t> > M_events;

    const std::string& source() const;
protected:

    // -----------------------------------------------------------------------
    //  Implementations of the SAX DocumentHandler interface
    // -----------------------------------------------------------------------
    void characters( const XMLCh* const chars,
                     const XMLSize_t length);

    void endDocument();

    void endElement ( const XMLCh* const uri,
                      const XMLCh* const localname,
                      const XMLCh* const qname);

    void ignorableWhitespace ( const XMLCh* const chars,
                               const XMLSize_t length );

    void processingInstruction ( const XMLCh* const target,
                                 const XMLCh* const data );

    void setDocumentLocator(const xercesc::Locator* const locator);

    void startDocument();

    void startElement ( const   XMLCh* const    uri,
                        const   XMLCh* const    localname,
                        const   XMLCh* const    qname,
                        const   xercesc::Attributes&     attrs );

    void startPrefixMapping( const XMLCh* const prefix,
                             const XMLCh* const uri );

    void endPrefixMapping( const XMLCh* const prefix);

    void skippedEntity( const XMLCh* const name );

private:
    bool parseEventBaseAttributes( const xercesc::Attributes& attrs,
                                   unsigned int& start_time,
                                   unsigned int& end_time );
    bool parseTeamSideAttribute( const xercesc::Attributes& attrs,
                                 const char* attr_name,
                                 TeamSide& side );

    bool parsePlayerNumberAttribute( const xercesc::Attributes& attrs,
                                     const char* attr_name,
                                     unsigned int& number);

    bool parseVector( const xercesc::Attributes& attrs,
                      ievent_visitor_t::position_t& pos );

    xercesc::SAX2XMLReader* M_parser;
    std::stack< std::tr1::shared_ptr< typename ievent_visitor_t::ievent_t > > M_stack;

    std::string M_source;
};

}
}

#endif // SOCCER_EVENTS_XML_EVENT_PARSER_H
