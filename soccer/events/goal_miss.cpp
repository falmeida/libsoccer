#include "goal_miss.h"


namespace soccer {
namespace event {

	GoalMiss::GoalMiss(unsigned time, GoalMissType miss_type)
		: Event(time)
		, M_miss_type(miss_type)
	{

	}


	GoalMiss::type() const
	{
		return M_miss_type;
	}

}
}
