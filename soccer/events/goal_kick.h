#ifndef SOCCER_EVENTS_GOAL_KICK_H
#define SOCCER_EVENTS_GOAL_KICK_H

#include <soccer/events/event.h>

namespace soccer {
namespace event {
	
        template <typename TeamSide,
                  typename Time,
                  typename TimeInterval = Time>
        class GoalKick
                : public Event<Time,TimeInterval> {
		public:
            GoalKick(Time time, TeamSide side)
                : Event<Time,TimeInterval>( time )
                , M_side( side )
            {

            }

            EventType type() const { return EventType::GoalKick; }
            TeamSide side() const { return M_side; }

		private:
            TeamSide M_side;
		};

}
}


#endif // SOCCER_EVENTS_GOAL_KICK_H
