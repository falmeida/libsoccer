#ifndef SOCCER_EVENTS_OFFSIDE_H
#define SOCCER_EVENTS_OFFSIDE_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {
		
    template < typename TeamSide,
               typename Time,
               typename TimeInterval = Time>
    class Offside
            : public TeamEvent< TeamSide, Time, TimeInterval> {
    public:
        Offside( Time start_time, TeamSide side )
            : TeamEvent< TeamSide, Time, TimeInterval>( start_time, side )
        {
        }

        EventType type() const { return EventType::Offside; }

    };

}
}

#endif // SOCCER_EVENTS_OFFSIDE_H
