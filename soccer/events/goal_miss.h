#ifndef GOAL_MISS_H
#define GOAL_MISS_H

include "event.h"

namespace soccer {
namespace event {

class GoalMiss: public Event {
public:
	typedef enum { OUT_OF_BOUNDS, GOALIE_CATCH } GoalMissType;

	GoalMiss(unsigned int time, GoalMissType miss_type);

	GoalMissType type() const;
private:
	GoalMissType M_miss_type;
};

}
}

#endif
