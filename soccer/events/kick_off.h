#ifndef SOCCER_EVENTS_KICK_OFF_H
#define SOCCER_EVENTS_KICK_OFF_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {

        template < typename TeamSide,
                   typename Time,
                   typename TimeInterval = Time>
        class KickOff
                : public TeamEvent< TeamSide, Time, TimeInterval> {
		public:
            KickOff( Time start_time, TeamSide side)
                : TeamEvent< TeamSide, Time,TimeInterval >( start_time, side )
            {

            }

            EventType type() const { return EventType::KickOff; }

		};

}
}


#endif
