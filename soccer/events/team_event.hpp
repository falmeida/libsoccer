#ifndef SOCCER_EVENTS_TEAM_EVENT_HPP
#define SOCCER_EVENTS_TEAM_EVENT_HPP

#include <soccer/events/event.h>

namespace soccer {
namespace event {

template < typename TeamSide,
           typename Time,
           typename TimeInterval = Time >
class TeamEvent
        : public Event< Time, TimeInterval >
{
public:
    TeamEvent( Time time, TeamSide team_side )
        : Event< Time, TimeInterval>( time )
        , M_team_side( team_side )
    {

    }

    TeamEvent( Time start_time, Time end_time, TeamSide team_side )
        : Event< Time, TimeInterval>( start_time, end_time )
        , M_team_side( team_side )
    {

    }

    TeamSide teamSide() const { return M_team_side; }

    private:
        TeamSide M_team_side;

};

}
}

#endif // SOCCER_EVENTS_TEAM_EVENT_HPP
