#ifndef SOCCER_EVENTS_IEVENT_VISITOR_H
#define SOCCER_EVENTS_IEVENT_VISITOR_H

#include <soccer/events.h>

namespace soccer {
namespace event {

    /*
        \brief Interface for to output events to an output stream
    */
    template < typename Position,
               typename PlayerId,
               typename TeamSide,
               typename Time,
               typename TimeInterval = Time >
    class IEventVisitor {
    public:
        virtual ~IEventVisitor() { }

        typedef IEvent< Time, TimeInterval > ievent_t;
        typedef IEventChain< Time, TimeInterval > ievent_chain_t;

        typedef Position position_t;
        typedef PlayerId player_id_t;
        typedef TeamSide team_side_t;
        typedef Time time_t;
        typedef TimeInterval time_interval_t;

        typedef KickOff< TeamSide, Time, TimeInterval > kick_off_t;
        typedef KickOffTaken< TeamSide, Time, TimeInterval > kick_off_taken_t;
        typedef ThrowIn< TeamSide, Time, TimeInterval > throw_in_t;
        typedef ThrowInTaken< TeamSide, Time, TimeInterval > throw_in_taken_t;
        typedef GoalKick< TeamSide, Time, TimeInterval > goal_kick_t;
        typedef GoalKickTaken< TeamSide, Time, TimeInterval > goal_kick_taken_t;
        typedef CornerKick< TeamSide, Time, TimeInterval > corner_kick_t;
        typedef CornerKickTaken< TeamSide, Time, TimeInterval > corner_kick_taken_t;
        typedef FreeKick< TeamSide, Time, TimeInterval > free_kick_t;
        typedef FreeKickTaken< TeamSide, Time, TimeInterval > free_kick_taken_t;
        typedef IndirectFreeKick< TeamSide, Time, TimeInterval > indirect_free_kick_t;
        typedef IndirectFreeKickTaken< TeamSide, Time, TimeInterval > indirect_free_kick_taken_t;

        typedef Goal< TeamSide, Time, TimeInterval > goal_t;

        typedef SoccerMomentChanged< TeamSide, Time, TimeInterval > soccer_moment_changed_t;
        typedef PossessionChanged< TeamSide, Time, TimeInterval > possession_changed_t;

        typedef Shoot< PlayerId, Time, TimeInterval > shoot_t;
        typedef Dribble< PlayerId, Time, TimeInterval > dribble_t;
        typedef Pass< PlayerId, Time, TimeInterval > pass_t;
        typedef Interception< PlayerId, Time, TimeInterval > interception_t;

        // typedef Turnover< PlayerId, Time, TimeInterval > turnover_t;

        typedef PlayerMove< PlayerId, Position, Time, TimeInterval > player_move_t;


        typedef GoalieCatch< TeamSide, Time, TimeInterval > goalie_catch_t;
        typedef BallKick< PlayerId, Time, TimeInterval > ball_kick_t;
        typedef BallDispute< PlayerId, Time, TimeInterval > ball_dispute_t;
        typedef BallMove< Position, Time, TimeInterval > ball_move_t;
        typedef BallHold< PlayerId, Time, TimeInterval > ball_hold_t;

        typedef Offside< TeamSide, Time, TimeInterval > offside_t;

        // typedef PassChain< PlayerId, Time, TimeInterval > pass_chain_t;
        typedef GoalPassChain< Time, TimeInterval > goal_pass_chain_t;

        /* virtual void visit(const KickOff< TeamSide, Time, TimeInterval >& event) = 0;
        virtual void visit(const KickOffTaken< & event) = 0;
        virtual void visit(const ThrowIn& event) = 0;
        virtual void visit(const ThrowInTaken& event) = 0;
        virtual void visit(const GoalKick& event) = 0;
        virtual void visit(const GoalKickTaken& event) = 0;
        virtual void visit(const CornerKick& event) = 0;
        virtual void visit(const CornerKickTaken& event) = 0;
        virtual void visit(const FreeKick& event) = 0;
        virtual void visit(const FreeKickTaken& event) = 0;
        virtual void visit(const IndirectFreeKick& event) = 0;
        virtual void visit(const IndirectFreeKickTaken& event) = 0;

        virtual void visit(const Goal& event) = 0;

        virtual void visit(const SoccerMomentChanged& event) = 0;
        virtual void visit(const PossessionChanged& event) = 0;

        virtual void visit(const Shoot& event) = 0;
        virtual void visit(const Dribble& event) = 0;
        virtual void visit(const Pass& event) = 0;

        virtual void visit(const Interception& event) = 0;
        // virtual void visit(const Turnover& event) = 0;

        virtual void visit(const PlayerMove& event) = 0;

        virtual void visit(const GoalieCatch& event) = 0;
        virtual void visit(const BallKick& event) = 0;
        virtual void visit(const BallDispute& event) = 0;
        virtual void visit(const BallMove& event) = 0;
        virtual void visit(const BallHold& event) = 0;

        virtual void visit(const Offside& event) = 0;

        /// Cooperative events
        virtual void visit(const PassChain& event) = 0;
        virtual void visit(const GoalPassChain& event ) = 0; */
    };

}
}

#endif
