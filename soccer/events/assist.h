#ifndef SOCCER_EVENTS_ASSIST_H
#define SOCCER_EVENTS_ASSIST_H

#include <soccer/events/pass.h>
#include <soccer/types.h>

namespace soccer {
namespace event {

/*!
\brief Describes a pass that leads to a goal from the receiver which can dribble to goal between shooting
*/

template < typename PlayerId,
           typename Time,
           typename TimeInterval = Time >
class Assist
        : public Pass< PlayerId, Time, TimeInterval> {
public:
	Assist(unsigned int start_time, unsigned int end_time,
           TeamSide team_side,
           Unum sender, Unum receiver);
};

}
}

#endif // SOCCER_EVENTS_ASSIST_H
