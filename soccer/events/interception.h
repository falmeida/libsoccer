#ifndef SOCCER_EVENTS_INTERCEPTION_H
#define SOCCER_EVENTS_INTERCEPTION_H

#include <soccer/events/player_event.hpp>

namespace soccer {
namespace event {

    template < typename PlayerId,
               typename Time,
               typename TimeInterval = Time >
    class Interception
            : public PlayerEvent< PlayerId,Time,TimeInterval > {
    public:
        Interception( Time start_time, PlayerId pid )
            : PlayerEvent< PlayerId,Time,TimeInterval >( start_time, pid )
        {

        }

        EventType type() const { return EventType::Interception; }

    };

}
}

#endif
