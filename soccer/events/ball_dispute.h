#ifndef SOCCER_EVENTS_BALL_DISPUTE_H
#define SOCCER_EVENTS_BALL_DISPUTE_H

#include <soccer/events/event.h>

#include <vector>

namespace soccer {
namespace event {

    template < typename PlayerId,
               typename Time,
               typename TimeInterval = Time >
    class BallDispute
            :    public Event< Time,TimeInterval >
    {
    public:

        BallDispute( Time start_time )
            : Event< Time, TimeInterval >( start_time )
        {

        }

        template <typename PlayerIdInputIterator>
        BallDispute( Time start_time, Time end_time,
                     PlayerIdInputIterator first,
                     const PlayerIdInputIterator last)
            : Event<Time,TimeInterval>( start_time, end_time )
            , M_players( first, last )
        {

        }

        EventType type() const { return EventType::BallDispute; }
        const std::vector<PlayerId>& players() const { return M_players; }

        void addPlayer( const PlayerId pid ) { M_players.push_back( pid ); }

    private:
        std::vector<PlayerId> M_players;
    };


} // end namespace event
} // end namespace soccer

#endif
