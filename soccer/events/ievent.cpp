#include <soccer/events/ievent.h>

using namespace std;

namespace soccer {
namespace event {


map<EventType, string> createEventTypeToStringMap()
{
    map<EventType, string> mmap;

    mmap.insert(pair<EventType, string>(EventType::ThrowIn, "throw-in"));
    mmap.insert(pair<EventType, string>(EventType::ThrowInTaken, "throw-in-taken" ));
    mmap.insert(pair<EventType, string>(EventType::KickOff, "kick-off"));
    mmap.insert(pair<EventType, string>(EventType::KickOffTaken, "kick-off-taken"));
    mmap.insert(pair<EventType, string>(EventType::CornerKick, "corner-kick"));
    mmap.insert(pair<EventType, string>(EventType::CornerKickTaken, "corner-kick-taken"));
    mmap.insert(pair<EventType, string>(EventType::FreeKick, "free-kick"));
    mmap.insert(pair<EventType, string>(EventType::FreeKickTaken, "free-kick-taken"));
    mmap.insert(pair<EventType, string>(EventType::IndirectFreeKick,"indirect-free-kick"));
    mmap.insert(pair<EventType, string>(EventType::IndirectFreeKickTaken, "indirect-free-kick-taken"));
    mmap.insert(pair<EventType, string>(EventType::GoalKick,"goal-kick"));
    mmap.insert(pair<EventType, string>(EventType::GoalKickTaken,"goal-kick-taken"));
    mmap.insert(pair<EventType, string>(EventType::GoalOpportunity,"goal-opportunity"));
    mmap.insert(pair<EventType, string>(EventType::Offside, "offside"));
    mmap.insert(pair<EventType, string>(EventType::Dribble, "dribble"));
    mmap.insert(pair<EventType, string>(EventType::Pass, "pass"));
    mmap.insert(pair<EventType, string>(EventType::Shoot, "shoot"));
    mmap.insert(pair<EventType, string>(EventType::Goal, "goal"));
    mmap.insert(pair<EventType, string>(EventType::BallDispute, "ball-dispute"));
    mmap.insert(pair<EventType, string>(EventType::BallKick, "ball-kick"));
    mmap.insert(pair<EventType, string>(EventType::BallHold, "ball-hold"));
    mmap.insert(pair<EventType, string>(EventType::GoalieCatch, "goalie-catch"));
    mmap.insert(pair<EventType, string>(EventType::Interception, "interception"));
    mmap.insert(pair<EventType, string>(EventType::BallMove, "ball-move"));
    mmap.insert(pair<EventType, string>(EventType::PlayerMove, "player-move"));
    mmap.insert(pair<EventType, string>(EventType::PossessionChanged, "possession-changed"));
    mmap.insert(pair<EventType, string>(EventType::Turnover, "turnover"));
    mmap.insert(pair<EventType, string>(EventType::SoccerMomentChanged, "soccer-moment-changed"));
    mmap.insert(pair<EventType, string>(EventType::PassChain, "pass-chain"));
    mmap.insert(pair<EventType, string>(EventType::GoalPassChain, "goal-pass-chain"));

    return mmap;
}

map<string, EventType> createStringToEventTypeMap()
{
    map<string, EventType> mmap;

    mmap.insert(pair<string, EventType>("throw-in", EventType::ThrowIn));
    mmap.insert(pair<string, EventType>("throw-in-taken", EventType::ThrowInTaken ));
    mmap.insert(pair<string, EventType>("kick-off", EventType::KickOff));
    mmap.insert(pair<string, EventType>("kick-off-taken", EventType::KickOffTaken));
    mmap.insert(pair<string, EventType>("corner-kick", EventType::CornerKick));
    mmap.insert(pair<string, EventType>("corner-kick-taken", EventType::CornerKickTaken));
    mmap.insert(pair<string, EventType>("free-kick", EventType::FreeKick));
    mmap.insert(pair<string, EventType>("free-kick-taken", EventType::FreeKickTaken));
    mmap.insert(pair<string, EventType>("indirect-free-kick", EventType::IndirectFreeKick));
    mmap.insert(pair<string, EventType>("indirect-free-kick-taken", EventType::IndirectFreeKickTaken));
    mmap.insert(pair<string, EventType>("goal-kick", EventType::GoalKick));
    mmap.insert(pair<string, EventType>("goal-kick-taken", EventType::GoalKickTaken));
    mmap.insert(pair<string, EventType>("goal-opportunity", EventType::GoalOpportunity));
    mmap.insert(pair<string, EventType>("offside", EventType::Offside));
    mmap.insert(pair<string, EventType>("dribble", EventType::Dribble));
    mmap.insert(pair<string, EventType>("pass", EventType::Pass));
    mmap.insert(pair<string, EventType>("shoot", EventType::Shoot));
    mmap.insert(pair<string, EventType>("goal", EventType::Goal));
    mmap.insert(pair<string, EventType>("ball-dispute", EventType::BallDispute));
    mmap.insert(pair<string, EventType>("ball-kick", EventType::BallKick));
    mmap.insert(pair<string, EventType>("ball-hold", EventType::BallHold));
    mmap.insert(pair<string, EventType>("goalie-catch", EventType::GoalieCatch));
    mmap.insert(pair<string, EventType>("interception", EventType::Interception));
    mmap.insert(pair<string, EventType>("ball-move", EventType::BallMove));
    mmap.insert(pair<string, EventType>("player-move", EventType::PlayerMove));
    mmap.insert(pair<string, EventType>("possession-changed", EventType::PossessionChanged));
    mmap.insert(pair<string, EventType>("turnover", EventType::Turnover));
    mmap.insert(pair<string, EventType>("soccer-moment-changed", EventType::SoccerMomentChanged));
    mmap.insert(pair<string, EventType>("pass-chain", EventType::PassChain));
    mmap.insert(pair<string, EventType>("goal-pass-chain", EventType::GoalPassChain));

    return mmap;
}

string eventType( const EventType& event_type )
{
    static map<EventType, string> event_type_to_str_map = createEventTypeToStringMap();

    return event_type_to_str_map.at(event_type);
}

EventType eventType( const string& event_type_str )
{
    static map<string, EventType> str_to_event_type_map = createStringToEventTypeMap();

    return str_to_event_type_map.at(event_type_str);
}



}
}
