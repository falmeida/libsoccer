#ifndef SOCCER_EVENTS_EVENT_PARTICIPANTS_H
#define SOCCER_EVENTS_EVENT_PARTICIPANTS_H


#include <soccer/events/ievent_visitor.h>
#include <soccer/types.h>

#include <map>
#include <vector>

namespace soccer {
namespace event {

/*!
  \brief Given a sequence of events, indexed them by player participation and sort them ascending by time
  */

class PlayersInEventsExtractor
        : public IEventVisitor< std::pair< int, TeamSide >,
                                TeamSide,
                                int >
{
public:
    PlayersInEventsExtractor();
    const std::map< player_id_t, std::vector< const ievent_t* > > &playerEvents() const {
        return M_player_events;
    }
    const std::vector< player_id_t >& players() const {
        return M_players;
    }

    void operator()(const kick_off_t& ) { }
    void operator()(const kick_off_taken_t& ) { }
    void operator()(const throw_in_t& ) { }
    void operator()(const throw_in_taken_t& ) { }
    void operator()(const goal_kick_t& ) { }
    void operator()(const goal_kick_taken_t& ) { }
    void operator()(const corner_kick_t& ) { }
    void operator()(const corner_kick_taken_t& ) { }
    void operator()(const free_kick_t& ) { }
    void operator()(const free_kick_taken_t& ) { }
    void operator()(const indirect_free_kick_t& ) { }
    void operator()(const indirect_free_kick_taken_t& ) { }

    void operator()(const goal_t& event) { }

    void operator()(const soccer_moment_changed_t& ) { }
    void operator()(const possession_changed_t& ) { }

    void operator()(const shoot_t& event)
    {
        addParticipant( player_id( event.executor() ), event );
    }

    void operator()(const dribble_t& event) {
        addParticipant( event.executor(), event );
    }
    void operator()(const pass_t& event) {
        addParticipant( event.executor(), event );
        addParticipant( event.receiver(), event );
    }

    void operator()(const interception_t& event) {
        addParticipant( event.executor(), event );
    }
    // void operator()(const turnover& event);

    void operator()(const player_move_t& event) {
        addParticipant( event.player(), event );
    }

    void operator()(const goalie_catch_t& ) { }
    void operator()(const ball_kick_t& event) {
        addParticipant( event.kicker(), event );
    }
    void operator()(const ball_dispute_t& ) { }
    void operator()(const ball_move_t& ) { }
    void operator()(const ball_hold_t& event){
        addParticipant( event.holder(), event );
    }

    void operator()(const offside_t& ) { }

    void operator()(const pass_chain_t& event) {
        addParticipants(event);
    }
    void operator()(const goal_pass_chain_t& event) {
        addParticipants(event);
    }
private:
    void addParticipants( const EventChain& event ) {
        for( std::vector< std::tr1::shared_ptr< ievent_t > >::const_iterator ite = event.begin();
             ite != event.end(); ite++ )
        {
            (*ite)->visit( *this );
        }
    }

    void addParticipant( player_id_t pid, const ievent_t& event )
    {
        map<soccer::player_id, vector< const IEvent*> >::iterator itp = M_player_events.find(player_id);

        if ( itp == M_player_events.end() )
        {
            // Not adding duplicate players
            std::vector< const ievent_t* > events;
            events.push_back( &event );
            M_players.push_back( pid );
            M_player_events.insert(pair<player_id_t, vector< const ievent_t*> >( player_id, events ));
            return;
        }

        itp->second.push_back( &event );
    }

    /// Events and pointers should be const
    std::map< player_id_t, std::vector< const ievent_t* > > M_player_events;
    std::vector< player_id_t > M_players;
};

}
}

#endif // SOCCER_EVENTS_EVENT_PARTICIPANTS_H
