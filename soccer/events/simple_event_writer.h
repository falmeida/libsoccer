#ifndef SOCCER_EVENTS_SIMPLE_EVENT_WRITER_H
#define SOCCER_EVENTS_SIMPLE_EVENT_WRITER_H

#include <soccer/events/ievent_visitor.h>

#include <string>
#include <fstream>

namespace soccer
{
	namespace event
	{
		/*
			\brief Interface to output& events to an output stream
		*/
        template < typename PlayerId = std::pair< int, side >,
                   typename TeamSide = TeamSide,
                   typename Time = int,
                   typename TimeInterval = int >
        class SimpleEventWriter
                : public IEventVisitor< PlayerId,
                                        TeamSide,
                                        Time,
                                        TimeInterval > {
		public:
            SimpleEventWriter(std::ostream& os);
			
            void operator()(const kick_off_t& event)
            {
                printTime("KickOff", event);
            }

            void operator()(const kick_off_taken_t& event)
            {
                printTime("KickOffTaken", event);
            }

            void operator()(const throw_in_t& event)
            {
                printTime("ThrowIn", event);
            }

            void operator()(const throw_in_taken_t& event)
            {
                printTime("ThrowInTaken", event);
            }

            void operator()(const goal_kick_t& event)
            {
                printTime("GoalKick", event);
            }

            void operator()(const goal_kick_taken_t& event)
            {
                printTime("GoalKickTaken", event);
            }

            void operator()(const corner_kick_t& event)
            {
                printTime("CornerKick", event);
            }

            void operator()(const corner_kick_taken_t& event)
            {
                printTime("CornerKickTaken", event);
            }

            void operator()(const free_kick_t& event)
            {
                printTime("FreeKick", event);
            }

            void operator()(const free_kick_taken_t& event)
            {
                printTime("FreeKickTaken", event);
            }

            void operator()(const indirect_free_kick_t& event)
            {
                printTime("IndirectFreeKick", event);
            }

            void operator()(const indirect_free_kick_taken_t& event)
            {
                printTime("IndirectFreeKickTaken", event);
            }

            void operator()(const soccer_moment_changed_t& event)
            {
                std::stringstream ss;
                ss << "SoccerMomentChanged: Team=" << event.teamMoment().teamSide() << " " << event.teamMoment().moment();
                printTime(ss.str(), event);
            }

            void operator()(const possession_changed_t& event)
            {
                std::stringstream ss;
                ss << "PossessionChanged: TeamInPossesssion=" << event.teamInPossession();
                printTime(ss.str(), event);
            }

            void operator()(const interception_t& event)
            {
                printTime("BallInterception", event);
            }

            // void operator()(const Turnover& event);

            void operator()(const goalie_catch_t& event)
            {
                printTime("GoalieCatch", event);
            }

            void operator()(const ball_kick_t& event)
            {
                printTime("BallKick", event);
            }

            void operator()(const ball_dispute_t& event)
            {
                printTime("BallDispute", event);
            }
            void operator()(const ball_hold_t& event)
            {
                printTime("BallHold", event);
            }

            void operator()(const goal_t& event)
            {
                printTime("Goal", event);
            }
			
            void operator()(const shoot_t& event)
            {
                printTime("Shoot", event);
            }

            void operator()(const dribble_t& event)
            {
                printTime("Dribble", event);
            }

            void operator()(const ball_move_t& event)
            {
                printTime("PlayerMove", event);
            }

            void operator()(const player_move_t& event)
            {
                printTime("BallMove", event);
            }

            void operator()(const pass_t& event)
            {
                printTime("Pass", event);
            }

			
            void operator()(const offside_t& event)
            {
                printTime("Offside", event);
            }

            void operator()(const pass_chain_t& event)
            {
                printTime("Pass-chain", event);
            }

            void operator()(const goal_pass_chain_t& event)
            {
                printTime("Goal-pass-chain", event);
            }
			
		private:

            void printTime( const std::string& label,
                           const ievent_t& event) {
                M_os << "[" << event.startTime()
                     << ","
                     << event.endTime()
                     << "] " << label
                     << std::endl;
            }

            std::ostream& M_os;
			
		};

	}
}

#endif
