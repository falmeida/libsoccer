#ifndef SOCCER_PLAYER_EVENT_HPP
#define SOCCER_PLAYER_EVENT_HPP

#include <soccer/events/event.h>

namespace soccer {
namespace event {

template <typename PlayerId,
          typename Time,
          typename TimeInterval = Time>
class PlayerEvent
        : public Event<Time, TimeInterval>
{
public:
    typedef PlayerId player_id_t;

    PlayerEvent( Time start_time, PlayerId executor_id)
        : Event<Time,TimeInterval>(start_time)
        , M_executor( executor_id )
    {

    }

    PlayerEvent( Time start_time, Time end_time, PlayerId executor_id)
        : Event<Time,TimeInterval>(start_time, end_time)
        , M_executor( executor_id )
    {

    }

    const PlayerId& executor() const { return M_executor; }

private:
    PlayerId M_executor;

};

}
}

#endif // SOCCER_PLAYER_EVENT_HPP
