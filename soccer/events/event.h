#ifndef SOCCER_EVENTS_EVENT_H
#define SOCCER_EVENTS_EVENT_H

#include <soccer/events/ievent.h>

#include <cassert>

namespace soccer  {
namespace event  {
		
        template < typename Time,
                   typename TimeInterval = Time >
        class Event
                : public IEvent< Time,TimeInterval > {
		public:

            Event(Time start_time)
                : M_start_time(start_time)
                , M_end_time(start_time)
            {
                checkRep();
            }

            Event(Time start_time, unsigned int end_time)
                : M_start_time(start_time)
                , M_end_time(end_time)
            {
                checkRep();
            }


			/// Implement IEvent interface methods
            Time startTime() const { return M_start_time; }
            Time endTime() const { return M_end_time; }

		private:
            void checkRep() const { assert ( M_end_time >= M_start_time ); }
			
            Time M_start_time;
            Time M_end_time;

		};

        // Accessors
        template <typename Time,
                  typename TimeInterval = Time>
        Time startTime( const Event<Time,TimeInterval>& e )
        {
            return e.startTime();
        }

        template <typename Time,
                  typename TimeInterval = Time>
        Time endTime( const Event<Time,TimeInterval>& e )
        {
            return e.endTime();
        }

        template <typename Time,
                  typename TimeInterval = Time>
        Time duration( const Event<Time,TimeInterval>& e )
        {
            return e.duration();
        }
	}


}



#endif // EVENT_H
