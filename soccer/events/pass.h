#ifndef SOCCER_EVENTS_PASS_H
#define SOCCER_EVENTS_PASS_H

#include <soccer/defines.h>

#include <soccer/events/player_event.hpp>

SOCCER_EVENTS_NAMESPACE_BEGIN
		
        template < typename PlayerId,
                   typename Time,
                   typename TimeInterval = Time >
        class Pass
                : public PlayerEvent< PlayerId,Time,TimeInterval > {
		public:
            Pass( Time start_time,
                  Time end_time,
                  PlayerId sender,
                  PlayerId receiver)
                : PlayerEvent< PlayerId, Time,TimeInterval>( start_time, end_time, sender )
                , M_receiver( receiver )
            {

            }
			
            EventType type() const { return soccer::event::EventType::Pass; }
            PlayerId receiver() const { return M_receiver; }

		private:
            PlayerId M_receiver;
		};
	
SOCCER_EVENTS_NAMESPACE_END

#endif // SOCCER_EVENTS_PASS_H
