#ifndef SOCCER_EVENTS_BALL_HOLD_H
#define SOCCER_EVENTS_BALL_HOLD_H

#include <soccer/events/player_event.hpp>

namespace soccer {
namespace event {
	
    template < typename PlayerId,
               typename Time,
               typename TimeInterval = Time >
        class BallHold
                : public PlayerEvent< PlayerId, Time, TimeInterval > {
		public:
            BallHold(Time start_time,
                     Time end_time,
                     PlayerId pid )
                : PlayerEvent< PlayerId, Time, TimeInterval>(start_time, end_time, pid )
            {

            }

            EventType type() const { return EventType::BallHold; }

		};

	}
}
#endif // SOCCER_EVENTS_BALL_HOLD_H
