#ifndef SOCCER_EVENTS_PLAYER_MOVE_H
#define SOCCER_EVENTS_PLAYER_MOVE_H

#include <soccer/defines.h>

#include <soccer/events/player_event.hpp>

SOCCER_EVENTS_NAMESPACE_BEGIN

        template < typename PlayerId,
                   typename Position,
                   typename Time,
                   typename TimeInterval = Time >
        class PlayerMove
                : public PlayerEvent< PlayerId, Time, TimeInterval > {
		public:
            // HACK Not initialize from and to => BadIdea (Use a class builder factory)
            PlayerMove( Time start_time, Time end_time, PlayerId pid )
                : PlayerEvent< PlayerId, Time,TimeInterval>( start_time, end_time, pid )
            {

            }

            PlayerMove( Time start_time, Time end_time, PlayerId pid,
                        Position from, Position to )
                : PlayerEvent<Time,TimeInterval>( start_time, end_time, pid )
                , M_from( from )
                , M_to( to )
            {

            }

            EventType type() const { return EventType::PlayerMove; }
            Position from() const { return M_from; }
            Position to() const { return M_to; }


            void setFrom( const Position& pos ) { M_from = pos; }
            void setTo( const Position& pos ) { M_to = pos; }

		private:
            Position M_from;
            Position M_to;
		};
	
SOCCER_EVENTS_NAMESPACE_END

#endif
