#ifndef SOCCER_EVENTS_FREE_KICK_H
#define SOCCER_EVENTS_FREE_KICK_H

#include <soccer/events/event.h>

namespace soccer {
namespace event {
	
        template <typename TeamSide,
                  typename Time,
                  typename TimeInterval = Time>
        class FreeKick
                : public Event<Time, TimeInterval> {
		public:
            FreeKick(Time time, TeamSide side)
                : Event<Time, TimeInterval>( time )
                , M_side( side)
            {

            }

            EventType type() const { return EventType::FreeKick; }
            TeamSide side() const { return M_side; }

        private:
            TeamSide M_side;
		};

}
}


#endif
