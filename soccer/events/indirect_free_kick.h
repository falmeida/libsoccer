#ifndef SOCCER_EVENTS_INDIRECT_FREE_KICK_H
#define SOCCER_EVENTS_INDIRECT_FREE_KICK_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {
	

    template < typename TeamSide,
               typename Time,
               typename TimeInterval = Time >
        class IndirectFreeKick
                : public TeamEvent< TeamSide, Time, TimeInterval > {
		public:
            IndirectFreeKick(Time start_time, TeamSide side)
                : TeamEvent<TeamSide, Time, TimeInterval>( start_time, side )
            {

            }

            EventType type() const { return EventType::IndirectFreeKick; }

		};

}
}


#endif
