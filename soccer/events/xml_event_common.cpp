#include "xml_event_common.h"

#include <xercesc/util/XMLString.hpp>

XERCES_CPP_NAMESPACE_USE

namespace soccer {
namespace event {


    /// XStr
    XStr::XStr(const char* const toTranscode)
    {
        // Call the private transcoding method
        fUnicodeForm = XMLString::transcode(toTranscode);
    }

    XStr::~XStr()
    {
        XMLString::release(&fUnicodeForm);
    }

    const XMLCh* XStr::unicodeForm() const
    {
        return fUnicodeForm;
    }


    /// Strx
    StrX::StrX(const XMLCh* const toTranscode)
    {
        // Call the private transcoding method
        fLocalForm = XMLString::transcode(toTranscode);
    }

    StrX::~StrX()
    {
        XMLString::release(&fLocalForm);
    }

    const char* StrX::localForm() const
    {
        return fLocalForm;
    }

}
}
