#ifndef SOCCER_EVENTS_CORNER_KICK_H
#define SOCCER_EVENTS_CORNER_KICK_H

#include <soccer/defines.h>

#include <soccer/events/team_event.hpp>


SOCCER_EVENTS_NAMESPACE_BEGIN

        template < typename TeamSide,
                   typename Time,
                   typename TimeInterval = Time>
        class CornerKick
                : public TeamEvent< TeamSide, Time, TimeInterval > {
        public:
            CornerKick(Time start_time, TeamSide team_side)
                : TeamEvent< TeamSide, Time, TimeInterval>( start_time, team_side )
            {

            }

            EventType type() const { return EventType::CornerKick; }

        };

SOCCER_EVENTS_NAMESPACE_END

#endif // SOCCER_EVENTS_CORNER_KICK_H
