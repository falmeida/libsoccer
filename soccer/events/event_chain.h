#ifndef SOCCER_EVENTS_EVENT_CHAIN_H
#define SOCCER_EVENTS_EVENT_CHAIN_H

#include <soccer/events/ievent.h>

#include <list>

namespace soccer {
namespace event {

template < typename Time,
           typename TimeInterval = Time >
class EventChain
    : public IEventChain< Time, TimeInterval >
{
public:
    typedef IEventChain<Time, TimeInterval> base_type;

    EventChain()
    { }

    template < typename EventInputIterator >
    EventChain( EventInputIterator first,
                const EventInputIterator last )
        : M_events( first, last )
    {

    }

    EventChain( const EventChain& other )
        : M_events( other )
    {

    }

    unsigned int size() const
    {
        return M_events.size();
    }

    /*!
      \brief Determine the number of events of a given type
      \param Type of event to count
      */
    unsigned int numEvents( EventType type)
    {
        unsigned int count = 0;
        for ( auto ite = M_events.begin(); ite != M_events.end(); ite++ )
        {
            if ( type == (*ite)->type() ) { count++; }
        }
        return count;
    }

    /*!
     * \brief Add a new event to a chain
     * \param ev pointer to an event
     */
    void push_back( typename EventChain< Time, TimeInterval>::event_pointer ev ) {
        M_events.push_back( ev );
    }

    // Override interface methods
    Time startTime() const
    {
        assert( !M_events.empty() );
        return M_events.front()->startTime();
    }

    Time endTime() const
    {
        assert( !M_events.empty() );

        return M_events.back()->endTime();
    }

private:
    std::list< typename EventChain< Time, TimeInterval>::event_pointer > M_events;
};

}
}

#endif // SOCCER_EVENTS_EVENT_CHAIN_H
