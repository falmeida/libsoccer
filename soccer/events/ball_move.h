#ifndef SOCCER_EVENTS_BALL_MOVE_H
#define SOCCER_EVENTS_BALL_MOVE_H

#include <soccer/events/event.h>

namespace soccer {
namespace event {

        template < typename Position,
                   typename Time,
                   typename TimeInterval = Time >
        class BallMove
                : public Event<Time,TimeInterval> {
		public:
            BallMove(Time start_time, Time end_time)
                : Event<Time,TimeInterval>( start_time, end_time )
            {

            }
					   
            EventType type() const { return EventType::BallMove; }
            Position from() const { return M_from; }
            Position to() const { return M_to; }

            void setFrom( const Position& pos ) { M_from = pos; }
            void setTo( const Position& pos ) { M_to = pos; }

		private:
            Position M_from;
            Position M_to;
		
		};
	
}
}

#endif
