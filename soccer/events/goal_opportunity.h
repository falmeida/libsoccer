#ifndef SOCCER_EVENTS_GOAL_OPPORTUNITY_H
#define SOCCER_EVENTS_GOAL_OPPORTUNITY_H

#include <soccer/events/player_event.hpp>

namespace soccer {
namespace event {

        template < typename PlayerId,
                   typename Time,
                   typename TimeInterval = Time>
        class GoalOpportunity
                : public PlayerEvent< PlayerId, Time, TimeInterval> {
		public:
            GoalOpportunity(Time time, PlayerId pid)
                : PlayerEvent< PlayerId, Time, TimeInterval>( time, pid )
            {

            }
			
            EventType type() const { return EventType::GoalOpportunity; }

		};
	
	
	}
}

#endif // SOCCER_EVENTS_GOAL_OPPORTUNITY_H
