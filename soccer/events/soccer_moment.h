#ifndef SOCCER_EVENTS_SOCCER_MOMENT_H
#define SOCCER_EVENTS_SOCCER_MOMENT_H

#include <soccer/events/event.h>
#include <soccer/types.h>

namespace soccer {
namespace event {

template < typename TeamSide,
           typename Time,
           typename TimeInterval = Time >
class SoccerMomentChanged
        : public Event< Time, TimeInterval > {
public:

    class TeamSideMoment
    {
    public:

        TeamSideMoment( TeamSide team_side, SoccerMoment moment )
            : M_team_side( team_side )
            , M_moment( moment )
        {

        }

        TeamSideMoment( const TeamSideMoment& other)
            : M_team_side( other.M_team_side )
            , M_moment( other.M_moment )
        {

        }

        TeamSide teamSide() const { return M_team_side; }
        SoccerMoment moment() const { return M_moment; }

        bool operator==( const TeamSideMoment& other) {
            return this->M_team_side == other.M_team_side &&
                   this->M_moment == other.M_moment;
        }

    private:

        TeamSide M_team_side; /// Team side that owns the moment
        SoccerMoment M_moment;
    };

    SoccerMomentChanged( Time time, TeamSideMoment team_moment)
        : Event< Time, TimeInterval >( time )
        , M_team_moment( team_moment )
    {

    }

    EventType type() const { return EventType::SoccerMomentChanged; }
    const TeamSideMoment& teamMoment() const { return M_team_moment; }

private:
    TeamSideMoment M_team_moment;
};

}
}
#endif // SOCCER_EVENTS_SOCCER_MOMENT_H
