#ifndef SOCCER_EVENTS_THROW_IN_TAKEN_H
#define SOCCER_EVENTS_THROW_IN_TAKEN_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {

        template < typename TeamSide,
                   typename Time,
                   typename TimeInterval = Time >
        class ThrowInTaken
                : public TeamEvent< TeamSide, Time, TimeInterval > {
        public:
            ThrowInTaken( Time start_time, TeamSide side )
                : TeamEvent<  TeamSide, Time, TimeInterval >( start_time, side )
            {

            }

            EventType type() const { return EventType::ThrowInTaken; }

        };

}
}


#endif // SOCCER_EVENTS_THROW_IN_TAKEN_H

