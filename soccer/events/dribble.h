#ifndef SOCCER_EVENTS_DRIBBLE_H
#define SOCCER_EVENTS_DRIBBLE_H

#include <soccer/defines.h>

#include <soccer/events/player_event.hpp>

SOCCER_EVENTS_NAMESPACE_BEGIN
		
        template < typename PlayerId,
                   typename Time,
                   typename TimeInterval >
        class Dribble
                : public PlayerEvent< PlayerId,Time,TimeInterval > {
		public:
            Dribble( Time start_time,
                     Time end_time,
                     PlayerId pid )
                : PlayerEvent<PlayerId,Time,TimeInterval>( start_time, end_time, pid )
            {

            }

            EventType type() const { return EventType::Dribble; }

		};

SOCCER_EVENTS_NAMESPACE_END

#endif
