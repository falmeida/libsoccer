#ifndef SOCCER_EVENTS_INDIRECT_FREE_KICK_TAKEN_H
#define SOCCER_EVENTS_INDIRECT_FREE_KICK_TAKEN_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {

        template <typename TeamSide,
                  typename Time,
                  typename TimeInterval = Time>
        class IndirectFreeKickTaken
                : public TeamEvent<TeamSide, Time, TimeInterval> {
        public:
            IndirectFreeKickTaken(Time start_time, TeamSide side )
                : TeamEvent< TeamSide, Time, TimeInterval>( start_time, side )
            {

            }

            EventType type() const { return EventType::IndirectFreeKickTaken; }

        };


}
}


#endif
