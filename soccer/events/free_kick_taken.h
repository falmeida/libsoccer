#ifndef SOCCER_EVENTS_FREE_KICK_TAKEN_H
#define SOCCER_EVENTS_FREE_KICK_TAKEN_H

#include <soccer/events/team_event.hpp>

namespace soccer {
namespace event {

        template < typename TeamSide,
                   typename Time,
                   typename TimeInterval = Time >
        class FreeKickTaken
                : public TeamEvent< TeamSide, Time, TimeInterval > {
        public:
            FreeKickTaken(Time start_time, TeamSide side )
                : TeamEvent< TeamSide, Time, TimeInterval>( start_time, side )
            {

            }

            EventType type() const { return EventType::FreeKickTaken; }

        };

}
}

#endif // SOCCER_EVENTS_FREE_KICK_TAKEN_H
