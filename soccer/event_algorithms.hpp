#ifndef SOCCER_EVENT_ALGORITHMS_HPP
#define SOCCER_EVENT_ALGORITHMS_HPP

#include <soccer/event_traits.hpp>
#include <soccer/event_concepts.hpp>

namespace soccer {
namespace event {

/*!
  * Concept interface
  * startTime( const Event& )
  * endTime( const Event& )
  * durationTime( const Event& )
  */
template <typename Event>
typename Event::time_descriptor
duration( const Event& e )
{
    return endTime( e ) - startTime( e );
}

}
}

#endif // SOCCER_EVENT_ALGORITHMS_HPP
