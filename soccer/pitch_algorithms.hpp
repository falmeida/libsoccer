#ifndef SOCCER_PITCH_ALGORITHMS_HPP
#define SOCCER_PITCH_ALGORITHMS_HPP

#include <soccer/pitch_concept.hpp>
#include <soccer/position_concepts.hpp>

#include <soccer/core/access.hpp>

namespace soccer {

template < typename Pitch >
typename length_type< Pitch >::type
get_pitch_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_pitch_half_length( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_length( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_goal_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_goal_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_goal_area_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_goal_area_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_goal_area_half_length( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_goal_area_length( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_penalty_area_half_width( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_penalty_area_width( pitch ) / 2.0;
    return res;
}

template < typename Pitch >
typename length_type< Pitch >::type
get_penalty_area_half_length( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_penalty_area_length( pitch ) / 2.0;
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_midfield_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_x( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_pitch_half_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_left_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_pitch_top_left_x( pitch );
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_right_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_x( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_pitch_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_left_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_left_goal_line( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_goal_area_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_right_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_right_goal_line( pitch ) -
                     (get_pitch_right_sign< Pitch >() * get_goal_area_length( pitch ));
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_left_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_left_goal_line( pitch ) +
                     (get_pitch_right_sign< Pitch >() * get_penalty_area_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_right_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_right_goal_line( pitch ) -
                     (get_pitch_right_sign< Pitch >() * get_penalty_area_length( pitch ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) - get_goal_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_goal_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) + get_goal_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) - get_penalty_area_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_penalty_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) + get_penalty_area_half_width( pitch ) ));
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) - get_goal_area_half_width( pitch ) ));
    return res;
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_goal_area_line( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() *
                        ( get_pitch_half_width( pitch ) + goal_area_half_width( pitch ) ));
    return res;
}


template < typename Pitch >
typename coordinate_type< Pitch >::type
get_top_sideline( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_pitch_top_left_y( pitch );
}

template < typename Pitch >
typename coordinate_type< Pitch >::type
get_bottom_sideline( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    const auto res = get_pitch_top_left_y( pitch ) +
                     (get_pitch_bottom_sign< Pitch >() * get_pitch_width( pitch ));
    return res;
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_top_left_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ),
                           get_pitch_top_left_y( pitch ) );
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_top_right_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ) + get_pitch_length( pitch ),
                           get_pitch_top_left_y( pitch ) );
}


template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_bottom_left_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_width( pitch ) );
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_pitch_bottom_right_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ) + get_pitch_length( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_center_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_center_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_upper_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) -
                           get_goal_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_upper_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) -
                           get_goal_half_width( pitch ));
}


template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_lower_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) +
                           get_goal_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_lower_post( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_line( pitch ),
                           get_pitch_top_left_y( pitch ) +
                           get_pitch_half_width( pitch ) +
                           get_goal_half_width( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_kick_off_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_pitch_top_left_x( pitch ) + get_pitch_half_length( pitch ),
                           get_pitch_top_left_y( pitch ) + get_pitch_half_width( pitch ));
}


template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_kick_upper_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_area_line( pitch ),
                           get_top_goal_area_line( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_kick_upper_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_right_goal_area_line( pitch ),
                           get_top_goal_area_line( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_left_goal_kick_lower_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( get_left_goal_area_line( pitch ),
                           get_bottom_goal_area_line( pitch ));
}

template < typename OutputPosition,
           typename Pitch >
OutputPosition
get_right_goal_kick_lower_position( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( PositionConcept<OutputPosition> ));

    return OutputPosition( right_goal_area_line( pitch ),
                           bottom_goal_area_line( pitch ));
}

template < typename Pitch >
bool
has_penalty_shootouts( Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return num_penalties_in_shootout( pitch ) > 0u;
}

/*!
 * \struct check if a position is within the pitch
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_pitch( InputPosition const& pos,
                      Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_bottom_sideline( pitch );
}

/*!
 * \struct check if a position is within the left penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_left_midfield_area( InputPosition const& pos,
                                   Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_midfield_line( pitch ) &&
           get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_bottom_side_line( pitch );
}

/*!
 * \struct check if a position is within the right penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_right_midfield_area( InputPosition const& pos,
                                    Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_x( pos ) >= get_midfield_line( pitch ) &&
           get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_bottom_side_line( pitch );
}

/*!
 * \struct check if a position is within the left penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_left_penalty_area( InputPosition const& pos,
                                  Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_left_penalty_area_line( pitch ) &&
           get_y( pos ) >= get_top_penalty_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_penalty_area_line( pitch );
}

/*!
 * \struct check if a position is within the right penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_right_penalty_area( InputPosition const& pos,
                                   Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_x( pos ) >= get_right_penalty_area_line( pitch ) &&
           get_y( pos ) >= get_top_penalty_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_penalty_area_line( pitch );
}

/*!
 * \struct check if a position is within the left goal area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_left_goal_area( InputPosition const& pos,
                               Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) >= get_left_goal_line( pitch ) &&
           get_x( pos ) <= get_left_goal_area_line( pitch ) &&
           get_y( pos ) >= get_top_goal_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_goal_area_line( pitch );
}

/*!
 * \struct check if a position is within the right penalty area
 */
template < typename InputPosition,
           typename Pitch >
bool is_within_right_goal_area( InputPosition const& pos,
                                Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_x( pos ) <= get_right_goal_line( pitch ) &&
           get_x( pos ) >= get_right_goal_area_line( pitch ) &&
           get_y( pos ) >= get_top_goal_area_line( pitch ) &&
           get_y( pos ) <= get_bottom_goal_area_line( pitch );
}



template < typename InputPosition,
           typename Pitch >
bool is_within_upper_field( InputPosition const& pos,
                            Pitch const& pitch )
{
    BOOST_CONCEPT_ASSERT(( PositionConcept< InputPosition > ));
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));

    return get_y( pos ) >= get_top_sideline( pitch ) &&
           get_y( pos ) <= get_top_sideline( pitch ) + get_pitch_half_width( pitch );
}

} // end namespace soccer

#endif // SOCCER_PITCH_ALGORITHMS_HPP
