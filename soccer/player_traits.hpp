#ifndef SOCCER_PLAYER_TRAITS_HPP
#define SOCCER_PLAYER_TRAITS_HPP

#include <soccer/core/number_type.hpp>
#include <soccer/core/side_type.hpp>

#include <soccer/object_traits.hpp>

namespace soccer {

template < typename Player >
struct player_id_traits {
    typedef typename number_type< Player >::type number_type;
    typedef typename side_type< Player >::type side_type;
};


template <typename Player>
struct player_traits
    : public player_id_traits< Player >
    , public soccer::dynamic_object_traits< Player >
{
    typedef typename Player::direction_descriptor direction_descriptor;

    inline
    static typename player_id_traits<Player>::number_descriptor unknown_number()
    {
        return Player::unknown_number();
    }

    static typename player_id_traits<Player>::side_t unknown_side()
    {
        return Player::unknown_side();
    }
};


} // end namespace soccer

#endif // SOCCER_PLAYER_TRAITS_HPP
