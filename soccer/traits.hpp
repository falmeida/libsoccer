#ifndef SOCCER_TRAITS_HPP
#define SOCCER_TRAITS_HPP


#include <soccer/traits/interception_traits.hpp>
#include <soccer/traits/tackle_traits.hpp>
#include <soccer/traits/action_chain_traits.hpp>
#include <soccer/traits/shoot_traits.hpp>
#include <soccer/traits/ball_traits.hpp>
#include <soccer/traits/position_traits.hpp>
#include <soccer/traits/velocity_traits.hpp>
#include <soccer/traits/formation_traits.hpp>
#include <soccer/traits/positionings_traits.hpp>
#include <soccer/traits/flux_traits.hpp>
#include <soccer/traits/generator_traits.hpp>
#include <soccer/traits/strategy_traits.hpp>
#include <soccer/traits/tactic_traits.hpp>
#include <soccer/traits/playmode_traits.hpp>
#include <soccer/traits/object_traits.hpp>
#include <soccer/traits/state_traits.hpp>
#include <soccer/traits/player_traits.hpp>
#include <soccer/traits/triangulation_traits.hpp>
#include <soccer/traits/event_traits.hpp>
#include <soccer/traits/action_traits.hpp>
#include <soccer/traits/dribble_traits.hpp>
#include <soccer/traits/pass_traits.hpp>
#include <soccer/traits/move_traits.hpp>

#endif // SOCCER_TRAITS_HPP
