#ifndef SOCCER_DRIBBLE_HPP
#define SOCCER_DRIBBLE_HPP

#include <soccer/action.hpp>

#include <soccer/core/tags.hpp>

namespace soccer {

//
// dribble implementation
//
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed = double,
           typename Direction = double
         >
struct dribble
    : public action< PlayerId,Duration >
{
    typedef Position position_descriptor;
    typedef dribble_tag action_category;

    dribble()
        : action<PlayerId,Duration>(ActionType::Dribble)
    {

    }

    Position M_target_position;
    Speed M_ball_first_speed;
    Direction M_direction;
};

} // end namespace soccer


namespace soccer { namespace traits {

//
// dribble TYPE TRAITS REGISTRATION
//
template <>
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed,
           typename Direction >
struct tag< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > > { typedef dribble_tag type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct player_id_type< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > > { typedef PlayerId type; };

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed,
          typename Direction >
struct duration_type< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > > { typedef Duration type; };


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct position_type< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > > { typedef Position type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct speed_type< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > > { typedef Speed type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct direction_type< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > > { typedef Direction type; };

//
// dribble DATA ACCESS REGISTRATION
//

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct executor_id_access< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > >
{
    typedef ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > dribble_t;

    static inline
    PlayerId const& get( dribble_t const& _dribble ) { return _dribble.M_executor_id; }

    static inline
    void set( PlayerId const& pid, dribble_t& _dribble ) { _dribble.M_executor_id = pid; }
};

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed,
          typename Direction >
struct duration_access< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > >
{
    typedef ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > dribble_t;

    static inline
    Duration const& get( dribble_t const& _dribble ) { return _dribble.M_duration; }

    static inline
    void set( Duration const& dur, dribble_t& _dribble ) { _dribble.M_duration = dur; }
};


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct position_access< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > >
{
    typedef ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > dribble_t;

    static inline
    Position const& get( dribble_t const& _dribble ) { return _dribble.M_target_position; }

    static inline
    void set( Position const& pos, dribble_t& _dribble ) { _dribble.M_target_position = pos; }
};

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct speed_access< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > >
{
    typedef ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > dribble_t;

    static inline
    Speed const& get( dribble_t const& _dribble ) { return _dribble.M_ball_first_speed; }

    static inline
    void set( Speed const& _speed, dribble_t& _dribble ) { _dribble.M_ball_first_speed = _speed; }
};

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct direction_access< ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > >
{
    typedef ::soccer::dribble< PlayerId, Duration, Position, Speed, Direction > dribble_t;

    static inline
    Direction const& get( dribble_t const& _dribble ) { return _dribble.M_direction; }

    static inline
    void set( Direction const& _dir, dribble_t& _dribble ) { _dribble.M_direction = _dir; }
};


}} // end namespace soccer::traits

#endif // SOCCER_DRIBBLE_HPP
