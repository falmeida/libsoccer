#ifndef SOCCER_IOBSERVABLE
#define SOCCER_IOBSERVABLE

#include <list>
#include <soccer/iobserver.h>
#include <iostream>
#include <cassert>
#include <typeinfo>

template <class E>
class IObservable {
public:
        virtual ~IObservable() { }

        void registerObserver(IObserver<E>* observer)
        {
            M_observers.push_back(observer);
        }

        void unregisterObserver(IObserver<E>* observer)
        {
            M_observers.remove(observer);
        }

protected:
        void notifyObservers(const E& object)
        {
            for ( typename std::list<IObserver<E>* >::const_iterator it_obs = M_observers.begin();
                  it_obs != M_observers.end();
                  it_obs++)
            {
                (*it_obs)->update( object );
            }
        }
private:
        std::list<IObserver<E>* > M_observers;
};


#endif
