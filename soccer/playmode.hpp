#ifndef SOCCER_PLAYMODE_HPP
#define SOCCER_PLAYMODE_HPP

namespace soccer {

enum class MatchSituationType {
    Undefined,
    BeforeKickOff,
    TimeOver,
    PlayOnInPossession,        // Our | Opp
    PlayOnLosingPossession,    // Our | Opp
    PlayOnRegainingPossession, // Our | Opp
    PlayOn,     // Our | Opp
    KickOff,    // Our | Opp
    ThrowIn,    // Our | Opp
    FreeKick,   // Our | Opp
    CornerKick, // Our | Opp
    GoalKick,   // Our | Opp
    Goal,  // Our | Opp
    //Drop_Ball,   // Our | Opp
    OffSide,    // Our | Opp
    PenaltyKick,         // Our | Opp
    FirstHalfOver,
    Pause,
    Human,
    FoulCharge, // Our | Opp
    FoulPush,   // Our | Opp
    FoulMultipleAttacker, // Our | Opp
    FoulBallOut,    // Our | Opp
    BackPass,       // Our | Opp
    FreeKickFault, // Our | Opp

    CatchFault, // Our | Opp
    IndirectFreeKick, // Our | Opp

    PenaltySetup, // Our | Opp
    PenaltyReady, // Our | Opp
    PenaltyTaken, // Our | Opp
    PenaltyMiss, // Our | Opp
    PenaltyScore, // Our | Opp

    // these are not a real playmode
    // PenaltyOnfield, // next real playmode is PenaltySetup_
    // PenaltyFoul,    // next real playmode is PenaltyMiss_ or PenaltyScore_
    //PenaltyWinner_,  // next real playmode is TimeOver
    //PenaltyDraw,     // next real playmode is TimeOver

    GoalieCatch, // Our | Opp
    OverTime
};

template < typename Side >
struct playmode {


    MatchSituationType M_situation;
    Side M_side;
};


} // end namespace soccer

#include <soccer/register/playmode.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// playmode TYPE REGISTRATION
//
template <>
template < typename Side >
struct side_type< playmode< Side > > { typedef Side type; };

template <>
template < typename Side >
struct match_situation_type< playmode< Side > > { typedef GameSituationType type; };

//
// playmode DATA ACCESS REGISTRATION
//
template <>
template < typename Side >
struct side_access< playmode< Side > >
{
    typedef playmode< Side > playmode_t;
    static inline
    Side const& get( playmode_t const& pmode ) { return pmode.M_side; }

    static inline
    void set( Side const& _side, playmode_t& pmode ) { pmode.M_side = _side; }
};

template <>
template < typename Side >
struct match_situation_access< playmode< Side > >
{
    typedef playmode< Side > playmode_t;

    static inline
    MatchSituationType const& get( playmode_t const& pmode ) { return pmode.M_situation; }

    static inline
    void set( MatchSituationType const& _situation, playmode_t& pmode ) { pmode.M_situation = _situation; }
};

SOCCER_TRAITS_NAMESPACE_END



#endif // SOCCER_GAME_MODE_HPP
