#ifndef SOCCER_CONVERT_HPP
#define SOCCER_CONVERT_HPP

#include <soccer/player_traits.hpp>

namespace soccer {


#ifndef DOXYGEN_NO_DISPATCH
namespace dispatch
{

template
<
    typename Tag,
    typename Player1,
    typename Player2
>
struct convert<Tag, Tag, Player1, Player2>
{
    // Same geometry type -> copy pos, vel, number and side
    static inline void apply(Player1 const& source, Player2& destination)
    {
        soccer::set_position( typename Player2::position_descriptor( soccer::get_position( soccer::get_x( source )),
                                                                     soccer::get_position( soccer::get_y( source ))),
                              destination );
        soccer::set_velocity( typename Player2::position_descriptor( soccer::get_velocity( soccer::get_x( source )),
                                                                     soccer::get_velocity( soccer::get_y( source ))),
                              destination );
        soccer::set_number( soccer::get_number( source ), destination );
        soccer::set_side( soccer::get_side( source ), destination );
    }
};

}

/*!
\brief Converts one player to another player
\details The convert algorithm converts one player to another player. Might not be possible and applicable.
\ingroup convert
\tparam Player1 \tparam_geometry
\tparam Player2 \tparam_geometry
\param player1 \param_player (source)
\param player2 \param_player (target)
 */
template <typename Player1, typename Player2>
inline void convert(Player1 const& player1, Player2& player2)
{
    // concept::check_concepts_and_equal_dimensions<Geometry1 const, Geometry2>();

    dispatch::convert
        <
            typename tag<Player1>::type,
            typename tag<Player2>::type,
            Player1,
            Player2
        >::apply(player1, player2);
}

}

#endif // SOCCER_CONVERT_HPP
