#ifndef SOCCER_TACTIC_HPP
#define SOCCER_TACTIC_HPP

#include <soccer/tactic_concepts.hpp>
#include <soccer/core/tags.hpp>

#include <boost/property_tree/ptree.hpp>

#include <string>


namespace soccer {

template < typename Value = double >
struct tactic
{

    boost::property_tree::ptree M_ptree;

    Value get( std::string const& key )
    {
        return M_ptree.get< Value >( key );
    }
};

}


/*namespace soccer { namespace traits {

//
// tactic TYPES REGISTRATION
//
template < >
template < typename Key, typename Value >
struct tag< tactic< Key, Value > > { typedef tactic_tag type; };

template < >
template < typename Key, typename Value >
struct value_type< tactic< Key, Value > > { typedef Value type; };

template < >
template < typename Key, typename Value >
struct key_type< tactic< Key, Value > > { typedef Key type; };

//
// tactic DATA ACCESS REGISTRATION
//
template < >
template < typename Key, typename Value >
struct key_value_access< tactic< Key, Value > >
{
    static inline
    Value const& get( Key const& key,
                       tactic< Key, Value > const& _tactic )
        { return _tactic.M_ptree_weights.get(key); }

    static inline
    void set( Key const& _key,
              Value const& _weight,
              tactic< Key, Value >& _tactic )
        { _tactic.M_ptree_weights.put( _key, _weight ); }
};

}} // end namespace soccer::traits
*/

#include <boost/program_options.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace soccer {

template < typename Value = double >
struct tactic_reader {

    tactic_reader() { }

    tactic< Value >
    operator()( std::istream& is )
    {
        tactic< Value > _tactic;
        boost::property_tree::ini_parser::read_ini( is, _tactic.M_ptree );
        return _tactic;

        /* tactic _new_tactic;

        boost::program_options::options_description tactic_options;
        double flux_weight, safety_weight, easiness_weight, pass_weight, dribble_weight;
        tactic_options.add_options()
            ("help", "produce help message")
            ("flux_weight", boost::program_options::value<double>(&flux_weight), "flux weight")
            ("safety_weight", boost::program_options::value<double>(&safety_weight), "safety weight")
            ("easiness_weight", boost::program_options::value<double>(&easiness_weight), "easiness weight")
            ("pass_weight", boost::program_options::value<double>(&pass_weight), "pass weight")
            ("dribble_weight", boost::program_options::value<double>(&dribble_weight), "dribble weight")
            ;

        boost::program_options::variables_map vm;
        boost::program_options::parse_config_file( is, tactic_options );
        if ( vm.count("help") )
        {
            std::cout << tactic_options << std::endl;
            exit( 1 );
        }

        _new_tactic.M_ptree_weights.put("flux_weight", flux_weight );
        _new_tactic.M_ptree_weights.put("flux_weight", flux_weight );
        _new_tactic.M_ptree_weights.put("flux_weight", flux_weight );
        _new_tactic.M_ptree_weights.put("flux_weight", flux_weight );
        _new_tactic.M_ptree_weights.put("flux_weight", flux_weight );

        return _new_tactic; */
    }


};

template < typename Value >
void read_ini( const std::string file_name,
               ::soccer::tactic< Value >& _tactic )
{
    typedef ::soccer::tactic< Value > tactic_t;
    // BOOST_CONCEPT_ASSERT(( MutableTacticConcept< Tactic > ));

    boost::property_tree::ini_parser::read_ini( file_name, _tactic.M_ptree );
}


/*!
 * \brief The tactic_reader struct
 */
template < typename Value >
void write_ini( const std::string& file_name,
                ::soccer::tactic< Value > const& _tactic )
{
    boost::property_tree::ini_parser::write_ini( file_name, _tactic );
}

} // end namespace soccer

#endif // SOCCER_TACTIC_HPP
