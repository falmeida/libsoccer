#ifndef ACTION_ALGORITHMS_HPP
#define ACTION_ALGORITHMS_HPP

#include <soccer/action_traits.hpp>

namespace soccer {

/*!
  \brief calculate the duration of the action
  */
template <typename Action>
typename duration_type< Action>::type duration( const Action& action )
{
    return endTime( action ) - startTime( action );
}

/*!
  \brief calculate the duration of the action
  */
template <typename Action>
typename duration_type< Action>::type distance( const Action& action )
{
    return distance( startPosition( action ), endPosition( action ) );
}

}

#endif // ACTION_ALGORITHMS_HPP
