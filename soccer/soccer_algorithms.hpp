#ifndef SOCCER_ALGORITHMS_HPP
#define SOCCER_ALGORITHMS_HPP

#include <soccer/state_traits.hpp>

#include <soccer/state_predicates.hpp>
#include <soccer/player_predicates.hpp>

#include <boost/property_map/property_map.hpp>
#include <boost/geometry.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/tuple/tuple.hpp>

#include <boost/type_traits.hpp>
#include <boost/concept_check.hpp>


#include <list>
#include <algorithm>
#include <functional>

namespace soccer {


/* template <typename MobileObject,
          typename Simulation>
void inertia( MobileObject& mobile_object,
              const Simulation& simulation )
{
    setPosition( mobile_object,
                 position( mobile_object ) + player_velocity( mobile_object ) );
    setVelocity( mobile_object,
                 player_velocity( mobile_object) * decay( mobile_object, simulation ) );
} */

/*!
  \brief Calculate the distance between two dynamic objects after a inertia movement
  */
template < typename DynamicObject1,
           typename DynamicObject2 >
double
inertia_next_distance( const DynamicObject1& dobj1,
                       const DynamicObject1& dobj2)
{
    BOOST_CONCEPT_ASSERT(( MobileObjectConcept<DynamicObject1> ));
    BOOST_CONCEPT_ASSERT(( MobileObjectConcept<DynamicObject2> ));

    return soccer::distance( soccer::get_position( dobj1 ) + soccer::get_velocity( dobj1 ),
                             soccer::get_position( dobj2 ) + soccer::get_velocity( dobj2 ) );
}

}

#endif // SOCCER_ALGORITHMS_HPP
