#ifndef SOCCER_STATE_SIMILARITY_HPP
#define SOCCER_STATE_SIMILARITY_HPP

#include <soccer/core/access.hpp>
#include <soccer/geometry.hpp>

#include <soccer/state_algorithms.hpp>
#include <soccer/boost/matching.hpp>

#include <boost/concept_check.hpp>

#include <map>
#include <functional>

namespace soccer {

/*!
 * \brief inter_state_player_matching
 * \tparam State model of the state
 * \param s1
 * \param s2
 * \param left_player_matching_cost_fn
 * \param right_player_matching_cost_fn
 * \param out_player_matching_pmap
 * \return
 */
template < typename State,
           typename PlayerMatchingMap,
           typename PlayerMatchingCostFunction,
           typename PlayerIdMatchingMap >
long
inter_state_player_matching( typename side_type< typename player_type< State >::type >::type side,
                             State const& s1,
                             State const& s2,
                             PlayerMatchingCostFunction player_matching_cost_fn,
                             PlayerIdMatchingMap out_player_matching_pmap )
{
    BOOST_CONCEPT_CHECK(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    BOOST_CONCEPT_CHECK(( boost::WritablePropertyMapConcept< PlayerIdMatchingMap, player_id_t >));

    auto _s1_team_players_iterators = get_players( side, s1 );
    auto _s2_team_players_iterators = get_players( side, s2 );


    const auto _matching_cost = matching( _s1_team_players_iterators.first, _s1_team_players_iterators.second,
                                          _s2_team_players_iterators.first, _s2_team_players_iterators.second,
                                          player_matching_cost_fn,
                                          out_player_matching_pmap );

    return _matching_cost;
}


/*!
 * \brief state_similarity
 * \tparam State model of the state
 * \param s1
 * \param s2
 * \param left_player_matching_cost_fn
 * \param right_player_matching_cost_fn
 * \param out_player_matching_pmap
 * \return
 */
template < typename State,
           typename PlayerMatchingMap,
           typename LeftPlayerMatchingCostFunction,
           typename RightPlayerMatchingCostFunction,
           typename PlayerIdMatchingMap >
long
state_similarity( State const& s1,
                  State const& s2,
                  LeftPlayerMatchingCostFunction left_player_matching_cost_fn,
                  RightPlayerMatchingCostFunction right_player_matching_cost_fn,
                  PlayerIdMatchingMap out_player_matching_pmap )
{
    BOOST_CONCEPT_CHECK(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    BOOST_CONCEPT_CHECK(( boost::WritablePropertyMapConcept< PlayerIdMatchingMap, player_id_t >));

    auto _left_players_matching_cost = inter_state_player_matching( get_left_side< side_t >(), s1, s2, left_player_matching_cost_fn, out_player_matching_pmap );
    auto _right_players_matching_cost = inter_state_player_matching( get_right_side< side_t >(), s1, s2, right_player_matching_cost_fn, out_player_matching_pmap );

    const auto _ball_matching_cost = 0ul;
    // Ball matching cost!!!
    const auto _total_cost = _ball_matching_cost +
                             _left_players_matching_cost +
                             _right_players_matching_cost;
    return _total_cost;
}

/*!
 * \brief state_similarity
 * \tparam State model of the state
 * \param s1
 * \param s2
 * \param player_matching_cost_fn
 * \param out_player_matching_pmap
 * \return
 */
template < typename State,
           typename PlayerMatchingMap,
           typename PlayerMatchingCostFunction,
           typename PlayerIdMatchingMap >
long
state_similarity( State const& s1,
                  State const& s2,
                  PlayerMatchingCostFunction player_matching_cost_fn,
                  PlayerIdMatchingMap out_player_matching_pmap )
{
    BOOST_CONCEPT_CHECK(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename side_type< typename player_type< State >::type >::type side_t;

    BOOST_CONCEPT_CHECK(( boost::WritablePropertyMapConcept< PlayerIdMatchingMap, player_id_t >));

    return state_similarity( s1, s2,
                             player_matching_cost_fn, player_matching_cost_fn,
                             out_player_matching_pmap );
}

} // end namespace soccer

#endif // SOCCER_STATE_SIMILARITY_HPP
