#ifndef SOCCER_TACKLE_TRAITS_HPP
#define SOCCER_TACKLE_TRAITS_HPP

#include <soccer/action_traits.hpp>

namespace soccer {

template <typename Tackle>
struct tackle_traits
    : public action_traits<Tackle>
{
    typedef typename Tackle::position_descriptor position_descriptor;
    typedef typename Tackle::foul_descriptor foul_descriptor;
};

}
#endif // SOCCER_TACKLE_TRAITS_HPP
