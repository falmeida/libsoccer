#ifndef SOCCER_INERTIA_EFFECTOR_HPP
#define SOCCER_INERTIA_EFFECTOR_HPP

#include <soccer/concepts/object_concepts.hpp>

namespace soccer {

namespace core_dispatch {

    template < typename Object, typename ObjectModel, typename Tag >
    struct inertia_effector_impl;

    template < typename Object,
               typename ObjectModel >
    struct inertia_effector_impl< Object, ObjectModel, dynamic_object_model_tag >
    {
        static inline void apply( Object& object,
                                  ObjectModel const& object_model )
        {
            BOOST_CONCEPT_ASSERT(( MutableMobileObjectConcept< Object > ));

            auto pos = soccer::get_position( object );
            auto vel = soccer::get_velocity( object );

            const auto decay = soccer::get_decay( object_model );

            boost::geometry::add_point( pos, vel );
            boost::geometry::multiply_value( vel, decay );

            soccer::set_position( pos, object );
            soccer::set_velocity( vel, object );
        }
    };

}


template < typename MobileObject,
           typename ObjectModel>
void
apply_inertia( MobileObject& mobile_object,
               ObjectModel const& object_model )
{
    BOOST_CONCEPT_ASSERT(( MutableMobileObjectConcept< MobileObject > ));
    typedef typename soccer::tag< ObjectModel >::type model_tag;

    typedef core_dispatch::inertia_effector_impl< MobileObject, ObjectModel, model_tag > impl;
    impl::apply( mobile_object, object_model );

}

template < typename MobileObject,
           typename ObjectModel >
/*!
 * \brief The ball_inertia_generator struct
 * \tparam Ball ball model
 * \tparam BallModel ball physics model
 */
struct inertia_effector
{
    BOOST_CONCEPT_ASSERT(( MutableMobileObjectConcept< MobileObject > ));
    // BOOST_CONCEPT_ASSERT(( InertiaModelConcept< InertiaModel > ));

    typedef typename soccer::tag< ObjectModel >::type model_tag;

    MobileObject M_object;
    ObjectModel const* M_object_model;

    inertia_effector( MobileObject const& object,
                       ObjectModel const& object_model )
        : M_object( object )
        , M_object_model( &object_model )
    {

    }

    ObjectModel operator()()
    {
        apply_inertia( M_object, *M_object_model );

        return M_object;
    }
};

} // end namespace soccer

#endif // SOCCER_INERTIA_EFFECTOR_HPP
