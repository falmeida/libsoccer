#ifndef PASS_ALGORITHMS_HPP
#define PASS_ALGORITHMS_HPP

namespace soccer {

// Receiver pre-selector
template <typename Scene>
struct AdjacentReceiverSelector {

};


}

// PassSuccessChecker algorithms


/// PassGenerator algorithms
/*!
  * 1) Pre-select receivers
  * 2) For each pre-selected receiver:
  * 2.1) Find relevant opponents
  * 2.2) If any relevant opponent can intercept
  */
#endif // PASS_ALGORITHMS_HPP
