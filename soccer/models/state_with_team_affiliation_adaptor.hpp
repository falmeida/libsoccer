#ifndef SOCCER_STATE_WITH_TEAM_AFFILIATION_HPP
#define SOCCER_STATE_WITH_TEAM_AFFILIATION_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/concepts/state_concepts.hpp>

#include <soccer/core/type_generators.hpp>

SOCCER_NAMESPACE_BEGIN

/*!
 * \brief generic state structure
 */
template < typename State >
struct state_with_team_affiliation_adaptor {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename side_type< typename player_type< State >::type >::type side_t;

    state_with_team_affiliation_adaptor( State const& state,
                                         side_t const our_side )
        : M_state( state )
        , M_our_side( side )
    {

    }

    State& M_state;
    side_t M_our_side;
};

SOCCER_NAMESPACE_END


SOCCER_TRAITS_NAMESPACE_BEGIN

//
// state_with_team_affiliation_adaptor TYPE REGISTRATION
//

template < >
template < typename State >
struct tag< state_with_team_affiliation_adaptor< State > > { typedef state_tag type; };

template < >
template < typename State >
struct player_id_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename player_id_type< State >::type type; };

template < >
template < typename State >
struct player_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename player_type< State >::type type; };

template < >
template < typename State >
struct player_iterator_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename player_iterator_type< State >::type type; };

template < >
template < typename State >
struct ball_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename ball_type< State >::type type; };

template < >
template < typename State >
struct time_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename time_type< State >::type type; };

template < >
template < typename State >
struct playmode_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename playmode_type< State >::type type; };

template < >
template < typename State >
struct team_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename team_type< State >::type type; };

template < >
template < typename State >
struct score_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename score_type< State >::type type; };


template < >
template < typename State >
struct left_strategy_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename left_strategy_type< State >::type type; };

template < >
template < typename State >
struct right_strategy_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename right_strategy_type< State >::type type; };


//
// TODO state_with_team_affiliation state specific TYPE REGISTRATION
//
template < >
template < typename State >
struct teammate_iterator_type< state_with_team_affiliation_adaptor< State > >
{ typedef typename player_filter_iterator_generator< State >::type type; };

//
// state_with_team_affiliation_adaptor DATA ACCESS REGISTRATION
//

template < >
template < typename State >
struct player_iterators_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename player_iterator_type< State >::type player_iterator_t;

    static inline
    std::pair< player_iterator_t, player_iterator_t >
    get( state_t const& s )
    {
        return get_players( s.M_state );
    }
};

template < >
template < typename State >
struct player_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;

    static inline
    player_t const&
    get( player_id_t const& pid, state_t const& s )
    {
        return get_player( pid, s.M_state );
    }

    static inline
    void
    set( player_id_t const& pid,
         player_t const& p,
         state_t& s )
    {
        set_player( pid, p, s.M_state );
    }
};

template < >
template < typename State >
struct ball_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename ball_type< State >::type ball_t;

    static inline
    ball_t const&
    get( state_t const& s ) { return get_ball( s.M_state ); }

    static inline
    void
    set( ball_t const& b, state_t& s ) { set_ball( b, s.M_state ); }
};


template < >
template < typename State >
struct time_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename time_type< State >::type time_t;

    static inline
    time_t const&
    get( state_t const& s ) { return get_time( s.M_state ); }

    static inline
    void
    set( time_t const& _time, state_t& s ) { set_time( _time, s.M_state ); }
};

template < >
template < typename State >
struct playmode_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename playmode_type< State >::type playmode_t;

    static inline
    playmode_t const&
    get( state_t const& s ) { return get_playmode( s.M_state ); }

    static inline
    void
    set( playmode_t const& _pmode, state_t& s ) { set_playmode( _pmode, s.M_state ); }
};


template < >
template < typename State >
struct left_team_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename team_type< State >::type team_t;

    static inline
    team_t const&
    get( state_t const& s ) { return get_left_team( s.M_state ); }

    static inline
    void
    set( team_t const& _team, state_t& s ) { set_left_team( _team, s.M_state; ); }
};

template < >
template < typename State >
struct right_team_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename team_type< State >::type team_t;

    static inline
    team_t const&
    get( state_t const& s ) { return get_right_team( s.M_state ); }

    static inline
    void
    set( team_t const& _team, state_t& s ) { set_right_team( _team, s.M_state ); }
};



template < >
template < typename State >
struct left_score_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename score_type< State >::type score_t;

    static inline
    score_t const&
    get( state_t const& s ) { return get_left_score( s.M_state ); }

    static inline
    void
    set( score_t const& _score, state_t& s ) { set_left_score( _score, s.M_state ); }
};

template < >
template < typename State >
struct right_score_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename score_type< State >::type score_t;

    static inline
    score_r const&
    get( state_t const& s ) { return get_right_score( s.M_state ); }

    static inline
    void
    set( score_t const& _score, state_t& s ) { set_right_score( _score, s.M_state ); }
};

//
// state_with_team_affiliation_adaptor DATA ACCESS REGISTRATION (specific)
//

template < >
template < typename State >
struct our_side_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename side_type< typename player_type< State > ::type >::type side_t;

    static inline
    side_t const&
    get( state_t const& s ) { return s.M_our_side; }

    static inline
    void
    set( side_t const& _side, state_t& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        s.M_our_side = _side;
    }
};

template < >
template < typename State >
struct their_side_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename side_type< typename player_type< State > ::type >::type side_t;

    static inline
    side_t const
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        return get_opposite_side< side_t > ( s.M_our_side );
    }

};

template < >
template < typename State >
struct our_players_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename teammate_iterator_type< state_with_team_affiliation_adaptor< State > >::type teammate_iterator_t;

    static inline
    std::pair< typename teammate_iterator_t,
               typename teammate_iterator_t > const
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        return make_teammate_iterator( s.M_our_side, s.M_state );
   }
};

template < >
template < typename State >
struct their_players_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename teammate_iterator_type< state_with_team_affiliation_adaptor< State > >::type teammate_iterator_t;

    static inline
    std::pair< typename teammate_iterator_t,
               typename teammate_iterator_t > const
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        return make_teammate_iterator( get_opposite_side< side_t >( s.M_our_side ), s.M_state );
    }
};

template < >
template < typename State >
struct our_players_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename teammate_iterator_type< state_with_team_affiliation_adaptor< State > >::type teammate_iterator_t;

    static inline
    std::pair< typename teammate_iterator_t,
               typename teammate_iterator_t > const
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        return make_teammate_iterator( s.M_our_side, s.M_state );
   }
};

template < >
template < typename State >
struct our_score_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename side_type< typename player_type< State > ::type >::type side_t;
    typedef typename score_type< State >::type score_t;

    static inline
    side_t const&
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        return get_score( s.M_our_side, s.M_state );
    }

    static inline
    void
    set( const score_t _score, state_t& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        set_score( s.M_our_side, _score, s.M_state );
    }
};

template < >
template < typename State >
struct their_score_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename side_type< typename player_type< State > ::type >::type side_t;
    typedef typename score_type< State >::type score_t;

    static inline
    side_t const&
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        return get_score( get_opposite_side< side_t >( s.M_our_side ), s.M_state );
    }

    static inline
    void
    set( const score_t& _score, state_t& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        set_score( get_opposite_side< side_t >( s.M_our_side ), _score, s.M_state );
    }
};

template < >
template < typename State >
struct our_team_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename side_type< typename player_type< State > ::type >::type side_t;
    typedef typename team_type< State >::type team_t;

    static inline
    team_t const&
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t >() );

        return get_team( s.M_our_side, s.M_state );
    }

    static inline
    void
    set( const team_t& _team, state_t& s ) {
        assert( s.M_our_side != get_unknown_side< side_t >() );

        set_team( s.M_our_side, _team, s.M_state );
    }
};

template < >
template < typename State >
struct their_score_access< state_with_team_affiliation_adaptor< State > >
{
    typedef state_with_team_affiliation_adaptor< State > state_t;
    typedef typename side_type< typename player_type< State > ::type >::type side_t;
    typedef typename team_type< State >::type team_t;

    static inline
    team_t const&
    get( state_t const& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        return get_team( get_opposite_side< side_t >( s.M_our_side ), s.M_state );
    }

    static inline
    void
    set( const team_t _team, state_t& s ) {
        assert( s.M_our_side != get_unknown_side< side_t>() );

        set_score( get_opposite_side< side_t >( s.M_our_side ), _team, s.M_state );
    }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_STATE_WITH_TEAM_AFFILIATION_HPP
