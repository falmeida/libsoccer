#ifndef SOCCER_FILTERED_STATE_HPP
#define SOCCER_FILTERED_STATE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/traits/state_traits.hpp>

#include <boost/iterator/filter_iterator.hpp>
#include <boost/concept_check.hpp>

#include <algorithm>


SOCCER_NAMESPACE_BEGIN

//===========================================================================
// Filtered State

template < typename State,
           typename PlayerIdPredicate >
struct filtered_state
{
    PlayerIdPredicate M_player_id_predicate;

    // Constructors
    filtered_state( )
        : M_state( NULL )
    {

    }

    filtered_state( State const& state, PlayerIdPredicate pred)
         : M_state( &state )
         , M_player_id_predicate( pred )
    { }

    State* M_state;
};

// Helper functions
template < typename State,
           typename PlayerIdPredicate >
inline filtered_state< State, PlayerIdPredicate >
make_filtered_state( State& state, PlayerIdPredicate _pid_pred )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( boost::UnaryPredicateConcept< PlayerIdPredicate,
                                                         typename player_id_type< State >::type > ));

    return filtered_state< State, PlayerIdPredicate >( state, _pid_pred );
}

SOCCER_NAMESPACE_END

//
// filtered_state REGISTRATION

#include <soccer/register/state.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// filtered_state TYPE REGISTRATION
template <>
template < typename State,
           typename PlayerIdPredicate >
struct tag< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef filtered_state_tag type;
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct player_id_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef typename player_id_type< State >::type type;
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct player_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef typename player_type< State >::type type;
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct ball_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef typename ball_type< State >::type type;
};

template < typename State,
           typename PlayerIdPredicate >
struct playmode_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef typename playmode_type< State >::type type;
};

template < typename State,
           typename PlayerIdPredicate >
struct time_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef typename time_type< State >::type type;
};

template < typename State,
           typename PlayerIdPredicate >
struct score_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef typename score_type< State >::type type;
};

template < typename State,
           typename PlayerIdPredicate >
struct team_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef typename team_type< State >::type type;
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct player_iterator_type < soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef boost::filter_iterator< PlayerIdPredicate,
                                    typename player_iterator_type< State >
                                  > type;
};

//
// filtered_state ACCESS REGISTRATION
//

template <>
template < typename State,
           typename PlayerIdPredicate >
struct player_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename player_type< filtered_state_t >::type
    get ( typename player_id_t< filtered_state_t >::type const pid,
          filtered_state_t const & state )
    {
        assert( state.M_state != NULL && state.M_player_id_predicate( pid ) );

        return get_player( pid, *state.M_state );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct player_iterators_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    std::pair< typename player_iterator_type< filtered_state_t >::type,
               typename player_iterator_type< filtered_state_t >::type >
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        typename player_iterator_type< State >::type _player_begin, _player_end;
        boost::tie( _player_begin, _player_end ) = get_players( *state.M_state );
        return std::make_pair( boost::make_filter_iterator( state.M_player_id_predicate, _player_begin, _player_end ),
                               boost::make_filter_iterator( state.M_player_id_predicate, _player_end, _player_end ) );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct ball_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename ball_type< filtered_state_t >::type
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        return get_ball( *state.M_state );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct playmode_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename playmode_type< filtered_state_t >::type
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        return get_playmode( *state.M_state );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct time_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename time_type< filtered_state_t >::type
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        return get_time( *state.M_state );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct left_score_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename score_type< filtered_state_t >::type
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        return get_left_score( *state.M_state );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct right_score_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename score_type< filtered_state_t >::type
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        return get_right_score( *state.M_state );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct left_team_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename team_type< filtered_state_t >::type
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        return get_left_team( *state.M_state );
    }
};

template <>
template < typename State,
           typename PlayerIdPredicate >
struct right_team_access< soccer::filtered_state< State, PlayerIdPredicate > >
{
    typedef soccer::filtered_state< State, PlayerIdPredicate > filtered_state_t;

    static inline
    typename team_type< filtered_state_t >::type
    get ( filtered_state_t const & state )
    {
        assert( state.M_state != NULL );

        return get_right_team( *state.M_state );
    }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_FILTERED_STATE_HPP
