#ifndef SOCCER_INTERCEPTION_HPP
#define SOCCER_INTERCEPTION_HPP

#include <soccer/move.hpp>

namespace soccer {

//
// interception implementation
//
/*!
 * \struct interception
 * \brief Describes how an interception should be executed
 */
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed = double >
struct interception
    : public move<PlayerId,Duration, Position, Speed>
{
    typedef Position position_descriptor;
    typedef interception_tag action_category;

    interception() :
        move< PlayerId,Duration,Position,Speed>( ActionType::Intercept )
            { }

    Position M_target_position;
    Speed M_first_speed;
};

} // end namespace soccer


#include <soccer/core/tags.hpp>

namespace soccer { namespace traits {

//
// interception TYPE TRAITS REGISTRATION
//
template <>
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed >
struct tag< ::soccer::interception< PlayerId, Duration, Position, Speed > > { typedef interception_tag type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct player_id_type< ::soccer::interception< PlayerId, Duration, Position, Speed > > { typedef PlayerId type; };

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed >
struct duration_type< ::soccer::interception< PlayerId, Duration, Position, Speed > > { typedef Duration type; };


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct position_type< ::soccer::interception< PlayerId, Duration, Position, Speed > > { typedef Position type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct speed_type< ::soccer::interception< PlayerId, Duration, Position, Speed > > { typedef Speed type; };

//
// interception DATA ACCESS REGISTRATION
//

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct executor_id_access< ::soccer::interception< PlayerId, Duration, Position, Speed > >
{
    typedef ::soccer::interception< PlayerId, Duration, Position, Speed > interception_t;

    static inline
    PlayerId const& get( interception_t const& _interception ) { return _interception.M_executor_id; }

    static inline
    void set( PlayerId const& pid, interception_t& _interception ) { _interception.M_executor_id = pid; }
};

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed >
struct duration_access< ::soccer::interception< PlayerId, Duration, Position, Speed > >
{
    typedef ::soccer::interception< PlayerId, Duration, Position, Speed > interception_t;

    static inline
    Duration const& get( interception_t const& _interception ) { return _interception.M_duration; }

    static inline
    void set( Duration const& dur, interception_t& _interception ) { _interception.M_duration = dur; }
};


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct position_access< ::soccer::interception< PlayerId, Duration, Position, Speed > >
{
    typedef ::soccer::interception< PlayerId, Duration, Position, Speed > interception_t;

    static inline
    Position const& get( interception_t const& _interception ) { return _interception.M_target_position; }

    static inline
    void set( Position const& pos, interception_t& _interception ) { _interception.M_target_position = pos; }
};

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct speed_access< ::soccer::interception< PlayerId, Duration, Position, Speed > >
{
    typedef ::soccer::interception< PlayerId, Duration, Position, Speed > interception_t;

    static inline
    Speed const& get( interception_t const& _interception ) { return _interception.M_first_speed; }

    static inline
    void set( Speed const& _speed, interception_t& _interception ) { _interception.M_first_speed = _speed; }
};

}} // end namespace soccer::traits

#endif // SOCCER_INTERCEPT_HPP
