#ifndef SOCCER_SHOOT_HPP
#define SOCCER_SHOOT_HPP

#include <soccer/action.hpp>

namespace soccer {

//
// shoot implementation
//
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed
           >
struct shoot
    : public action<PlayerId,Duration>
{
    typedef Position position_descriptor;
    typedef shoot_tag action_category;

    shoot()
        : action<PlayerId,Duration>(ActionType::Shoot)
    { }

    virtual ~shoot() { }

    Position M_target_position;
    Speed M_ball_first_speed;
};

} // end namespace soccer


namespace soccer { namespace traits {

//
// shoot TYPE TRAITS REGISTRATION
//
template <>
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed >
struct tag< soccer::shoot< PlayerId, Duration, Position, Speed > > { typedef shoot_tag type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct player_id_type< soccer::shoot< PlayerId, Duration, Position, Speed > > { typedef PlayerId type; };

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed >
struct duration_type< soccer::shoot< PlayerId, Duration, Position, Speed > > { typedef Duration type; };


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct position_type< soccer::shoot< PlayerId, Duration, Position, Speed > > { typedef Position type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct speed_type< soccer::shoot< PlayerId, Duration, Position, Speed > > { typedef Speed type; };


//
// shoot DATA ACCESS REGISTRATION
//

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct executor_id_access< soccer::shoot< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::shoot< PlayerId, Duration, Position, Speed > shoot_t;

    static inline
    PlayerId const& get( shoot_t const& _shoot ) { return _shoot.M_executor_id; }

    static inline
    void set( PlayerId const& pid, shoot_t& _shoot ) { _shoot.M_executor_id = pid; }
};

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed >
struct duration_access< soccer::shoot< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::shoot< PlayerId, Duration, Position, Speed > shoot_t;

    static inline
    Duration const& get( shoot_t const& _shoot ) { return _shoot.M_duration; }

    static inline
    void set( Duration const& dur, shoot_t& _shoot ) { _shoot.M_duration = dur; }
};


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct position_access< soccer::shoot< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::shoot< PlayerId, Duration, Position, Speed > shoot_t;

    static inline
    Position const& get( shoot_t const& _shoot ) { return _shoot.M_target_position; }

    static inline
    void set( Position const& pos, shoot_t& _shoot ) { _shoot.M_target_position = pos; }
};

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct ball_first_speed_access< soccer::shoot< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::shoot< PlayerId, Duration, Position, Speed > shoot_t;

    static inline
    Speed const& get( shoot_t const& _shoot ) { return _shoot.M_ball_first_speed; }

    static inline
    void set( Speed const& _speed, shoot_t& _shoot ) { _shoot.M_ball_first_speed = _speed; }
};

}} // end namespace soccer::traits

#endif // SOCCER_SHOOT_HPP
