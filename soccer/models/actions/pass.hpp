#ifndef SOCCER_PASS_HPP
#define SOCCER_PASS_HPP

#include <soccer/action.hpp>
#include <soccer/core/tags.hpp>

namespace soccer {

//
// pass implementation
//
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed = double,
           typename Direction = double >
struct pass
    : action< PlayerId, Duration >
{
    pass()
        : action<PlayerId,Duration>(ActionType::Pass)
    { }

    PlayerId M_receiver_id;
    Position M_receive_position;
    Speed M_ball_first_speed;
    Direction M_direction;
};

} // end namespace soccer


namespace soccer { namespace traits {

//
// pass TYPE TRAITS REGISTRATION
//
template <>
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed,
           typename Direction >
struct tag< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > > { typedef pass_tag type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct player_id_type< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > > { typedef PlayerId type; };

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed,
          typename Direction >
struct duration_type< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > > { typedef Duration type; };


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct position_type< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > > { typedef Position type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct speed_type< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > > { typedef Speed type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct direction_type< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > > { typedef Direction type; };


//
// pass DATA ACCESS REGISTRATION
//

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct executor_id_access< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > >
{
    typedef ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > pass_t;

    static inline
    PlayerId const& get( pass_t const& _pass ) { return _pass.M_executor_id; }

    static inline
    void set( PlayerId const& pid, pass_t& _pass ) { _pass.M_executor_id = pid; }
};

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct receiver_id_access< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > >
{
    typedef ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > pass_t;

    static inline
    PlayerId const& get( pass_t const& _pass ) { return _pass.M_receiver_id; }

    static inline
    void set( PlayerId const& pid, pass_t& _pass ) { _pass.M_receiver_id = pid; }
};

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed,
          typename Direction >
struct duration_access< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > >
{
    typedef ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > pass_t;

    static inline
    Duration const& get( pass_t const& _pass ) { return _pass.M_duration; }

    static inline
    void set( Duration const& dur, pass_t& _pass ) { _pass.M_duration = dur; }
};


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct position_access< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > >
{
    typedef ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > pass_t;

    static inline
    Position const& get( pass_t const& _pass ) { return _pass.M_receive_position; }

    static inline
    void set( Position const& pos, pass_t& _pass ) { _pass.M_receive_position = pos; }
};

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed,
         typename Direction >
struct velocity_access< ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > >
{
    typedef ::soccer::pass< PlayerId, Duration, Position, Speed,  Direction > pass_t;

    static inline
    Speed const& get( pass_t const& _pass ) { return _pass.M_ball_first_speed; }

    static inline
    void set( Speed const& _speed, pass_t& _pass ) { _pass.M_ball_first_speed = _speed; }
};

}} // end namespace soccer::traits

#endif // SOCCER_PASS_HPP
