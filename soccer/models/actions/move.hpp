#ifndef SOCCER_MOVE_HPP
#define SOCCER_MOVE_HPP

#include <soccer/action.hpp>

namespace soccer {

//
// move implementation
//
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed = double
           >
struct move
	: public action<PlayerId,Duration> {
    typedef Position position_descriptor;
    typedef move_tag action_category;

    move( const ActionType = ActionType::Move)
        : action<PlayerId,Duration>(ActionType::Move) { }

    Position M_target_position;
    Speed M_first_speed;
};


}

#include <soccer/core/access.hpp>

namespace soccer { namespace traits {

//
// move TYPE TRAITS REGISTRATION
//
template <>
template < typename PlayerId,
           typename Duration,
           typename Position,
           typename Speed >
struct tag< soccer::move< PlayerId, Duration, Position, Speed > > { typedef move_tag type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct player_id_type< soccer::move< PlayerId, Duration, Position, Speed > > { typedef PlayerId type; };

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed >
struct duration_type< soccer::move< PlayerId, Duration, Position, Speed > > { typedef Duration type; };


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct position_type< soccer::move< PlayerId, Duration, Position, Speed > > { typedef Position type; };

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct speed_type< soccer::move< PlayerId, Duration, Position, Speed > > { typedef Speed type; };

//
// move DATA ACCESS REGISTRATION
//

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct executor_id_access< soccer::move< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::move< PlayerId, Duration, Position, Speed > move_t;

    static inline
    PlayerId const& get( move_t const& _move ) { return _move.M_executor_id; }

    static inline
    void set( PlayerId const& pid, move_t& _move ) { _move.M_executor_id = pid; }
};

template <>
template < typename PlayerId,
          typename Duration,
          typename Position,
          typename Speed >
struct duration_access< soccer::move< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::move< PlayerId, Duration, Position, Speed > move_t;

    static inline
    Duration const& get( move_t const& _move ) { return _move.M_duration; }

    static inline
    void set( Duration const& dur, move_t& _move ) { _move.M_duration = dur; }
};


template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct position_access< soccer::move< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::move< PlayerId, Duration, Position, Speed > move_t;

    static inline
    Position const& get( move_t const& _move ) { return _move.M_target_position; }

    static inline
    void set( Position const& pos, move_t& _move ) { _move.M_target_position = pos; }
};

template <>
template < typename PlayerId,
         typename Duration,
         typename Position,
         typename Speed >
struct speed_access< soccer::move< PlayerId, Duration, Position, Speed > >
{
    typedef soccer::move< PlayerId, Duration, Position, Speed > move_t;

    static inline
    Speed const& get( move_t const& _move ) { return _move.M_first_speed; }

    static inline
    void set( Speed const& _speed, move_t& _move ) { _move.M_first_speed = _speed; }
};



}} // end namespace soccer::traits

#endif // SOCCER_MOVE_HPP
