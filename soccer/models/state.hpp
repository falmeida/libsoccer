#ifndef SOCCER_STATE_HPP
#define SOCCER_STATE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/traits/state_traits.hpp>
#include <boost/iterator/transform_iterator.hpp>

#include <soccer/core/access.hpp>
#include <soccer/core/tags.hpp>

#include <boost/pending/property.hpp>

#include <map>
#include <functional>


SOCCER_NAMESPACE_BEGIN

/*!
 * \brief generic state structure
 */
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy = use_default,
           typename RightStrategy = use_default,
           typename StateProperties = boost::no_property,
           typename PlayerProperties = boost::no_property >
struct state {
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > self_t;
    typedef std::map< PlayerId, Player > PlayerMap;

    typedef boost::transform_iterator< std::function<PlayerId(typename PlayerMap::value_type)>,
                                       typename PlayerMap::const_iterator > player_iterator_t;

    /*!
     * \brief default constructor
     */
    state()
    {

    }

    std::pair< player_iterator_t,
               player_iterator_t > players( ) const
    {
        static std::function<PlayerId(const typename PlayerMap::value_type)>
                       map_key_getter = [](typename PlayerMap::value_type const& v){ return v.first; };

        return std::make_pair( boost::make_transform_iterator( M_players.begin(), map_key_getter ),
                               boost::make_transform_iterator( M_players.end(), map_key_getter ));
    }

    PlayerMap M_players;
    Ball M_ball;
    Time M_time;
    PlayMode M_playmode;
    Team M_left_team;
    Team M_right_team;
    Score M_left_score;
    Score M_right_score;
    LeftStrategy M_left_strategy;
    RightStrategy M_right_strategy;
};

SOCCER_NAMESPACE_END


#include <soccer/register/state.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// state TYPE REGISTRATION
//
template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct tag< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef state_tag type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct player_id_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef PlayerId type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct player_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef Player type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct player_iterator_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
    { typedef typename state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy >::player_iterator_t type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct ball_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef Ball type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy  >
struct time_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef Time type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct playmode_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef PlayMode type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct team_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef Team type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct score_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef Score type; };


template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct left_strategy_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef LeftStrategy type; };

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct right_strategy_type< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > > { typedef RightStrategy type; };

//
// state DATA ACCESS REGISTRATION
//

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct player_iterators_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;
    typedef typename state_t::player_iterator_t player_iterator_t;

    static inline
    std::pair< player_iterator_t, player_iterator_t >
    get( state_t const& s )
    {
        return s.players();
    }
};

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct player_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    Player const&
    get( PlayerId const& pid, state_t const& s )
    {
        return s.M_players.at( pid );
    }

    static inline
    void
    set( PlayerId const& pid,
         Player const& p,
         state_t& s )
    {
        s.M_players[ pid ] = p;
    }
};

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct ball_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    Ball const&
    get( state_t const& s ) { return s.M_ball; }

    static inline
    void
    set( Ball const& b, state_t& s ) { s.M_ball = b; }
};


template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct time_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    Time const&
    get( state_t const& s ) { return s.M_time; }

    static inline
    void
    set( Time const& _time, state_t& s ) { s.M_time = _time; }
};

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct playmode_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    PlayMode const&
    get( state_t const& s ) { return s.M_playmode; }

    static inline
    void
    set( PlayMode const& _pmode, state_t& s ) { s.M_playmode = _pmode; }
};


template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct left_team_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    Team const&
    get( state_t const& s ) { return s.M_left_team; }

    static inline
    void
    set( Team const& _team, state_t& s ) { s.M_left_team = _team; }
};

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct right_team_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    Team const&
    get( state_t const& s ) { return s.M_right_team; }

    static inline
    void
    set( Team const& _team, state_t& s ) { s.M_right_team = _team; }
};



template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct left_score_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    Score const&
    get( state_t const& s ) { return s.M_left_score; }

    static inline
    void
    set( Score const& _score, state_t& s ) { s.M_left_score = _score; }
};

template < >
template < typename PlayerId,
           typename Player,
           typename Ball,
           typename Time,
           typename PlayMode,
           typename Team,
           typename Score,
           typename LeftStrategy,
           typename RightStrategy >
struct right_score_access< state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > >
{
    typedef state< PlayerId, Player, Ball, Time, PlayMode, Team, Score, LeftStrategy, RightStrategy > state_t;

    static inline
    Score const&
    get( state_t const& s ) { return s.M_right_score; }

    static inline
    void
    set( Score const& _score, state_t& s ) { s.M_right_score = _score; }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_STATE_HPP
