#ifndef SOCCER_INERTIA_MODEL_HPP
#define SOCCER_INERTIA_MODEL_HPP

#include <soccer/register/util.hpp>

namespace soccer {

template < typename Decay >
struct inertia_model {
    Decay M_decay;

};

} // end namespace soccer

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// state TYPE REGISTRATION
//
template < >
template < typename Decay >
struct tag< inertia_model< Decay > > { typedef inertia_model_tag type; };

template < >
template < typename Decay >
struct decay_type< inertia_model< Decay > > { typedef Decay type; };

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_INERTIA_MODEL_HPP
