#ifndef SOCCER_PLAYER_TYPE_HPP
#define SOCCER_PLAYER_TYPE_HPP

namespace soccer {

    template <typename Coordinate = double,
              typename Attraction = double>
    struct player_type
    {
        typedef Coordinate coordinate_descriptor;
        typedef Attraction attraction_descriptor;


        player_type() { }
        player_type( PlayerRole player_role,
                    Attraction attraction_x, Attraction attraction_y,
                    Attraction area_attr_attack, Attraction area_attr_defense,
                    bool behind_ball,
                    Coordinate min_pos_x, Coordinate max_pos_x,
                    Coordinate min_pos_y, Coordinate max_pos_y,
                    DecisionAlgorithm decision_algorithm);

        // Acessors
        PlayerRole role() const { return M_role; }
        Attraction attractionX() const { return M_attraction_x; }
        Attraction attractionY() const { return M_attraction_y; }
        Attraction areaAttrAttack() const { return M_area_attr_attack; }
        Attraction areaAttrDefense() const { return M_area_attr_defense; }
        bool behindBall() const { return M_behind_ball; }
        Coordinate minPosX() const { return M_min_pos_x; }
        Coordinate maxPosX() const { return M_max_pos_x; }
        Coordinate minPosY() const { return M_min_pos_y; }
        Coordinate maxPosY() const { return M_max_pos_y; }
        DecisionAlgorithm decisionAlgorithm() const { return M_decision_algorithm; }

        friend std::ostream& operator<<(std::ostream & os,
                                        const player_type<Coordinate, Attraction>& rhs)
        {
            os << "Role: " << rhs.M_role << std::endl;
            os << "PosConstrain: [(" << rhs.M_min_pos_x << "," << rhs.M_min_pos_y << ")"
               << " (" << rhs.M_max_pos_x << "," << rhs.M_max_pos_y << ")" << std::endl;
            os << "Attraction: [x,y]=(" << rhs.M_attraction_x << "," << rhs.M_attraction_y << ")"
               << " Defense= " << rhs.M_area_attr_defense
               << " Attack= " << rhs.M_area_attr_attack
               << std::endl;
            os << "BehindBall: " << rhs.M_behind_ball << std::endl;
            os << "DecisionAlgorithm: " << rhs.M_decision_algorithm << std::endl;

            return os;
        }

        PlayerRole M_role;
        Coordinate M_attraction_x; //  (SP)
        Coordinate M_attraction_y; //  (SP)
        Coordinate M_area_attr_attack; //  AreaAttrAttack (last 1/4) not used
        Coordinate M_area_attr_defense; //  AreaAttrAttack (last 1/4) not used
        bool M_behind_ball; // BehindBall (SP and DT)
        Coordinate M_min_pos_x; // MinX (SP and DT)
        Coordinate M_max_pos_x; // MaxX (SP and DT)
        Coordinate M_min_pos_y; // MinX (SP and DT)
        Coordinate M_max_pos_y; // MaxX (SP and DT)
        DecisionAlgorithm M_decision_algorithm;
    };

}

#include <boost/program_options.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>

namespace soccer {
/*!
 * \brief The tactic_reader struct
 */
struct player_type_reader {

    const std::string M_file_name;
    player_type_reader( const std::string& file_path )
        : M_file_name( file_path )
    {

    }

    template <typename Coordinate = double,
              typename Attraction = double>
    void operator()( player_type<Coordinate, Attraction>& pt )
    {

        std::string decision_algorithm_name = "";
        std::string player_role_name = "";
        // Declare the supported options.
        boost::program_options::options_description desc("Player Type options");
        desc.add_options()
            ("help", "produce help message")
            ("player_role", boost::program_options::value<std::string>(&player_role_name)->required(), "player role")
            ("attraction_x", boost::program_options::value<Attraction>(&pt.M_attraction_x)->required(), "attraction X")
            ("attraction_y", boost::program_options::value<Attraction>(&pt.M_attraction_y)->required(), "attraction Y")
            ("attraction_attack_area", boost::program_options::value<Attraction>(&pt.M_area_attr_attack)->required(), "attraction attack area")
            ("attraction_defense_area", boost::program_options::value<Attraction>(&pt.M_area_attr_defense)->required(), "attraction defense area")
            ("behind_ball", boost::program_options::value<bool>(&pt.M_behind_ball)->required(), "behind ball")
            ("min_x", boost::program_options::value<Coordinate>(&pt.M_min_pos_x)->required(), "min X")
            ("max_x", boost::program_options::value<Coordinate>(&pt.M_max_pos_x)->required(), "max X")
            ("min_y", boost::program_options::value<Coordinate>(&pt.M_min_pos_y)->required(), "min Y")
            ("max_y", boost::program_options::value<Coordinate>(&pt.M_max_pos_y)->required(), "max Y")
            ("decision_algorithm", boost::program_options::value<std::string>(&decision_algorithm_name), "decision algorithm")
        ;

        std::ifstream settings_file( M_file_name.c_str() );
        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_config_file( settings_file, desc), vm);
        boost::program_options::notify(vm);

        assert ( !decision_algorithm_name.empty() );
        // convert string to lower case
        std::transform(decision_algorithm_name.begin(),
                       decision_algorithm_name.end(),
                       decision_algorithm_name.begin(),
                       ::tolower);
        pt.M_decision_algorithm = soccer::string_to_decision_algorithm_map.at( decision_algorithm_name );

        assert( !player_role_name.empty() );
        // convert string to lower case
        std::transform(player_role_name.begin(),
                       player_role_name.end(),
                       player_role_name.begin(),
                       ::tolower);
        pt.M_role = soccer::string_to_player_role_map.at( player_role_name );

        if ( vm.count("help") )
        {
            std::cout << desc << std::endl;
            return;
        }

        /* std::stringstream ss_err;
        if ( !vm.count( "flux_weight ") ) { ss_err << "flux_weight "; }
        if ( !vm.count( "safety_weight ") ) { ss_err << "safety_weight "; }
        if ( !vm.count( "easiness_weight ") ) { ss_err << "easiness_weight "; }
        if ( !vm.count( "pass_weight ") ) { ss_err << "pass_weight "; }
        if ( !vm.count( "dribble_weight ") ) { ss_err << "dribble_weight "; }

        std::string err_str = ss_err.str();
        if ( !err_str.empty() )
        {
            std::cerr << "The following fields were not set" << err_str << std::endl;
        }*/
    }
};

}


#endif // SOCCER_PLAYER_TYPE_HPP
