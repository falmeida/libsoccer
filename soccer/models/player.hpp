#ifndef SOCCER_PLAYER_HPP
#define SOCCER_PLAYER_HPP

namespace soccer {

template < typename Side,
           typename Number,
           typename Position,
           typename Direction,
           typename Velocity = Position >
struct player {
    typedef Side side_t;
    typedef Number number_descriptor;
    typedef Position position_descriptor;
    typedef Direction direction_descriptor;
    typedef Velocity velocity_descriptor;


    Side M_side;
    Number M_number;
    Position M_position;
    Direction M_direction;
    Velocity M_velocity;
    bool M_goalie;
    // PlayerTypeId M_type;
};

} // end namespace soccer

#include <soccer/register/player.hpp>
#include <soccer/core/tags.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// player TYPE REGISTRATION
//
template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct tag< player< Side, Number, Position, Direction, Velocity > > { typedef player_tag type; };

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct side_type< player< Side, Number, Position, Direction, Velocity > > { typedef Side type; };

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct number_type< player< Side, Number, Position, Direction, Velocity > > { typedef Number type; };

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct position_type< player< Side, Number, Position, Direction, Velocity > > { typedef Position type; };

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct direction_type< player< Side, Number, Position, Direction, Velocity > > { typedef Direction type; };

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct velocity_type< player< Side, Number, Position, Direction, Velocity > > { typedef Velocity type; };

//
// player DATA ACCESS REGISTRATION
//
template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct side_access< player< Side, Number, Position, Direction, Velocity > >
{
    typedef player< Side, Number, Position, Direction, Velocity > player_t;
    static inline Side const & get( player_t const& p ) { return p.M_side; }

    static inline void set( Side const& _side, player_t& p ) { p.M_side = _side; }
};

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct number_access< player< Side, Number, Position, Direction, Velocity > >
{
    typedef player< Side, Number, Position, Direction, Velocity > player_t;
    static inline Number const & get( player_t const& p ) { return p.M_number; }

    static inline void set( Number const& _num, player_t& p ) { p.M_number = _num; }
};

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct position_access< player< Side, Number, Position, Direction, Velocity > >
{
    typedef player< Side, Number, Position, Direction, Velocity > player_t;
    static inline Position const & get( player_t const& p ) { return p.M_position; }

    static inline void set( Position const& _pos, player_t& p ) { p.M_position = _pos; }
};

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct direction_access< player< Side, Number, Position, Direction, Velocity > >
{
    typedef player< Side, Number, Position, Direction, Velocity > player_t;
    static inline Direction const & get( player_t const& p ) { return p.M_direction; }

    static inline void set( Direction const& _dir, player_t& p ) { p.M_direction = _dir; }
};

template <>
template <typename Side,
          typename Number,
          typename Position,
          typename Direction,
          typename Velocity >
struct velocity_access< player< Side, Number, Position, Direction, Velocity > >
{
    typedef player< Side, Number, Position, Direction, Velocity > player_t;
    static inline Velocity const & get( player_t const& p ) { return p.M_velocity; }

    static inline void set( Velocity const& _vel, player_t& p ) { p.M_velocity = _vel; }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_PLAYER_HPP
