#ifndef SOCCER_BALL_HPP
#define SOCCER_BALL_HPP

#include <soccer/object.hpp>

namespace soccer {

template < typename Position,
           typename Velocity = Position >
struct ball
    : public dynamic_object< Position, Velocity >
{

};

} // end namespace soccer

#include <soccer/register/ball.hpp>
#include <soccer/core/tags.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

//
// ball TYPE REGISTRATION
//
template <>
template < typename Position,
           typename Velocity >
struct tag< ball< Position, Velocity > > { typedef ball_tag type; };

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_BALL_HPP
