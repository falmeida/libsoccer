#ifndef SOCCER_UTIL_BARE_TYPE_HPP
#define SOCCER_UTIL_BARE_TYPE_HPP

//! Useful define macros
#include <soccer/defines.h>

#include <boost/type_traits.hpp>

SOCCER_UTIL_NAMESPACE_BEGIN

template <typename T>
struct bare_type
{
    typedef typename boost::remove_const<
                typename boost::remove_cv<
                    typename boost::remove_pointer<T>::type
                >::type
            >::type type;
};

SOCCER_UTIL_NAMESPACE_END

#endif
