#ifndef SOCCER_CONCEPTS_HPP
#define SOCCER_CONCEPTS_HPP

#include <soccer/concepts/action_chain_concepts.hpp>
#include <soccer/concepts/game_concepts.hpp>
#include <soccer/concepts/soccer_concepts.hpp>
#include <soccer/concepts/state_concepts.hpp>
#include <soccer/concepts/object_concepts.hpp>
#include <soccer/concepts/ball_concepts.hpp>
#include <soccer/concepts/match_situation_concept.hpp>
#include <soccer/concepts/action_concepts.hpp>
#include <soccer/concepts/dribble_concept.hpp>
#include <soccer/concepts/pass_concept.hpp>
#include <soccer/concepts/shoot_concept.hpp>
#include <soccer/concepts/move_concept.hpp>
#include <soccer/concepts/tackle_concept.hpp>
#include <soccer/concepts/pitch_concept.hpp>
#include <soccer/concepts/formation_concepts.hpp>
#include <soccer/concepts/flux_concepts.hpp>
#include <soccer/concepts/positionings_concepts.hpp>
#include <soccer/concepts/generator_concepts.hpp>
#include <soccer/concepts/strategy_concepts.hpp>
#include <soccer/concepts/interception_concepts.hpp>
#include <soccer/concepts/position_concepts.hpp>
#include <soccer/concepts/tactic_concepts.hpp>

#endif // SOCCER_CONCEPTS_HPP
