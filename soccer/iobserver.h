#ifndef SOCCER_IOBSERVER
#define SOCCER_IOBSERVER

template <class T>
class IObserver {
public:
    virtual ~IObserver() { }

    // Event listeners
    virtual void update(const T& object) = 0;

};

#endif
