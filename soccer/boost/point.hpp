#ifndef SOCCER_BOOST_GEOMETRY_POINT_HPP
#define SOCCER_BOOST_GEOMETRY_POINT_HPP

//! Import relevant macros
#include <soccer/defines.h>

#include <boost/geometry.hpp>

// Adapta boost geometry points to be used with soccer

SOCCER_TRAITS_NAMESPACE_BEGIN

template < typename Point >
struct coordinate_type< boost::enable_if< boost::geometry::tag< Point >::type,
                                          Point >
                      >
    { typedef boost::geometry::coordinate_type< Point > type; };

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_BOOST_GEOMETRY_POINT_HPP
