#ifndef SOCCER_BOOST_MATCHING_HPP
#define SOCCER_BOOST_MATCHING_HPP

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

//! Import relevant macros
#include <soccer/defines.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>
#include <boost/property_map/property_map.hpp>

#include <boost/functional.hpp>

#include <boost/type_traits.hpp>
#include <boost/utility/result_of.hpp>
#include <boost/concept_check.hpp>

#include <algorithm>
#include <iterator>

SOCCER_NAMESPACE_BEGIN

/*!
 * \brief Generic matching algorithm fucntion proxy based on boost
 * \tparam InputIterator1 Model of an input iterator
 * \tparam InputIterator2 Model of an input iterator
 * \tparam CostFunctor Model of a function that calculates the cost of matching items from both InputIterators
 * \tparam MatchingMap Model of a WritablePropertyMap that will store the matching that minimizes the cost
 * \param it1_begin
 * \param it1_end
 * \param it2_begin
 * \param it2_begin
 * \param cost_functor
 * \param pmap
 * \return
 */
template < typename InputIterator1,
           typename InputIterator2,
           typename CostFunctor,
           typename MatchingMap >
typename boost::result_of< CostFunctor >::type
matching( InputIterator1 it1_begin, const InputIterator1 it1_end,
          InputIterator2 it2_begin, const InputIterator2 it2_end,
          CostFunctor cost_functor,
          MatchingMap pmap
        )
{

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<InputIterator1> ));
    typedef typename boost::iterator_value< InputIterator1 >::type input_iterator1_value_type;

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept<InputIterator2> ));
    typedef typename boost::iterator_value< InputIterator2 >::type input_iterator2_value_type;

    BOOST_CONCEPT_ASSERT(( boost::WritablePropertyMapConcept<MatchingMap, input_iterator1_value_type> ));

    //! not necessary, already validated in concept assertion
    /* BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< MatchingMap >::key_type,
                                              input_iterator1_value_type>::value),
                          "MatchingMap key type and InputIterator1 value type mismatch"); */
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< typename boost::property_traits< MatchingMap >::value_type,
                                              input_iterator2_value_type>::value),
                          "MatchingMap value type and InputIterator2 value type mismatch");

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< input_iterator1_value_type,
                              typename boost::binary_traits< CostFunctor >::first_argument_type >::value ),
                            "InputIterator1 value type and CostFunctor first argument type mismatch");
    BOOST_STATIC_ASSERT_MSG(( boost::is_same< input_iterator2_value_type,
                              typename boost::binary_traits< CostFunctor >::second_argument_type >::value ),
                            "InputIterator2 value type and CostFunctor second argument type mismatch");

    BOOST_STATIC_ASSERT_MSG(( boost::is_unsigned< typename boost::binary_traits< CostFunctor >::result_type >::value ),
                            "CostFunctor result type is not unsigned");


    typedef boost::adjacency_list_traits < boost::vecS, boost::vecS, boost::directedS > adjacency_list_traits;

    typedef boost::no_property VertexProperty;

    typedef boost::property< boost::edge_capacity_t, long,
                boost::property< boost::edge_residual_capacity_t, long,
                    boost::property< boost::edge_reverse_t, typename adjacency_list_traits::edge_descriptor,
                        boost::property< boost::edge_weight_t, long
                        >
                    >
                >
            >
            EdgeProperty;

    typedef boost::adjacency_list< boost::vecS,
                                   boost::vecS,
                                   boost::directedS,
                                   VertexProperty,
                                   EdgeProperty > graph;

    typedef boost::property_map < graph, boost::edge_capacity_t >::type CapacityMap;
    typedef boost::property_map < graph, boost::edge_residual_capacity_t >::type ResidualCapacityMap;
    typedef boost::property_map < graph, boost::edge_weight_t >::type WeightMap;
    typedef boost::property_map < graph, boost::edge_reverse_t>::type ReverseEdgeMap;
    typedef boost::graph_traits<graph>::vertices_size_type vertices_size_type;
    typedef graph::vertex_descriptor vertex_descriptor;
    typedef graph::edge_descriptor edge_descriptor;
    typedef graph::edge_iterator edge_iterator;

    typedef std::map< input_iterator1_value_type, vertex_descriptor > input_iterator1_value_to_vertex_map_type;
    typedef std::map< input_iterator2_value_type, vertex_descriptor > input_iterator2_to_vertex_map_type;

    input_iterator1_value_to_vertex_map_type source_value_to_vertex_map;
    input_iterator2_to_vertex_map_type target_value_to_vertex_map;

    vertices_size_type _num_values1 = std::distance( it1_begin, it1_end );
    vertices_size_type _num_values2 = std::distance( it2_begin, it2_end );
    assert( _num_values1 == _num_values2 ); // HACK Only works for equal size sets of values, for now!!
    graph _graph( _num_values1 );

    CapacityMap capacity_map = get( boost::edge_capacity, _graph );
    ReverseEdgeMap edge_reverse_map = get( boost::edge_reverse, _graph );
    ResidualCapacityMap residual_capacity_map = get( boost::edge_residual_capacity, _graph );
    WeightMap weight_map = get( boost::edge_weight, _graph );

    auto s_vd = boost::add_vertex( _graph );
    auto t_vd = boost::add_vertex( _graph );

    auto add_edge_fn = [&] ( const vertex_descriptor s,
                             const vertex_descriptor t,
                             const long _weight,
                             const long _capacity )
    {
            bool b = false;
            edge_descriptor ed;

            boost::tie(  ed, b ) = boost::add_edge( s, t, _graph );
            if ( !b )
            {
                std::cerr << "The edge (" << s << "," << t << ") with weight=" << _weight << std::endl;
                std::abort();
            }
            weight_map[ed] = _weight;
            capacity_map[ed] = _capacity;
            return ed;
    };

    auto add_edge_rev_fn = [&] ( const vertex_descriptor s,
                                 const vertex_descriptor t,
                                 const long _weight = 0 )
    {
            auto e1 = add_edge_fn( s, t, _weight, 1 );
            auto e2 = add_edge_fn( t, s, -_weight, 0 );
            edge_reverse_map[e1] = e2;
            edge_reverse_map[e2] = e1;
    };

    // Save mapping of values to graph vertex ids
    for ( auto it1 = it1_begin; it1 != it1_end ; it1++ )
    {
        //
        // Create source vertices
        //
        auto source_pid_vd = boost::add_vertex( _graph );
        source_value_to_vertex_map[*it1]  = source_pid_vd;
        // Add edges from source to the value vertex
        add_edge_rev_fn( s_vd, source_pid_vd );
    }

    // Save mapping of values to graph vertex ids
    for ( auto it2 = it2_begin; it2 != it2_end ; it2++ )
    {
        //
        // Create target vertices
        //
        auto target_pid_vd = boost::add_vertex( _graph );
        target_value_to_vertex_map[*it2]  = target_pid_vd;

        // Add edges from target to the value vertex
        add_edge_rev_fn( target_pid_vd, t_vd );
    }

    // Create edges between values
    for ( auto its = source_value_to_vertex_map.begin() ; its != source_value_to_vertex_map.end(); its++ )
    {
        for ( auto itt = target_value_to_vertex_map.begin() ; itt != target_value_to_vertex_map.end(); itt++ )
        {
            const auto weight = cost_functor( its->first, itt->first );
            add_edge_rev_fn( its->second, itt->second, weight );
        }
    }

    boost::successive_shortest_path_nonnegative_weights( _graph, s_vd, t_vd );

#if defined(DEBUG_MATCHING_RESULT_WEIGHT) || \
    defined(DEBUG_MATCHING_RESULT_CAPACITY) || \
      defined(DEBUG_MATCHING_RESULT_RESIDUAL_CAPACITY)

    std::cout << "All Edge Weights in Matrix based format => each cell is a tuple (weight, capacity, residual_capacity)" << std::endl;
    for ( auto it1 = source_value_to_vertex_map.begin(); it1 != source_value_to_vertex_map.end(); it1++ )
    {
        for ( auto it2 = target_value_to_vertex_map.begin(); it2 != target_value_to_vertex_map.end(); it2++ )
        {
            bool b = false;
            edge_descriptor ed;
            boost::tie( ed, b ) = boost::edge( it1->second, it2->second, _graph );
            assert( b );
            #if defined(DEBUG_MATCHING_RESULT_WEIGHT) && \
                !defined(DEBUG_MATCHING_RESULT_CAPACITY) && \
                !defined(DEBUG_MATCHING_RESULT_RESIDUAL_CAPACITY)
                std::cout << weight_map[ed] << " ";
            #else
                std::cout << "(" << weight_map[ed]
                          << "," << capacity_map[ed]
                          << "," << residual_capacity_map[ed]
                          << ") ";
            #endif
        }
        std::cout << std::endl;
    }
#endif

    std::cout << "Weights=";
    for ( auto its = source_value_to_vertex_map.begin() ; its != source_value_to_vertex_map.end(); its++ )
    {
        for ( auto itt = target_value_to_vertex_map.begin() ; itt != target_value_to_vertex_map.end() ; itt++ )
        {
            bool b = false;
            edge_descriptor ed;
            boost::tie( ed, b ) = boost::edge( its->second,
                                               itt->second,
                                               _graph );
            assert( b );
            if ( residual_capacity_map[ed] == 0 )
            {
                // Found matching for value => there can be only one
                // boost::put( pmap, it1, std::make_pair( it2, weight_map[ed] ) );
                boost::put( pmap, its->first, itt->first );
                std::cout << "[(" << its->first << "," << itt->first << ")=" << weight_map[ed] << "] ";
            }
        }
    }
    std::cout << std::endl;

    auto cost =  boost::find_flow_cost( _graph );

    return cost;
}

SOCCER_NAMESPACE_END

#endif // SOCCER_BOOST_MATCHING_HPP
