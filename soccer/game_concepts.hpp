#ifndef GAME_CONCEPT_HPP
#define GAME_CONCEPT_HPP

#include <soccer/core/time_type.hpp>
#include <soccer/core/speed_type.hpp>
#include <soccer/core/size_type.hpp>

#include <boost/concept/detail/concept_def.hpp>
#include <boost/concept_check.hpp>
#include <boost/type_traits.hpp>

namespace soccer {

BOOST_concept(GameConcept,(G))
{
    typedef typename time_type< G >::type time_t;
    typedef std::size_t players_size_t;
    typedef typename speed_type< G >::type speed_t;
    typedef typename size_type< G >::type size_t;

    BOOST_STATIC_ASSERT_MSG(( boost::is_integral<players_size_t>::value),
                            "Players size type is not an integral type");
   /* BOOST_STATIC_ASSERT_MSG(( boost::is_integral<time_t>::value),
                            "Time descriptor type is not an integral type"); */

    BOOST_CONCEPT_USAGE(GameConcept)
    {
        const_constraints( t );
    };

private:
    void const_constraints( const G& const_t )
    {
        _num_players = get_num_players_per_team( const_t );

        _time = match_start_time( const_t );
        _time = normal_half_time_duration( const_t );
        _time = extra_half_time_duration( const_t );
        _time = drop_ball_time( const_t );

        _bool = has_extra_time( const_t );
        _bool = has_golden_goal( const_t );

        _num = num_penalties_in_shootout( const_t );

        // ball model type
        _size = ball_size( const_t );
        _speed = ball_decay( const_t );
        _speed = ball_speed_max( const_t );
        _speed = ball_accel_max( const_t );

        // player model type

    }

    G t;
    players_size_t _num_players;
    time_t _time;
    size_t _size;
    speed_t _speed;
    unsigned int _num;
    bool _bool;

};

}

#endif // GAME_CONCEPT_HPP
