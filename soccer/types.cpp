#include "types.h"

#include <cassert>

using namespace std;

namespace soccer {

    IntegerToScoreAssessmentMap createIntegerToScoreAssessmentMap()
    {
        IntegerToScoreAssessmentMap score_assessment_map;
        score_assessment_map.insert( std::make_pair(0, ScoreAssessment::VERY_BAD ));
        score_assessment_map.insert( std::make_pair(1, ScoreAssessment::BAD ));
        score_assessment_map.insert( std::make_pair(2, ScoreAssessment::MEDIUM ));
        score_assessment_map.insert( std::make_pair(3, ScoreAssessment::GOOD ));
        score_assessment_map.insert( std::make_pair(4, ScoreAssessment::VERY_GOOD ));

        return score_assessment_map;
    }

    StringToScoreAssessmentMap createStringToScoreAssessmentMap()
    {
        StringToScoreAssessmentMap score_assessment_map;
        score_assessment_map.insert( std::make_pair("verybad", ScoreAssessment::VERY_BAD ));
        score_assessment_map.insert( std::make_pair("bad", ScoreAssessment::BAD ));
        score_assessment_map.insert( std::make_pair("medium", ScoreAssessment::MEDIUM ));
        score_assessment_map.insert( std::make_pair("good", ScoreAssessment::GOOD ));
        score_assessment_map.insert( std::make_pair("verygood", ScoreAssessment::VERY_GOOD ));
        return score_assessment_map;
    }

    IntegerToSituationMap createIntegerToSituationMap()
    {
        IntegerToSituationMap situation_map;
        situation_map.insert( std::make_pair(0, Situation::Unknown));
        situation_map.insert( std::make_pair(1, Situation::OurAttack ));
        situation_map.insert( std::make_pair(2, Situation::TheirAttack ));
        situation_map.insert( std::make_pair(3, Situation::OurKickOff));
        situation_map.insert( std::make_pair(4, Situation::TheirKickOff));
        situation_map.insert( std::make_pair(5, Situation::OurCornerKick));
        situation_map.insert( std::make_pair(6, Situation::TheirCornerKick));
        situation_map.insert( std::make_pair(7, Situation::OurThrowIn));
        situation_map.insert( std::make_pair(8, Situation::TheirThrowIn));
        situation_map.insert( std::make_pair(9, Situation::OurFreeKick));
        situation_map.insert( std::make_pair(10, Situation::TheirFreeKick));
        situation_map.insert( std::make_pair(11, Situation::OurGoalieFreeKick));
        situation_map.insert( std::make_pair(12, Situation::TheirGoalieFreeKick));
        situation_map.insert( std::make_pair(13, Situation::OurGoalKick));
        situation_map.insert( std::make_pair(14, Situation::TheirGoalKick));
        situation_map.insert( std::make_pair(15, Situation::OurIndirectFreeKick));
        situation_map.insert( std::make_pair(16, Situation::TheirIndirectFreeKick));
        situation_map.insert( std::make_pair(17, Situation::OurPenaltyKick));
        situation_map.insert( std::make_pair(18, Situation::TheirPenaltyKick));

        return situation_map;
    }

    StringToSituationMap createStringToSituationMap()
    {
        StringToSituationMap situation_map;
        situation_map.insert( std::make_pair("default", Situation::Unknown ) );
        situation_map.insert( std::make_pair("ourattack", Situation::OurAttack ) );
        situation_map.insert( std::make_pair("theirattack", Situation::TheirAttack ) );
        situation_map.insert( std::make_pair("ourkickoff", Situation::OurKickOff) );
        situation_map.insert( std::make_pair("theirkickoff", Situation::TheirKickOff ) );
        situation_map.insert( std::make_pair("ourcornerkick", Situation::OurCornerKick) );
        situation_map.insert( std::make_pair("theircornerkick", Situation::TheirCornerKick ) );
        situation_map.insert( std::make_pair("ourthrowin", Situation::OurThrowIn ) );
        situation_map.insert( std::make_pair("theirthrowin", Situation::TheirThrowIn ) );
        situation_map.insert( std::make_pair("ourfreekick", Situation::OurFreeKick ) );
        situation_map.insert( std::make_pair("theirfreekick", Situation::TheirFreeKick ) );
        situation_map.insert( std::make_pair("ourgoaliefreekick", Situation::OurGoalieFreeKick ) );
        situation_map.insert( std::make_pair("theirgoaliefreekick", Situation::TheirGoalieFreeKick ) );
        situation_map.insert( std::make_pair("ourgoalkick", Situation::OurGoalKick ) );
        situation_map.insert( std::make_pair("theirgoalkick", Situation::TheirGoalKick ) );
        situation_map.insert( std::make_pair("ourindirectfreekick", Situation::OurIndirectFreeKick ) );
        situation_map.insert( std::make_pair("theirindirectfreekick", Situation::TheirIndirectFreeKick ) );
        situation_map.insert( std::make_pair("ourpenaltykick", Situation::OurPenaltyKick ) );
        situation_map.insert( std::make_pair("theirpenaltykick", Situation::TheirPenaltyKick ) );
        return situation_map;
    }

    IntegerToDecisionAlgorithmMap createIntegerToDecisionAlgorithmMap()
    {
        IntegerToDecisionAlgorithmMap map;
        map.insert( std::make_pair( 1, DecisionAlgorithm::Goalie ));
        map.insert( std::make_pair( 2, DecisionAlgorithm::Defense ));
        map.insert( std::make_pair( 3, DecisionAlgorithm::Attack));

        return map;
    }

    StringToDecisionAlgorithmMap createStringToDecisionAlgorithmMap()
    {
        StringToDecisionAlgorithmMap map;
        map.insert( std::make_pair( "goalie", DecisionAlgorithm::Goalie ));
        map.insert( std::make_pair( "defense", DecisionAlgorithm::Defense ));
        map.insert( std::make_pair( "attack", DecisionAlgorithm::Attack));

        return map;
    }

    IntegerToPlayerRoleMap createIntegerToPlayerRoleMap()
    {
        IntegerToPlayerRoleMap map;
        map.insert( std::make_pair( 0, PlayerRole::None ));
        map.insert( std::make_pair( 1, PlayerRole::Goaltender ));
        map.insert( std::make_pair( 2, PlayerRole::Sweeper ));
        map.insert( std::make_pair( 3, PlayerRole::Defender ));
        map.insert( std::make_pair( 4, PlayerRole::Midfielder ));
        map.insert( std::make_pair( 5, PlayerRole::Forward ));

        return map;
    }

    StringToPlayerRoleMap createStringToPlayerRoleMap()
    {
        StringToPlayerRoleMap map;
        map.insert( std::make_pair( "none", PlayerRole::None ));
        map.insert( std::make_pair( "goaltender", PlayerRole::Goaltender ));
        map.insert( std::make_pair( "sweeper", PlayerRole::Sweeper ));
        map.insert( std::make_pair( "defender", PlayerRole::Defender ));
        map.insert( std::make_pair( "midfielder", PlayerRole::Midfielder ));
        map.insert( std::make_pair( "forward", PlayerRole::Forward ));

        return map;
    }

    std::map<TeamSide, std::string> createTeamSideToStringMap()
    {
        map<TeamSide, std::string> map;
        map.insert(pair<TeamSide, std::string>( TeamSide::Left, "left" ));
        map.insert(pair<TeamSide, std::string>( TeamSide::Right, "right"));
        map.insert(pair<TeamSide, std::string>( TeamSide::Unknown, "unknown"));

        return map;
    }

    const std::string& teamSide(TeamSide team_side)
    {
        static map<TeamSide, std::string> team_side_to_string = createTeamSideToStringMap();
        return team_side_to_string.at( team_side );
    }

    std::map<std::string, TeamSide> createStringToTeamSideMap()
    {
        map<std::string, TeamSide> map;
        map.insert(pair<std::string, TeamSide>( "left", TeamSide::Left ));
        map.insert(pair<std::string, TeamSide>( "right", TeamSide::Right ));
        map.insert(pair<std::string, TeamSide>( "unknown", TeamSide::Unknown));

        return map;
    }

    TeamSide teamSide(const std::string& team_side)
    {
        static map<std::string, TeamSide> string_to_team_side = createStringToTeamSideMap();
        return string_to_team_side.at( team_side );
    }

    map<SoccerMoment, std::string> createSoccerMomentToStringMap()
    {
        map<SoccerMoment, std::string> map;
        map.insert(pair<SoccerMoment, std::string>( SoccerMoment::GameStoppedOpponentPossession, "game_stopped_their_possession" ));
        map.insert(pair<SoccerMoment, std::string>( SoccerMoment::GameStoppedOurPossession, "game_stopped_our_possession" ));
        map.insert(pair<SoccerMoment, std::string>( SoccerMoment::OurPossession, "our_possession" ));
        map.insert(pair<SoccerMoment, std::string>( SoccerMoment::OpponentPossession, "their_possession" ));
        map.insert(pair<SoccerMoment, std::string>( SoccerMoment::RegainingPossession, "regaining_possession" ));
        map.insert(pair<SoccerMoment, std::string>( SoccerMoment::LosingPossession, "losing_possession" ));

        return map;
    }

    std::map< std::string, SoccerMoment> createStringToSoccerMomentMap()
    {
        map< std::string, SoccerMoment > map;
        map.insert(pair< std::string, SoccerMoment >( "game_stopped_their_possession", SoccerMoment::GameStoppedOpponentPossession ));
        map.insert(pair< std::string, SoccerMoment >( "game_stopped_our_possession", SoccerMoment::GameStoppedOurPossession ));
        map.insert(pair< std::string, SoccerMoment >( "our_possession", SoccerMoment::OurPossession));
        map.insert(pair< std::string, SoccerMoment >( "their_possession", SoccerMoment::OpponentPossession ));
        map.insert(pair< std::string, SoccerMoment >( "regaining_possession", SoccerMoment::RegainingPossession ));
        map.insert(pair< std::string, SoccerMoment >( "losing_possession", SoccerMoment::LosingPossession ));

        return map;
    }

    TeamSide oppositeSide(TeamSide side)
    {
        assert ( side != TeamSide::Unknown );
        return side == TeamSide::Left ? TeamSide::Right : TeamSide::Left;
    }

    TeamAffiliation oppositeAffiliation(TeamAffiliation affiliation)
    {
        assert ( affiliation != TeamAffiliation::Unknown );
        return affiliation == TeamAffiliation::Our ? TeamAffiliation::Opp : TeamAffiliation::Our;
    }

    const SoccerMoment soccerMoment( const std::string& moment_str)
    {
        static map<string, SoccerMoment> string_to_moment_map = createStringToSoccerMomentMap();
        return string_to_moment_map.at( moment_str );
    }

    const std::string& soccerMoment( SoccerMoment moment )
    {
        map<SoccerMoment, string> moment_to_string_map = createSoccerMomentToStringMap();
        return moment_to_string_map.at( moment );
    }

}


std::ostream& operator<<(std::ostream& os, const soccer::TeamSide side )
{
    switch ( side)
    {
    case soccer::TeamSide::Left:
        os << string("L");
        break;
    case soccer::TeamSide::Right:
        os << string("R");
        break;
    case soccer::TeamSide::Unknown:
        os << string("U");
        break;
    default:
        assert( false );
        break;
    }

    return os;
}
