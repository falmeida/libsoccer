#ifndef SOCCER_CONCEPTS_HPP
#define SOCCER_CONCEPTS_HPP

#include <soccer/object_traits.hpp>
#include <soccer/state_traits.hpp>
#include <soccer/player_traits.hpp>
#include <soccer/ball_traits.hpp>
#include <soccer/position_concepts.hpp>
#include <soccer/velocity_traits.hpp>

/// Concept checks
#include <boost/functional.hpp>
#include <boost/iterator/iterator_traits.hpp>
#include <boost/type_traits.hpp>

#include <boost/concept/assert.hpp>
#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {


BOOST_concept(DirectionConcept,(Direction))
{

    BOOST_CONCEPT_USAGE(DirectionConcept)
    {
        const_constraints( dir );
    }

private:
    void const_constraints(const Direction& )
    {
        /* bool is_left = isLeft( const_dir, const_dir );
        bool is_right = isRight( const_dir, const_dir );
        bool is_behind = isBehind( const_dir, const_dir );
        bool is_in_front = isInFront( const_dir, const_dir ); */
    }

    Direction dir;
};


BOOST_concept(VelocityConcept,(Velocity))
{
    // typedef typename velocity_traits<Velocity>::direction_descriptor direction_descriptor;
    // typedef typename velocity_traits<Velocity>::magnitude_descriptor magnitude_descriptor;

    BOOST_CONCEPT_USAGE(VelocityConcept)
    {
        // boost::function_requires< DirectionConcept< direction_descriptor > >();
        // boost::function_requires< boost::AssignableConcept< magnitude_descriptor > >();
        // boost::function_requires< boost::ComparableConcept< magnitude_descriptor > >();

        const_constraints( vel );
    }

private:
    void const_constraints(const Velocity& /*const_vel*/ )
    {
        // mag = magnitude( const_vel );
        // dir = direction( const_vel );
    }

    Velocity vel;
    // magnitude_descriptor mag;
    // direction_descriptor dir;
};


BOOST_concept(TimeConcept,(T))
{

    BOOST_CONCEPT_ASSERT(( boost::ComparableConcept<T> ));
    BOOST_CONCEPT_ASSERT(( boost::AssignableConcept<T> ));

    BOOST_CONCEPT_USAGE(TimeConcept)
    {

    }
};


} // end namespace soccer

#endif // SOCCER_CONCEPTS_HPP
