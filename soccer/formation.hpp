#ifndef SOCCER_FORMATION_HPP
#define SOCCER_FORMATION_HPP

#include <soccer/state_concepts.hpp>

namespace soccer {

template < typename State >
struct formation_state {
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    State const* M_state;
    formation_state( State const& _state )
        : M_state( &_state )
    {

    }

};

}
#endif // SOCCER_FORMATION_HPP
