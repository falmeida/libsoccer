#ifndef SOCCER_POSITION_CONCEPTS_HPP
#define SOCCER_POSITION_CONCEPTS_HPP

#include <soccer/position_traits.hpp>

#include <boost/geometry/geometry.hpp>

#include <boost/type_traits.hpp>
#include <boost/concept/detail/concept_def.hpp>
#include <boost/concept_check.hpp>

namespace soccer {

BOOST_concept(PositionConcept,(P))
{
    // typedef position_traits<P> position_traits_t;
    // typedef typename position_traits_t::coordinate_descriptor coordinate_descriptor;
    // typedef typename position_traits_t::distance_descriptor distance_descriptor;
    // typedef typename position_traits<P>::direction_descriptor direction_descriptor;

    /* BOOST_CONCEPT_ASSERT(( boost::AssignableConcept< distance_descriptor > ));
    BOOST_CONCEPT_ASSERT(( boost::ComparableConcept< distance_descriptor > ));
    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructibleConcept< distance_descriptor > )); */
    /* BOOST_CONCEPT_ASSERT(( boost::AssignableConcept< coordinate_descriptor > ));
    BOOST_CONCEPT_ASSERT(( boost::ComparableConcept< coordinate_descriptor > ));
    BOOST_CONCEPT_ASSERT(( boost::DefaultConstructibleConcept< coordinate_descriptor > ));

    BOOST_CONCEPT_ASSERT(( boost::AssignableConcept< P > )); */

    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::ConstPoint<P> ));

    BOOST_CONCEPT_USAGE(PositionConcept)
    {
        const_constraints( pos );
    }

private:
    void const_constraints(const P& )
    {

    }

    P pos;
};

BOOST_concept(MutablePositionConcept,(P))
    : public PositionConcept<P>
{
    // typedef position_traits<P> position_traits_t;
    // typedef typename position_traits_t::coordinate_descriptor coordinate_descriptor;

    BOOST_CONCEPT_ASSERT(( boost::geometry::concept::Point<P> ));

    BOOST_CONCEPT_USAGE(MutablePositionConcept)
    {

    }

private:
     P pos;
};


}

#endif // SOCCRE_POSITION_CONCEPTS_HPP
