#ifndef SOCCER_GEOMETRY_DISTANCE_HPP
#define SOCCER_GEOMETRY_DISTANCE_HPP

#include <soccer/position_concepts.hpp>

#include <soccer/core/position_type.hpp>
#include <soccer/core/tag.hpp>
#include <soccer/core/tags.hpp>

#include <boost/geometry.hpp>

#include <boost/mpl/if.hpp>
#include <boost/mpl/and.hpp>
#include <boost/mpl/or.hpp>
#include <boost/mpl/not.hpp>
#include <boost/mpl/has_xxx.hpp>
#include <boost/mpl/assert.hpp>

// Getter/setter version
#define BOOST_GEOMETRY_DETAIL_SPECIALIZE_POINT_ACCESS_GET_SET_FREE_FUNCTION(Point, Dim, CoordinateType, Get, Set) \
    template<> struct access<Point, Dim> \
    { \
        static inline CoordinateType get(Point const& p) { return  Get( p ) (); } \
        static inline void set(Point& p, CoordinateType const& value) { Set( value, p ); } \
    };

/*!
\brief \brief_macro{2D point type} \brief_macro_getset
\ingroup register
\details \details_macro{BOOST_GEOMETRY_REGISTER_POINT_2D_GET_SET, two-dimensional point type}. \details_macro_getset
\param Point \param_macro_type{Point}
\param CoordinateType \param_macro_coortype{point}
\param CoordinateSystem \param_macro_coorsystem
\param Get0 \param_macro_getset{get, \macro_x}
\param Get1 \param_macro_getset{get, \macro_y}
\param Set0 \param_macro_getset{set, \macro_x}
\param Set1 \param_macro_getset{set, \macro_y}
*/
#define BOOST_GEOMETRY_REGISTER_POINT_2D_GET_SET_FREE_FUNCTION(Point, CoordinateType, CoordinateSystem, Get0, Get1, Set0, Set1) \
namespace boost { namespace geometry { namespace traits {  \
    BOOST_GEOMETRY_DETAIL_SPECIALIZE_POINT_TRAITS(Point, 2, CoordinateType, CoordinateSystem) \
    BOOST_GEOMETRY_DETAIL_SPECIALIZE_POINT_ACCESS_GET_SET_FREE_FUNCTION(Point, 0, CoordinateType, Get0, Set0) \
    BOOST_GEOMETRY_DETAIL_SPECIALIZE_POINT_ACCESS_GET_SET_FREE_FUNCTION(Point, 1, CoordinateType, Get1, Set1) \
}}}


namespace soccer {

namespace detail {
    BOOST_MPL_HAS_XXX_TRAIT_DEF( object_category );
}

/* template < typename Object1,
           typename Object2,
           typename Strategy >
double // typename boost::geometry::default_distance_result< Object1, Object2, Strategy >::type
distance( const Object1& obj1,
          const Object1& obj2,
          Strategy strategy )
{
    typedef typename boost::mpl::if_< boost::mpl::and_< boost::mpl::not_< typename boost::is_same< typename boost::geometry::tag< Object1 >::type, void > >,
                                                        boost::mpl::not_< typename boost::is_same< typename boost::geometry::tag< Object2 >::type, void > >
                                                      >,
                                       boost_geometry_distance_impl< Object1, Object2, Strategy >,
                                       void
                                    >::type impl_type;
    // typedef boost::mpl::if_< >::type impl_type;
    //       // BOOST_CONCEPT_ASSERT(( PositionConcept<Position1> ));
            // BOOST_CONCEPT_ASSERT(( PositionConcept<Position2> ));

    return impl_type::apply( obj1, obj2, strategy );
} */

namespace dispatch
{
    template < typename Object1Tag, typename Object2Tag, typename Object1, typename Object2 >
    struct distance {
        BOOST_MPL_ASSERT_MSG( false, NOT_IMPLEMENTED_FOR_THIS_TYPE, (types<Object1Tag,Object2Tag, Object1, Object2>) );
    };

    template < typename Point1, typename Point2 >
    struct distance < point_tag, point_tag, Point1, Point2 >
    {
        static
        inline typename boost::geometry::default_distance_result< Point1, Point2 >::type
        apply( Point1 const& pos1, Point2 const& pos2 )
        {
            return boost::geometry::distance( pos1, pos2 );
        }
    };

    template < typename Point1, typename Object2 >
    struct distance < point_tag, typename boost::is_base_of< static_object_tag, Object2 >::type, Point1, Object2 >
    {
        static
        inline typename boost::geometry::default_distance_result< Point1, typename position_type< Object2 >::type >::type
        apply( Point1 const& pos1, Object2 const& obj2 )
        {
            return distance< position_tag,
                             typename tag< typename position_type< Object2 >::type >::type,
                             Point1,
                             typename position_type< Object2 >::type
                    >::apply( pos1, get_position( obj2 ));
        }
    };

    template < typename Object1, typename Point2 >
    struct distance < typename boost::is_base_of< static_object_tag, Object1 >::type, point_tag, Object1, Point2 >
    {
        static
        inline typename boost::geometry::default_distance_result< typename position_type< Object1 >::type, Point2 >::type
        apply( Object1 const& obj1, Point2 const& pos2 )
        {
            return distance< typename tag< typename position_type< Object1 >::type >::type,
                             point_tag,
                             typename position_type< Object1 >::type,
                             Point2
                    >::apply( get_position( obj1 ), pos2 );
        }
    };

    template < typename Object1, typename Object2 >
    struct distance< typename boost::is_base_of< static_object_tag, Object1 >::type,
                     typename boost::is_base_of< static_object_tag, Object2 >::type,
                     Object1,
                     Object2 >
    {
        static
        inline typename boost::geometry::default_distance_result< typename position_type< Object1 >::type,
                                                                  typename position_type< Object2 >::type >::type
        apply( Object1 const& obj1, Object2 const& obj2 )
        {
            return distance< typename tag< typename position_type< Object1 >::type >::type,
                             typename tag< typename position_type< Object2 >::type >::type,
                             typename position_type< Object1 >::type,
                             typename position_type< Object2 >::type
                    >::apply( get_position( obj1 ), get_position( obj2 ) );
        }
    };
} // end namespace dispatch

/*!
\brief Get the distance between two position concepts
\note proxy method for boost::geometry::distance
\tparam Position1 Position descriptor
\tparam Position2 Position descriptor
\param pos1
\param pos2
\return the distance between the two positions
*/
template < typename Object1,
           typename Object2 >
double // HACK typename default_distance_result<Object1, Object2>::type
distance( Object1 const& obj1,
          Object2 const& obj2 )
{
   // BOOST_CONCEPT_ASSERT(( PositionConcept<Position1> ));
   // BOOST_CONCEPT_ASSERT(( PositionConcept<Position2> ));

   return dispatch::distance< typename tag< Object1 >::type,
                              typename tag< Object2 >::type,
                              Object1,
                              Object2
                            >::apply( obj1, obj2 );
}


} // end namespace soccer

#endif // SOCCER_GEOMETRY_DISTANCE_HPP
