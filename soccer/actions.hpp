#ifndef SOCCER_ACTIONS_HPP
#define SOCCER_ACTIONS_HPP

#include <soccer/shoot.hpp>
#include <soccer/pass.hpp>
#include <soccer/dribble.hpp>

#endif // SOCCER_ACTIONS_HPP
