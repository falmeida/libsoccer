#ifndef SOCCER_EVENT_CONCEPTS_HPP
#define SOCCER_EVENT_CONCEPTS_HPP

#include <soccer/event_traits.hpp>
#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {
namespace event {

BOOST_concept(EventConcept,(E))
    : public boost::AssignableConcept<E>
    , public boost::EqualityComparableConcept<E>
    , public boost::ComparableConcept<E> {
{
    typedef typename time_type< E >::type time_descriptor;

    BOOST_CONCEPT_USAGE( EventConcept )
    {
        const_constraints( e );
    }

protected:
    void const_constraints( const E& const_event )
    {
        start_time( const_event );
        end_time( const_event );
    }

    E _event;
    time_descriptor _time;
};

BOOST_concept(MutableEventConcept,(E))
    : public EventConcept<E>
{
    typedef typename time_type< E >::type time_descriptor;

    BOOST_CONCEPT_USAGE( EventConcept )
    {
        set_start_time( _time, _event );
        set_end_time( _time, _event );
    }

    E _event;
    time_descriptor _time;
}


};


} // end namespace event
} // end namespace soccer

#endif // EVENT_CONCEPTS_HPP
