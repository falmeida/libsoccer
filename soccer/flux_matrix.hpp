#ifndef SOCCER_FLUX_MATRIX_HPP
#define SOCCER_FLUX_MATRIX_HPP

#include <soccer/defines.h>

#include <soccer/concepts/flux_concepts.hpp>

#include <limits>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>

SOCCER_NAMESPACE_BEGIN

/*!
 \brief Zero-indexed flux matrix
 */
template < typename FluxValue = double >
struct flux_matrix
{
    typedef std::size_t index_t;

    index_t M_length;
    index_t M_width;

    FluxValue M_min_value;
    FluxValue M_max_value;
    FluxValue* M_values;

    /*!
     * \brief flux_matrix constructor
     * \param length
     * \param width
     * \param min_value
     * \param max_value
     */
    flux_matrix( index_t const length,
                 index_t const width,
                 FluxValue const min_value,
                 FluxValue const max_value)
        : M_length( length )
        , M_width( width )
        , M_min_value( min_value )
        , M_max_value( max_value )
    {
        M_values = new FluxValue[ M_width * M_length ];
        // Initialize values with min_value
        for ( auto i = 0; i < M_width * M_length ; i++ )
        {
            M_values[i] = M_min_value;
        }
    }

    /*!
     * \brief Copy constructor
     * \param other
     */
    flux_matrix( flux_matrix<FluxValue> const& other )
        : M_length( other.M_length )
        , M_width( other.M_width )
        , M_max_value( other.M_max_value )
        , M_min_value( other.M_min_value )
    {
        M_values = new FluxValue[ M_length * M_width ];
        std::copy( other.M_values,
                   other.M_values + other.M_width * other.M_length,
                   M_values );
    }


    ~flux_matrix()
    {
        clear();
    }

    virtual void
    set_max_value( FluxValue const value )
    {
        M_max_value = value;
    }

    virtual void
    set_min_value( FluxValue const value )
    {
        M_min_value = value;
    }

    virtual void set_value( index_t const length_index,
                            index_t const width_index,
                            FluxValue const value )
    {
        // runtime asserts
        assert( length_index >= 0 || length_index < M_length );
        assert( width_index >= 0 || width_index < M_width);

        M_values[ width_index * M_width + length_index ] = value;
    }

    virtual FluxValue
    value( const index_t width_index,
           const index_t length_index )
    {
        return M_values[ width_index * M_width + length_index ];
    }



    flux_matrix<FluxValue>& operator=( flux_matrix<FluxValue> const& other )
    {
        if ( this != &other )
        {
            this->M_max_value = other.M_max_value;
            this->M_min_value = other.M_min_value;
            this->resize( other.M_length, other.M_width );
            std::copy( other.M_values,
                       other.M_values + other.M_length * other.M_width,
                       M_values );
        }
        return *this;
    }

    void resize( index_t const length,
                 index_t const width )
    {
        assert( length >= 0 && width >= 0 );
        clear();
        this->M_length = length;
        this->M_width = width;
        M_values = new FluxValue[ M_length * M_width ];
    }

    void clear()
    {
        delete [] M_values;
    }

};

template < typename FluxValue = double >
flux_matrix< FluxValue >
make_flux_matrix( std::size_t length,
                  std::size_t width,
                  FluxValue min_value,
                  FluxValue max_value )
{
    return flux_matrix<FluxValue>( length, width, min_value, max_value );
}

} // end namespace soccer

#include <soccer/core/access.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Type registration
template < >
template < typename FluxValue >
struct tag < soccer::flux_matrix< FluxValue > > { typedef flux_matrix_tag type; };

template < >
template < typename FluxValue >
struct flux_value_type< soccer::flux_matrix< FluxValue > > { typedef FluxValue type; };

template < >
template < typename FluxValue >
struct matrix_size_type< soccer::flux_matrix< FluxValue > > { typedef std::size_t type; };

// Data access
template < >
template < typename FluxValue >
struct min_flux_value_access< soccer::flux_matrix< FluxValue > >
{
    static inline
    FluxValue get( soccer::flux_matrix< FluxValue > const& fm ) { return fm.M_min_value; }

    static inline
    void set( FluxValue const& value, soccer::flux_matrix< FluxValue >& fm ) { fm.M_min_value = value; }
};

template < >
template < typename FluxValue >
struct max_flux_value_access< soccer::flux_matrix< FluxValue > >
{
    static inline
    FluxValue get( soccer::flux_matrix< FluxValue > const& fm ) { return fm.M_max_value; }

    static inline
    void set( FluxValue const& value, soccer::flux_matrix< FluxValue >& fm ) { fm.M_max_value = value; }
};

template < >
template < typename FluxValue >
struct flux_value_access< soccer::flux_matrix< FluxValue > >
{
    static inline
    FluxValue get( size_t const line,
                   size_t const col,
                   soccer::flux_matrix< FluxValue > const& fm )  { return fm.value( line, col ); }

    static inline
    void set( size_t const line,
              size_t const col,
              FluxValue const& value,
              soccer::flux_matrix< FluxValue >& fm ) { fm.set_value( line, col, value, fm ); }
};

template < >
template < typename FluxValue >
struct length_access< soccer::flux_matrix< FluxValue > >
{
    static inline
    FluxValue get( soccer::flux_matrix< FluxValue > const& fm ) { return fm.M_length; }

    static inline
    void set( size_t const value, soccer::flux_matrix< FluxValue >& fm ) { fm.M_length = value; }
};

template < >
template < typename FluxValue >
struct width_access< soccer::flux_matrix< FluxValue > >
{
    static inline
    size_t get( soccer::flux_matrix< FluxValue > const& fm ) { return fm.M_width; }

    static inline
    void set( size_t const value, soccer::flux_matrix< FluxValue >& fm ) { fm.M_width = value; }
};


SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN

/*!
 * \brief The flux_matrix_reader struct
 */
struct flux_matrix_reader
{
private:

    std::string read_line( std::istream& is )
    {
        std::string line_buf;
        while (std::getline( is, line_buf )) {
            if ( line_buf.empty()
                         || line_buf[0] == '#'
                         || ! line_buf.compare( 0, 2, "//" ) )
            {
                // TODO Does this ignore empty spaces?
                continue; // Ignore comments and empty lines
            }
            break;
        }
        return line_buf;
    }

    std::ifstream M_ifs;

public:
    flux_matrix_reader( const std::string& file_name )
        : M_ifs( file_name.c_str(), std::ifstream::in )
    {

    }

    template < typename FluxValue >
    void operator()( flux_matrix< FluxValue >& fm )
    {
        typedef flux_matrix< FluxValue > flux_matrix_t;

        BOOST_CONCEPT_ASSERT(( MutableFluxMatrixConcept< flux_matrix_t > ));

        int width = -1;
        int length = -1;
        std::string line_buf = this->read_line( M_ifs );
        // read the number of lines and columns
        if ( std::sscanf( line_buf.c_str(), " %d %d ",
                          &width , &length ) != 2 )
        {
            assert( false );
        }

        assert( width  != -1 && length != -1 );

        // initialize the minimum and maximum value (if available)
        auto min_value = std::numeric_limits<FluxValue>::max();
        auto max_value = std::numeric_limits<FluxValue>::min();

        // read individual flux matrix values
        for ( std::size_t width_index = 0 ; width_index < width ; width_index++ )
        {
            std::istringstream istr( this->read_line( M_ifs ) );
            for ( int length_index = 0 ; length_index < length ; length_index++ )
            {
                FluxValue flux_matrix_value;
                istr >> flux_matrix_value;
                set_value( length_index, width_index, flux_matrix_value, fm );

                if ( flux_matrix_value < min_value )
                {
                    min_value = flux_matrix_value;
                }

                if ( flux_matrix_value > max_value )
                {
                    max_value = flux_matrix_value;
                }
            }
        }

        // Update the minimum and maximum values
        set_max_value( max_value, fm );
        set_min_value( min_value, fm );
    }
};

SOCCER_NAMESPACE_END

#endif // SOCCER_FLUX_MATRIX_HPP
