#ifndef SOCCER_STATE_EVALUATOR_DEFENSE_HPP
#define SOCCER_STATE_EVALUATOR_DEFENSE_HPP

//! Import useful macros
#include <soccer/defines.h>

SOCCER_NAMESPACE_BEGIN

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/algorithms/pitch_algorithms.hpp>
#include <soccer/algorithms/state_algorithms.hpp>

#include <soccer/algorithms/comparable_distance.hpp>

#include <boost/property_map/property_map.hpp>

#include <algorithm>

#if defined(RCSC) && (defined(DEBUG_PROFILE) || defined(DEBUG_STATE_EVALUATION))
#include <rcsc/common/logger.h>
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
#include <rcsc/timer.h>
#endif

SOCCER_NAMESPACE_BEGIN


/*!
    \brief Team defense ball position evaluator
    \tparam Pitch Model of a soccer pitch
    \tparam Pitch Model of a soccer state
    \struct Team defense ball position evaluator
*/
template < typename Pitch,
           typename State >
struct ball_position_defense_state_evaluator
//    : public state_evaluator<State, float>
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;
    typedef typename position_type< typename player_type< State >::type >::type ball_position_t;

    Pitch const M_pitch;
    side_t M_defense_side;

    ball_position_offense_state_evaluator( Pitch const& pitch,
                                           side_t defense_side )
        : M_pitch( pitch )
        , M_defense_side( defense_side )
    {

    }

    // template < typename State >
    float operator()( State const& state )
    {

#if defined(RCSC) && defined(DEBUG_PROFILE)
    rcsc::Timer profile_timer;
#endif
    static const auto max_dist_to_goal = std::sqrt( std::pow( get_pitch_length( M_pitch ), 2.0) +
                                                    std::pow( get_pitch_width( M_pitch ), 2.0) );

    // TODO To be more effective should analyze the ball y-coordinate with regard to the goal posts
    const auto team_goal_nearest_position = get_goal_center_position< ball_position_t >( M_defense_side, M_pitch );

    //! prefer states in which the ball is closer to opponent goal
    const auto ball_dist_to_goal = distance( get_position( get_ball( state ) ),
                                             team_goal_nearest_position);

    // Simplistic ball position offense evaluation
    auto ball_dist_evaluation = 1.0f - (ball_dist_to_goal / max_dist_to_goal);

#if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (BallPositionEvaluator=%.2f) bPos=(%.2f,%.2f) fballDistToGoal=%.2f max_dist_to_goal=%.2f",
                            ball_dist_evaluation,
                            get_x( get_position( get_ball( state ))),
                            get_y( get_position( get_ball( state ))),
                            ball_dist_to_goal, max_dist_to_goal);
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
    rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                        __FILE__ " (BallPositionEvaluator) profile time=%.3f[ms]",
                        profile_timer.elapsedReal() );
#endif
        return ball_dist_evaluation;
    }

};


/*!
    \brief Team defense ball position evaluator
    \tparam Pitch Model of a soccer pitch
    \tparam Pitch Model of a soccer state
    \class Team defense ball position evaluator
*/
template < typename Pitch,
           typename TeammateToOpponentIdMap,
           typename State >
class man_marking_defense_state_evaluator
//    : public state_evaluator<State, float>
{
public:
    BOOST_CONCEPT_ASSERT(( PitchConcept< Pitch > ));
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;
    typedef typename player_id_type< player_t >::type player_id_t;

    typedef typename teammate_iterator_generator< State >::type player_filter_iterator_t;

    BOOST_CONCEPT_ASSERT(( boost::ReadablePropertyMapConcept< TeammateToOpponentIdMap, player_id_t > ));

    man_marking_defense_state_evaluator( Pitch const& pitch,
                                         side_t defense_side,
                                         TeammateToOpponentIdMap teammate_to_opponent_id_map )
        : M_pitch( pitch )
        , M_defense_side( defense_side )
        , M_teammate_to_opponent_id_map( teammate_to_opponent_id_map )
    {

    }

    float operator()( State const& state )
    {
        player_filter_iterator_t player_begin, player_end;
        boost::tie( player_begin, player_end ) = get_players( side, state );

        auto max_dist = 0;

        auto count = 0u;
        std::list< double > distances;
        for ( auto itp = player_begin; itp != player_end; it++, count++ )
        {
            auto opponent_id_to_mark = ::boost::get( M_teammate_to_opponent_id_map, *itp );
            auto opponent = get_player( opponent_id_to_mark, state );
            auto teammate = get_player( *itp, state );
            auto dist = comparable_distance( opponent, teammate );
            distances.push_back( dist );
            // check if it's necessary to update max dist for normalization of results
            if ( dist > max_dist )
                max_dist = dist;
        }

        // Calculate the final evaluation
        double eval = 0.0f;
        std::for_each( distances.begin(),
                       distances.end(),
                       [&eval] ( const double dist ){ eval += dist / max_dist; }
                     );
        eval /= max_dist;

        return eval;
    }

private:
    const Pitch& M_pitch;
    const side_t M_defense_side;
    const TeammateToOpponentIdMap M_teammate_to_opponent_id_map;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_EVALUATOR_DEFENSE_HPP
