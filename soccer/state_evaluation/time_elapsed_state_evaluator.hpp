#ifndef SOCCER_STATE_EVALUATOR_TIME_ELAPSED_HPP
#define SOCCER_STATE_EVALUATOR_TIME_ELAPSED_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/concepts/state_concepts.hpp>

#include <soccer/core/access.hpp>

#if defined(RCSC) && ( defined(DEBUG_STATE_EVALUATION) || defined(RCSC_DEBUG_PROFILE))
#include <rcsc/common/logger.h>
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
#include <rcsc/timer.h>
#endif

SOCCER_NAMESPACE_BEGIN

/*!
    \brief Evaluates the time required to execute the chain
    \abstract Predicted states closer to the "present" have a higher confidence
    \class TimeSpentEvaluator
*/
template < typename State >
struct elapsed_time_state_evaluator
//     : public state_evaluator<State>
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename time_type< State >::type time_t;

    elapsed_time_state_evaluator( const time_t start_time )
        : M_start_time( start_time )
        , M_time_spent_conf_decay( 0.95 )
    {

    }

    /*!
      \brief constructor
      \param time_spent_conf_decay the confidence decay in the prediction for each future cycle [0,1]
      */
    elapsed_time_state_evaluator( typename time_type< State >::type start_time,
                                  const double time_spent_conf_decay )
        : M_start_time( start_time )
        , M_time_spent_conf_decay( time_spent_conf_decay )
    {
        assert( time_spent_conf_decay >= 0.0 &&
                time_spent_conf_decay <= 1.0 );
    }

    float operator()( State const& state )
    {
#if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::Timer profile_timer;
#endif

        double time_spent_evaluation = std::pow( M_time_spent_conf_decay,
                                                 soccer::get_time( state ) - M_start_time );

#if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (TimeSpentEvaluator=%.2f) %d cycles",
                            time_spent_evaluation,
                            soccer::get_time( state ) );
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (TimeSpentEvaluator) profile time=%.3f[ms]",
                            profile_timer.elapsedReal() );
#endif

        return time_spent_evaluation;
    }

    /* const std::string& name() const
    {
        static const std::string _name = "ElapsedTimeStateEvaluator";
        return _name;
    } */

private:
    time_t M_start_time;
    double M_time_spent_conf_decay;
};

SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_EVALUATOR_TIME_ELAPSED_HPP
