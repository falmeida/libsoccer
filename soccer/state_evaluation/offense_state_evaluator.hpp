#ifndef SOCCER_STATE_EVALUATOR_OFFENSE_HPP
#define SOCCER_STATE_EVALUATOR_OFFENSE_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/algorithms/pitch_algorithms.hpp>
#include <soccer/algorithms/state_algorithms.hpp>

#if defined(RCSC) && (defined(DEBUG_PROFILE) || defined(DEBUG_STATE_EVALUATION))
#include <rcsc/common/logger.h>
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
#include <rcsc/timer.h>
#endif

SOCCER_NAMESPACE_BEGIN


/*!
    \brief Team offense ball position evaluator
    \class Team offense ball position evaluator
*/
template < typename Pitch,
           typename State >
struct ball_position_offense_state_evaluator
//    : public state_evaluator<State, float>
{
    BOOST_CONCEPT_ASSERT(( PitchConcept<Pitch> ));

    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;
    typedef typename position_type< typename player_type< State >::type >::type ball_position_t;

    Pitch const M_pitch;
    side_t M_offense_side;

    ball_position_offense_state_evaluator( Pitch const& pitch,
                                           side_t offense_side )
        : M_pitch( pitch )
        , M_offense_side( offense_side )
    {

    }

    // template < typename State >
    float operator()( State const& state )
    {

#if defined(RCSC) && defined(DEBUG_PROFILE)
    rcsc::Timer profile_timer;
#endif
    static const auto max_dist_to_goal = std::sqrt( std::pow( get_pitch_length( M_pitch ), 2.0) +
                                                    std::pow( get_pitch_width( M_pitch ), 2.0) );

    //! prefer states in which the ball is closer to opponent goal
    const auto opposite_side = get_opposite_side( M_offense_side );
    const auto opponent_goal_nearest_position = get_goal_center_position< ball_position_t >( opposite_side, M_pitch );
    // TODO To be more effective should analyze the ball y-coordinate with regard to the goal posts
    const auto ball_dist_to_goal = distance( get_position( get_ball( state ) ), opponent_goal_nearest_position );

    // Simplistic ball position offense evaluation
    auto ball_dist_evaluation = 1.0f - (ball_dist_to_goal / max_dist_to_goal);

#if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (BallPositionEvaluator=%.2f) bPos=(%.2f,%.2f) fballDistToGoal=%.2f max_dist_to_goal=%.2f",
                            ball_dist_evaluation,
                            get_x( get_position( get_ball( state ))),
                            get_y( get_position( get_ball( state ))),
                            ball_dist_to_goal, max_dist_to_goal);
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
    rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                        __FILE__ " (BallPositionEvaluator) profile time=%.3f[ms]",
                        profile_timer.elapsedReal() );
#endif
        return ball_dist_evaluation;
    }

};


SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_EVALUATOR_OFFENSE_HPP
