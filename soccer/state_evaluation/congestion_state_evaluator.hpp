#ifndef SOCCER_BALL_CONGESTION_STATE_EVALUATOR_HPP
#define SOCCER_BALL_CONGESTION_STATE_EVALUATOR_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/core/access.hpp>

#if defined(RCSC) && (defined(DEBUG_STATE_EVALUATION) || defined(DEBUG_PROFILE))
#include <rcsc/common/logger.h>
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
#include <rcsc/timer.h>
#endif

SOCCER_NAMESPACE_BEGIN

/*!
    \brief Evaluate the opponent congestion at the ball position
    \class OpponentBallCongestionEvaluator
*/
template < typename State,
           typename PositionDistanceFunctor >
struct opponent_ball_congestion_state_evaluator
//     : public state_evaluator< State, float >
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_id_type< State >::type player_id_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    // typedef state_evaluator<State,float> base_type;

    const side_t M_opponent_side;
    PositionDistanceFunctor M_position_distance_fn;
    const float M_min_congestion_dist;

    opponent_ball_congestion_state_evaluator( const side_t opponent_side,
                                     PositionDistanceFunctor distance_fn,
                                     const float min_congestion_dist )
        : M_opponent_side( opponent_side  )
        , M_position_distance_fn( distance_fn )
        , M_min_congestion_dist( min_congestion_dist )
    {

    }


    float operator()( State const& state )
    {
    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::Timer profile_timer;
    #endif
        // double opp_ball_congestion = 0.0;
        // unsigned int opps_considered = 0u;

        if ( M_opponent_side == ::soccer::get_unknown_side< side_t >() )
        {
            return 1.0;
        }

        auto opponents_iterators = ::soccer::get_players( M_opponent_side, state );

        if ( opponents_iterators.first == opponents_iterators.second )
        {
            #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
            rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                          __FILE__ " (OpponentBallCongestionEvaluator) no opponents found.");
            #endif
            return 1.0;

        }

        const auto ball_position = get_position( get_ball( state ) );
        auto ito = opponents_iterators.first;
        auto opponent = get_player( *ito, state );
        auto min_ball_dist = M_position_distance_fn( get_position( opponent ), ball_position );
        ito++;

        for ( ; ito != opponents_iterators.second; ito++ )
        {
            opponent = get_player( *ito, state );

            const auto ball_dist = M_position_distance_fn( ball_position, get_position( opponent ) );
            const double safety_dist2 = std::pow( opponent.playerTypePtr()->playerSpeedMax(), 2 );
            if ( ball_dist < safety_dist2 )
            {
    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
                rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                                    __FILE__ " OpponentBallCongestionEvaluator: opp#%d is too close ballDist=%.2f < safetyDist=%.2f",
                                    soccer::get_number( opponent ),
                                    ball_dist, safety_dist2);
    #endif
                return 0.0;
            }

            if ( ball_dist < min_ball_dist )
            {
                min_ball_dist = ball_dist;
            }

            /* opp_ball_congestion += (ball_dist2 < 1.0) ? 1.0 : 1.0 / ( ball_dist2 + 0.00001 );
            opps_considered++;*/
        }

        auto evaluation = rcsc::bound(0.0, sqrt( min_ball_dist ) , M_min_congestion_dist) / M_min_congestion_dist;

    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (OpponentBallCongestionEvaluator) Closest opp ball congestion=%.2f minDist=%.2f",
                            evaluation, min_ball_dist2);
    #endif

    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (OpponentBallCongestionEvaluator) profile time=%.3f[ms]",
                            profile_timer.elapsedReal() );
    #endif

        return evaluation;
    }

    /* const std::string& name() const
    {
        static const std::string _name = "BallCongestionStateEvaluator";
        return _name;
    } */
};

SOCCER_NAMESPACE_END

#endif // SOCCER_BALL_CONGESTION_STATE_EVALUATOR_HPP
