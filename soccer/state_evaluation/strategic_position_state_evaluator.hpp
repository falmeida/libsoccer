#ifndef SOCCER_STATE_EVALUATOR_STRATEGIC_POSITIONING_HPP
#define SOCCER_STATE_EVALUATOR_STRATEGIC_POSITIONING_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/concepts/state_concepts.h>
#include <soccer/concepts/strategy_concepts.hpp>

SOCCER_NAMESPACE_BEGIN

/*!
    \brief Evaluate the relevant of players strategic positioning
    \class StrategicPositioningStateEvaluator
*/
template < typename State,
           typename Strategy >
class strategic_positioning_state_evaluator
//     : public state_evaluator< State, float >
{
private:
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));
    typedef typename player_iterator_type< State >::type player_iterator_t;
    typedef typename player_type< State >::type player_t;
    typedef typename side_type< player_t >::type side_t;

    BOOST_CONCEPT_ASSERT(( StrategyConcept< Strategy > ));

    const Strategy& M_strategy;
    const side_t M_side;

    static constexpr auto max_player_dist2_to_spos = std::pow( 15.0, 2);

public:
    strategic_positioning_state_evaluator( Strategy const& strategy,
                                           side_t const side )
        : M_strategy( strategy )
        , M_side( side )
    {

    }

    float operator()( State const& state )
    {
        player_iterator_t _player_begin, _player_end;
        boost::tie(_player_begin, _player_end) = get_players( M_side, state );

        if ( _player_begin == _player_end )
        {
            return 0.0;
        }

        double team_dist2_to_spos = 0.0;
        unsigned int players_considered = 0u;

        for ( auto itp = _player_begin ; itp != _player_end; itp++ )
        {
            const auto _player = get_player( *itp, state );
            const auto _player_number = get_number( _player );
            if ( _player_number == get_unknown_number< player_t >() )
            {
                continue;
            }

            auto strategic_pos = get_position( current_positioning, M_formation );

            auto current_position = get_position( _player );

            auto dist2_to_spos = comparable_distance( strategic_pos, current_position );
            if ( dist2_to_spos > max_player_dist2_to_spos )
            {
    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
                rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                                    __FILE__ " (StrategicPositioningStateEvaluator=0.0) %d dist2ToSpos(%.2f) >maxPlayerDist2ToSpos(%.2f)",
                                    _player_number,
                                    dist2_to_spos,
                                    max_player_dist2_to_spos );
    #endif

    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                      "(StrategicPositioningStateEvaluator) profile time=%.3f[ms]",
                      profile_timer.elapsedReal() );
    #endif
                return 0.0;
            }
            team_dist2_to_spos += dist2_to_spos;
            players_considered++;
        }

        if ( players_considered == 0 )
        {
    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
                rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                              " (StrategicPositioningStateEvaluator=0.0) no players considered");
    #endif
            return 0.0;
        }

        const auto max_team_dist2_to_spos = players_considered * std::pow( 7.5, 2 );

        if ( team_dist2_to_spos > max_team_dist2_to_spos )
        {
    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
            rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                          " (StrategicPositioningStateEvaluator=0.0) teamDist2ToSpos(%.2f) >maxTeamDist2ToSpos(%.2f) playersConsidered=%d",
                          team_dist2_to_spos,
                          max_team_dist2_to_spos,
                          players_considered );
    #endif

    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            " (StrategicPositioningStateEvaluator) profile time=%.3f[ms]",
                            profile_timer.elapsedReal() );
    #endif
            return 0.0;
        }

        auto evaluation = team_dist2_to_spos / max_team_dist2_to_spos;

    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                          __FILE__ " (StrategicPositioningStateEvaluator=%.2f)=teamDist2ToSpos(%.2f) / maxTeamDist2ToSpos(%.2f) playersConsidered=%d",
                          evaluation,
                          team_dist2_to_spos,
                          max_team_dist2_to_spos,
                          players_considered );
    #endif

    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                          __FILE__ " (StrategicPositioningStateEvaluator) profile time=%.3f[ms]",
                          profile_timer.elapsedReal() );
    #endif

        return evaluation;
    }

    /* const std::string& name() const
    {
        static const std::string _name = "StrategicPositioningStateEvaluator";
        return _name;
    }*/

};

SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_EVALUATOR_STRATEGIC_POSITIONING_HPP
