#ifndef SOCCER_STATE_EVALUATOR_LINEAR_BALL_MOTION_HPP
#define SOCCER_STATE_EVALUATOR_LINEAR_BALL_MOTION_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/concepts/state_concepts.hpp>

#include <soccer/traits/ball_traits.hpp>

#include <boost/iterator.hpp>

#if defined(RCSC) && (defined(DEBUG_PROFILE) || defined(DEBUG_STATE_EVALUATION))
#include <rcsc/common/logger.h>
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
#include <rcsc/timer.h>
#endif

SOCCER_NAMESPACE_BEGIN

/*!
    \brief Evaluate the "straightness" of a ball path across the predicted state space
    \class StraightBallPathEvaluator
*/
struct linear_ball_state_path_evaluator
//     : public state_path_evaluator< StateInputIterator, float >
{

    template < typename StateInputIterator,
               typename ObjectDistanceFunctor >
    float operator()( StateInputIterator first,
                      const StateInputIterator last,
                      ObjectDistanceFunctor distance_fn)
    {
        BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< StateInputIterator > ));
        typedef boost::iterator_value< StateInputIterator >::type state_t;
        typedef typename state_traits< state_t >::ball_type ball_t;
        typedef typename ball_traits< ball_t >::position_type ball_position_t;

        BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::Timer profile_timer;
    #endif

        assert( first != last );

        if ( first == last ) { return 0.0f; }

        auto prev = first;
        auto curr = first;
        curr++;

        if ( curr == last ) { return 0.0f; }

        //! evaluate straight path motion => privilege straightest motion
        float ball_min_dist_evaluation = 1.0;

        auto ball_estimated_min_dist = 0.0;
        for ( ; curr != last; prev++, curr++ )
        {
            ball_estimated_min_dist += distance_fn( get_position( get_ball( *prev )),
                                                    get_position( get_ball( *curr )) );
        }

        auto shortest_ball_path_dist = distance_fn( get_position( get_ball( *first )),
                                                     get_position( get_ball( *prev )));
        ball_min_dist_evaluation = shortest_ball_path_dist / ball_estimated_min_dist;

    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
        rscc::dlog.addText( rscc::Logger::ANALYZER,
                            __FILE__ " (LinearBallStatePathEvaluator=%.2f)",
                            ball_min_dist2_evaluation );
    #endif

    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (LinearBallStatePathEvaluator=%.2f) profile time=%.3f[ms]",
                            ball_min_dist2_evaluation,
                            profile_timer.elapsedReal() );
    #endif

        return ball_min_dist_evaluation;
    }


    /* const std::string& name() const
    {
        static const std::string _name = "LinearBallStatePathEvaluator";
        return _name;
    }*/
};

SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_EVALUATOR_LINEAR_BALL_MOTION_HPP
