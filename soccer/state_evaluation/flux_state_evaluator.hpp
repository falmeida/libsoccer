#ifndef SOCCER_STATE_EVALUATOR_FLUX_HPP
#define SOCCER_STATE_EVALUATOR_FLUX_HPP

//! Import useful macros
#include <soccer/defines.h>

#include <soccer/algorithms/pitch_algorithms.hpp>
#include <soccer/concepts/flux_concepts.hpp>

#if defined(RCSC) && (defined(DEBUG_STATE_EVALUATION) || defined(DEBUG_PROFILE))
#include <rcsc/common/logger.h>
#endif

#if defined(RCSC) && defined(DEBUG_PROFILE)
#include <rcsc/timer.h>
#endif

SOCCER_NAMESPACE_BEGIN

/*!
    \brief Evaluate the flux at the position of the ball
    \class FluxBallEvaluator
*/
template < typename Pitch,
           typename State,
           typename Flux >
class ball_flux_state_evaluator {
//    : public state_evaluator<State, float> {
private:
    typedef state_evaluator<State> base_type;

    BOOST_CONCEPT_ASSERT(( soccer::PitchConcept<Pitch> ));
    BOOST_CONCEPT_ASSERT(( soccer::StateConcept< State > ));
    BOOST_CONCEPT_ASSERT(( soccer::FluxConcept<Flux> ));

    Pitch const M_pitch;
    Flux const M_flux;
public:
    ball_flux_state_evaluator( Pitch const& pitch,
                               Flux const& flux )
        : M_pitch( pitch )
        , M_flux( flux )
    {

    }

    typename base_type::result_type operator()( State const& state )
    {
    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::Timer profile_timer;
    #endif

        constrain_position_to_pitch< Pitch > _constrain_position_to_pitch( M_pitch );

        auto ball_position = get_position( get_ball( state ) );
        _constrain_position_to_pitch( ball_position );

        auto ball_flux_value = flux_value( ball_position, M_flux );
        const auto _max_flux = get_max_flux( M_flux );
        const auto _min_flux = get_min_flux( M_flux );
        auto value = ( ball_flux_value - _min_flux ) / ( _max_flux - _min_flux ) * 1.0;

    #if defined(RCSC) && defined(DEBUG_STATE_EVALUATION)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                          __FILE__ " (FluxBallEvaluator=%.2f) ball(%.2f,%.2f)=%.2f minFlux=%.2f maxFlux=%.2f value=%.2f",
                          value,
                          get_x( ball_position ),
                          get_y( ball_position ),
                          ball_flux_value,
                          _min_flux,
                          _max_flux,
                          value );
    #endif

    #if defined(RCSC) && defined(DEBUG_PROFILE)
        rcsc::dlog.addText( rcsc::Logger::ANALYZER,
                            __FILE__ " (FluxBallEvaluator) profile time=%.3f[ms]",
                            profile_timer.elapsedReal() );
    #endif

        return value;
    }

    const std::string& name() const
    {
        static const std::string _name = "FluxBallEvaluator";
        return _name;
    }
};


SOCCER_NAMESPACE_END

#endif // SOCCER_STATE_EVALUATOR_FLUX_HPP
