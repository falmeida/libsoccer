#ifndef SOCCER_ACTION_GENERATOR_HPP
#define SOCCER_ACTION_GENERATOR_HPP

//! Import useful macros
#include <soccer/defines.h>

//! Import relevant action models
#include <soccer/models/actions.hpp>

//! Import relevant concepts to be checked
#include <soccer/concepts/state_concepts.hpp>

SOCCER_NAMESPACE_BEGIN

/*!
  \brief shortcut to create a dribble model from a static state model
  */
template < typename State >
struct dribble_from_state_generator {
    BOOST_CONCEPT_ASSERT(( StateConcept< State> ));

    typedef dribble< typename player_id_type< State >::type,
                     typename time_type< State >::type,
                     typename position_type< typename ball_type< State >::type >::type
                    > type;
};

/*!
  \brief shortcut to create a move model from a static state model
  */
template < typename State >
struct move_from_state_generator
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef move< typename player_id_type< State >::type,
                  typename time_type< State >::type,
                  typename position_type< typename ball_type< State >::type >::type
                > type;
};

/*!
  \brief shortcut to create an intercept model from a static state model
  */
template < typename State >
struct intercept_from_state_generator
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef interception< typename player_id_type< State >::type,
                          typename time_type< State >::type,
                          typename position_type< typename ball_type< State >::type >::type
                        > type;
};


/*!
  \brief shortcut to create a shoot type from a state descriptor
  */
template < typename State >
struct shoot_from_state_generator
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State> ));

    typedef shoot< typename player_id_type< State >::type,
                   typename time_type< State >::type,
                   typename position_type< typename ball_type< State >::type >::type,
                   double // HACK => TODO use velocity type of ball to infer speed type
                 > type;
};


/*!
  \brief shortcut to create a pass type from a state descriptor
  */
template < typename State >
struct pass_from_state_generator
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State> ));

    typedef pass< typename player_id_type< State >::type,
                  typename time_type< State >::type,
                  typename position_type< typename ball_type< State >::type >::type
                > type;
};

/*!
  \brief shortcut to create a tackle type from a static state model
  */
template < typename State >
struct tackle_from_state_generator
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef tackle< typename player_id_type< State >::type,
                    typename time_type< State >::type,
                    typename position_type< typename ball_type< State >::type >::type
                  > type;
};






/// Action Generation Algorithms
#include <soccer/position_concepts.hpp>
#include <soccer/core/access.hpp>


/// Generate passes
template < typename TargetPlayerIdInputIterator,
           typename PassGenerator,
           typename State,
           typename PassOutputIterator,
         >
void generate_passes( TargetPlayerIdInputIterator player_begin,
                      TargetPlayerIdInputIterator player_end,
                      typename PassGenerator pass_generator, // State as well as other decision structures should be encapsulated within PassGenerator
                      typename PassOutputIterator out_passes ) {

}


template < typename TargetPositionInputIterator,
           typename ActionGenerator, // e.g. ShootGenerator, MoveGenerator, TackleGenerator
           typename ActionPredicate,
           typename ActionOutputIterator >
void generate_moves( TargetPositionInputIterator pos_begin,
                     TargetPlayerIdInputIterator pos_end,
                     ActionGenerator action_generator,
                     Actionredicate action_checker,
                     ActionOutputIterator out_actions)
{
    for ( auto itp = pos_begin; itp != pos_end; itp++ ) {
        if ( action_checker( *itp ) )
        {
            action_generator( out_actions );
        }
    }
}

/// Generate any action
template < typename ActionGenerator,
           typename State,
           typename TargetPositionInputIterator,
           typename ActionOutputIterator
           >
/*!
 * \brief Generate actions for a given player id for a given range of target positions
 * \param executor_pid player identified
 * \param state current state
 * \param target_position_begin Iterator to the first target position
 * \param target_position_end Iterator to the end target position
 * \param action_generator Action generator (pid, pos, state)
 * \param out_actions Action output iterator
 */
void generate_actions( typename player_id_type< State >::type const executor_pid,
                       State const& state,
                       TargetPositionInputIterator target_position_begin,
                       TargetPositionInputIterator target_position_end,
                       ActionGenerator action_generator,
                       ActionOutputIterator out_actions )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< TargetPositionInputIterator > ));

    typedef typename boost::iterator_value< TargetPositionInputIterator >::type target_position_type;
    BOOST_CONCEPT_ASSERT(( PositionConcept< target_position_type > ));


    for ( auto it_pos = target_position_begin; it_pos != target_position_end; it_pos++ )
    {
        auto _action = action_generator( executor_pid, *it_pos, state );
        if ( !_action ) { continue; }
        *out_actions = _action;
    }
}


SOCCER_NAMESPACE_END

#endif // SOCCER_ACTION_GENERATOR_HPP
