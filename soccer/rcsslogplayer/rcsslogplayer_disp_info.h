#ifndef SOCCER_RCSSLOGPLAYER_DISP_INFO_H
#define SOCCER_RCSSLOGPLAYER_DISP_INFO_H

//! Import useful macros
#include <soccer/defines.h>

#include "rcsslogplayer_soccer_model.h"

#include <rcsslogplayer/types.h>

#include <soccer/traits/state_traits.hpp>

//! Soccer types registration
#include <soccer/register/state.hpp>
#include <soccer/register/ball.hpp>
#include <soccer/register/player.hpp>

#include <soccer/core/access.hpp>


#include <boost/iterator/counting_iterator.hpp>

#include <map>
#include <algorithm>
#include <functional>
#include <iterator>
#include <cassert>

namespace rcss {
namespace rcg {    

    typedef std::pair< float, float > position_type;
    typedef std::pair< float, float > velocity_type;
    typedef boost::counting_iterator< unsigned short > player_iterator_type;

    //
    // BallT Free Functions
    //
    std::pair< float, float >
    get_position ( const rcss::rcg::BallT& ball ) { return std::make_pair( ball.x_, ball.y_ ); }

    std::pair< float, float >
    get_velocity( const rcss::rcg::BallT& ball ) { return std::make_pair( ball.vx_, ball.vy_ ); }

    void
    set_position ( const std::pair< float, float >& pos, rcss::rcg::BallT& ball ) { ball.x_ = pos.first; ball.y_ = pos.second; }

    void
    set_velocity( const std::pair< float, float >& vel, rcss::rcg::BallT& ball ) { ball.vx_ = vel.first; ball.vy_ = vel.second; }

    //
    // PlayerT Free Functions
    //
    std::pair< float, float >
    get_position ( const rcss::rcg::PlayerT& player ) { return std::make_pair( player.x_, player.y_ ); }

    std::pair< float, float >
    get_velocity( const rcss::rcg::PlayerT& player ) { return std::make_pair( player.vx_, player.vy_ ); }

    rcss::rcg::Side
    get_side( const rcss::rcg::PlayerT& player ) { return player.side(); }

    Int16
    get_number( const rcss::rcg::PlayerT& player ) { return player.unum_; }

    void
    set_position ( const std::pair< float, float >& pos, rcss::rcg::PlayerT& player ) { player.x_ = pos.first; player.y_ = pos.second; }

    void
    set_velocity( const std::pair< float, float >& vel, rcss::rcg::PlayerT& player ) { player.vx_ = vel.first; player.vy_ = vel.second; }

    void
    set_side( const rcss::rcg::Side& side, rcss::rcg::PlayerT& player ) { assert( side != NEUTRAL ); player.side_ = side == LEFT ? 'l' : 'r'; }

    void
    set_number( const Int16& number, rcss::rcg::PlayerT& player ) { player.unum_ = number; }

    //
    // DispInfoT
    //
    BallT get_ball( const rcss::rcg::DispInfoT& state ) { return state.show_.ball_; }
    void set_ball( const BallT& ball, rcss::rcg::DispInfoT& state ) { state.show_.ball_ = ball; }

    std::pair< player_iterator_type, player_iterator_type >
    get_players( const rcss::rcg::DispInfoT&  )
    {
        return std::make_pair( player_iterator_type(0), player_iterator_type( 22 ) );
    }

    PlayerT get_player( const int pid, const rcss::rcg::DispInfoT& state )
    {
        assert( pid >= 0 && pid < 11 * 2 );

        return state.show_.player_[pid];
    }
    void set_player( const int pid,
                     const PlayerT& player,
                     rcss::rcg::DispInfoT& state )
    {
        assert( pid >= 0 && pid < 11 * 2 );

        state.show_.player_[pid] = player;
    }

    Int32 get_time( const DispInfoT& state ) { return state.show_.time_; }
    void set_time( const Int32 time, DispInfoT& state ) { state.show_.time_ = time; }

    PlayMode get_playmode( const DispInfoT& state ) { return state.pmode_; }
    void set_playmode( const PlayMode pmode, DispInfoT& state ) { state.pmode_ = pmode; }
}
}

// Register state traits of rcss::rcg::DispInfoT
/* namespace soccer { namespace traits {
SOCCER_DETAIL_SPECIALIZE_STATE_TRAITS( rcss::rcg::DispInfoT, \
                                       rcss::rcg::BallT, \
                                       std::size_t, \
                                       rcss::rcg::PlayerT, \
                                       rcss::rcg::ShowInfoTPlayerIterator )
}}*/
SOCCER_REGISTER_DYNAMIC_BALL_GET_SET_FREE(rcss::rcg::BallT, \
                                          rcss::rcg::position_type, rcss::rcg::get_position, rcss::rcg::set_position, \
                                          rcss::rcg::velocity_type, rcss::rcg::get_velocity, rcss::rcg::set_velocity)

SOCCER_REGISTER_DYNAMIC_PLAYER_GET_SET_FREE(rcss::rcg::PlayerT, \
                                            rcss::rcg::position_type, rcss::rcg::get_position, rcss::rcg::set_position, \
                                            rcss::rcg::velocity_type, rcss::rcg::get_velocity, rcss::rcg::set_velocity, \
                                            rcss::rcg::Side, rcss::rcg::get_side, rcss::rcg::set_side, \
                                            rcss::rcg::Int16, rcss::rcg::get_number, rcss::rcg::set_number )

SOCCER_REGISTER_STATE_GET_SET_FREE(rcss::rcg::DispInfoT, \
                                   rcss::rcg::BallT, rcss::rcg::get_ball, rcss::rcg::set_ball, \
                                   int, rcss::rcg::player_iterator_type, rcss::rcg::get_players, \
                                   rcss::rcg::PlayerT, rcss::rcg::get_player, rcss::rcg::set_player, \
                                   rcss::rcg::UInt32, rcss::rcg::get_time, rcss::rcg::set_time, \
                                   rcss::rcg::PlayMode, rcss::rcg::get_playmode, rcss::rcg::set_playmode )


SOCCER_TRAITS_NAMESPACE_BEGIN

    // HACK DispInfoT Types
    template <>
    struct team_type< rcss::rcg::DispInfoT > { typedef std::string type; };

    template <>
    struct score_type< rcss::rcg::DispInfoT > { typedef rcss::rcg::UInt16 type; };

    // HACK DispInfoT Access
    template <>
    struct left_team_access< rcss::rcg::DispInfoT >
    {
        static inline
        std::string const& get( rcss::rcg::DispInfoT const& _state ) { return _state.team_[0].name_; }

        static inline
        void set( std::string const& _team, rcss::rcg::DispInfoT& _state ) { _state.team_[0].name_ = _team; }
    };

    template <>
    struct right_team_access< rcss::rcg::DispInfoT >
    {
        static inline
        std::string const& get( rcss::rcg::DispInfoT const& _state ) { return _state.team_[1].name_; }

        static inline
        void set( std::string const& _team, rcss::rcg::DispInfoT& _state ) { _state.team_[1].name_ = _team; }
    };

    template <>
    struct left_score_access< rcss::rcg::DispInfoT >
    {
        static inline
        rcss::rcg::UInt16 const& get( rcss::rcg::DispInfoT const& _state ) { return _state.team_[0].score_; }

        static inline
        void set(rcss::rcg::UInt16 const& _score, rcss::rcg::DispInfoT& _state ) { _state.team_[0].score_ = _score; }
    };

    template <>
    struct right_score_access< rcss::rcg::DispInfoT >
    {
        static inline
        rcss::rcg::UInt16 const& get( rcss::rcg::DispInfoT const& _state ) { return _state.team_[1].score_; }

        static inline
        void set( rcss::rcg::UInt16 const& _score, rcss::rcg::DispInfoT& _state ) { _state.team_[1].score_ = _score; }
    };

SOCCER_TRAITS_NAMESPACE_END

SOCCER_TRAITS_NAMESPACE_BEGIN

    //
    // playmode registration
    //
    template <>
    struct tag < rcss::rcg::PlayMode > { typedef playmode_tag type; };

    template <>
    struct side_type < rcss::rcg::PlayMode > { typedef rcss::rcg::Side type; };

    // HACK Defining the same type
    template <>
    struct match_situation_type< rcss::rcg::PlayMode > { typedef rcss::rcg::PlayMode type; };

    //
    // Data Access Registration
    //

    template <>
    struct side_access < rcss::rcg::PlayMode >
    {
        static inline
        rcss::rcg::Side
        get( rcss::rcg::PlayMode const& pmode )
        {
            if ( pmode == rcss::rcg::PM_Null ||
                 pmode == rcss::rcg::PM_BeforeKickOff ||
                 pmode == rcss::rcg::PM_TimeOver ||
                 pmode == rcss::rcg::PM_FirstHalfOver ||
                 pmode == rcss::rcg::PM_Pause ||
                 pmode == rcss::rcg::PM_Human ||
                 pmode == rcss::rcg::PM_PlayOn ||
                 pmode == rcss::rcg::PM_Drop_Ball )
            {
                return rcss::rcg::NEUTRAL;
            }

            if ( pmode == rcss::rcg::PM_KickOff_Left ||
                 pmode == rcss::rcg::PM_KickIn_Left ||
                 pmode == rcss::rcg::PM_FreeKick_Left ||
                 pmode == rcss::rcg::PM_CornerKick_Left ||
                 pmode == rcss::rcg::PM_GoalKick_Left ||
                 pmode == rcss::rcg::PM_AfterGoal_Left ||
                 pmode == rcss::rcg::PM_OffSide_Left ||
                 pmode == rcss::rcg::PM_PK_Left ||
                 pmode == rcss::rcg::PM_Foul_Charge_Left ||
                 pmode == rcss::rcg::PM_Foul_Push_Left ||
                 pmode == rcss::rcg::PM_Foul_MultipleAttacker_Left ||
                 pmode == rcss::rcg::PM_Foul_BallOut_Left ||
                 pmode == rcss::rcg::PM_Back_Pass_Left ||
                 pmode == rcss::rcg::PM_Free_Kick_Fault_Left ||
                 pmode == rcss::rcg::PM_CatchFault_Left ||
                 pmode == rcss::rcg::PM_IndFreeKick_Left ||
                 pmode == rcss::rcg::PM_PenaltySetup_Left ||
                 pmode == rcss::rcg::PM_PenaltyReady_Left ||
                 pmode == rcss::rcg::PM_PenaltyTaken_Left ||
                 pmode == rcss::rcg::PM_PenaltyMiss_Left ||
                 pmode == rcss::rcg::PM_PenaltyScore_Left )
            {
                return rcss::rcg::LEFT;
            }

            return rcss::rcg::RIGHT;
        }
    };

    template <>
    struct match_situation_access< rcss::rcg::PlayMode >
    {
        static inline rcss::rcg::PlayMode get( rcss::rcg::PlayMode const& pmode ) { return pmode; }
    };

    template < >
    struct is_kick_off_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_KickOff_Left ||
                    _situation == rcss::rcg::PM_KickOff_Right;
        }

    };

    template < >
    struct is_throw_in_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_KickIn_Left ||
                    _situation == rcss::rcg::PM_KickIn_Right;
        }
    };

    template < >
    struct is_goal_kick_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_GoalKick_Left ||
                    _situation == rcss::rcg::PM_GoalKick_Right;
        }
    };

    template < >
    struct is_corner_kick_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_CornerKick_Left ||
                    _situation == rcss::rcg::PM_CornerKick_Right;
        }
    };

    template < >
    struct is_free_kick_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_FreeKick_Left ||
                    _situation == rcss::rcg::PM_FreeKick_Right;
        }
    };

    template < >
    struct is_indirect_free_kick_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_IndFreeKick_Left ||
                    _situation == rcss::rcg::PM_IndFreeKick_Right;
        }
    };

    template < >
    struct is_goalie_catch_kick_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const&  )
        {
            return false; // Cannot detect this using this playmode type
        }
    };

    template < >
    struct is_offside_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_OffSide_Left ||
                   _situation == rcss::rcg::PM_OffSide_Right;
        }
    };

    template < >
    struct is_play_on_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_PlayOn;
        }
    };

    template < >
    struct is_penalty_setup_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_PenaltySetup_Left ||
                   _situation == rcss::rcg::PM_PenaltySetup_Right;
        }

    };


    template < >
    struct is_penalty_ready_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_PenaltyReady_Left ||
                   _situation == rcss::rcg::PM_PenaltyReady_Right;
        }
    };

    template < >
    struct is_before_kick_off_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_BeforeKickOff;
        }
    };

    template < >
    struct is_time_over_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_TimeOver;
        }
    };

    template < >
    struct is_goal_access< rcss::rcg::PlayMode >
    {
        static inline bool get( rcss::rcg::PlayMode const& _situation )
        {
            return _situation == rcss::rcg::PM_AfterGoal_Left ||
                   _situation == rcss::rcg::PM_AfterGoal_Right;
        }
    };


SOCCER_TRAITS_NAMESPACE_END

SOCCER_TRAITS_NAMESPACE_BEGIN

    /**
     * TeamSide "registration"
     */

    template <>
    struct left_side_access< rcss::rcg::Side > { static inline rcss::rcg::Side get( ) { return rcss::rcg::LEFT; } };
    template <>
    struct right_side_access< rcss::rcg::Side > { static inline rcss::rcg::Side get( ) { return rcss::rcg::RIGHT; } };
    template <>
    struct unknown_side_access< rcss::rcg::Side > { static inline rcss::rcg::Side get( ) { return rcss::rcg::NEUTRAL; } };


SOCCER_TRAITS_NAMESPACE_END


SOCCER_NAMESPACE_BEGIN


bool is_game_stopped( const rcss::rcg::DispInfoT& state )
{
    return state.pmode_ != rcss::rcg::PM_PlayOn;
}

bool is_setplay_mode( const rcss::rcg::DispInfoT& state )
{
    return state.pmode_ == rcss::rcg::PM_GoalKick_Left ||
           state.pmode_ == rcss::rcg::PM_GoalKick_Right ||
           state.pmode_ == rcss::rcg::PM_FreeKick_Left ||
           state.pmode_ == rcss::rcg::PM_FreeKick_Right||
           state.pmode_ == rcss::rcg::PM_IndFreeKick_Left ||
           state.pmode_ == rcss::rcg::PM_IndFreeKick_Right ||
           state.pmode_ == rcss::rcg::PM_CornerKick_Left ||
           state.pmode_ == rcss::rcg::PM_CornerKick_Right ||
           state.pmode_ == rcss::rcg::PM_KickOff_Left ||
           state.pmode_ == rcss::rcg::PM_KickOff_Right ||
           state.pmode_ == rcss::rcg::PM_KickIn_Left ||
           state.pmode_ == rcss::rcg::PM_KickIn_Right;
}


SOCCER_NAMESPACE_END


std::ostream& operator<<( std::ostream& os, rcss::rcg::Side const side )
{
    switch( side )
    {
        case rcss::rcg::LEFT:
            os << "L";
            break;
        case rcss::rcg::RIGHT:
            os << "R";
            break;
        case rcss::rcg::NEUTRAL:
            os << "N";
            break;
    }
    return os;
}


#endif // SOCCER_RCSSLOGPLAYER_DISP_INFO_H
