#ifndef RCSSLOGPLAYER_DISP_INFO_PREDICATES_H
#define RCSSLOGPLAYER_DISP_INFO_PREDICATES_H

#include "rcsslogplayer_disp_info.h"

#include <soccer/geometry.hpp>
#include <soccer/algorithms/distance.hpp>

#include <map>
#include <functional>

namespace rcss {
namespace rcg {

/*!
 * \brief The SampleCollectorPolicyPlayerSpeedMax struct
 */
struct player_speed_max
    : public std::unary_function< rcss::rcg::DispInfoT, bool >
{
    const double M_player_speed_max;
    const rcss::rcg::Side M_team_side;

    player_speed_max( const double player_speed_max,
                                         const rcss::rcg::Side side )
        : M_player_speed_max( player_speed_max )
        , M_team_side( side )
    {

    }

    bool operator() ( const rcss::rcg::DispInfoT& disp_info ) const
    {
        for ( int i = 0; i < MAX_PLAYER * 2 ; i++ )
        {
            const rcss::rcg::PlayerT& player = disp_info.show_.player_[i];
            if ( player.side() != M_team_side )
            {
                continue;
            }

            const rcsc::Vector2D player_vel = rcsc::Vector2D( player.vx_, player.vy_ );
            const double player_speed = player_vel.r();
            if ( player_speed > M_player_speed_max )
            {
                return false;
            }
        }
        return true;
    }
};

/*!
 * \brief The SampleCollectorPolicyBallSpeedMax struct
 */
struct ball_speed_max
    : public std::unary_function< rcss::rcg::DispInfoT, bool >
{
    const double M_ball_speed_max;

    ball_speed_max( const double ball_speed_max )
     : M_ball_speed_max( ball_speed_max )
    {

    }

    bool operator() ( const rcss::rcg::DispInfoT& disp_info ) const
    {
        const rcss::rcg::BallT& ball = disp_info.show_.ball_;
        const rcsc::Vector2D ball_vel = rcsc::Vector2D( ball.vx_, ball.vy_ );
        const double ball_speed = ball_vel.r();
        if ( ball_speed  > M_ball_speed_max )
        {
            return false;
        }
        return true;
    }
};


/*!
 * \brief Check if a state is at a given playmode
 */
struct state_in_playmode
   : public std::unary_function< rcss::rcg::DispInfoT, bool >  {
	PlayMode M_playmode;
	
    state_in_playmode( const PlayMode playmode )
		: M_playmode( playmode )
	{
	}

	bool operator()( const DispInfoT& state )
	{
		return state.pmode_ == M_playmode;
	}
};

/*!
 * \brief The player_can_kick_predicate struct
 */
struct player_can_kick_predicate
    : public std::unary_function< rcss::rcg::DispInfoT, bool >
{
    unsigned int M_pid;
    ServerParamT M_server_param;
    std::map< int, PlayerTypeT > M_player_types;

    player_can_kick_predicate( unsigned int pid,
                               const ServerParamT& server_param,
                               const std::map< int, PlayerTypeT >& player_types )
        : M_pid( pid )
        , M_server_param( server_param )
        , M_player_types( player_types )
    {

    }

    bool operator()( const DispInfoT& state )
    {
        const auto _player = soccer::get_player( M_pid, state );
        const auto _player_pos = soccer::get_position( _player );
        const auto _ball = soccer::get_ball( state );
        const auto _ball_pos = soccer::get_position( _ball );

        // HACK NON-GENERIC CODE
        const auto _ball_dist = soccer::distance( _player_pos, _ball_pos );
        if ( _player.type_ == -1 )
        {
            return _ball_dist < M_server_param.ball_size_ + M_server_param.player_size_ + M_server_param.kickable_margin_;
        }

        const auto itpt = M_player_types.find( _player.type_ );

        if ( itpt == M_player_types.end() )
        {
            std::cerr << "Player type " << _player.type_ << " not found (fallback to default params)??" << std::endl;
            assert( false );
            return _ball_dist < M_server_param.ball_size_ + M_server_param.player_size_ + M_server_param.kickable_margin_;
        }

        return _ball_dist < M_server_param.ball_size_ + itpt->second.player_size_ + itpt->second.kickable_margin_;
    }

};
/*!
 * \brief Check if a player from a give team can kick
 */
struct team_can_kick_predicate
    : public std::unary_function< rcss::rcg::DispInfoT, bool > {

    Side M_side;
    ServerParamT M_server_param;
    std::map< int, PlayerTypeT > M_player_types;

    team_can_kick_predicate( const Side side,
                             const ServerParamT& server_param,
                             const std::map< int, PlayerTypeT >& player_types )
        : M_side( side )
        , M_server_param( server_param )
        , M_player_types( player_types )
    {

    }

    bool operator()( const DispInfoT& state )
    {
        assert( M_side != NEUTRAL );

        const auto _team_player_ids = M_side == LEFT
                        ? std::make_pair( 0, 11 )
                        : std::make_pair( 11, 22 );
        for ( auto pid = _team_player_ids.first; pid != _team_player_ids.second; pid++ )
        {
            player_can_kick_predicate _player_can_kick( pid, M_server_param, M_player_types );
            if ( _player_can_kick( state ) )
            {
                return true;
            }
        }
        return false;
    }

};

/*!
 * \brief Binary predicate to check if the ball is kickable by any player predicate
 */
struct player_can_kick_binary_predicate
    : public std::binary_function< int, DispInfoT, bool >
{
    // BOOST_CONCEPT_ASSERT(( soccer::StateConcept< State > ));
    // typename State::player_id_t player_id_t;
    typedef int player_id_t;

    ServerParamT M_server_param;
    std::map< int, PlayerTypeT > M_player_types;

    player_can_kick_binary_predicate( const ServerParamT& server_param,
                                      const std::map< int, PlayerTypeT >& player_types )
        : M_server_param( server_param )
        , M_player_types( player_types )
    {

    }

    player_can_kick_binary_predicate( const player_can_kick_binary_predicate& other )
        : M_server_param( other.M_server_param )
        , M_player_types( other.M_player_types )
    {

    }


    bool operator()( const player_id_t pid, const DispInfoT& state )
    {
        player_can_kick_predicate _player_can_kick_pred( pid,
                                                         M_server_param,
                                                         M_player_types );
        return _player_can_kick_pred( state );
    }
};

}
}

#endif // RCSSLOGPLAYER_DISP_INFO_PREDICATES_H
