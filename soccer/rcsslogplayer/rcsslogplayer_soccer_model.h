#ifndef SOCCER_RCSSLOGPLAYER_RCSS_RCG_SERVER_PARAM_H
#define SOCCER_RCSSLOGPLAYER_RCSS_RCG_SERVER_PARAM_H

#include <rcsslogplayer/types.h>

#include <soccer/core/access.hpp>
#include <soccer/register/pitch.hpp>

SOCCER_TRAITS_NAMESPACE_BEGIN

// Type Registration
template <>
struct tag< rcss::rcg::ServerParamT > { typedef pitch_tag type; };

template <>
struct coordinate_type< rcss::rcg::ServerParamT > { typedef float type; };

template <>
struct length_type< rcss::rcg::ServerParamT > { typedef float type; };

// Access Registration
template <>
struct pitch_right_sign_access< rcss::rcg::ServerParamT > { static inline int get() { return 1.0; } };

template <>
struct pitch_bottom_sign_access< rcss::rcg::ServerParamT > { static inline int get() { return 1.0; } };

template <>
struct pitch_top_left_x_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return -52.5f; }
};

template <>
struct pitch_top_left_y_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return -34.0f; }
};

template <>
struct pitch_length_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 105.0f; }
};

template <>
struct pitch_width_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 68.0f; }
};

template <>
struct goal_area_length_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 5.5f; }
};

template <>
struct goal_area_width_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 18.32f; }
};

template <>
struct penalty_area_length_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 16.5f; }
};

template <>
struct penalty_area_width_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 40.32f; }
};

template <>
struct penalty_circle_radius_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 9.15f; }
};

template <>
struct penalty_spot_distance_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 11.0f; }
};

template <>
struct goal_depth_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 2.44f; }
};

template <>
struct goal_width_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& sp ) { return sp.goal_width_; }
};

template <>
struct center_circle_radius_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 9.15f; }
};

template <>
struct corner_arc_radius_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 1.0f; }
};

template <>
struct goal_post_radius_access< rcss::rcg::ServerParamT >
{
    static inline double
    get ( rcss::rcg::ServerParamT const& ) { return 0.06f; }
};

SOCCER_TRAITS_NAMESPACE_END

#endif // SOCCER_RCSSLOGPLAYER_RCSS_RCG_SERVER_PARAM_H
