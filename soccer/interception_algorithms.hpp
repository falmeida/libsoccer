#ifndef SOCCER_INTERCEPTION_ALGORITHMS_HPP
#define SOCCER_INTERCEPTION_ALGORITHMS_HPP

#include <soccer/interception_concepts.hpp>
#include <soccer/state_concepts.hpp>

#include <boost/property_map/property_map.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/iterator/transform_iterator.hpp>

#include <map>
#include <functional>

namespace soccer {


/*!
 * \brief Determine the fastest interception registered in the interception table
 * \param interception_table the table of previously recorded interceptions (model of InterceptionTableConcept)
 * \returns interception id descriptor for a null or existing interception
 */
template < typename InterceptionInputIterator >
InterceptionInputIterator
fastest_interception( InterceptionInputIterator first,
                      const InterceptionInputIterator last )
{
    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< InterceptionInputIterator > ));

    // BOOST_CONCEPT_ASSERT(( InterceptionConcept< typename boost::iterator_value< InterceptionInputIterator >::type > ));

    if ( first == last )
    {
        return last;
    }

    auto fastest_interception = first;
    first++;
    for ( ; first != last; first ++ )
    {
        if ( get_duration( first->second ) < get_duration( fastest_interception->second ) )
        {
            fastest_interception = first;
        }
    }

    return fastest_interception;
}

/*!
 * \brief Determine the fastest interception registered in the interception table for a player of a given side
 */
template < typename State,
           typename InterceptionInputIterator
         >
InterceptionInputIterator
fastest_interception( typename side_type< typename player_type< State >::type >::type const& _side,
                      State const& _state,
                      InterceptionInputIterator first,
                      const InterceptionInputIterator last )
{
    BOOST_CONCEPT_ASSERT(( StateConcept< State > ));

    typedef typename player_type< State >::type state_player_t;
    typedef typename player_id_type< State >::type state_player_id_t;

    BOOST_CONCEPT_ASSERT(( boost::InputIteratorConcept< InterceptionInputIterator > ));

    // BOOST_CONCEPT_ASSERT(( InterceptionConcept< typename boost::iterator_value< InterceptionInputIterator >::type > ));
    typedef typename boost::iterator_value< InterceptionInputIterator >::type interception_t;

    BOOST_STATIC_ASSERT_MSG(( boost::is_same< state_player_t,
                                              typename player_id_type< interception_t >::type >::value ),
                            "Player id descriptor types mismatch");

    if ( first == last )
    {
        return last;
    }

    auto fastest_interception = last;
    for ( ; first != last; first++ )
    {
        const auto _player = get_player( get_executor_id( first->second ), _state );
        if ( get_side( _player ) != _side )
        {
            continue;
        }

        if ( fastest_interception == last )
        {
            fastest_interception = *first;
            continue;
        }

        if ( get_duration( first->second ) < get_duration( fastest_interception->second ) )
        {
            fastest_interception = *first;
        }
    }

    return fastest_interception;
}

/*!
 * \brief binary predicate to compare interceptions based on their duration ( use for sorting containers )
 */
template < typename Interception >
struct interception_duration_cmp
    : public std::binary_function<Interception, Interception, bool>
{
    // BOOST_CONCEPT_ASSERT(( InterceptionConcept<Interception> ));

    interception_duration_cmp() { }

    bool operator() ( Interception const& i1, Interception const& i2 )
    {
        return get_duration( i1 ) < get_duration( i2 );
    }
};

}
#endif // SOCCER_INTERCEPTION_ALGORITHMS_HPP
