#ifndef SOCCER_MATCH_TIME_CONCEPT_HPP
#define SOCCER_MATCH_TIME_CONCEPT_HPP

#include <soccer/core/access.hpp>

#include <boost/concept_check.hpp>
#include <boost/concept/detail/concept_def.hpp>

namespace soccer {

BOOST_concept(MatchTimeConcept,(MT))
{
    /* BOOST_STATIC_ASSERT_MTG(( boost::is_base_of< playmode_tag, typename tag< MT >::type >::value ),
                            "Type category mismatch"); */

    BOOST_CONCEPT_USAGE( MatchTimeConcept )
    {
        const_constraints( _mt );
    }

private:
    void const_constraints( MT const& const_mt )
    {

    }

    MT _mt;
};


BOOST_concept(MutableMatchTimeConcept,(MT))
    : public MatchTimeConcept<MT>
{

    BOOST_CONCEPT_USAGE(MutableMatchTimeConcept)
    {

    }

    MT _mt;
};

} // end namespace soccer


#endif // SOCCER_MATCH_TIME_CONCEPT_HPP
