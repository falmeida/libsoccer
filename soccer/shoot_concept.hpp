#ifndef SOCCER_SHOOT_CONCEPT_HPP
#define SOCCER_SHOOT_CONCEPT_HPP

#include <soccer/shoot_traits.hpp>

#include <soccer/soccer_concepts.hpp>
#include <soccer/position_concepts.hpp>
#include <soccer/action_concepts.hpp>

namespace soccer {

BOOST_concept(ShootConcept,(S))
    : public ActionConcept<S>
{
    BOOST_STATIC_ASSERT_MSG(( boost::is_base_of< shoot_tag, typename tag< S >::type >::value ),
                              "Type is not categorized with the expected category tag");
    typedef typename position_type< S >::type position_t;
    typedef typename speed_type< S >::type speed_t;


    BOOST_CONCEPT_ASSERT(( PositionConcept<position_t> ));

    BOOST_CONCEPT_USAGE(ShootConcept)
    {
        const_constraints( _shoot );
    }

    void const_constraints( const S& const_shoot )
    {
        _pos = get_position( const_shoot );
        _speed = get_ball_first_speed( const_shoot );
    }

private:
    S _shoot;
    position_t _pos;
    speed_t _speed;
};

BOOST_concept(MutableShootConcept,(S))
    : public ShootConcept<S>
{
    typedef typename speed_type< S >::type speed_t;
    typedef typename position_type< S >::type position_t;

    BOOST_CONCEPT_USAGE(MutableShootConcept)
    {
        set_position( _pos, _shoot );
        set_ball_first_speed( _speed, _shoot );
    }

private:
    S _shoot;
    speed_t _speed;
    position_t _pos;
};

}

#endif // SOCCER_SHOOT_CONCEPT_HPP
