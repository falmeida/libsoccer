#ifndef SOCCER_ACTION_CHAIN_HPP
#define SOCCER_ACTION_CHAIN_HPP

#include <soccer/action_chain_traits.hpp>

#include <boost/shared_ptr.hpp>
#include <vector>

namespace soccer {

// struct action_state_pair
template < typename Action,
           typename State >
struct action_state_pair {
    typedef Action action_type;
    typedef State state_type;
    typedef const Action& action_reference;
    typedef State const& state_reference;
    typedef boost::shared_ptr< const Action > action_pointer;
    typedef boost::shared_ptr< const State > state_pointer;

    action_state_pair( Action* action, State* state )
        : m_action( action )
        , m_state( state )
    {

    }

    action_pointer m_action;
    state_pointer m_state;
};


template < typename Action,
           typename State >
const Action&
get_action( const action_state_pair<Action,State>& asp )
{
    return *asp.m_action;
}

template < typename DerivedAction,
           typename Action,
           typename State >
const DerivedAction&
get_action( const action_state_pair<Action,State>& asp )
{
    return static_cast<const DerivedAction&>( *asp.m_action );
}

template < typename Action,
           typename State >
State const&
get_state( const action_state_pair<Action,State>& asp )
{
    return *asp.m_state;
}

template < typename Action,
           typename State >
typename action_state_pair<Action,State>::action_pointer
get_action_ptr( const action_state_pair<Action,State>& asp )
{
    return asp.m_action;
}

template < typename Action,
           typename State >
typename action_state_pair<Action,State>::state_pointer
get_state_ptr( const action_state_pair<Action,State>& asp )
{
    return asp.m_state;
}


/* template < typename Action,
           typename State >
const Action&
get_action( const std::pair<Action,State>& asp )
{
    return *asp.first;
}

template < typename Action,
           typename State >
State const&
get_state( const std::pair<Action,State>& asp )
{
    return *asp.second;
} */

// struct action_chain

template < typename ActionStatePair >
struct action_chain {
    typedef ActionStatePair value_type; // compatibility with container concept
    typedef ActionStatePair action_state_pair_type;
    typedef typename std::vector< ActionStatePair >::const_iterator iterator;
    typedef typename std::vector< ActionStatePair >::const_reference reference;
    typedef typename std::vector< ActionStatePair >::size_type size_type;


    /*!
     * \brief action_chain default constructor
     */
    action_chain()
    {

    }

    /*!
     * \brief action_chain copy constructor
     * \param other the action chain instance to copy from
     */
    action_chain( const action_chain& other )
        : m_path( other.m_path )
    {

    }

    action_chain& operator+=( const std::pair< typename action_state_pair_type::action_type,
                                               typename action_state_pair_type::state_type >& asp )
    {
        m_path.push_back( ActionStatePair( asp.first, asp.second ) );
        return *this;
    }

    action_chain& operator+=( const ActionStatePair& asp )
    {
        m_path.push_back( asp );
        return *this;
    }

    action_chain& operator=( const action_chain& other )
    {
        if ( this != &other )
        {
            this->m_path = other.m_path;
        }
        return *this;
    }

    size_type size() const
    {
        return m_path.size();
    }

    bool empty() const
    {
        return m_path.empty();

    }

    reference front() const
    {
        return m_path.front();
    }

    reference back() const
    {
        return m_path.back();
    }

    iterator begin() const
    {
        return m_path.begin();
    }

    iterator end() const
    {
        return m_path.end();
    }

    // push_back is meant for compatibility with std::back_inserter
    void push_back( const std::pair< typename action_state_pair_type::action_type,
                                     typename action_state_pair_type::state_type >& asp )
    {
        m_path.push_back( ActionStatePair( asp.first, asp.second ) );
    }

    void push_back( const ActionStatePair& asp )
    {
        m_path.push_back( asp );
    }

    void clear()
    {
        m_path.clear();
    }

    std::vector< ActionStatePair > m_path;
};

template < typename Action, typename State >
struct make_action_chain {
    typedef action_chain< action_state_pair<Action, State> > type;
};

template < typename ActionStatePair >
const typename ActionStatePair::action_type&
first_action( const action_chain<ActionStatePair>& ac )
{
    return get_action( ac.m_path.front() );
}

template < typename ActionStatePair >
const typename ActionStatePair::state_type&
first_state( const action_chain<ActionStatePair>& ac )
{
    return get_state( ac.m_path.front() );
}

template < typename ActionStatePair >
std::pair< typename action_chain<ActionStatePair>::action_state_pair_iterator,
           typename action_chain<ActionStatePair>::action_state_pair_iterator>
action_state_pairs( const action_chain<ActionStatePair>& ac )
{
    return std::make_pair( ac.m_path.begin(), ac.m_path.end() );
}

template < typename ActionStatePair >
void
push_action_state_pair( action_chain<ActionStatePair>& ac,
                        const ActionStatePair& val )
{
    ac.m_path.push_back( val );
}

template < typename ActionStatePair >
const typename ActionStatePair::action_type&
last_action( const action_chain<ActionStatePair>& ac )
{
    return get_action( ac.m_path.back() );
}

template < typename ActionStatePair >
const typename ActionStatePair::state_type&
last_state( const action_chain<ActionStatePair>& ac )
{
    return get_state( ac.m_path.back() );
}


}

namespace std {

template < typename ActionStatePair >
typename soccer::action_chain<ActionStatePair>::iterator
begin( soccer::action_chain<ActionStatePair>& container )
{
    return container.begin();
}

template < typename ActionStatePair >
typename soccer::action_chain<ActionStatePair>::iterator
end( soccer::action_chain<ActionStatePair>& container )
{
    return container.end();
}

}


#endif // SOCCER_ACTION_CHAIN_HPP
