#ifndef SOCCER_ACTION_HPP
#define SOCCER_ACTION_HPP

#include <soccer/action_traits.hpp>

namespace soccer {

/*#define SOCCER_INSTALL_ACTION(KIND,NAME) \
    template <> struct action_kind<KIND##_##NAME##_t> { typedef KIND##_tag type; };

#define SOCCER_DEF_ACTION(KIND, NAME) \
  enum KIND##_##NAME##_t { KIND##_##ACTION }; \
  SOCCER_INSTALL_ACTION(KIND, ACTION) */

template < typename ActionFactory >
struct shoot_generator { typedef typename ActionFactory::shoot_type type; };
template < typename ActionFactory >
struct pass_generator { typedef typename ActionFactory::pass_type type; };
template < typename ActionFactory >
struct dribble_generator { typedef typename ActionFactory::dribble_type type; };
template < typename ActionFactory >
struct move_generator { typedef typename ActionFactory::move_type type; };
template < typename ActionFactory >
struct hold_ball_generator { typedef typename ActionFactory::hold_ball_type type; };
template < typename ActionFactory >
struct clear_ball_generator { typedef typename ActionFactory::clear_ball_type type; };
template < typename ActionFactory >
struct intercept_ball_generator { typedef typename ActionFactory::intercept_ball_type type; };
template < typename ActionFactory >
struct tackle_generator { typedef typename ActionFactory::tackle_type type; };


enum class ActionType {
    Pass,
    Dribble,
    Intercept,
    Hold,
    Shoot,
    Move,
    Tackle,
    Clear,
    MarkPlayer,
    NoAction
};

/* template <>
struct action_tag<ActionType::Pass> { typedef pass_tag type; };

template <>
struct action_tag<ActionType::Dribble> { typedef dribble_tag type; };

template <>
struct action_tag<ActionType::Hold> { typedef hold_ball_tag type; };

template <>
struct action_tag<ActionType::Shoot> { typedef shoot_tag type; };

template <>
struct action_tag<ActionType::Intercept> { typedef intercept_tag type; };

template <typename T>
struct action_tag {
// No type defined
}; */

template < typename PlayerId,
           typename Duration >
struct action {
    typedef action<PlayerId, Duration> action_base_type;
    typedef individual_action_tag action_category;
    typedef PlayerId player_id_t;
    typedef Duration duration_descriptor;

    action( ActionType type )
        : M_type( type )
    {

    }

    virtual ~action() { }

    virtual void set_duration( const Duration dur )
    {
        this->M_duration = dur;
    }

    PlayerId M_executor_id;
    Duration M_duration;
    ActionType M_type;
};

template <typename PlayerId,
          typename Duration>
PlayerId
get_executor_id( const action<PlayerId,Duration>& _action )
{
    return _action.M_executor_id;
}

template <typename PlayerId,
          typename Duration>
Duration
get_duration( const action<PlayerId,Duration>& _action )
{
    return _action.M_duration;
}

template <typename PlayerId,
          typename Duration>
ActionType
get_action_type( const action<PlayerId,Duration>& _action )
{
    return _action.M_type;
}


template <typename PlayerId,
          typename Duration>
void
set_executor_id( const PlayerId pid,
              action<PlayerId, Duration>& _action )
{
    _action.M_executor_id = pid;
}

template <typename PlayerId,
          typename Duration>
void
set_duration( const Duration _dur,
              action<PlayerId,Duration>& _action )
{
    return _action.set_duration( _dur );
}


} // end namespace soccer

#endif // SOCCER_ACTION_HPP
