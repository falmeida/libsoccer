#ifndef SOCCER_TYPES_H
#define SOCCER_TYPES_H

//! Import useful macros
#include <soccer/defines.h>

#include <map>
#include <string>

SOCCER_NAMESPACE_BEGIN

    typedef unsigned int Time;
    typedef unsigned int Unum;

    /*!
     * \brief The ScoreAssessment enum
     */
    enum class ScoreAssessment { VERY_BAD, BAD, MEDIUM, GOOD, VERY_GOOD };

    typedef std::map< unsigned int, soccer::ScoreAssessment> IntegerToScoreAssessmentMap;
    IntegerToScoreAssessmentMap createIntegerToScoreAssessmentMap();
    static IntegerToScoreAssessmentMap integer_to_score_assessment_map = createIntegerToScoreAssessmentMap();

    typedef std::map< std::string, soccer::ScoreAssessment > StringToScoreAssessmentMap;
    StringToScoreAssessmentMap createStringToScoreAssessmentMap();
    static StringToScoreAssessmentMap string_to_score_assessment_map = createStringToScoreAssessmentMap();


    /*!
     * \brief The Situation enum
     */
    enum class Situation {
        Unknown,
        OurGoalKick,
        TheirGoalKick,
        OurGoalieFreeKick,
        TheirGoalieFreeKick,
        OurFreeKick,
        TheirFreeKick,
        OurCornerKick,
        TheirCornerKick,
        OurIndirectFreeKick,
        TheirIndirectFreeKick,
        OurKickOff,
        TheirKickOff,
        OurThrowIn,
        TheirThrowIn,
        OurPenaltyKick,
        TheirPenaltyKick,
        OurAttack,
        TheirAttack
    };

    typedef std::map< unsigned int, soccer::Situation > IntegerToSituationMap;
    IntegerToSituationMap createIntegerToSituationMap();
    static IntegerToSituationMap integer_to_situation_map = createIntegerToSituationMap();

    typedef std::map< std::string, soccer::Situation > StringToSituationMap;
    StringToSituationMap createStringToSituationMap();
    static StringToSituationMap string_to_situation_map = createStringToSituationMap();

    /*!
     * \brief The DecisionAlgorithm enum
     */
    enum class DecisionAlgorithm {
        Goalie,
        Defense,
        Attack
    };

    typedef std::map< unsigned int, soccer::DecisionAlgorithm > IntegerToDecisionAlgorithmMap;
    IntegerToDecisionAlgorithmMap createIntegerToDecisionAlgorithmMap();
    static IntegerToDecisionAlgorithmMap integer_to_decision_algorithm_map = createIntegerToDecisionAlgorithmMap();

    typedef std::map< std::string, soccer::DecisionAlgorithm > StringToDecisionAlgorithmMap;
    StringToDecisionAlgorithmMap createStringToDecisionAlgorithmMap();
    static StringToDecisionAlgorithmMap string_to_decision_algorithm_map = createStringToDecisionAlgorithmMap();

    /*!
     * \brief The PlayerRole enum
     */
    enum class PlayerRole {
        None,
        Goaltender,
        Sweeper,
        Defender,
        Midfielder,
        Forward
    };


    /*!
     * \brief The PlayMode enum
     */
    enum class PlayMode {

    };

    typedef std::map< unsigned int, soccer::PlayerRole> IntegerToPlayerRoleMap;
    IntegerToPlayerRoleMap createIntegerToPlayerRoleMap();
    static IntegerToPlayerRoleMap integer_to_player_role_map = createIntegerToPlayerRoleMap();

    typedef std::map< std::string, soccer::PlayerRole> StringToPlayerRoleMap;
    StringToPlayerRoleMap createStringToPlayerRoleMap();
    static StringToPlayerRoleMap string_to_player_role_map = createStringToPlayerRoleMap();

    /*!
     * \brief The SituationAssessment enum
     */
    enum class SituationAssessment
    {
        Normal,
        OurAttack,
        TheirAttack,
        OurCounterAttack,
        TheirCounterAttack,
        OurSetplay,
        TheirSetplay,
        OurPenaltyKick,
        TheirPenaltyKick
    };

    /*!
     * \brief The TeamSide enum
     */
    enum class TeamSide { Unknown, Left, Right };

    /*!
     * \brief The TeamAffiliation enum
     */
    enum class TeamAffiliation { Unknown, Our, Opp };

    /*!
     * \brief The TimePeriod enum
     */
    enum class TimePeriod
    {
        FirstHalf,
        SecondHalf,
        ExtraTimeFirstHalf,
        ExtraTimeSecondHalf,
        GameOver
    };

    /*!
     * \brief The SoccerMomentenum
     */
    enum class SoccerMoment
    {
        GameStoppedOpponentPossession,
        GameStoppedOurPossession,
        OurPossession,
        OpponentPossession,
        LosingPossession,
        RegainingPossession
    };

    std::map<TeamSide, std::string> createTeamSideToStringMap();
    const std::string& teamSide(TeamSide team_side);
    std::map<std::string, TeamSide> createStringToTeamSideMap();
    TeamSide teamSide(const std::string& team_side);
    std::map<SoccerMoment, std::string> createSoccerMomentToStringMap();
    std::map< std::string, SoccerMoment> createStringToSoccerMomentMap();
    const std::string& soccerMoment(SoccerMoment soccer_moment);
    const SoccerMoment soccerMoment(const std::string& soccer_moment);
    TeamSide oppositeSide(TeamSide side);
    TeamAffiliation oppositeAffiliation(TeamAffiliation affiliation);


SOCCER_NAMESPACE_END

std::ostream& operator<<(std::ostream& os, const soccer::TeamSide side );

#endif // SOCCER_TYPES_H
