#ifndef SOCCER_PLAYER_ID_HPP
#define SOCCER_PLAYER_ID_HPP


namespace soccer
{

    template < typename Side,
               typename Number >
    struct player_id
    {
        player_id(Number number, Side side)
            : M_number( number )
            , M_side( side )
        {

        }

        player_id(const player_id& other)
            : M_number( other.M_number )
            , M_side( other.M_side )
        {

        }
        // Operator overload
        player_id& operator=(const player_id& other)
        {
            if ( this != &other )
            {
                this->M_number = other.M_number;
                this->M_side = other.M_side;
            }
            return *this;
        }

        bool operator==(const player_id& other) const
        {
            return this->M_number == other.M_number &&
                   this->M_side == other.M_side;
        }
        bool operator!=(const player_id& other) const
        {
            return ! (*this == other );
        }

        Number M_number;
        Side M_side;
    };

}

#include <soccer/core/tags.hpp>

namespace soccer { namespace traits {

//
// player_id TYPES REGISTRAGION
//
template < typename Number,
           typename Side >
struct tag< player_id< Number, Side > > { typedef player_id_tag type; };

template <>
template < typename Number,
           typename Side >
struct side_type< player_id< Number, Side > > { typedef Side type; };

template <>
template < typename Number,
           typename Side >
struct number_type< player_id< Number, Side > > { typedef Number type ; };

//
// player_id TYPES REGISTRAGION
//
template <>
template < typename Number,
           typename Side >
struct side_access< player_id< Number, Side > >
{
    static inline
    const Side& get( player_id< Number, Side > const& _pid ) { return _pid.M_side; }
    static inline
    void set( const Side& _side,
              player_id< Number, Side >& _pid ) { _pid.M_side = _side; }
};

template <>
template < typename Number,
           typename Side >
struct number_access< player_id< Number, Side > >
{
    static inline
    const Number& get( player_id< Number, Side > const& _pid ) { return _pid.M_number; }
    static inline
    void set( const Number& _number,
              player_id< Number, Side >& _pid ) { _pid.M_number = _number; }
};

}} // end namespace soccer::traits

namespace std {

    template < >
    template < typename Number, typename Side >
    struct less< soccer::player_id< Number, Side > >
    {
       bool operator()(soccer::player_id< Number, Side > const & pid1,
                       soccer::player_id< Number, Side > const& pid2) const
       {
           return pid1.M_side < pid2.side ||
                ( pid1.M_side == pid2.M_side && pid1.M_number < pid2.M_number );
       }
    };
}

#endif // SOCCER_PLAYER_ID_HPP
