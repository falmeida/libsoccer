#ifndef SOCCER_PLAYER_MODEL_HPP
#define SOCCER_PLAYER_MODEL_HPP

namespace soccer
{

template < typename Size = float,
           typename Speed = float,
           typename Acceleration = Speed >
struct player_model_simple
{
    Size M_size;
    Speed M_decay;
    Speed M_speed_max;
    Acceleration M_accel_max;
};

} // end namespace soccer

namespace soccer { namespace traits {

//
// player_model_simple TYPE REGISTRATION
//
template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct tag< player_model_simple< Size, Speed, Acceleration > >
{
    typedef /* player_model_simple_tag */ void type;
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct size_type< player_model_simple< Size, Speed, Acceleration > >
{
    typedef Size type;
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct speed_type< player_model_simple< Size, Speed, Acceleration > >
{
    typedef Speed type;
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct acceleration_type< player_model_simple< Size, Speed, Acceleration > >
{
    typedef Acceleration type;
};

//
// player_model_simple DATA ACCESS REGISTRATION
//

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct size_access < player_model_simple< Size, Speed, Acceleration > >
{
    typedef player_model_simple< Size, Speed, Acceleration > player_model_simple_t;

    static inline
    Size const& get( player_model_simple_t const& bm ) { return bm.M_size; }
    static inline
    void set( Size const& _val, player_model_simple_t& bm ) { bm.M_size = _val; }
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct speed_max_access < player_model_simple< Size, Speed, Acceleration > >
{
    typedef player_model_simple< Size, Speed, Acceleration > player_model_simple_t;

    static inline
    Speed const& get( player_model_simple_t const& bm ) { return bm.M_speed_max; }
    static inline
    void set( Speed const& _val, player_model_simple_t& bm ) { bm.M_speed_max = _val; }
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct accel_max_access < player_model_simple< Size, Speed, Acceleration > >
{
    typedef player_model_simple< Size, Speed, Acceleration > player_model_simple_t;

    static inline
    Acceleration const& get( player_model_simple_t const& bm ) { return bm.M_accel_max; }
    static inline
    void set( Acceleration const& _val, player_model_simple_t& bm ) { bm.M_accel_max = _val; }
};

template < >
template < typename Size,
           typename Speed,
           typename Acceleration >
struct decay_access < player_model_simple< Size, Speed, Acceleration > >
{
    typedef player_model_simple< Size, Speed, Acceleration > player_model_simple_t;

    static inline
    Decay const& get( player_model_simple_t const& bm ) { return bm.M_decay; }
    static inline
    void set( Decay const& _val, player_model_simple_t& bm ) { bm.M_decay = _val; }
};

}}


#endif // SOCCER_PLAYER_MODEL_HPP
